{-# LANGUAGE BangPatterns, RankNTypes #-}

-- | Handler calculus in PHOAS ('a' is the type of variable)
data H a
  = HVar a
  | Handle (H a) (a -> a -> H a)
  | HApp (H a) (H a)
  | HDo (H a)
  | HVal (V H a)

-- | Values: booleans, unit, functions (parameterized by the language h)
data V h a
  = B Bool
  | U
  | F (a -> h a)

-- | let x = M in N   =   (\x. N) M
hLet :: H a -> (a -> H a) -> H a
hLet m k = HApp (HVal (F k)) m

-- | \f -> handle (f (); get()) (get() k -> k true)
m0 :: H a
m0 = HVal (F (\f ->
  Handle
    (hLet (HApp (HVar f) (HVal U)) (\_ -> HDo (HVal U)))
    (\x k -> HApp (HVar k) (HVal (B True)))))

-- | \f -> true
m1 :: H a
m1 = HVal (F (\f -> HVal (B True)))

-- | Delimited control (shift/reset) in PHOAS
data D a
  = DVar a
  | DReset (D a)
  | DShift (a -> D a)
  | DApp (D a) (D a)
  | DVal (V D a)

dLet :: D a -> (a -> D a) -> D a
dLet m k = DApp (DVal (F k)) m

-- \_ -> ()
n0 :: H a
n0 = HVal (F (\_ -> HVal U))

-- | Override the handler to (\_x k' -> k' false) for the rest of the computation.
-- \_ -> shift (\k -> \_h -> k () (\_x k' -> k' false))
n1 :: D a
n1 = DVal (F (\_ -> DShift (\k -> DVal (F (\_h -> DApp (DApp (DVar k) (DVal U)) (DVal (F (\_x -> DVal (F (\k -> DApp (DVar k) (DVal (B False))))))))))))

-- Translation from Handlers to shift/reset
translate :: H a -> D a
translate (HVar a) = DVar a
translate (Handle m h) = DApp (DReset (dLet (translate m) (\v -> DVal (F (\_h -> DVar v))))) (DVal (F (\x -> DVal (F (\k -> translate (h x k))))))
translate (HApp f x) = DApp (translate f) (translate x)
translate (HDo m) = DShift (\k -> DVal (F (\h -> DApp (DApp (DVar h) (translate m)) (DVal (F (\y -> DApp (DApp (DVar k) (DVar y)) (DVar h)))))))
translate (HVal v) = DVal (vmap translate v)

newtype Fix f = Fix (f (Fix f))

type H' = Fix (V H)
type D' = Fix (V D)

data HC
  = HRet H'
  | HDoing H' (H' -> H H')

-- | Big step semantics of handlers
evalH :: H H' -> HC
evalH (HVar v) = HRet v
evalH (HVal v) = HRet (Fix v)
evalH (Handle m h) = case evalH m of
  HRet v -> HRet v
  HDoing v k -> evalH (h v (Fix (F (\y -> Handle (k y) h))))
evalH (HDo m) = case evalH m of
  HRet v -> HDoing v HVar
  HDoing v' k -> HDoing v' (\y -> HDo (k y))
evalH (HApp f x) = case evalH x of
  HRet x ->
    case evalH f of
      HRet (Fix (F f)) -> evalH (f x)
      HDoing v kf -> HDoing v (\y -> HApp (kf y) (HVar x))
  HDoing v kx -> HDoing v (\y -> HApp f (kx y))

data DC
  = DRet D'
  | DShifting (D' -> D D') (D' -> D D')

-- | Big step semantics of shift/reset
evalD :: D D' -> DC
evalD (DVar v) = DRet v
evalD (DVal v) = DRet (Fix v)
evalD (DReset m) = case evalD m of
  DRet v -> DRet v
  DShifting k h -> evalD (h (Fix (F (\y -> DReset (k y)))))
evalD (DShift h) = DShifting DVar h
evalD (DApp f x) = case evalD x of
  DRet x ->
    case evalD f of
      DRet (Fix (F f)) -> evalD (f x)
      DRet (Fix U) -> error "bad unit"
      DRet (Fix (B b)) -> error ("bad bool: " ++ show b)
      DShifting kf h -> DShifting (\y -> DApp (kf y) (DVar x)) h
  DShifting kx h -> DShifting (\y -> DApp f (kx y)) h

data D2 a
  = V2 a
  | D2 (V D (D2 a))

data DStep d a
  = DStep (d a)
  | DRet2 (V d a)
  | DShifting2 (a -> d a) (a -> d a)

-- | small step semantics of shift/reset
stepD :: D (D2 a) -> DStep D (D2 a)
stepD (DVar (D2 v)) = DRet2 v
stepD (DVar _) = error "bad variable"
stepD (DVal v) = DRet2 v
stepD (DReset m) = case stepD m of
  DRet2 v -> DRet2 v
  DStep m -> DStep (DReset m)
  DShifting2 k h -> DStep (h (D2 (F (\y -> DReset (k y)))))
stepD (DShift h) = DShifting2 DVar h
stepD (DApp f x) = case stepD x of
  DStep x -> DStep (DApp f x)
  DShifting2 kx h -> DShifting2 (\y -> DApp f (kx y)) h
  DRet2 x -> case stepD f of
    DStep f -> DStep (DApp f (DVal x))
    DShifting2 kf h -> DShifting2 (\y -> DApp (kf y) (DVal x)) h
    DRet2 (F f) -> DStep (f (D2 x))

joinD :: D (D2 a) -> D a
joinD (DVar (D2 v)) = DVal (joinDVal v)
joinD (DVar (V2 v)) = DVar v
joinD (DVal v) = DVal (joinDVal v)
joinD (DApp f x) = DApp (joinD f) (joinD x)
joinD (DReset m) = DReset (joinD m)
joinD (DShift h) = DShift (joinD . h . V2)

joinDVal :: V D (D2 a) -> V D a
joinDVal (F f) = F (\y -> joinD (f (V2 y)))
joinDVal (B b) = B b
joinDVal U = U

vmap :: (h a -> d a) -> V h a -> V d a
vmap f (F g) = F (f . g)
vmap f (B b) = B b
vmap f U = U

-- Printers

showHC :: HC -> String
showHC (HRet (Fix (B True))) = "true"
showHC (HRet (Fix (B False))) = "false"
showHC _ = "bug"

showDC :: DC -> String 
showDC (DRet (Fix (B True))) = "true"
showDC (DRet (Fix (B False))) = "false"
showDC (DRet (Fix U)) = "()"
showDC _ = "bug"

dumpD :: D Int -> String
dumpD = dumpD' 0

dumpD' :: Int -> D Int -> String
dumpD' !d (DVar v) = "#" ++ show v
dumpD' d (DVal v) = dumpV dumpD' d v
dumpD' d (DApp f x) = "(" ++ dumpD' d f ++ ")" ++ dumpD' d x
dumpD' d (DReset m) = "reset (" ++ dumpD' d m ++ ")"
dumpD' d (DShift f) = "shift (#" ++ show d ++ " -> " ++ dumpD' (d + 1) (f d) ++ ")"

dumpV :: (Int -> d Int -> String) -> Int -> V d Int -> String
dumpV dump d (B True) = "true"
dumpV dump d (B False) = "false"
dumpV dump d U = "()"
dumpV dump d (F k) = "(\\#" ++ show d ++ " -> " ++ dump (d + 1) (k d) ++ ")"

dumpDV :: V D Int -> String
dumpDV = dumpV dumpD' 0

stepDIO :: D (D2 Int) -> IO (Maybe (D (D2 Int)))
stepDIO m = do
  putStrLn (dumpD (joinD m))
  case stepD m of
    DRet2 v -> putStrLn (dumpDV (joinDVal v)) >> pure Nothing
    DStep m -> pure (Just m)
    DShifting2 _ _ -> error "shift escape"

stepsDIO :: D (D2 Int) -> IO ()
stepsDIO m = stepDIO m >>= \z -> case z of
  Nothing -> putStrLn "DONE"
  Just m -> stepsDIO m

main :: IO ()
main = do
  -- putStrLn (showDC (evalD (DApp (DReset (DShift (\k -> DVal (F (\x -> DApp (DVar k) (DVar x)))))) (DVal (B True)))))
  -- putStrLn (showHC (evalH (HApp m0 n0)))
  -- putStrLn (showDC (evalD (translate (HApp m1 n0))))

  -- stepsDIO (translate (HApp m0 n0))

  -- m0 and m1 are undistinguishable using handlers
  putStrLn (showDC (evalD (translate (HApp m0 n0))))  -- true
  putStrLn (showDC (evalD (translate (HApp m1 n0))))  -- true

  -- translate m0 and translate m1 are distinguishable using shift/reset
  putStrLn (showDC (evalD (DApp (translate m0) n1)))  -- false
  putStrLn (showDC (evalD (DApp (translate m1) n1)))  -- true

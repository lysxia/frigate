Translation from First Class Names to (a variant of) Binders By Day

```
module FCN2BBD where

open import Function.Base using (_∘_; it)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Empty using (⊥)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; cong; cong₂; subst; trans)
open import Relation.Nullary using (Dec; yes; no)
```

```
open import Common using (_$_≡_; `_; `refl; cong₃)
open import Labels
open import BindersByDay as B
open import FirstClassNames as F
open import FCNSubstitution as F
```

```
private
  variable
    L L′ : Labels
    ℓ ℓ′ : -∈ᴸ L
    κ κ′ : F.Kind
    Δ Δ′ : F.Contextᴷ
    LΔ LΔ′ : Labels × F.Contextᴷ
    τ τ′ τ₁ τ₂ σ τε : L F.⨟ Δ ⊢ᵀ κ
    σ′ : L F.⨟ Δ ⊢ᵀ SIG
    ε ε′ : L F.⨟ Δ ⊢ᵀ EFF
```

Decidable equality for kinds.

```
_≡ᴷ?_ : (κ κ' : F.Kind) → Dec (κ ≡ κ')
TYP ≡ᴷ? TYP = yes refl
SCO ≡ᴷ? SCO = yes refl
COM ≡ᴷ? COM = yes refl
SIG ≡ᴷ? SIG = yes refl
EFF ≡ᴷ? EFF = yes refl
TYP ≡ᴷ? COM = no λ()
TYP ≡ᴷ? SIG = no λ()
TYP ≡ᴷ? EFF = no λ()
TYP ≡ᴷ? SCO = no λ()
COM ≡ᴷ? TYP = no λ()
COM ≡ᴷ? SIG = no λ()
COM ≡ᴷ? EFF = no λ()
COM ≡ᴷ? SCO = no λ()
SIG ≡ᴷ? TYP = no λ()
SIG ≡ᴷ? COM = no λ()
SIG ≡ᴷ? EFF = no λ()
SIG ≡ᴷ? SCO = no λ()
EFF ≡ᴷ? TYP = no λ()
EFF ≡ᴷ? COM = no λ()
EFF ≡ᴷ? SIG = no λ()
EFF ≡ᴷ? SCO = no λ()
SCO ≡ᴷ? TYP = no λ()
SCO ≡ᴷ? COM = no λ()
SCO ≡ᴷ? SIG = no λ()
SCO ≡ᴷ? EFF = no λ()
```

# General approach

A naive attempt at defining this translation failed (TODO: figure out why),
so instead we define the translation as follows.

For each source (FCN) syntax construct `Source` (kind, contexts, variables, types, and terms),
the translation from `Source` to a corresponding `Target` syntax construct
(BBD) consists of three components:
1. a type of annotated source (FCN) terms, denoted by double brackets: `⟪_⟫ : Source → Set`
2. an annotation function, denoted by ceiling brackets: `⌜_⌝ : (M : Source) → ⟪ M ⟫`
3. a translation from annotated terms to the target (BBD) syntax, denoted by
   floor brackets: `⌞_⌟ : ∀ {M} → ⟪ M ⟫ → Target`

We obtain the complete translation as `(λ M → ⌞ ⌜ M ⌝ ⌟) : Source → Target`.

The annotation function `⌜_⌝` is where we spend most of the effort in
programming with dependent types.
The definition of the annotation type `⟪_⟫` resembles the definition of the target syntax,
which makes `⌞_⌟` quite straightforward to define.

A source construct may correspond to more than one target construct:
in our case, FCN type variables of kind `SCO` are translated to BBD handler names,
whereas FCN type variables of kind `κ ≢ SCO` are translated to BBD type variables.

#### Remark

Another natural way of defining the translation could be:
1. first as a relation: `⟪_↝_⟫ : Source → Target → Set`.
2. then as a function: `⌜_⌝ : (M : SourceSyntax) → ∃[ N ] ⟪ M ↝ N ⟫`

We note that these are actually two isomorphic ways of defining the translation.
Indeed, observe that an indexed type `I → Set`
can be represented as a fibration `Σ[ T ∈ Set ] (T → I)`.

```
-- Isomorphism between (I → Set) and Σ[ T ∈ Set ] (T → I).
module Remark {I : Set} where
  open import Data.Product using (Σ-syntax; _,_; proj₁)

  fibrate : (I → Set) → Σ[ T ∈ Set ] (T → I)
  fibrate F = (Σ[ i ∈ I ] (F i)) , proj₁

  unfibrate : Σ[ T ∈ Set ] (T → I) → (I → Set)
  unfibrate (T , index) = λ i → Σ[ t ∈ T ] index t ≡ i

  -- TODO? show these are inverses
```

With `I = Target`, that isomorphism sends the
translation relation `⟪_↝_⟫ : Source → Target`
to the pair of the annotation type and translation function
`⦅ ⟪_⟫ , ⌞_⌟ ⦆ : Source → Σ[ ⟪M⟫ ∈ Set ] (⟪M⟫ → Target)`

We chose our formulation for being terser, because it avoids having to
unpack pairs of type `∃[ N ] ⟪ M ↝ N ⟫` in recursive definitions of `⌜_⌝`.

# Translate types

Kinds other than scopes (`SCO`) in FCN are translated to kinds in BBD.
Variables of kind `SCO` in FCN will be translated to handler names in BBD
(which have separate binders and contexts).

Kinds will be annotated with evidence that they are not `SCO`.

```
⟪_⟫K : F.Kind → Set
⟪ κ ⟫K = κ ≢ SCO
```

```
⌜EFF⌝ : ⟪ EFF ⟫K
⌜EFF⌝ = λ()
```

```
⌜TYP⌝ : ⟪ TYP ⟫K
⌜TYP⌝ = λ()
```

```
⌜COM⌝ : ⟪ COM ⟫K
⌜COM⌝ = λ()
```

```
⌜SIG⌝ : ⟪ SIG ⟫K
⌜SIG⌝ = λ()
```

The annotation function is partial `⌜ κ ⌝K` evaluates to either `yes refl`
(then `κ = SCO`, which will be treated specially),
or to `no ⌜κ⌝`, for some annotation `⌜κ⌝`.

```
⌜_⌝K : (κ : F.Kind) → Dec (κ ≡ SCO)   -- isomorphic to (κ ≡ SCO) ⊎ ⟪ κ ⟫K
⌜ κ ⌝K = κ ≡ᴷ? SCO
```

Our translation from annotated FCN kinds `⌜κ⌝` to BBD kinds `⌞ ⌜κ⌝ ⌟K`
will use the following proof-irrelevant `⊥` elimination.
The `.` in the type means that `⊥-elim x` is definitionally
equal to `⊥-elim y` for any two `x : ⊥` `y : ⊥`.
In contrast, the `⊥-elim` from the standard library is not proof-irrelevant.
We could also have used the proof-relevant definition, at the cost
of having to use transport (`subst`) in some places (TODO: explain).

```
⊥-elim : {A : Set} → .⊥ → A
⊥-elim ()
```

```
⌞_⌟K : {κ : F.Kind} → .(⟪ κ ⟫K) → B.Kind
⌞_⌟K {TYP} _ = TYP
⌞_⌟K {COM} _ = COM
⌞_⌟K {SIG} _ = SIG
⌞_⌟K {EFF} _ = EFF
⌞_⌟K {SCO} SCO≢SCO = ⊥-elim (SCO≢SCO refl)
```

Annotation of type contexts `⌜Δ⌝ : ⟪ Δ ⟫ᴷ`.
We distinguish type variables of kind `SCO`, from others annotated with `⌜κ⌝ : ⟪ κ ⟫K`.

```
data ⟪_⟫ᴷ : F.Contextᴷ → Set where
  ∅ : ⟪ ∅ ⟫ᴷ
  _▷ᴺ- : ⟪ Δ ⟫ᴷ → ⟪ Δ F.▷ SCO ⟫ᴷ
  _▷_ : ⟪ Δ ⟫ᴷ → ⟪ κ ⟫K → ⟪ Δ F.▷ κ ⟫ᴷ
```

Annotate FCN type contexts `⌜ Δ ⌝ᴷ`.

```
⌜_⌝ᴷ : (Δ : F.Contextᴷ) → ⟪ Δ ⟫ᴷ
⌜ ∅ ⌝ᴷ = ∅
⌜ Δ F.▷ κ ⌝ᴷ with ⌜ κ ⌝K
... | yes refl = ⌜ Δ ⌝ᴷ ▷ᴺ-
... | no ⌜κ⌝ = ⌜ Δ ⌝ᴷ ▷ ⌜κ⌝
```

Translate annotated FCN type contexts `⌜Δ⌝` to BBD type contexts `⌞ ⌜Δ⌝ ⌟ᴷ`.

```
⌞_⌟ᴷ : ⟪ Δ ⟫ᴷ → B.Contextᴷ
⌞ ∅ ⌟ᴷ = ∅
⌞ Δ ▷ᴺ- ⌟ᴷ = ⌞ Δ ⌟ᴷ ▷ᴺ-
⌞ Δ ▷ ⌜κ⌝ ⌟ᴷ = ⌞ Δ ⌟ᴷ B.▷ ⌞ ⌜κ⌝ ⌟K
```

```
private
  variable
    ⌜κ⌝ : ⟪ κ ⟫K
    ⌜κ′⌝ : ⟪ κ′ ⟫K
    ⌜Δ⌝ ⌜Δ′⌝ : ⟪ Δ ⟫ᴷ
    Γ : F.Context L Δ
```

BBD has two kinds of "type variables" (BBD type variables and handler names),
so there are two type variable translations.

For type variables, no annotation is necessary, so we skip the definition of `⟪_⟫` and `⌜_⌝`,
defining only translation functions `⌞_⌟∈★` and `⌞_∣_⌟∈ᴺ`.

Translate FCN type variables to BBD type variables.

```
⦅_∈★_⦆⌞_⌟ : (⌜κ⌝ : ⟪ κ ⟫K) → (⌜Δ⌝ : ⟪ Δ ⟫ᴷ) → κ F.∈★ Δ → ⌞ ⌜κ⌝ ⌟K B.∈★ ⌞ ⌜Δ⌝ ⌟ᴷ
⦅ ⌜κ⌝ ∈★ (⌜Δ⌝ ▷ᴺ-) ⦆⌞ Z ⌟   = ⊥-elim (⌜κ⌝ refl)
⦅ ⌜κ⌝ ∈★ (⌜Δ⌝ ▷ _) ⦆⌞ Z ⌟   = Z
⦅ ⌜κ⌝ ∈★ (⌜Δ⌝ ▷ᴺ-) ⦆⌞ S α ⌟ = Sᴺ ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟
⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ▷ _   ⦆⌞ S α ⌟ = S  ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟
```

```
⌞_⌟∈★ : κ F.∈★ Δ → {⌜κ⌝ : ⟪ κ ⟫K} → {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} → ⌞ ⌜κ⌝ ⌟K B.∈★ ⌞ ⌜Δ⌝ ⌟ᴷ
⌞ α ⌟∈★ {⌜κ⌝} {⌜Δ⌝} = ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟
```


Translate FCN type variables of kind `SCO` to BBD handler names.

```
⌞_∣_⌟∈ᴺ : {Δ : F.Contextᴷ} → SCO F.∈★ Δ → (⌜Δ⌝ : ⟪ Δ ⟫ᴷ) → B.-∈ᴺ ⌞ ⌜Δ⌝ ⌟ᴷ
⌞ Z ∣ Δ ▷ᴺ- ⌟∈ᴺ = Z
⌞ S α ∣ Δ ▷ᴺ- ⌟∈ᴺ = Sᴺ ⌞ α ∣ Δ ⌟∈ᴺ
⌞ S α ∣ Δ ▷ ⌜κ⌝ ⌟∈ᴺ = S ⌞ α ∣ Δ ⌟∈ᴺ
⌞ Z ∣ Δ ▷ ⌜κ⌝ ⌟∈ᴺ = ⊥-elim (⌜κ⌝ refl)
```

```
⌞_⌟ᶻ : {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} (η : L F.⨟ Δ ⊢ᵀ SCO) → -∈ᴺ L ⨟ ⌞ ⌜Δ⌝ ⌟ᴷ
⌞ Var α ⌟ᶻ = static ⌞ α ∣ _ ⌟∈ᴺ
⌞ dynamic ℓ ⌟ᶻ = dynamic ℓ
```

Annotated FCN types `⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ` (for a type `τ : Δ ⊢ᵀ κ`).

```
data ⦅_,_⦆⟪_⟫ᵀ {L} (⌜Δ⌝ : ⟪ Δ ⟫ᴷ) : (⌜κ⌝ : ⟪ κ ⟫K) → L F.⨟ Δ ⊢ᵀ κ → Set where
  Var : (α : κ F.∈★ Δ) → ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ Var α ⟫ᵀ
  Name : (ε : L F.⨟ Δ ⊢ᵀ SCO) → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Name ε ⟫ᵀ
  _⇒_ : ∀ {A B} → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⇒ B ⟫ᵀ
  Forallᵀ : ∀ {A} → (⌜κ'⌝ : ⟪ κ ⟫K) → ⦅ ⌜Δ⌝ ▷ ⌜κ'⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Forallᵀ κ A ⟫ᵀ
  Forallᴺ : ∀ {A} → ⦅ ⌜Δ⌝ ▷ᴺ- , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Forallᵀ SCO A ⟫ᵀ
  _⇒ˢ_ : ∀ {A B} → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⇒ˢ B ⟫ᵀ
  _⇒ᵖ_ : ∀ {A B} → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⇒ᵖ B ⟫ᵀ
  _⇒ʰ_ : ∀ {A B} → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⇒ʰ B ⟫ᵀ
  Forallˢ : ∀ {A} → (⌜κ'⌝ : ⟪ κ ⟫K) → ⦅ ⌜Δ⌝ ▷ ⌜κ'⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Forallˢ κ A ⟫ᵀ
  Forallˢᴺ : ∀ {A} → ⦅ ⌜Δ⌝ ▷ᴺ- , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Forallˢ SCO A ⟫ᵀ
  _/_ : ∀ {A B} → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A / B ⟫ᵀ
  Unit : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ Unit ⟫ᵀ
  ι : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ι ⟫ᵀ
  _⦂_∷_ : (α : L F.⨟ Δ ⊢ᵀ SCO) → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ α ⦂ σ ∷ ε ⟫ᵀ
  ⟨_⟩ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ τ ⟫ᵀ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ⟨ τ ⟩ ⟫ᵀ
```

Annotate FCN types.

```
⌜_⌝ᵀ : (A : L F.⨟ Δ ⊢ᵀ κ) → ∀ {⌜Δ⌝} → ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ A ⟫ᵀ
⌜ Var α ⌝ᵀ = Var α
⌜ Name ε ⌝ᵀ = Name ε
⌜ A ⇒ B ⌝ᵀ = ⌜ A ⌝ᵀ ⇒ ⌜ B ⌝ᵀ
⌜ Forallᵀ κ A ⌝ᵀ with ⌜ κ ⌝K
... | yes refl = Forallᴺ ⌜ A ⌝ᵀ
... | no ⌜κ⌝ = Forallᵀ ⌜κ⌝ ⌜ A ⌝ᵀ
⌜ ι ⌝ᵀ = ι
⌜ a ⦂ σ ∷ ε ⌝ᵀ = a ⦂ ⌜ σ ⌝ᵀ ∷ ⌜ ε ⌝ᵀ
⌜ A ⇒ˢ B ⌝ᵀ = ⌜ A ⌝ᵀ ⇒ˢ ⌜ B ⌝ᵀ
⌜ A ⇒ʰ B ⌝ᵀ = ⌜ A ⌝ᵀ ⇒ʰ ⌜ B ⌝ᵀ
⌜ A ⇒ᵖ B ⌝ᵀ = ⌜ A ⌝ᵀ ⇒ᵖ ⌜ B ⌝ᵀ
⌜ Forallˢ κ A ⌝ᵀ with ⌜ κ ⌝K
... | yes refl = Forallˢᴺ ⌜ A ⌝ᵀ
... | no ⌜κ⌝ = Forallˢ ⌜κ⌝ ⌜ A ⌝ᵀ
⌜ A / B ⌝ᵀ = ⌜ A ⌝ᵀ / ⌜ B ⌝ᵀ
⌜ Unit ⌝ᵀ = Unit
⌜ ⟨ τ ⟩ ⌝ᵀ = ⟨ ⌜ τ ⌝ᵀ ⟩
⌜_⌝ᵀ {⌜κ⌝ = ⌜κ⌝} (dynamic _) = ⊥-elim (⌜κ⌝ refl)
```

```
⦅_⦆⌜_⌝ᵀ : (⌜κ⌝ : ⟪ κ ⟫K) → (A : L F.⨟ Δ ⊢ᵀ κ) → ∀ {⌜Δ⌝} → ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ A ⟫ᵀ
⦅_⦆⌜_⌝ᵀ ⌜κ⌝ = ⌜_⌝ᵀ
```

Translate annotated FCN types to BBD types.

```
⌞_⌟ᵀ : {A : L F.⨟ Δ ⊢ᵀ κ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜κ⌝ : ⟪ κ ⟫K} → ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ A ⟫ᵀ → L B.⨟ ⌞ ⌜Δ⌝ ⌟ᴷ ⊢ᵀ ⌞ ⌜κ⌝ ⌟K
⌞_⌟ᵀ {⌜Δ⌝ = ⌜Δ⌝} {⌜κ⌝} (Var α) = Var (⌞ α ⌟∈★ {⌜κ⌝} {⌜Δ⌝})
⌞ Name ε ⌟ᵀ = Unit
⌞ A ⇒ B ⌟ᵀ = ⌞ A ⌟ᵀ ⇒ ⌞ B ⌟ᵀ
⌞ Forallᵀ ⌜κ⌝ A ⌟ᵀ = Forallᵀ ⌞ ⌜κ⌝ ⌟K ⌞ A ⌟ᵀ
⌞ Forallᴺ A ⌟ᵀ = Forallᴺ ⌞ A ⌟ᵀ
⌞ A ⇒ˢ B ⌟ᵀ = ⌞ A ⌟ᵀ ⇒ˢ ⌞ B ⌟ᵀ
⌞ A ⇒ʰ B ⌟ᵀ = ⌞ A ⌟ᵀ ⇒ʰ ⌞ B ⌟ᵀ
⌞ A ⇒ᵖ B ⌟ᵀ = ⌞ A ⌟ᵀ ⇒ᵖ ⌞ B ⌟ᵀ
⌞ Forallˢ ⌜κ⌝ A ⌟ᵀ = Forallˢ ⌞ ⌜κ⌝ ⌟K ⌞ A ⌟ᵀ
⌞ Forallˢᴺ A ⌟ᵀ = Forallˢᴺ ⌞ A ⌟ᵀ
⌞ A / B ⌟ᵀ = ⌞ A ⌟ᵀ / ⌞ B ⌟ᵀ
⌞ Unit ⌟ᵀ = Unit
⌞ ι ⌟ᵀ = ι
⌞ a ⦂ σ ∷ ε ⌟ᵀ = ⌞ a ⌟ᶻ ⦂ ⌞ σ ⌟ᵀ ∷ ⌞ ε ⌟ᵀ
⌞ ⟨ τ ⟩ ⌟ᵀ = ⟨ ⌞ τ ⌟ᵀ ⟩
```

## Translation of terms

Annotated FCN term contexts `⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ`.

```
infixl 5 _▷_ _▷★_ _▷ᴺ-
data ⟪_⊢_⟫ᴳ {L} : ⟪ Δ ⟫ᴷ → F.Context L Δ → Set where
  ∅ : ⟪ ∅ ⊢ ∅ ⟫ᴳ
  _▷_ : {A : L F.⨟ Δ ⊢ᵀ TYP} → ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ → ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ → ⟪ ⌜Δ⌝ ⊢ Γ F.▷ A ⟫ᴳ
  _▷★_ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ → (⌜κ⌝ : ⟪ κ ⟫K) → ⟪ ⌜Δ⌝ ▷ ⌜κ⌝ ⊢ Γ F.▷★ κ ⟫ᴳ
  _▷ᴺ- : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ → ⟪ ⌜Δ⌝ ▷ᴺ- ⊢ Γ F.▷★ SCO ⟫ᴳ
```

```
private variable
  ⌜Γ⌝ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ
```

Annotate FCN term contexts.

```
⌜_⌝ᴳ : (Γ : F.Context L Δ) → ⟪ ⌜ Δ ⌝ᴷ ⊢ Γ ⟫ᴳ
⌜ ∅ ⌝ᴳ = ∅
⌜ Γ F.▷ A ⌝ᴳ = ⌜ Γ ⌝ᴳ ▷ ⌜ A ⌝ᵀ
⌜ Γ F.▷★ κ ⌝ᴳ with ⌜ κ ⌝K
... | yes refl = ⌜ Γ ⌝ᴳ ▷ᴺ-
... | no ⌜κ⌝ = ⌜ Γ ⌝ᴳ ▷★ ⌜κ⌝
```

Translate annotated term contexts.

```
⌞_⌟ᴳ : {Γ : F.Context L Δ} → ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ → B.Context L ⌞ ⌜Δ⌝ ⌟ᴷ
⌞ ∅ ⌟ᴳ = ∅
⌞ Γ ▷ A ⌟ᴳ = ⌞ Γ ⌟ᴳ ▷ ⌞ A ⌟ᵀ
⌞ Γ ▷★ ⌜κ⌝ ⌟ᴳ = ⌞ Γ ⌟ᴳ ▷★ ⌞ ⌜κ⌝ ⌟K
⌞ Γ ▷ᴺ- ⌟ᴳ = ⌞ Γ ⌟ᴳ ▷ᴺ-
```

Uniqueness of annotations.

```
≡-⌜⌝ᵀ : (⌜τ⌝ ⌜τ′⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ)
  → ⌜τ⌝ ≡ ⌜τ′⌝
≡-⌜⌝ᵀ (Var α) (Var .α) = refl
≡-⌜⌝ᵀ (Name ε) (Name .ε) = refl
≡-⌜⌝ᵀ (⌜τ₁⌝ ⇒ ⌜τ₂⌝)  (⌜τ₁′⌝ ⇒  ⌜τ₂′⌝) = cong₂ _⇒_  (≡-⌜⌝ᵀ ⌜τ₁⌝ ⌜τ₁′⌝) (≡-⌜⌝ᵀ ⌜τ₂⌝ ⌜τ₂′⌝)
≡-⌜⌝ᵀ (⌜τ₁⌝ ⇒ˢ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ˢ ⌜τ₂′⌝) = cong₂ _⇒ˢ_ (≡-⌜⌝ᵀ ⌜τ₁⌝ ⌜τ₁′⌝) (≡-⌜⌝ᵀ ⌜τ₂⌝ ⌜τ₂′⌝)
≡-⌜⌝ᵀ (⌜τ₁⌝ ⇒ᵖ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ᵖ ⌜τ₂′⌝) = cong₂ _⇒ᵖ_ (≡-⌜⌝ᵀ ⌜τ₁⌝ ⌜τ₁′⌝) (≡-⌜⌝ᵀ ⌜τ₂⌝ ⌜τ₂′⌝)
≡-⌜⌝ᵀ (⌜τ₁⌝ ⇒ʰ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ʰ ⌜τ₂′⌝) = cong₂ _⇒ʰ_ (≡-⌜⌝ᵀ ⌜τ₁⌝ ⌜τ₁′⌝) (≡-⌜⌝ᵀ ⌜τ₂⌝ ⌜τ₂′⌝)
≡-⌜⌝ᵀ (Forallᵀ ⌜κ⌝ ⌜τ⌝) (Forallᵀ ⌜κ'⌝ ⌜τ′⌝) = cong (Forallᵀ _) (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)
≡-⌜⌝ᵀ (Forallᵀ ⌜κ⌝ ⌜τ⌝) (Forallᴺ ⌜τ′⌝) = ⊥-elim (⌜κ⌝ refl)
≡-⌜⌝ᵀ (Forallᴺ ⌜τ⌝) (Forallᵀ ⌜κ′⌝ ⌜τ′⌝) = ⊥-elim (⌜κ′⌝ refl)
≡-⌜⌝ᵀ (Forallᴺ ⌜τ⌝) (Forallᴺ ⌜τ′⌝) = cong Forallᴺ (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)
≡-⌜⌝ᵀ (Forallˢ ⌜κ⌝ ⌜τ⌝) (Forallˢ ⌜κ′⌝₁ ⌜τ′⌝) = cong (Forallˢ _) (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)
≡-⌜⌝ᵀ (Forallˢ ⌜κ⌝ ⌜τ⌝) (Forallˢᴺ ⌜τ′⌝) = ⊥-elim (⌜κ⌝ refl)
≡-⌜⌝ᵀ (Forallˢᴺ ⌜τ⌝) (Forallˢ ⌜κ′⌝ ⌜τ′⌝) = ⊥-elim (⌜κ′⌝ refl)
≡-⌜⌝ᵀ (Forallˢᴺ ⌜τ⌝) (Forallˢᴺ ⌜τ′⌝) = cong Forallˢᴺ (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)
≡-⌜⌝ᵀ (⌜τ₁⌝ / ⌜τ₂⌝) (⌜τ₁′⌝ / ⌜τ₂′⌝) = cong₂ _/_ (≡-⌜⌝ᵀ ⌜τ₁⌝ ⌜τ₁′⌝) (≡-⌜⌝ᵀ ⌜τ₂⌝ ⌜τ₂′⌝)
≡-⌜⌝ᵀ Unit Unit = refl
≡-⌜⌝ᵀ ι ι = refl
≡-⌜⌝ᵀ (α ⦂ ⌜σ⌝ ∷ ⌜ε⌝) (.α ⦂ ⌜σ′⌝ ∷ ⌜ε′⌝) = cong₂ (_ ⦂_∷_) (≡-⌜⌝ᵀ ⌜σ⌝ ⌜σ′⌝) (≡-⌜⌝ᵀ ⌜ε⌝ ⌜ε′⌝)
≡-⌜⌝ᵀ ⟨ ⌜τ⌝ ⟩ ⟨ ⌜τ′⌝ ⟩ = cong ⟨_⟩ (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)

≡-⌞⌟ᵀ : (⌜τ⌝ ⌜τ′⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ)
  → ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
≡-⌞⌟ᵀ ⌜τ⌝ ⌜τ′⌝ = cong ⌞_⌟ᵀ (≡-⌜⌝ᵀ ⌜τ⌝ ⌜τ′⌝)
```

Substitution lemmas.

```
private variable
  _#★_ : Labels × F.Contextᴷ → F.Kind → Set
  _#★′_ : Labels × B.Contextᴷ → B.Kind → Set

open F.Kit★
open B.Kit★
open F.⦅_⦆_→★_
open B.⦅_⦆_→★_

record ⟪Kit★⟫ (K : F.Kit★ _#★_) (K′ : B.Kit★ _#★′_) : Set where
  field
    ⟪GK⟫ : F.GoodKit★ K
    ⦅_⦆⦅_#★_⦆⌞_⌟ : ∀ {L Δ} (⌜Δ⌝ : ⟪ Δ ⟫ᴷ) (⌜κ⌝ : ⟪ κ ⟫K) → (L , Δ) #★ κ → (L , ⌞ ⌜Δ⌝ ⌟ᴷ) #★′ ⌞ ⌜κ⌝ ⌟K
    ⌞from★⌟ : ∀ {L Δ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜κ⌝ : ⟪ κ ⟫K} (α : (L , Δ) F.∋★ κ) → from-∈★ K′ {L = L} ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟ ≡ ⦅_⦆⦅_#★_⦆⌞_⌟ ⌜Δ⌝ ⌜κ⌝ (from-∈★ K α)
    ⌞to★⌟ : ∀ {L Δ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜κ⌝ : ⟪ κ ⟫K} (α : (L , Δ) #★ κ) (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ to-⊢★ K α ⟫ᵀ) → to-⊢★ K′ (⦅_⦆⦅_#★_⦆⌞_⌟ ⌜Δ⌝ ⌜κ⌝ α) ≡ ⌞ ⌜τ⌝ ⌟ᵀ
    ⌞wkn★⌟ : ∀ {L Δ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜κ⌝ : ⟪ κ ⟫K} (α : (L , Δ) #★ κ) → wkn★ K′ (⦅_⦆⦅_#★_⦆⌞_⌟ ⌜Δ⌝ ⌜κ⌝ α) ≡ ⦅_⦆⦅_#★_⦆⌞_⌟ (⌜Δ⌝ ▷ ⌜κ′⌝) ⌜κ⌝ (wkn★ K α)
    ⌞wkn★ᴺ⌟ : ∀ {L Δ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜κ⌝ : ⟪ κ ⟫K} (α : (L , Δ) #★ κ) → wkn★ᴺ K′ (⦅_⦆⦅_#★_⦆⌞_⌟ ⌜Δ⌝ ⌜κ⌝ α) ≡ ⦅_⦆⦅_#★_⦆⌞_⌟ (⌜Δ⌝ ▷ᴺ-) ⌜κ⌝ (wkn★ K α)

open ⟪Kit★⟫
open GoodKit★

⟪K∈★⟫ : ⟪Kit★⟫ F.K∈★ B.K∈★
⟪K∈★⟫ = record {
  ⟪GK⟫ = it ;
  ⦅_⦆⦅_#★_⦆⌞_⌟ = λ ⌜Δ⌝ ⌜κ⌝ α → ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟ ;
  ⌞from★⌟ = λ α → refl ;
  ⌞to★⌟ = λ{ α (Var α′) → refl } ;
  ⌞wkn★⌟ = λ α → refl ;
  ⌞wkn★ᴺ⌟ = λ α → refl }

record ⦅_⦆⦅_→★_⦆_≡⌞→★⌟_ {K : F.Kit★ _#★_} {K′ : B.Kit★ _#★′_} (⟪K⟫ : ⟪Kit★⟫ K K′) (⌜Δ⌝ : ⟪ Δ ⟫ᴷ) (⌜Δ′⌝ : ⟪ Δ′ ⟫ᴷ) (r★ : F.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)) (⟦r★⟧ : B.⦅ K′ ⦆ (L , ⌞ ⌜Δ⌝ ⌟ᴷ) →★ (L′ , ⌞ ⌜Δ′⌝ ⌟ᴷ)) : Set where
  field
    ⌞tra∈★⌟ : ∀ (α : κ F.∈★ Δ) → tra∈★ ⟦r★⟧ ⦅ ⌜κ⌝ ∈★ ⌜Δ⌝ ⦆⌞ α ⌟ ≡ ⦅ ⟪K⟫ ⦆⦅ ⌜Δ′⌝ #★ ⌜κ⌝ ⦆⌞ tra∈★ r★ α ⌟
    ⌞relabel⌟ : relabel ⟦r★⟧ ≡ relabel r★
    ⌞tra∈ᴺ⌟ : ∀ (η : SCO F.∈★ Δ) → tra∈ᴺ ⟦r★⟧ ⌞ η ∣ ⌜Δ⌝ ⌟∈ᴺ ≡ ⌞ to-⊢★ K (tra∈★ r★ η) ⌟ᶻ

open ⦅_⦆⦅_→★_⦆_≡⌞→★⌟_

weakenᶻ-⌞⌟ : (η : L F.⨟ Δ ⊢ᵀ SCO) → weakenᶻ {Δ = ⌞ ⌜Δ⌝ ⌟ᴷ} ⌞ η ⌟ᶻ ≡ ⌞_⌟ᶻ {⌜Δ⌝ = (⌜Δ⌝ ▷ ⌜κ⌝)} (F.weaken★ η)
weakenᶻ-⌞⌟ (Var η) = refl
weakenᶻ-⌞⌟ (dynamic ℓ) = refl

weakenᶻᴺ-⌞⌟ : (η : L F.⨟ Δ ⊢ᵀ SCO) → weakenᶻᴺ {Δ = ⌞ ⌜Δ⌝ ⌟ᴷ} ⌞ η ⌟ᶻ ≡ ⌞_⌟ᶻ {⌜Δ⌝ = (⌜Δ⌝ ▷ᴺ-)} (F.weaken★ η)
weakenᶻᴺ-⌞⌟ (Var η) = refl
weakenᶻᴺ-⌞⌟ (dynamic ℓ) = refl

⌞tra∈ᶻ⌟ : {K : F.Kit★ _#★_} {K′ : B.Kit★ _#★′_} {⟪K⟫ : ⟪Kit★⟫ K K′}
  → {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜Δ′⌝ : ⟪ Δ′ ⟫ᴷ}
  → {r★ : F.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : B.⦅ K′ ⦆ (L , ⌞ ⌜Δ⌝ ⌟ᴷ) →★ (L′ , ⌞ ⌜Δ′⌝ ⌟ᴷ)}
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ′⌝ ⦆ r★ ≡⌞→★⌟ ⟦r★⟧
  → (η : L F.⨟ Δ ⊢ᵀ SCO)
  → tra∈ᶻ ⟦r★⟧ ⌞ η ⌟ᶻ ≡ ⌞ F.tra★ r★ η ⌟ᶻ
⌞tra∈ᶻ⌟ r≡ (Var η) = ⌞tra∈ᴺ⌟ r≡ η
⌞tra∈ᶻ⌟ r≡ (dynamic ℓ) = cong (λ rᴸ → dynamic (ren∈ᴸ rᴸ ℓ)) (⌞relabel⌟ r≡)

⌞tra★▷⌟ : {K : F.Kit★ _#★_} {K′ : B.Kit★ _#★′_} {⟪K⟫ : ⟪Kit★⟫ K K′}
  → {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜Δ′⌝ : ⟪ Δ′ ⟫ᴷ}
  → {r★ : F.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : B.⦅ K′ ⦆ (L , ⌞ ⌜Δ⌝ ⌟ᴷ) →★ (L′ , ⌞ ⌜Δ′⌝ ⌟ᴷ)}
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ′⌝ ⦆ r★ ≡⌞→★⌟ ⟦r★⟧
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ ▷ ⌜κ⌝ →★ ⌜Δ′⌝ ▷ ⌜κ⌝ ⦆ F.tra★▷ r★ ≡⌞→★⌟ B.tra★▷ ⟦r★⟧
⌞tra∈★⌟ (⌞tra★▷⌟ {⟪K⟫ = ⟪K⟫} r≡) Z = ⌞from★⌟ ⟪K⟫ Z
⌞tra∈★⌟ (⌞tra★▷⌟ {K′ = K′} {⟪K⟫} r≡) (S α) = trans (cong (wkn★ K′) (⌞tra∈★⌟ r≡ α)) (⌞wkn★⌟ ⟪K⟫ _)
⌞relabel⌟ (⌞tra★▷⌟ r≡) = ⌞relabel⌟ r≡
⌞tra∈ᴺ⌟ (⌞tra★▷⌟ {⌜κ⌝ = ⌜κ⌝} r≡) Z = ⊥-elim (⌜κ⌝ refl)
⌞tra∈ᴺ⌟ (⌞tra★▷⌟ {K = K} {⟪K⟫ = ⟪K⟫} r≡) (S η) = trans (cong B.weakenᶻ (⌞tra∈ᴺ⌟ r≡ η)) (trans (weakenᶻ-⌞⌟ (to-⊢★ K _)) (cong ⌞_⌟ᶻ (sym (to★-wkn★ (⟪GK⟫ ⟪K⟫) _))))

⌞tra★▷ᴺ⌟ : {K : F.Kit★ _#★_} {K′ : B.Kit★ _#★′_} {⟪K⟫ : ⟪Kit★⟫ K K′} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜Δ′⌝ : ⟪ Δ′ ⟫ᴷ} {r★ : F.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : B.⦅ K′ ⦆ (L , ⌞ ⌜Δ⌝ ⌟ᴷ) →★ (L′ , ⌞ ⌜Δ′⌝ ⌟ᴷ)}
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ′⌝ ⦆ r★ ≡⌞→★⌟ ⟦r★⟧
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ ▷ᴺ- →★ ⌜Δ′⌝ ▷ᴺ- ⦆ F.tra★▷ r★ ≡⌞→★⌟ B.tra★▷ᴺ ⟦r★⟧
⌞tra∈★⌟ (⌞tra★▷ᴺ⌟ r≡) {⌜κ⌝ = ⌜κ⌝} Z = ⊥-elim (⌜κ⌝ refl)
⌞tra∈★⌟ (⌞tra★▷ᴺ⌟ {K′ = K′} {⟪K⟫ = ⟪K⟫} r≡) (S α) = trans (cong (wkn★ᴺ K′) (⌞tra∈★⌟ r≡ α)) (⌞wkn★ᴺ⌟ ⟪K⟫ _)
⌞relabel⌟ (⌞tra★▷ᴺ⌟ r≡) = ⌞relabel⌟ r≡
⌞tra∈ᴺ⌟ (⌞tra★▷ᴺ⌟ {⟪K⟫ = ⟪K⟫} r≡) Z = sym (cong ⌞_⌟ᶻ (to★-from★ (⟪GK⟫ ⟪K⟫) _))
⌞tra∈ᴺ⌟ (⌞tra★▷ᴺ⌟ {K = K} {⟪K⟫ = ⟪K⟫} r≡) (S η) = trans (cong B.weakenᶻᴺ (⌞tra∈ᴺ⌟ r≡ η)) (trans (weakenᶻᴺ-⌞⌟ (to-⊢★ K _)) (cong ⌞_⌟ᶻ (sym (to★-wkn★ (⟪GK⟫ ⟪K⟫) _))))

⌞tra★⌟ : {K : F.Kit★ _#★_} {K′ : B.Kit★ _#★′_} {⟪K⟫ : ⟪Kit★⟫ K K′} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {⌜Δ′⌝ : ⟪ Δ′ ⟫ᴷ}
  → {r★ : F.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : B.⦅ K′ ⦆ (L , ⌞ ⌜Δ⌝ ⌟ᴷ) →★ (L′ , ⌞ ⌜Δ′⌝ ⌟ᴷ)}
  → ⦅ ⟪K⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ′⌝ ⦆ r★ ≡⌞→★⌟ ⟦r★⟧
  → (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ) (⌜τ′⌝ : ⦅ ⌜Δ′⌝ , ⌜κ⌝ ⦆⟪ F.tra★ r★ τ ⟫ᵀ)
  → B.tra★ ⟦r★⟧ ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
⌞tra★⌟ {K′ = K′} {⟪K⟫ = ⟪K⟫} r≡ (Var α) ⌜τ′⌝ = trans (cong (to-⊢★ K′) (⌞tra∈★⌟ r≡ α)) (⌞to★⌟ ⟪K⟫ _ ⌜τ′⌝)
⌞tra★⌟ r≡ (Name ε) (Name ε′) = refl
⌞tra★⌟ r≡ (⌜τ₁⌝ ⇒ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ ⌜τ₂′⌝)   = cong₂ _⇒_  (⌞tra★⌟ r≡ ⌜τ₁⌝ ⌜τ₁′⌝) (⌞tra★⌟ r≡ ⌜τ₂⌝ ⌜τ₂′⌝)
⌞tra★⌟ r≡ (⌜τ₁⌝ ⇒ˢ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ˢ ⌜τ₂′⌝) = cong₂ _⇒ˢ_ (⌞tra★⌟ r≡ ⌜τ₁⌝ ⌜τ₁′⌝) (⌞tra★⌟ r≡ ⌜τ₂⌝ ⌜τ₂′⌝)
⌞tra★⌟ r≡ (⌜τ₁⌝ ⇒ᵖ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ᵖ ⌜τ₂′⌝) = cong₂ _⇒ᵖ_ (⌞tra★⌟ r≡ ⌜τ₁⌝ ⌜τ₁′⌝) (⌞tra★⌟ r≡ ⌜τ₂⌝ ⌜τ₂′⌝)
⌞tra★⌟ r≡ (⌜τ₁⌝ ⇒ʰ ⌜τ₂⌝) (⌜τ₁′⌝ ⇒ʰ ⌜τ₂′⌝) = cong₂ _⇒ʰ_ (⌞tra★⌟ r≡ ⌜τ₁⌝ ⌜τ₁′⌝) (⌞tra★⌟ r≡ ⌜τ₂⌝ ⌜τ₂′⌝)
⌞tra★⌟ r≡ (⌜τ₁⌝ / ⌜τ₂⌝) (⌜τ₁′⌝ / ⌜τ₂′⌝)   = cong₂ _/_  (⌞tra★⌟ r≡ ⌜τ₁⌝ ⌜τ₁′⌝) (⌞tra★⌟ r≡ ⌜τ₂⌝ ⌜τ₂′⌝)
⌞tra★⌟ r≡ (Forallᵀ ⌜κ⌝ ⌜τ⌝) (Forallᵀ ⌜κ′⌝ ⌜τ′⌝) = cong (Forallᵀ _) (⌞tra★⌟ (⌞tra★▷⌟ r≡) ⌜τ⌝ ⌜τ′⌝)
⌞tra★⌟ r≡ (Forallˢ ⌜κ⌝ ⌜τ⌝) (Forallˢ ⌜κ′⌝ ⌜τ′⌝) = cong (Forallˢ _) (⌞tra★⌟ (⌞tra★▷⌟ r≡) ⌜τ⌝ ⌜τ′⌝)
⌞tra★⌟ r≡ (Forallᴺ ⌜τ⌝)  (Forallᴺ ⌜τ′⌝)  = cong Forallᴺ  (⌞tra★⌟ (⌞tra★▷ᴺ⌟ r≡) ⌜τ⌝ ⌜τ′⌝)
⌞tra★⌟ r≡ (Forallˢᴺ ⌜τ⌝) (Forallˢᴺ ⌜τ′⌝) = cong Forallˢᴺ (⌞tra★⌟ (⌞tra★▷ᴺ⌟ r≡) ⌜τ⌝ ⌜τ′⌝)
⌞tra★⌟ r≡ (Forallᵀ ⌜κ⌝ ⌜τ⌝) (Forallᴺ ⌜τ′⌝)  = ⊥-elim (⌜κ⌝ refl)
⌞tra★⌟ r≡ (Forallᴺ ⌜τ⌝) (Forallᵀ ⌜κ⌝ ⌜τ′⌝)  = ⊥-elim (⌜κ⌝ refl)
⌞tra★⌟ r≡ (Forallˢ ⌜κ⌝ ⌜τ⌝) (Forallˢᴺ ⌜τ′⌝) = ⊥-elim (⌜κ⌝ refl)
⌞tra★⌟ r≡ (Forallˢᴺ ⌜τ⌝) (Forallˢ ⌜κ⌝ ⌜τ′⌝) = ⊥-elim (⌜κ⌝ refl)
⌞tra★⌟ r≡ Unit Unit = refl
⌞tra★⌟ r≡ ι ι = refl
⌞tra★⌟ r≡ (η ⦂ ⌜σ⌝ ∷ ⌜ε⌝) (_ ⦂ ⌜σ′⌝ ∷ ⌜ε′⌝) = cong₃ _⦂_∷_ (⌞tra∈ᶻ⌟ r≡ η) (⌞tra★⌟ r≡ ⌜σ⌝ ⌜σ′⌝) (⌞tra★⌟ r≡ ⌜ε⌝ ⌜ε′⌝)
⌞tra★⌟ r≡ ⟨ ⌜τ⌝ ⟩ ⟨ ⌜τ′⌝ ⟩ = cong ⟨_⟩ (⌞tra★⌟ r≡ ⌜τ⌝ ⌜τ′⌝)
```

```
⌞→★-S⌟ : ⦅ ⟪K∈★⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ⌝ ▷ ⌜κ⌝ ⦆ F.→★-S★ ≡⌞→★⌟ B.→★-S★ {L = L}
⌞tra∈★⌟ ⌞→★-S⌟ = λ α → refl
⌞relabel⌟ ⌞→★-S⌟ = refl
⌞tra∈ᴺ⌟ ⌞→★-S⌟ = λ η → refl
```

```
⌞weaken★⌟ : ∀ {τ : L F.⨟ _ ⊢ᵀ _} (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ) (⌜τ′⌝ : ⦅ ⌜Δ⌝ ▷ ⌜κ′⌝ , ⌜κ⌝ ⦆⟪ F.weaken★ τ ⟫ᵀ) → B.weaken★ ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
⌞weaken★⌟ = ⌞tra★⌟ ⌞→★-S⌟
```

```
⌞→★-Sᴺ⌟ : ⦅ ⟪K∈★⟫ ⦆⦅ ⌜Δ⌝ →★ ⌜Δ⌝ ▷ᴺ- ⦆ F.→★-S★ ≡⌞→★⌟ B.→★-Sᴺ {L = L}
⌞tra∈★⌟ ⌞→★-Sᴺ⌟ = λ α → refl
⌞relabel⌟ ⌞→★-Sᴺ⌟ = refl
⌞tra∈ᴺ⌟ ⌞→★-Sᴺ⌟ = λ η → refl
```

```
⌞weaken★ᴺ⌟ : ∀ {τ : L F.⨟ _ ⊢ᵀ _} (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ) (⌜τ′⌝ : ⦅ ⌜Δ⌝ ▷ᴺ- , ⌜κ⌝ ⦆⟪ F.weaken★ τ ⟫ᵀ) → B.weaken★ᴺ ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
⌞weaken★ᴺ⌟ = ⌞tra★⌟ ⌞→★-Sᴺ⌟
```

```
⟦weaken★⟧∙ : ∀ {τ : L F.⨟ _ ⊢ᵀ _} → {⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ} {⌜τ′⌝ : ⦅ ⌜Δ⌝ ▷ ⌜κ′⌝ , ⌜κ⌝ ⦆⟪ τ′ ⟫ᵀ} → F.weaken★ $ τ ≡ τ′ → B.weaken★ $ ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
⟦weaken★⟧∙ {⌜τ⌝ = ⌜τ⌝} {⌜τ′⌝} `refl = ` ⌞weaken★⌟ ⌜τ⌝ ⌜τ′⌝

⟦weaken★ᴺ⟧∙ : ∀ {τ : L F.⨟ _ ⊢ᵀ _} → {⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ} {⌜τ′⌝ : ⦅ ⌜Δ⌝ ▷ᴺ- , ⌜κ⌝ ⦆⟪ τ′ ⟫ᵀ} → F.weaken★ $ τ ≡ τ′ → weaken★ᴺ $ ⌞ ⌜τ⌝ ⌟ᵀ ≡ ⌞ ⌜τ′⌝ ⌟ᵀ
⟦weaken★ᴺ⟧∙ {⌜τ⌝ = ⌜τ⌝} {⌜τ′⌝} `refl = ` ⌞weaken★ᴺ⌟ ⌜τ⌝ ⌜τ′⌝
```

```
⟪K⊢★⟫ : ⟪Kit★⟫ F.K⊢★ B.K⊢★
⟪K⊢★⟫ = record {
  ⟪GK⟫ = it ;
  ⦅_⦆⦅_#★_⦆⌞_⌟ = λ _ ⌜κ⌝ τ → ⌞ ⦅ ⌜κ⌝ ⦆⌜ τ ⌝ᵀ ⌟ᵀ ;
  ⌞from★⌟ = λ α → refl ;
  ⌞to★⌟ = λ τ ⌜τ⌝ → ≡-⌞⌟ᵀ ⌜ τ ⌝ᵀ ⌜τ⌝ ;
  ⌞wkn★⌟ = λ τ → ⌞weaken★⌟ ⌜ τ ⌝ᵀ ⌜ F.weaken★ τ ⌝ᵀ ;
  ⌞wkn★ᴺ⌟ = λ τ → ⌞weaken★ᴺ⌟ ⌜ τ ⌝ᵀ ⌜ F.weaken★ τ ⌝ᵀ }
```

```
⌞-[_]★⌟ : (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ) → ⦅ ⟪K⊢★⟫ ⦆⦅ ⌜Δ⌝ ▷ ⌜κ⌝ →★ ⌜Δ⌝ ⦆ F.-[ τ ]★ ≡⌞→★⌟ B.-[ ⌞ ⌜τ⌝ ⌟ᵀ ]★
⌞tra∈★⌟ (⌞-[_]★⌟ {τ = τ} ⌜τ⌝) Z = ≡-⌞⌟ᵀ ⌜τ⌝ ⌜ τ ⌝ᵀ
⌞tra∈★⌟ (⌞-[_]★⌟ ⌜τ⌝) (S α) = refl
⌞relabel⌟ (⌞-[_]★⌟ ⌜τ⌝) = refl
⌞tra∈ᴺ⌟ (⌞-[_]★⌟ {⌜κ⌝ = ⌜κ⌝} ⌜τ⌝) Z = ⊥-elim (⌜κ⌝ refl)
⌞tra∈ᴺ⌟ (⌞-[_]★⌟ ⌜τ⌝) (S η) = refl
```

```
⌞-[_]★ᴺ⌟ : (η : L F.⨟ Δ ⊢ᵀ SCO) → ⦅ ⟪K⊢★⟫ ⦆⦅ ⌜Δ⌝ ▷ᴺ- →★ ⌜Δ⌝ ⦆ F.-[ η ]★ ≡⌞→★⌟ B.-[ ⌞ η ⌟ᶻ ]★ᴺ
⌞tra∈★⌟ (⌞-[_]★ᴺ⌟ η) {⌜κ⌝ = ⌜κ⌝} Z = ⊥-elim (⌜κ⌝ refl)
⌞tra∈★⌟ (⌞-[_]★ᴺ⌟ η) (S α) = refl
⌞relabel⌟ (⌞-[_]★ᴺ⌟ η) = refl
⌞tra∈ᴺ⌟ (⌞-[_]★ᴺ⌟ n) Z = refl
⌞tra∈ᴺ⌟ (⌞-[_]★ᴺ⌟ n) (S η′) = refl
```

```
⌞_[_]ᵀ⌟ : ∀ {σ : L F.⨟ Δ ▷ κ′ ⊢ᵀ κ} {τ} {σ[τ] : L F.⨟ Δ ⊢ᵀ κ}
  → (⌜σ⌝ : ⦅ ⌜Δ⌝ ▷ ⌜κ′⌝ , ⌜κ⌝ ⦆⟪ σ ⟫ᵀ)
  → (⌜τ⌝ : ⦅ _ , ⌜κ′⌝ ⦆⟪ τ ⟫ᵀ)
  → (⌜σ[τ]⌝ : ⦅ _ , ⌜κ⌝ ⦆⟪ σ[τ] ⟫ᵀ)
  → (σ[τ]≡ : σ F.[ τ ]★ ≡ σ[τ])
  → ⌞ ⌜σ⌝ ⌟ᵀ B.[ ⌞ ⌜τ⌝ ⌟ᵀ ]★ ≡ ⌞ ⌜σ[τ]⌝ ⌟ᵀ
⌞_[_]ᵀ⌟ ⌜σ⌝ ⌜τ⌝ ⌜σ[τ]⌝ refl = ⌞tra★⌟ ⌞-[ ⌜τ⌝ ]★⌟ ⌜σ⌝ ⌜σ[τ]⌝

⌞_[_]ᴺ⌟ : ∀ {σ : L F.⨟ Δ ▷ SCO ⊢ᵀ _} {σ[η]} (⌜σ⌝ : ⦅ ⌜Δ⌝ ▷ᴺ- , ⌜κ⌝ ⦆⟪ σ ⟫ᵀ) η (⌜σ[η]⌝ : ⦅ _ , ⌜κ⌝ ⦆⟪ σ[η] ⟫ᵀ) (σ[η]≡ : σ F.[ η ]★ ≡ σ[η]) → ⌞ ⌜σ⌝ ⌟ᵀ B.[ ⌞ η ⌟ᶻ ]★ᴺ ≡ ⌞ ⌜σ[η]⌝ ⌟ᵀ
⌞_[_]ᴺ⌟ ⌜σ⌝ η ⌜σ[η]⌝ refl = ⌞tra★⌟ ⌞-[ η ]★ᴺ⌟ ⌜σ⌝ ⌜σ[η]⌝
```

```
private variable
  A A′ B : L F.⨟ Δ ⊢ᵀ TYP
  C : L F.⨟ Δ ⊢ᵀ COM
  η η′ : L F.⨟ Δ ⊢ᵀ SCO
  ⌜A⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ
  ⌜A′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A′ ⟫ᵀ
  ⌜B⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ B ⟫ᵀ
  ⌜C⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ C ⟫ᵀ
  ⌜σ⌝ ⌜σ′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ′ ⟫ᵀ
  ⌜ε⌝ ⌜ε′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ
  x : A F.∈ Γ
```

```
data ⦅_⦂_∈ᵉ_⦆⟪_⟫ : (η : L F.⨟ Δ ⊢ᵀ SCO) → (⌜σ⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ ⟫ᵀ) → (⌜ε⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ) → η F.⦂ σ ∈ᵉ ε → Set where
  Z : ⦅ η ⦂ ⌜σ⌝ ∈ᵉ (η ⦂ ⌜σ⌝ ∷ ⌜ε⌝) ⦆⟪ Z ⟫
  S_ : {η∈ : η F.⦂ σ ∈ᵉ ε}
    → ⦅ η ⦂ ⌜σ⌝ ∈ᵉ ⌜ε⌝ ⦆⟪ η∈ ⟫
    → ⦅ η ⦂ ⌜σ⌝ ∈ᵉ (η′ ⦂ ⌜σ′⌝ ∷ ⌜ε⌝) ⦆⟪ (S η∈) ⟫
```

```
⌜_⌝∈ᵉ : (η∈ : η F.⦂ σ ∈ᵉ ε) → ⦅ η ⦂ ⌜ σ ⌝ᵀ {⌜Δ⌝ = ⌜Δ⌝} ∈ᵉ ⌜ ε ⌝ᵀ ⦆⟪ η∈ ⟫
⌜ Z ⌝∈ᵉ = Z
⌜ S η∈ ⌝∈ᵉ = S ⌜ η∈ ⌝∈ᵉ
```

```
⌞_⌟∈ᵉ : {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} {σ : L F.⨟ Δ ⊢ᵀ SIG} {⌜σ⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ ⟫ᵀ} {⌜ε⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ} {η∈ : η F.⦂ σ ∈ᵉ ε} → ⦅ η ⦂ ⌜σ⌝ ∈ᵉ ⌜ε⌝ ⦆⟪ η∈ ⟫ → ⌞ η ⌟ᶻ B.⦂ ⌞ ⌜σ⌝ ⌟ᵀ ∈ᵉ ⌞ ⌜ε⌝ ⌟ᵀ
⌞ Z ⌟∈ᵉ = Z
⌞ S η∈ ⌟∈ᵉ = S ⌞ η∈ ⌟∈ᵉ
```

```
data ⦅_⊑_⦆⟪_⟫ : (⌜ε⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ) → (⌜ε′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε′ ⟫ᵀ) → ε F.⊑ ε′ → Set where
  ι : ⦅ ι ⊑ ⌜ε⌝ ⦆⟪ ι ⟫
  _∷_ : ∀ {η} {η∈ : η F.⦂ σ ∈ᵉ ε} {ε′⊑ : ε′ F.⊑ ε}
    → ⦅ η ⦂ ⌜σ⌝ ∈ᵉ ⌜ε⌝ ⦆⟪ η∈ ⟫
    → ⦅ ⌜ε′⌝ ⊑ ⌜ε⌝ ⦆⟪ ε′⊑ ⟫
    → ⦅ (η ⦂ ⌜σ⌝ ∷ ⌜ε′⌝) ⊑ ⌜ε⌝ ⦆⟪ η∈ ∷ ε′⊑ ⟫
```

```
⌜_⌝⊑ : (ε⊑ : ε F.⊑ ε′) → ⦅ ⌜ ε ⌝ᵀ {⌜Δ⌝ = ⌜Δ⌝} ⊑ ⌜ ε′ ⌝ᵀ ⦆⟪ ε⊑ ⟫
⌜ ι ⌝⊑ = ι
⌜ η∈ ∷ ε⊑ ⌝⊑ = ⌜ η∈ ⌝∈ᵉ ∷ ⌜ ε⊑ ⌝⊑
```

```
⌞_⌟⊑ : {ε⊑ : ε F.⊑ ε′} → ⦅ ⌜ε⌝ ⊑ ⌜ε′⌝ ⦆⟪ ε⊑ ⟫ → ⌞ ⌜ε⌝ ⌟ᵀ B.⊑ ⌞ ⌜ε′⌝ ⌟ᵀ
⌞ ι ⌟⊑ = ι
⌞ η∈ ∷ ε⊑ ⌟⊑ = ⌞ η∈ ⌟∈ᵉ ∷ ⌞ ε⊑ ⌟⊑
```

```
data ⦅_∉_⦆⟪_⟫ : (ℓ : -∈ᴸ L) → (⌜ε⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ) → ℓ F.∉ ε → Set where
  ι : ⦅ ℓ ∉ ι {⌜Δ⌝ = ⌜Δ⌝} ⦆⟪ ι ⟫
  _∷_ : ∀ {ℓ∉ : ℓ F.∉ ε}
    → .(≢ℓ : ℓ′ ≢ ℓ)
    → ⦅ ℓ ∉ ⌜ε′⌝ ⦆⟪ ℓ∉ ⟫
    → ⦅ ℓ ∉ (dynamic ℓ′ ⦂ ⌜σ′⌝ ∷ ⌜ε′⌝) ⦆⟪ ≢ℓ ∷ ℓ∉ ⟫
```

```
⌜_⌝∉ : (ℓ∉ : ℓ F.∉ ε) → ∀ {⌜Δ⌝} → ⦅ ℓ ∉ ⌜ ε ⌝ᵀ {⌜Δ⌝} ⦆⟪ ℓ∉ ⟫
⌜ ι ⌝∉ = ι
⌜ ≢ℓ ∷ ℓ∉ ⌝∉ = ≢ℓ ∷ ⌜ ℓ∉ ⌝∉
```

```
⌞_⌟∉ : {ℓ∉ : ℓ F.∉ ε} → ⦅ ℓ ∉ ⌜ε⌝ ⦆⟪ ℓ∉ ⟫ → ℓ B.∉ ⌞ ⌜ε⌝ ⌟ᵀ
⌞ ι ⌟∉ = ι
⌞ ≢ℓ ∷ ℓ∉ ⌟∉ = ≢ℓ ∷ ⌞ ℓ∉ ⌟∉
```

Annotated FCN variables `⦅ ⌜A⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈`.

```
data ⦅_,_⦆⟪_⟫∈ : (⌜τ⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ τ ⟫ᵀ) (⌜Γ⌝ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ) → τ F.∈ Γ → Set where
  Z : {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} → ⦅ ⌜τ⌝ , ⌜Γ⌝ ▷ ⌜τ⌝ ⦆⟪ Z ⟫∈
  S_ : {σ : _} {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜σ⌝ : ⦅ _ , _ ⦆⟪ σ ⟫ᵀ} → ⦅ ⌜τ⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈ → ⦅ ⌜τ⌝ , ⌜Γ⌝ ▷ ⌜σ⌝ ⦆⟪ S x ⟫∈
  Sᴺ_ : {⌜τ⌝ : _} {⌜τ′⌝ : _} → ⦅ ⌜τ⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈ → (wkn≡ : F.weaken★ $ τ ≡ τ′) → ⦅ ⌜τ′⌝ , ⌜Γ⌝ ▷ᴺ- ⦆⟪ F.S★_ x wkn≡ ⟫∈
  S★_ : {⌜τ⌝ : _} {⌜τ′⌝ : _} → ⦅ ⌜τ⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈ → (wkn≡ : F.weaken★ $ τ ≡ τ′) → ⦅ ⌜τ′⌝ , ⌜Γ⌝ ▷★ ⌜κ⌝ ⦆⟪ F.S★_ x wkn≡ ⟫∈
```

Annotate FCN variables

```
⌜_⌝∈ : (x : A F.∈ Γ) → ⦅ ⌜ A ⌝ᵀ , ⌜ Γ ⌝ᴳ ⦆⟪ x ⟫∈
⌜ Z ⌝∈ = Z
⌜ S x ⌝∈ = S ⌜ x ⌝∈
⌜ S★_ {κ = κ} x wkn≡ ⌝∈ with ⌜ κ ⌝K
... | yes refl = Sᴺ_ ⌜ x ⌝∈ wkn≡
... | no ⌜κ⌝ = S★_ ⌜ x ⌝∈ wkn≡
```

Translate annotated FCN variables to BBD variables

```
⌞_⌟∈ : ∀ {x} → ⦅ ⌜A⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈ → ⌞ ⌜A⌝ ⌟ᵀ B.∈ ⌞ ⌜Γ⌝ ⌟ᴳ
⌞ Z ⌟∈ = Z
⌞ S x ⌟∈ = S ⌞ x ⌟∈
⌞ Sᴺ_ x wkn≡ ⌟∈ = B.Sᴺ_ ⌞ x ⌟∈ (⟦weaken★ᴺ⟧∙ wkn≡)
⌞ S★_ x wkn≡ ⌟∈ = B.S★_ ⌞ x ⌟∈ (⟦weaken★⟧∙ wkn≡)
```

### Translation of terms

```
data ⟪_⊢_∣_⟫ {Γ : F.Context L Δ} {⌜Δ⌝ : ⟪ Δ ⟫ᴷ} (⌜Γ⌝ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ) : {A : _} (⌜A⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ A ⟫ᵀ) → Γ F.⊢ A → Set where
```

Annotated values

```
  Var :
      ⦅ ⌜A⌝ , ⌜Γ⌝ ⦆⟪ x ⟫∈
      ----------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ∣ Var x ⟫
  ƛ_ : ∀ {⌜A⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ A ⟫ᵀ} {⌜C⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ C ⟫ᵀ} {M}
    → ⟪ ⌜Γ⌝ ▷ ⌜A⌝ ⊢ ⌜C⌝ ∣ M ⟫
      --------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ⇒ ⌜C⌝ ∣ ƛ M ⟫
  Λ_ : ∀ {V}
    → ⟪ ⌜Γ⌝ ▷★ ⌜κ⌝ ⊢ ⌜A⌝ ∣ V ⟫
      --------------------------------
    → ⟪ ⌜Γ⌝ ⊢ Forallᵀ ⌜κ⌝ ⌜A⌝ ∣ Λ V ⟫
  ƛᴺ_ : ∀ {V}
    → ⟪ ⌜Γ⌝ ▷ᴺ- ⊢ ⌜A⌝ ∣ V ⟫
      -----------------------------
    → ⟪ ⌜Γ⌝ ⊢ Forallᴺ ⌜A⌝ ∣ Λ V ⟫
  tt : ⟪ ⌜Γ⌝ ⊢ Unit ∣ tt ⟫

  name : (ℓ : _) → ⟪ ⌜Γ⌝ ⊢ Name (dynamic ℓ) ∣ name ℓ ⟫
```

Annotated computations

```
  return : ∀ {V}
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ∣ V ⟫
      ------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / ⌜ε⌝ ∣ return V ⟫
  _>>=_ : ∀ {M} {N}
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / ⌜ε⌝ ∣ M ⟫
    → ⟪ ⌜Γ⌝ ▷ ⌜A⌝ ⊢ ⌜B⌝ / ⌜ε⌝ ∣ N ⟫
      -----------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜B⌝ / ⌜ε⌝ ∣ M >>= N ⟫
  appᵛ : ∀ {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {N M}
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ⇒ ⌜τ⌝ ∣ N ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ∣ M ⟫
      -----------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜τ⌝ ∣ appᵛ N M ⟫
  appᵀ : ∀ {⌜σ⌝ : ⦅ ⌜Δ⌝ ▷ ⌜κ⌝ , (λ()) ⦆⟪ σ ⟫ᵀ} {M} {τ}
    → ⟪ ⌜Γ⌝ ⊢ Forallᵀ ⌜κ⌝ ⌜σ⌝ ∣ M ⟫
    → (⌜τ⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ)
    → {σ[τ] : _}
    → {⌜σ[τ]⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ[τ] ⟫ᵀ}
    → (σ[τ]≡ : σ F.[ τ ]★ ≡ σ[τ])
      ------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ[τ]⌝ / ι ∣ appᵀ M τ σ[τ]≡ ⟫
  appᴺ : ∀ {⌜σ⌝ : ⦅ ⌜Δ⌝ ▷ᴺ- , (λ()) ⦆⟪ σ ⟫ᵀ} {M}
    → ⟪ ⌜Γ⌝ ⊢ Forallᴺ ⌜σ⌝ ∣ M ⟫
    → (η : _ F.⨟ Δ ⊢ᵀ SCO)
    → {σ[η] : _}
    → {⌜σ[η]⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ[η] ⟫ᵀ}
    → (σ[η]≡ : σ F.[ η ]★ ≡ σ[η])
      -----------------------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ[η]⌝ / ι ∣ appᵀ M η σ[η]≡ ⟫
  perform : ∀ {η} {⌜σ⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ ⟫ᵀ} {V}
    → (n : Γ F.⊢ Name η)
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ⌝ ⇒ᵖ ⌜B⌝ ∣ V ⟫
      -----------------------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜B⌝ / (η ⦂ ⌜σ⌝ ∷ ⌜ε⌝) ∣ perform n V ⟫
  handle : ∀ {⌜τ′⌝ : ⦅ _ , _ ⦆⟪ τ′ ⟫ᵀ} {⌜σ′⌝ : ⦅ _ , _ ⦆⟪ σ′ ⟫ᵀ} {⌜ε′⌝  : ⦅ _ , _ ⦆⟪ ε′ ⟫ᵀ}
             {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜σ⌝ : ⦅ _ , _ ⦆⟪ σ ⟫ᵀ} {⌜ε⌝ : ⦅ _ , _ ⦆⟪ ε ⟫ᵀ}
             {M} {H}
    → ⟪ ⌜Γ⌝ ▷ᴺ- ▷ Name (Var Z) ⊢ ⌜τ′⌝ / (Var Z ⦂ ⌜σ′⌝ ∷ ⌜ε′⌝) ∣ M ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ⌝ ⇒ʰ (⌜τ⌝ / ⌜ε⌝) ∣ H ⟫
    → (wkn≡³ : _)
      --------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜τ⌝ / ⌜ε⌝ ∣ F.handle M H wkn≡³ ⟫
  handle′ : ∀ {⌜ε⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ} {M} {H}
    → {ℓ∉ : ℓ F.∉ ε}
    → (⌜ℓ∉⌝ : ⦅ ℓ ∉ ⌜ε⌝ ⦆⟪ ℓ∉ ⟫)
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / (dynamic ℓ ⦂ ⌜σ⌝ ∷ ⌜ε⌝) ∣ M ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ⌝ ⇒ʰ (⌜A⌝ / ⌜ε⌝) ∣ H ⟫
      --------------------------------
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / ⌜ε⌝ ∣ F.handle′ ℓ∉ M H ⟫
  subeffect : ∀ {ε⊑ε′} {M}
    → ⦅ ⌜ε⌝ ⊑ ⌜ε′⌝ ⦆⟪ ε⊑ε′ ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / ⌜ε⌝ ∣ M ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ / ⌜ε′⌝ ∣ subeffect ε⊑ε′ M ⟫
```

Annotated handlers.

```
  Λʰ_ : ∀ {⌜σ⌝ : ⦅ _ , _ ⦆⟪ σ ⟫ᵀ} {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜τ′⌝ : ⦅ _ , _ ⦆⟪ τ′ ⟫ᵀ} {H}
    → ⟪ ⌜Γ⌝ ▷★ ⌜κ⌝ ⊢ ⌜σ⌝ ⇒ʰ ⌜τ′⌝ ∣ H ⟫
    → (wkn≡ : F.weaken★ $ τ ≡ τ′)
      -----------------------------------------------
    → ⟪ ⌜Γ⌝ ⊢ Forallˢ ⌜κ⌝ ⌜σ⌝ ⇒ʰ ⌜τ⌝ ∣ Λʰ_ H wkn≡ ⟫

  ƛʰᴺ_ : ∀ {⌜σ⌝ : ⦅ _ , _ ⦆⟪ σ ⟫ᵀ} {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜τ′⌝ : ⦅ _ , _ ⦆⟪ τ′ ⟫ᵀ} {H}
    → ⟪ ⌜Γ⌝ ▷ᴺ- ⊢ ⌜σ⌝ ⇒ʰ ⌜τ′⌝ ∣ H ⟫
    → (wkn≡ : F.weaken★ $ τ ≡ τ′)
      -----------------------------------------------
    → ⟪ ⌜Γ⌝ ⊢ Forallˢᴺ ⌜σ⌝ ⇒ʰ ⌜τ⌝ ∣ Λʰ_ H wkn≡ ⟫

  ƛʰ_ : ∀ {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜τ₁⌝ : ⦅ _ , _ ⦆⟪ τ₁ ⟫ᵀ} {⌜τ₂⌝ : ⦅ _ , _ ⦆⟪ τ₂ ⟫ᵀ} {M}
    → ⟪ ⌜Γ⌝ ▷ ⌜τ₁⌝ ⊢ ⌜τ₂⌝ ⇒ʰ ⌜τ⌝ ∣ M ⟫
      -----------------------------------------------
    → ⟪ ⌜Γ⌝ ⊢ (⌜τ₁⌝ ⇒ˢ ⌜τ₂⌝) ⇒ʰ ⌜τ⌝ ∣ ƛʰ M ⟫

  Handler : ∀ {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜τ₁⌝ : ⦅ _ , _ ⦆⟪ τ₁ ⟫ᵀ} {M}
    → ⟪ ⌜Γ⌝ ▷ (⌜τ₁⌝ ⇒ ⌜τ⌝) ⊢ ⌜τ⌝ ∣ M ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⟨ ⌜τ₁⌝ ⟩ ⇒ʰ ⌜τ⌝ ∣ Handler M ⟫
```

Annotated operation calls.

```
  ⦅⦆ : ∀ {⌜τ⌝ : ⦅ _ , (λ()) ⦆⟪ τ ⟫ᵀ}
    → ⟪ ⌜Γ⌝ ⊢ ⟨ ⌜τ⌝ ⟩ ⇒ᵖ ⌜τ⌝ ∣ ⦅⦆ ⟫
  _,_ : ∀ {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜τ₁⌝ : ⦅ _ , _ ⦆⟪ τ₁ ⟫ᵀ} {⌜τ₂⌝ : ⦅ _ , _ ⦆⟪ τ₂ ⟫ᵀ} {V W}
    → ⟪ ⌜Γ⌝ ⊢ ⌜τ₁⌝ ∣ V ⟫
    → ⟪ ⌜Γ⌝ ⊢ ⌜τ₂⌝ ⇒ᵖ ⌜τ⌝ ∣ W ⟫
    → ⟪ ⌜Γ⌝ ⊢ (⌜τ₁⌝ ⇒ˢ ⌜τ₂⌝) ⇒ᵖ ⌜τ⌝ ∣ V , W ⟫

  _,ᵀ_⦅_⦆ : ∀ {τ₁} {σ[τ₁]} {σ : _ F.⨟ _ ⊢ᵀ SIG} {⌜τ⌝ : ⦅ _ , _ ⦆⟪ τ ⟫ᵀ} {⌜σ⌝ : ⦅ _ , _ ⦆⟪ σ ⟫ᵀ} {⌜σ[τ₁]⌝ : ⦅ _ , ⌜SIG⌝ ⦆⟪ σ[τ₁] ⟫ᵀ } {W}
    → (⌜τ₁⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ τ₁ ⟫ᵀ)
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ[τ₁]⌝ ⇒ᵖ ⌜τ⌝ ∣ W ⟫
    → (σ[τ₁]≡ : σ F.[ τ₁ ]★ ≡ σ[τ₁])
    → ⟪ ⌜Γ⌝ ⊢ Forallˢ ⌜κ⌝ ⌜σ⌝ ⇒ᵖ ⌜τ⌝ ∣ τ₁ ,ᵀ W ⦅ σ[τ₁]≡ ⦆ ⟫

  _,ᴺ_⦅_⦆ : ∀ {σ[η]} {σ : L F.⨟ (Δ ▷ SCO) ⊢ᵀ SIG} {τ : _ F.⨟ _ ⊢ᵀ TYP} {⌜τ⌝ : ⦅ _ , (λ()) ⦆⟪ τ ⟫ᵀ} {⌜σ⌝ : ⦅ ⌜Δ⌝ ▷ᴺ- , _ ⦆⟪ σ ⟫ᵀ}
    → (η : _ F.⨟ _ ⊢ᵀ SCO)
    → ∀ {⌜σ[η]⌝ : ⦅ ⌜Δ⌝ , ⌜SIG⌝ ⦆⟪ σ[η] ⟫ᵀ} {W}
    → ⟪ ⌜Γ⌝ ⊢ ⌜σ[η]⌝ ⇒ᵖ ⌜τ⌝ ∣ W ⟫
    → (σ[η]≡ : σ F.[ η ]★ ≡ σ[η])
    → ⟪ ⌜Γ⌝ ⊢ Forallˢᴺ ⌜σ⌝ ⇒ᵖ ⌜τ⌝ ∣ η ,ᵀ W ⦅ σ[η]≡ ⦆ ⟫
```

Annotate FCN terms.

```
⌜_⌝  : ∀ (M : Γ F.⊢ τ) → ⟪ ⌜ Γ ⌝ᴳ ⊢ ⦅ ⌜κ⌝ ⦆⌜ τ ⌝ᵀ ∣ M ⟫

⦅_⦆⌜_⌝ : ∀ ⌜κ⌝ (M : Γ F.⊢ τ) → ⟪ ⌜ Γ ⌝ᴳ ⊢ ⦅ ⌜κ⌝ ⦆⌜ τ ⌝ᵀ ∣ M ⟫
⦅_⦆⌜_⌝ ⌜κ⌝ = ⌜_⌝

⌜ Var x ⌝ = Var ⌜ x ⌝∈
⌜ ƛ M ⌝ = ƛ ⌜ M ⌝
⌜ Λ_ {κ = κ} V ⌝ with ⌜ κ ⌝K | ⦅ (λ()) ⦆⌜ V ⌝
... | yes refl | ⌜V⌝ = ƛᴺ ⌜V⌝
... | no ⌜κ⌝   | ⌜V⌝ = Λ_ {⌜κ⌝ = ⌜κ⌝} ⌜V⌝
⌜ tt ⌝ = tt

⌜ return V ⌝ = return ⌜ V ⌝
⌜ M >>= N ⌝ = ⌜ M ⌝ >>= ⌜ N ⌝
⌜ appᵛ N M ⌝ = appᵛ ⌜ N ⌝ ⌜ M ⌝
⌜ appᵀ {κ = κ} M τ σ[τ]≡ ⌝ with ⌜ κ ⌝K | ⦅ (λ()) ⦆⌜ M ⌝
... | yes refl | ⌜M⌝ = appᴺ ⌜M⌝ τ σ[τ]≡
... | no ⌜κ⌝   | ⌜M⌝ = appᵀ ⌜M⌝ ⌜ τ ⌝ᵀ σ[τ]≡
⌜ perform N V ⌝ = perform N ⌜ V ⌝
⌜ handle M H wkn≡³ ⌝ =
  handle ⌜ M ⌝ ⌜ H ⌝ wkn≡³
⌜ handle′ ℓ∉ M H ⌝ = handle′ ⌜ ℓ∉ ⌝∉ ⌜ M ⌝ ⌜ H ⌝

⌜ Λʰ_ {κ = κ} H wkn≡ ⌝ with ⌜ κ ⌝K | ⦅ (λ()) ⦆⌜ H ⌝
... | yes refl | ⌜H⌝ = ƛʰᴺ_ ⌜H⌝ wkn≡
... | no ⌜κ⌝   | ⌜H⌝ = Λʰ_ ⌜H⌝ wkn≡
⌜ ƛʰ M ⌝ = ƛʰ ⌜ M ⌝

⌜ name ℓ ⌝ = name ℓ
⌜ ⦅⦆ ⌝ = ⦅⦆
⌜ V , W ⌝ = ⌜ V ⌝ , ⌜ W ⌝
⌜ _,ᵀ_⦅_⦆ {κ = κ} {σ = σ} {σ[τ] = σ[τ]} τ V σ[τ]≡ ⌝ with ⌜ κ ⌝K | ⦅ (λ()) ⦆⌜ V ⌝
... | yes refl | ⌜V⌝ = τ ,ᴺ ⌜V⌝ ⦅ σ[τ]≡ ⦆
... | no ⌜κ⌝ | ⌜V⌝ = ⌜ τ ⌝ᵀ ,ᵀ ⌜V⌝ ⦅ σ[τ]≡ ⦆
⌜ Handler M ⌝ = Handler ⌜ M ⌝
⌜ subeffect ε⊑ M ⌝ = subeffect ⌜ ε⊑ ⌝⊑ ⌜ M ⌝
```

Translate FCN terms to BBD terms.

```
⌞_⌟ : ∀ {A : L F.⨟ Δ ⊢ᵀ κ} {⌜A⌝ : ⦅ ⌜Δ⌝ , ⌜κ⌝ ⦆⟪ A ⟫ᵀ} {M : Γ F.⊢ A} → ⟪ ⌜Γ⌝ ⊢ ⌜A⌝ ∣ M ⟫ → ⌞ ⌜Γ⌝ ⌟ᴳ B.⊢ ⌞ ⌜A⌝ ⌟ᵀ

⌞ Var x ⌟ = Var ⌞ x ⌟∈
⌞ ƛ M ⌟ = ƛ ⌞ M ⌟
⌞ Λ V ⌟ = Λ ⌞ V ⌟
⌞ ƛᴺ V ⌟ = ƛᴺ ⌞ V ⌟
⌞ tt ⌟ = tt

⌞ return V ⌟ = return ⌞ V ⌟
⌞ M >>= N ⌟ = ⌞ M ⌟ >>= ⌞ N ⌟
⌞ appᵛ N M ⌟ = appᵛ ⌞ N ⌟ ⌞ M ⌟
⌞ appᵀ {⌜σ⌝ = ⌜σ⌝} M τ σ[τ]≡ ⌟ = appᵀ ⌞ M ⌟ ⌞ τ ⌟ᵀ (⌞ ⌜σ⌝ [ _ ]ᵀ⌟ _ σ[τ]≡)
⌞ appᴺ {⌜σ⌝ = ⌜σ⌝} M η σ[η]≡ ⌟ = appᴺ ⌞ M ⌟ ⌞ η ⌟ᶻ (⌞ ⌜σ⌝ [ _ ]ᴺ⌟ _ σ[η]≡)
⌞ perform η V ⌟ = perform _ ⌞ V ⌟
⌞ handle M H (τ≡ , σ≡ , ε≡) ⌟ = handle (appᵛ (ƛ ⌞ M ⌟) tt) ⌞ H ⌟ (⟦weaken★ᴺ⟧∙ τ≡ , ⟦weaken★ᴺ⟧∙ σ≡ , ⟦weaken★ᴺ⟧∙ ε≡)
⌞ Λʰ_ H wkn≡ ⌟ = Λʰ_ ⌞ H ⌟ (⟦weaken★⟧∙ wkn≡)
⌞ ƛʰᴺ_ H wkn≡ ⌟ = ƛʰᴺ_  ⌞ H ⌟ (⟦weaken★ᴺ⟧∙ wkn≡)
⌞ Handler M ⌟ = Handler ⌞ M ⌟
⌞ name _ ⌟ = tt
⌞ ⦅⦆ ⌟ = ⦅⦆
⌞ _,ᵀ_⦅_⦆ {⌜σ⌝ = ⌜σ⌝} τ M σ[τ]≡ ⌟ = ⌞ τ ⌟ᵀ ,ᵀ ⌞ M ⌟ ⦅ ⌞ ⌜σ⌝ [ _ ]ᵀ⌟ _ σ[τ]≡ ⦆
⌞ _,ᴺ_⦅_⦆ {⌜σ⌝ = ⌜σ⌝} η M σ[η]≡ ⌟ = ⌞ η ⌟ᶻ ,ᴺ ⌞ M ⌟ ⦅ ⌞ ⌜σ⌝ [ _ ]ᴺ⌟ _ σ[η]≡ ⦆
⌞ V , W ⌟ = ⌞ V ⌟ , ⌞ W ⌟
⌞ handle′ ℓ∉ M H ⌟ = handle′ ⌞ ℓ∉ ⌟∉ ⌞ M ⌟ ⌞ H ⌟
⌞ ƛʰ M ⌟ = ƛʰ ⌞ M ⌟
⌞ subeffect ε⊑ M ⌟ = subeffect ⌞ ε⊑ ⌟⊑ ⌞ M ⌟
```

```
module WIP where

open import Function.Base using (_∘_; it)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Empty using (⊥)
open import Data.Product using (Σ-syntax; _×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; trans; cong; cong₂; subst)
open import Relation.Nullary using (Dec; yes; no)
```

```
open import Common using (_$_≡_; `_; `refl)
open import Labels
open import BindersByDay as B
open import BBDSubstitution as B
open import BBDSemantics as B
open import FirstClassNames as F
open import FCNSubstitution as F
open import FCNSemantics as F
open import FCN2BBD
open import FCN2BBDSubstitution
open import FCN2BBDSemantics
```

```
private variable
  L L′ : Labels
  ℓ ℓ′ : -∈ᴸ L
  κ κ′ : F.Kind
  Δ Δ′ : F.Contextᴷ
  τ τ′ τ₁ τ₂ τ₃ σ τε : L F.⨟ Δ ⊢ᵀ κ
  σ′ : L F.⨟ Δ ⊢ᵀ SIG
  ε ε′ ε₁ ε₁′ : L F.⨟ Δ ⊢ᵀ EFF
  ⌜κ⌝ ⌜κ₂⌝ : ⟪ κ ⟫K
  ⌜κ′⌝ : ⟪ κ′ ⟫K
  ⌜Δ⌝ ⌜Δ′⌝ : ⟪ Δ ⟫ᴷ
  Γ Γ′ : F.Context L Δ
  ⌜Γ⌝ ⌜Γ′⌝ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ
  η η′ : L F.⨟ Δ ⊢ᵀ SCO
  ⌜σ⌝ ⌜σ′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ′ ⟫ᵀ
  ⌜ε⌝ ⌜ε′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ
  x : τ F.∈ Γ
  M N V : Γ F.⊢ τ
```

```
private variable
  _#★_ : Labels × F.Contextᴷ → F.Kind → Set
  _#★′_ : Labels × B.Contextᴷ → B.Kind → Set
  _#_ : ∀ {L Δ} → F.Context L Δ → L F.⨟ Δ ⊢ᵀ TYP → Set
  _#′_ : ∀ {L Δ} → B.Context L Δ → L B.⨟ Δ ⊢ᵀ TYP → Set

open F.Kit★
open B.Kit★
open F.⦅_⦆_→★_
open B.⦅_⦆_→★_
open ⦅_⦆⦅_→★_⦆_≡⌞→★⌟_
open ⦅_⦆⦅_↠_⦆_≡⌞↠⌟_
```

```
module BBD2FCNSimulation where

open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; cong; cong₂; subst)
```

```
open import Common using (_$_≡_; `_; `refl)
open import Labels
open import BindersByDay as B
open import BBDSubstitution as B
open import BBDSemantics as B
open import FirstClassNames as F
open import FCNSubstitution as F
open import FCNSemantics as F
open import BBD2FCN
open import BBD2FCNSubstitution
```

```
private variable
  L L′ : Labels
  rᴸ : L →ᴸ L′
  ℓ ℓ′ : -∈ᴸ L
  κ κ′ : B.Kind
  Δ : B.Contextᴷ
  Γ Γ′ : B.Context L Δ
  n : B.-∈ᴺ L ⨟ Δ
  τ τ₁ τ₂ τ₃ τ′ τ₁′ τ₂′ σ σ′ : L B.⨟ Δ ⊢ᵀ κ
  ε ε′ ε₁′ ε₂′ : L B.⨟ Δ ⊢ᵀ EFF
  τ/ε τ/ε′ τ/ε₁ τ/ε₂ τ/ε₁′ τ/ε₂′ : L B.⨟ Δ ⊢ᵀ COM
  M : ∅ B.⊢ τ
  M′ : ∅ B.⊢ τ′
  E : B.□: τ₁ ⊢ᴱ τ₂
  E′ : B.□: τ₁′ ⊢ᴱ τ₂′
```

```
⟦_⟧ᴱ : B.□: τ/ε₁ ⊢ᴱ τ/ε₂ → F.□: ⟦ τ/ε₁ ⟧ᵀ ⊢ᴱ ⟦ τ/ε₂ ⟧ᵀ
⟦ □ ⟧ᴱ = □
⟦ [ E ]>>= N ⟧ᴱ = [ ⟦ E ⟧ᴱ ]>>= ⟦ N ⟧
⟦ handle′ ℓ∉E [ E ] H ⟧ᴱ = handle′ ⟦ ℓ∉E ⟧∉ [ ⟦ E ⟧ᴱ ] ⟦ H ⟧
⟦ subeffect′ ε⊑ε′ [ E ] ⟧ᴱ = subeffect′ ⟦ ε⊑ε′ ⟧⊑ [ ⟦ E ⟧ᴱ ]
```

```
⟦⟧∘renᴱᴸ : {E : B.□: τ/ε₁ ⊢ᴱ τ/ε₂} → ⟦ B.renᴱᴸ rᴸ E ⟧ᴱ F.≡ᴱ F.renᴱᴸ rᴸ ⟦ E ⟧ᴱ
⟦⟧∘renᴱᴸ {τ/ε₁ = τ/ε₁} {E = □} = F.□≡□ (⟦ren★L⟧ {τ = τ/ε₁})
⟦⟧∘renᴱᴸ {E = [ E ]>>= N} = F.⦅[ ⟦⟧∘renᴱᴸ ]>>= ⟦⟧∘renᴸ {M = N} ⦆
⟦⟧∘renᴱᴸ {E = handle′ ℓ∉ε [ E ] H} = F.⦅handle′ refl [ ⟦⟧∘renᴱᴸ ] ⟦⟧∘renᴸ {M = H} ⦆
⟦⟧∘renᴱᴸ {E = subeffect′ ε⊑ [ E ]} = F.⦅subeffect′ ⟦⟧∘ren⊑ᴸ [ ⟦⟧∘renᴱᴸ ]⦆

⟦⟧∘renᴱᴸ∙ :
    B.renᴱᴸ rᴸ E B.≡ᴱ E′
  → F.renᴱᴸ rᴸ ⟦ E ⟧ᴱ F.≡ᴱ ⟦ E′ ⟧ᴱ
⟦⟧∘renᴱᴸ∙ refl = F.≡ᴱ-sym ⟦⟧∘renᴱᴸ
```

```
≈ᴱ-⟦⟧ : E B.≈ᴱ E′ → ⟦ E ⟧ᴱ F.≈ᴱ ⟦ E′ ⟧ᴱ
≈ᴱ-⟦⟧ (rᴸ , e) = rᴸ , ⟦⟧∘renᴱᴸ∙ e
```

```
⟦_[_/□]⟧≡ : (E : B.□: τ/ε₁ ⊢ᴱ τ/ε₂) (M : ∅ B.⊢ _)
  → ⟦ E B.[ M /□] ⟧ F.≡ᴹ ⟦ E ⟧ᴱ F.[ ⟦ M ⟧ /□]
⟦ □ [ M /□]⟧≡ = refl
⟦ [ E ]>>= N [ M /□]⟧≡ = F.⦅ ⟦ E [ M /□]⟧≡ >>= refl ⦆
⟦ handle′ ℓ∉ε [ E ] H [ M /□]⟧≡ = F.≡ᴹ-handle′ refl ⟦ E [ M /□]⟧≡ refl
⟦ subeffect′ ε⊑ [ E ] [ M /□]⟧≡ = F.≡ᴹ-subeffect refl ⟦ E [ M /□]⟧≡
```

```
⟦_[_/□]₁⟧≡ : (E : B.□: τ/ε₁ ⊢ᴱ τ/ε₂) (M : Γ B.⊢ _)
  → ⟦ E B.[ M /□]₁ ⟧ F.≡ᴹ ⟦ E ⟧ᴱ F.[ ⟦ M ⟧ /□]₁
⟦ □ [ M /□]₁⟧≡ = refl
⟦ [ E ]>>= N [ M /□]₁⟧≡ = F.⦅ ⟦ E [ M /□]₁⟧≡ >>= ⟦weaken₁⟧ N ⦆
⟦ handle′ ℓ∉ε [ E ] H [ M /□]₁⟧≡ = F.≡ᴹ-handle′ refl ⟦ E [ M /□]₁⟧≡ (⟦weaken₀⟧ H)
⟦ subeffect′ ε⊑ [ E ] [ M /□]₁⟧≡ = F.≡ᴹ-subeffect refl ⟦ E [ M /□]₁⟧≡
```

```
⟦_⟧∉ᴱ : ℓ B.∉ᴱ E → ℓ F.∉ᴱ ⟦ E ⟧ᴱ
⟦ □ ⟧∉ᴱ = □
⟦ [ ℓ∉E ]>>= N ⟧∉ᴱ = [ ⟦ ℓ∉E ⟧∉ᴱ ]>>= ⟦ N ⟧
⟦ handle′ ℓ≢ℓ′ [ ℓ∉E ] H ⟧∉ᴱ = handle′ ℓ≢ℓ′ [ ⟦ ℓ∉E ⟧∉ᴱ ] ⟦ H ⟧
⟦ subeffect′ ε⊑ε′ [ ℓ∉E ] ⟧∉ᴱ = subeffect′ ⟦ ε⊑ε′ ⟧⊑ [ ⟦ ℓ∉E ⟧∉ᴱ ]
```

```
⟦resume⟧ : ∀ ℓ∉ε (E : B.□: τ / (dynamic ℓ ⦂ σ ∷ ε) ⊢ᴱ τ′ / (dynamic ℓ ⦂ σ ∷ ε′)) H
  → ⟦ B.resume ℓ∉ε E H ⟧ ≡ F.resume ⟦ ℓ∉ε ⟧∉ ⟦ E ⟧ᴱ ⟦ H ⟧
⟦resume⟧ ℓ∉ε E H = F.≡ᴹ⇒≡ (
  ƛ (handle′ ⟦ ℓ∉ε ⟧∉ ⟦ E B.[ return (Var Z) /□]₁ ⟧ ⟦ B.weaken H ⟧)      ≡⟨ F.≡ᴹ-ƛ (F.≡ᴹ-handle′ refl ⟦ E [ return (Var Z) /□]₁⟧≡ (⟦weaken⟧ H)) ⟩
  ƛ (handle′ ⟦ ℓ∉ε ⟧∉ (⟦ E ⟧ᴱ F.[ return (Var Z) /□]₁) (F.weaken ⟦ H ⟧)) ∎)
  where open F.≡ᴹ-Reasoning
```

```
lemma-subst-ƛʰ : ∀ {σ σ′ τ₁ τ₂ τ₂′}
  → (H : ∅ {L = L} ▷ τ₁ F.⊢ σ ⇒ʰ τ₂)
  → (σ≡ : σ ≡ σ′)
  → (τ₂≡ : τ₂ ≡ τ₂′)
  → subst (∅ F.⊢_) (cong₂ _⇒ʰ_ (cong₂ _⇒ˢ_ refl σ≡) τ₂≡) (ƛʰ H) ≡ ƛʰ subst (∅ ▷ τ₁ F.⊢_) (cong₂ _⇒ʰ_ σ≡ τ₂≡) H
lemma-subst-ƛʰ H refl refl = refl
```

```
⟦_[_∣_]ʰ⟧≡ : ∀ (H : ∅ B.⊢ σ ⇒ʰ τ/ε) (V : ∅ B.⊢ σ ⇒ᵖ τ₂) (K : ∅ B.⊢ τ₂ ⇒ τ/ε)
  → ⟦ H B.[ V ∣ K ]ʰ ⟧ F.≡ᴹ ⟦ H ⟧ F.[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ
⟦ (Λʰ H) w≡ [ _,ᵀ_⦅_⦆ {σ = σ} τ V σ[τ]≡ ∣ K ]ʰ⟧≡ =
  ⟦ subst (∅ B.⊢_) eq (H B.[ τ ]ᵀ) B.[ V ∣ K ]ʰ ⟧          ≡⟨ ⟦ subst (∅ B.⊢_) eq (H B.[ τ ]ᵀ) [ V ∣ K ]ʰ⟧≡ ⟩
  ⟦ subst (∅ B.⊢_) eq (H B.[ τ ]ᵀ) ⟧ F.[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ  ≡⟨
    F.≡ᴹ-refl (cong F._[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ (F.≡ᴹ⇒≡ (
      ⟦ subst (∅ B.⊢_) eq (H B.[ τ ]ᵀ) ⟧   ≡⟨ ⟦ B.≡ᴹ-subst eq (H B.[ τ ]ᵀ) ⟧≡ ⟩
      ⟦ H B.[ τ ]ᵀ ⟧                       ≡⟨ ⟦ H [ τ ]ᵀ⟧≡ ⟩
      ⟦ H ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ                  ≡⟨ F.≡ᴹ-sym (F.≡ᴹ-subst eq′ (⟦ H ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ)) ⟩
      subst (∅ F.⊢_) eq′ (⟦ H ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ) ∎))) ⟩
  subst (∅ F.⊢_) eq′ (⟦ H ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ) F.[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ  ∎
  where
    eq = cong₂ _⇒ʰ_ σ[τ]≡ (B.[]★∘weaken★∙ w≡)
    eq′ = cong₂ _⇒ʰ_ (⟦ σ [ τ ]★ᵀ⟧∙ σ[τ]≡) (F.[]★∘weaken★∙ (⟦weaken★⟧∙ w≡))
    open F.≡ᴹ-Reasoning
⟦ (ƛʰᴺ H) w≡ [ _,ᴺ_⦅_⦆ {σ = σ} n V σ[n]≡ ∣ K ]ʰ⟧≡ =
  ⟦ subst (∅ B.⊢_) eq (H B.[ n ]ᴺ) B.[ V ∣ K ]ʰ ⟧          ≡⟨ ⟦ subst (∅ B.⊢_) eq (H B.[ n ]ᴺ) [ V ∣ K ]ʰ⟧≡ ⟩
  ⟦ subst (∅ B.⊢_) eq (H B.[ n ]ᴺ) ⟧ F.[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ  ≡⟨
    F.≡ᴹ-refl (cong F._[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ (F.≡ᴹ⇒≡ (
      ⟦ subst (∅ B.⊢_) eq (H B.[ n ]ᴺ) ⟧   ≡⟨ ⟦ B.≡ᴹ-subst eq (H B.[ n ]ᴺ) ⟧≡ ⟩
      ⟦ H B.[ n ]ᴺ ⟧                       ≡⟨ ⟦ H [ n ]ᴺ⟧ ⟩
      ⟦H⟧[n]ᶻ F.[ ⟦ n ⟧N ]                 ≡⟨ F.≡ᴹ-[] (F.≡ᴹ-sym (F.≡ᴹ-subst eq₄ ⟦H⟧[n]ᶻ)) refl ⟩
      ⟦H⟧[n]ᶻ[n] ∎))) ⟩
  ⟦H⟧[n]ᶻ[n] F.[ ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ
    ≡⟨ F.≡ᴹ-refl (cong F._[ ⟦ n ⟧N , ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ (sym (lemma-subst-ƛʰ (F.tra (F.tra▷ F.-[ ⟦ n ⟧∈ᶻ ]ᵀ) ⟦ H ⟧) σ≡ τ≡))) ⟩
  subst (∅ F.⊢_) eq′ ((ƛʰ ⟦ H ⟧) F.[ ⟦ n ⟧∈ᶻ ]ᵀ) F.[ ⟦ n ⟧N , ⟦ V ⟧ ∣ ⟦ K ⟧ ]ʰ  ∎
  where
    eq = cong₂ _⇒ʰ_ σ[n]≡ (B.[]★ᴺ∘weaken★ᴺ∙ w≡)
    σ≡ = (⟦ σ [ n ]★ᴺ⟧∙ σ[n]≡)
    τ≡ = (F.[]★∘weaken★∙ (⟦weaken★ᴺ⟧∙ w≡))
    eq″ = cong₂ _⇒ˢ_ refl σ≡
    eq′ = cong₂ _⇒ʰ_ eq″ τ≡
    eq₄ = cong₂ _⇒ʰ_ σ≡ τ≡
    ⟦H⟧[n]ᶻ = F.tra (F.tra▷ F.-[ ⟦ n ⟧∈ᶻ ]ᵀ) ⟦ H ⟧
    ⟦H⟧[n]ᶻ[n] = (subst (∅ ▷ Name ⟦ n ⟧∈ᶻ F.⊢_) eq₄ ⟦H⟧[n]ᶻ) F.[ ⟦ n ⟧N ]
    open F.≡ᴹ-Reasoning
⟦ ƛʰ H [ V , W ∣ K ]ʰ⟧≡ = F.≡ᴹ-trans (⟦ (H B.[ V ]) [ W ∣ K ]ʰ⟧≡) (F.≡ᴹ-refl (cong (F._[ ⟦ W ⟧ ∣ ⟦ K ⟧ ]ʰ) (F.≡ᴹ⇒≡ ⟦ H [ V ]⟧≡)))
⟦ Handler H [ ⦅⦆ ∣ K ]ʰ⟧≡ = ⟦ H [ K ]⟧≡
```

```
⟦fresh⟧ : ⟦ B.fresh {ε = ε} ⟧∉ F.≡[∉] F.fresh {ε = ⟦ ε ⟧ᵀ}
⟦fresh⟧ {ε = ι} = refl
⟦fresh⟧ {ε = dynamic ℓ ⦂ σ ∷ ε} = F.≡[∉]-∷ refl (⟦tra★⟧ (≈-→★-ᴸ →ᴸ-S) σ) ⟦fresh⟧
```

```
open _≈[↠]_

⟦↠-new⟧ : B.lift B.↠-new ≈[↠] F.↠-new {L = L}
_≈[→★]_.≈-→ᴰ (≈[↠]⇒≈[↠★] ⟦↠-new⟧) (Sᴺ ())
_≈[→★]_.≈-→ᴺ (≈[↠]⇒≈[↠★] ⟦↠-new⟧) Z = refl
≈-tra∈ ⟦↠-new⟧ (Sᴺ_ () _)
```

```
⟦new⟧ :
    {M : ∅ ▷ᴺ- B.⊢ τ′ / static Z ⦂ σ′ ∷ ε′}
    {wkn≡³ : B.weaken★ᴺ {L = L} $ τ ≡ τ′ × B.weaken★ᴺ $ σ ≡ σ′ × B.weaken★ᴺ $ ε ≡ ε′}
    {⟦wkn≡³⟧ : F.weaken★ $ ⟦ τ ⟧ᵀ ≡ ⟦ τ′ ⟧ᵀ × F.weaken★ $ ⟦ σ ⟧ᵀ ≡ ⟦ σ′ ⟧ᵀ × F.weaken★ $ ⟦ ε ⟧ᵀ ≡ ⟦ ε′ ⟧ᵀ }
  → ⟦ B.new M wkn≡³ ⟧ F.≡ᴹ F.new {τ = ⟦ τ ⟧ᵀ} {σ = ⟦ σ ⟧ᵀ} ⟦ M ⟧ ⟦wkn≡³⟧
⟦new⟧ {M = M} {wkn≡³ = wkn≡³} {⟦wkn≡³⟧} =
  ⟦ B.new M wkn≡³ ⟧             ≡⟨ ⟦ B.≡ᴹ-subst _ _ ⟧≡ ⟩
  ⟦ B.tra B.↠-new M ⟧           ≡⟨ ⟦ B.≡ᴹ-sym (tra-lift M) ⟧≡ ⟩
  ⟦ B.tra (B.lift B.↠-new) M ⟧  ≡⟨ ⟦tra⟧ ⟦↠-new⟧ M ⟩
  F.tra F.↠-new ⟦ M ⟧           ≡⟨ F.≡ᴹ-sym (F.≡ᴹ-subst _ (F.tra F.↠-new ⟦ M ⟧)) ⟩
  F.new ⟦ M ⟧ ⟦wkn≡³⟧           ∎
  where open F.≡ᴹ-Reasoning
```

```
open F.⟶★-Reasoning
```

```
sim↦ :
    M B.↦ M′
  → ⟦ M ⟧ F.⟶★ ⟦ M′ ⟧
sim↦ {M = .(appᵛ (ƛ N) V)} {M′ = .(N B.[ V ])} (step₀ (β N V)) =
  appᵛ (ƛ ⟦ N ⟧) ⟦ V ⟧   ⟶⟨ F.^₀ (β ⟦ N ⟧ ⟦ V ⟧) ⟩
  ⟦ N ⟧ F.[ ⟦ V ⟧ ]      ≡⟨ F.≡ᴹ-sym (⟦ N [ V ]⟧≡) ⟩
  ⟦ N B.[ V ] ⟧ ∎
sim↦ {M = .(appᵀ (Λ N) τ refl)} {M′ = .(return (N B.[ τ ]ᵀ))} (step₀ (βᵀ N τ)) =
  appᵀ (Λ ⟦ N ⟧) ⟦ τ ⟧ᵀ _           ≡⟨ F.appᵀ-irr ⟩
  appᵀ (Λ ⟦ N ⟧) ⟦ τ ⟧ᵀ refl        ⟶⟨ F.^₀ (βᵀ ⟦ N ⟧ ⟦ τ ⟧ᵀ) ⟩
  return (⟦ N ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ)      ≡⟨ F.≡ᴹ-sym (F.≡ᴹ-return refl ⟦ N [ τ ]ᵀ⟧≡) ⟩
  return ⟦ N B.[ τ ]ᵀ ⟧ ∎
sim↦ {M = .(appᴺ (ƛᴺ N) n refl)} {M′ = .(return (N [ n ]ᴺ))} (step₀ (βᴺ {τ = τ} N n)) =
  appᵀ   (Λ ƛ return ⟦ N ⟧) ⟦ n ⟧∈ᶻ _               >>= appᵛ (Var Z) (F.weaken ⟦ n ⟧N)   ≡⟨ ⦅1⦆ ⟩
  appᵀ   (Λ ƛ return ⟦ N ⟧) ⟦ n ⟧∈ᶻ refl            >>= appᵛ (Var Z) (F.weaken ⟦ n ⟧N)   ⟶⟨ ⦅2⦆ ⟩
  return ( (ƛ return {ε = ι} ⟦ N ⟧) F.[ ⟦ n ⟧∈ᶻ ]ᵀ) >>= appᵛ (Var Z) (F.weaken ⟦ n ⟧N)   ⟶⟨ ⦅3⦆ ⟩
  (appᵛ (Var Z) (F.weaken ⟦ n ⟧N)) F.[ (ƛ return {ε = ι} ⟦ N ⟧) F.[ ⟦ n ⟧∈ᶻ ]ᵀ ]         ≡⟨ F.unfold: (appᵛ (Var Z) (F.weaken ⟦ n ⟧N)) [ _ ] ⟩
  appᵛ (subst (F._⊢_ ∅) (sym (F.tra★-id _)) ((ƛ return {ε = ι} ⟦ N ⟧) F.[ ⟦ n ⟧∈ᶻ ]ᵀ))
       (F.tra F.-[ _ ] (F.weaken ⟦ n ⟧N))                                                ≡⟨ F.≡ᴹ-appᵛ (F.≡ᴹ-subst (sym (F.tra★-id _)) _) F.⦅weaken ⟦ n ⟧N [ _ ]′⦆ ⟩
  appᵛ (ƛ return {ε = ι} ⟦N⟧[n]ᶻ) ⟦ n ⟧N            ⟶⟨ ⦅4⦆ ⟩
  (return ⟦N⟧[n]ᶻ) F.[ ⟦ n ⟧N ]                     ≡⟨ F.[]∘return (F.tra (F.tra▷ F.-[ ⟦ n ⟧∈ᶻ ]ᵀ) ⟦ N ⟧) ⟦ n ⟧N ⟩
  return (⟦N⟧[n]ᶻ F.[ ⟦ n ⟧N ])                     ≡⟨ F.≡ᴹ-return refl (F.≡ᴹ-sym ⟦ N [ n ]ᴺ⟧) ⟩
  return ⟦ N [ n ]ᴺ ⟧ ∎
  where
    ⟦N⟧[n]ᶻ = F.tra (F.tra▷ F.-[ ⟦ n ⟧∈ᶻ ]ᵀ) ⟦ N ⟧
    n⇒⟦τ[n]⟧/ι≡ : Name ⟦ n ⟧∈ᶻ ⇒ ⟦ τ B.[ _ ]★ᴺ ⟧ᵀ / ι ≡ Name ⟦ n ⟧∈ᶻ ⇒ ⟦ τ ⟧ᵀ F.[ _ ]★ / ι
    n⇒⟦τ[n]⟧/ι≡ = cong (λ τ → _ ⇒ τ / _) ⟦ τ [ _ ]★ᴺ⟧≡
    ⦅1⦆ = F.⦅ F.appᵀ-irr >>= F.≡ᴹ-appᵛ (F.≡ᴹ-var (F.≡[∈]-Z refl n⇒⟦τ[n]⟧/ι≡)) (F.≡ᴹ-weaken n⇒⟦τ[n]⟧/ι≡ (refl {M = ⟦ n ⟧N})) ⦆
    ⦅2⦆ = ξ (step₀ (βᵀ (ƛ return {ε = ι} ⟦ N ⟧) ⟦ n ⟧∈ᶻ)) ([ □ ]>>= _) ([ □ ]>>= _) F.≈ᴱ-refl refl refl
    ⦅3⦆ = ξ (step₀ (β->>= {ε = ι} ((ƛ return {ε = ι} ⟦ N ⟧) F.[ _ ]ᵀ) _)) □ □ F.≈ᴱ-refl refl refl
    ⦅4⦆ = ξ (step₀ (β (return _) ⟦ n ⟧N)) □ □ F.≈ᴱ-refl refl refl

sim↦ {M = .(return V >>= N)} {M′ = .(N B.[ V ])} (step₀ (β->>= V N)) =
  return ⟦ V ⟧ >>= ⟦ N ⟧ ⟶⟨ F.^₀ (β->>= ⟦ V ⟧ ⟦ N ⟧) ⟩
  ⟦ N ⟧ F.[ ⟦ V ⟧ ]      ≡⟨ F.≡ᴹ-sym ⟦ N [ V ]⟧≡ ⟩
  ⟦ N B.[ V ] ⟧ ∎
sim↦ {M = .(handle′ _ _ H)} {M′ = .(H B.[ V ∣ B.resume _ _ H ]ʰ)} (step₀ (hop {ℓ∉ = ℓ∉ε} {E = E} ℓ∉E V H refl)) =
  handle′ _ ⟦ _ ⟧ ⟦ H ⟧                                ≡⟨ F.≡ᴹ-handle′ refl ⟦ E [ _ /□]⟧≡ refl ⟩
  handle′ _ (⟦ E ⟧ᴱ F.[ perform _ ⟦ V ⟧ /□]) ⟦ H ⟧     ⟶⟨ F.^₀ (hop ⟦ ℓ∉E ⟧∉ᴱ ⟦ V ⟧ ⟦ H ⟧ refl) ⟩
  ⟦ H ⟧ F.[ ⟦ V ⟧ ∣ F.resume ⟦ ℓ∉ε ⟧∉ ⟦ E ⟧ᴱ ⟦ H ⟧ ]ʰ  ≡⟨ F.≡ᴹ-refl (cong (⟦ H ⟧ F.[ ⟦ V ⟧ ∣_]ʰ) (sym (⟦resume⟧ ℓ∉ε E H))) ⟩
  ⟦ H ⟧ F.[ ⟦ V ⟧ ∣ ⟦ B.resume ℓ∉ε E H ⟧ ]ʰ            ≡⟨ F.≡ᴹ-sym ⟦ H [ V ∣ B.resume ℓ∉ε E H ]ʰ⟧≡ ⟩
  ⟦ H B.[ V ∣ B.resume ℓ∉ε E H ]ʰ ⟧ ∎
sim↦ {M = .(handle′ _ (return V) H)} {M′ = .(return V)} (step₀ (hret V H)) =
  handle′ _ (return ⟦ V ⟧) ⟦ H ⟧  ⟶⟨ F.^₀ (hret ⟦ V ⟧ ⟦ H ⟧) ⟩
  return ⟦ V ⟧ ∎
sim↦ {M = .(subeffect ε⊑ε′ (return V))} {M′ = .(return V)} (step₀ (subret ε⊑ε′ V)) =
  subeffect ⟦ ε⊑ε′ ⟧⊑ (return ⟦ V ⟧)  ⟶⟨ F.^₀ (subret ⟦ ε⊑ε′ ⟧⊑ ⟦ V ⟧) ⟩
  return ⟦ V ⟧ ∎
sim↦ {M = .(handle M H wkn≡³)} {M′ = .(handle′ _ (B.new M wkn≡³) (B.weakenᴸ H))} (step₁ (generate {ε = ε} M {wkn≡³} H)) =
  handle ⟦ M ⟧ ⟦ H ⟧ ⟦wkn≡³⟧                                   ⟶⟨ F.^₁ (generate ⟦ M ⟧ ⟦ H ⟧) ⟩
  handle′ F.fresh (F.new ⟦ M ⟧ ⟦wkn≡³⟧) (F.weakenᴸ ⟦ H ⟧)      ≡⟨ F.≡ᴹ-sym (F.≡ᴹ-handle′ ⟦fresh⟧ (⟦new⟧ {M = M} {wkn≡³} {⟦wkn≡³⟧}) (⟦weakenᴸ⟧ H)) ⟩
  handle′ ⟦ B.fresh {ε = ε} ⟧∉ ⟦ B.new M wkn≡³ ⟧ ⟦ B.weakenᴸ H ⟧ ∎
  where ⟦wkn≡³⟧ = _  -- unification variable
```

```
⟦_[_/□]⟶★_[_/□]⟧ : ∀ (E : B.□: τ/ε₁ ⊢ᴱ τ/ε₂) M (E′ : B.□: τ/ε₁′ ⊢ᴱ τ/ε₂′)  M′
  → ⟦ E ⟧ᴱ F.[ ⟦ M ⟧ /□] F.⟶★ ⟦ E′ ⟧ᴱ F.[ ⟦ M′ ⟧ /□]
  → ⟦ E B.[ M /□] ⟧ F.⟶★ ⟦ E′ B.[ M′ /□] ⟧
⟦ E [ M /□]⟶★ E′ [ M′ /□]⟧ = F.⦅ ⟦ E [ M /□]⟧≡ ⟶★ ⟦ E′ [ M′ /□]⟧≡ ⦆
```

```
sim : ∀ {M : ∅ B.⊢ τ} {M′ : ∅ B.⊢ τ′} → M B.⟶ M′ → ⟦ M ⟧ F.⟶★ ⟦ M′ ⟧
sim (ξ N↦N′ E E′ E≈E′ refl refl)
  = ⟦ E [ _ /□]⟶★ E′ [ _ /□]⟧ (F.⟶★-cong (≈ᴱ-⟦⟧ E≈E′) (sim↦ N↦N′))
```

```
module FCNSubstitution where

open import Function.Base using (_∘_; it)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; sym; trans; cong; cong₂; subst; module ≡-Reasoning)

open import Common using (⊥; ⊥-elim; cong₃; _$_≡_; `_; `refl)
open import Labels
open import FirstClassNames
```

```
private variable
  κ κ′ : Kind
  L L′ L″ L₂′ : Labels
  Δ Δ′ Δ″ Δ₂′ : Contextᴷ
  LΔ LΔ′ LΔ″ : Labels × Contextᴷ
  ℓ ℓ′ ℓ₁ ℓ₁′ : -∈ᴸ L
  rᴸ : L →ᴸ L′
  rᴸ′ : L′ →ᴸ L″
  rᴸ″ : L →ᴸ L″
  rᴰ rᴰ′ : Δ →ᴰ Δ′
  τ τ₁ τ₂ τ₁′ τ₂′ τ′ τ″ τ₃ η η′ η₁ η₁′ : L ⨟ Δ ⊢ᵀ κ
  ε ε′ ε₁ ε₁′ ε″ ε₁″ : L ⨟ Δ ⊢ᵀ EFF
  σ σ′ σ₁ σ₁′ : L ⨟ Δ ⊢ᵀ SIG
  τ/ε τ/ε′ τ/ε₁ τ/ε₁′ : L ⨟ Δ ⊢ᵀ COM
  Γ Γ′ Γ″ Γ³ : Context L Δ
  r★ r★′ r★″ : L →ᴸ L′ × Δ →ᴰ Δ′
  sᴰ sᴰ′ : Δ ↠ᴰ L′ ⨟ Δ′
  s★ s★′ s★″ : L →ᴸ L′ × Δ ↠ᴰ L′ ⨟ Δ′
```

```
open ⦅_⦆_→★_
open Kit★
```

```
→ᴸ⇒→★ : (L →ᴸ L′) → (L , Δ) →★ (L′ , Δ)
→ᴸ⇒→★ rᴸ = (rᴸ , →ᴰ-id)
```

```
ren★L : ∀ {L L′} → (L →ᴸ L′) → ∀ {Δ κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ ⊢ᵀ κ
ren★L rᴸ = ren★ (→ᴸ⇒→★ rᴸ)

weaken★L : ∀ {Δ κ} → L ⨟ Δ ⊢ᵀ κ → L ▷- ⨟ Δ ⊢ᵀ κ
weaken★L = ren★L →ᴸ-S
```

### Substitution lemmas

```
⦅_,_⦆_≡ᵀ_ : ∀ {_#_} {_%_} (K : Kit★ _#_) (K′ : Kit★ _%_) → ∀ {κ} → LΔ # κ → LΔ % κ → Set
⦅ K , K′ ⦆ t ≡ᵀ u = to-⊢★ K t ≡ to-⊢★ K′ u
```

```
record _≡[↠★]_ {_#_} {_%_} {K : Kit★ _#_} {K′ : Kit★ _%_} (r : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)) (r′ : ⦅ K′ ⦆ (L , Δ) →★ (L′ , Δ′)) : Set where
  constructor _,_
  field
    tra∈★-≡ : ∀ {κ} (α : κ ∈★ Δ) → ⦅ K , K′ ⦆ tra∈★ r α ≡ᵀ tra∈★ r′ α

open _≡[↠★]_
```

```
≡[↠★]-refl : ∀ {_#_} {K : Kit★ _#_} {s : ⦅ K ⦆ LΔ →★ LΔ′} → s ≡[↠★] s
≡[↠★]-refl = record {
  tra∈★-≡ = λ _ → refl }
```

```
≡[↠★]-sym : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} {s : ⦅ K ⦆ LΔ →★ LΔ′} {s′ : ⦅ K′ ⦆ LΔ →★ LΔ′} → s ≡[↠★] s′ → s′ ≡[↠★] s
≡[↠★]-sym s≡ = record {
  tra∈★-≡ = λ α → sym (tra∈★-≡ s≡ α) }
```

```
≡[↠★]-trans : ∀ {_#_ _%_ _&_} {K : Kit★ _#_} {K′ : Kit★ _%_} {K″ : Kit★ _&_}
  → ∀ {s : ⦅ K ⦆ LΔ →★ LΔ′} {s′ : ⦅ K′ ⦆ LΔ →★ LΔ′} {s″ : ⦅ K″ ⦆ LΔ →★ LΔ′}
  → s ≡[↠★] s′ → s′ ≡[↠★] s″ → s ≡[↠★] s″
≡[↠★]-trans s≡ s≡′ = record {
  tra∈★-≡ = λ α → trans (tra∈★-≡ s≡ α) (tra∈★-≡ s≡′ α) }
```

```
record GoodKit★ {_#_ : Labels × Contextᴷ → Kind → Set} (K : Kit★ _#_) : Set where
  no-eta-equality
  field
    to★-from★ : ∀ {κ} (α : κ ∈★ Δ) → to-⊢★ K (from-∈★ K {L = L} α) ≡ Var α
    to★-wkn★ : ∀ {κ} (t : LΔ # κ) → to-⊢★ K (wkn★ K t) ≡ weaken★ {κ′ = κ′} (to-⊢★ K t)

open GoodKit★
```

```
instance
  GK∈★ : GoodKit★ K∈★
  GK∈★ = record {
    to★-from★ = λ α → refl ;
    to★-wkn★ = λ α → refl }
```

```
instance
  GK⊢★ : GoodKit★ K⊢★
  GK⊢★ = record {
    to★-from★ = λ α → refl ;
    to★-wkn★ = λ τ → refl }
```

```
module _ {_#_ _%_ : _} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ where
  tra★▷-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → tra★▷ r ≡[↠★] tra★▷ r′ {κ}
  tra∈★-≡ (tra★▷-≡ r≡) Z = trans (to★-from★ GK Z) (sym (to★-from★ GK′ Z))
  tra∈★-≡ (tra★▷-≡ {r = r} {r′} r≡) (S α) = trans (to★-wkn★ GK (tra∈★ r α)) (trans (cong weaken★ (tra∈★-≡ r≡ α)) (sym (to★-wkn★ GK′ (tra∈★ r′ α))))

  tra★-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ)
    → tra★ r τ ≡ tra★ r′ τ
  tra★-≡ r≡ (Var α) = tra∈★-≡ r≡ α
  tra★-≡ r≡ (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ / τ₂) = cong₂ _/_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-≡ (tra★▷-≡ r≡) τ)
  tra★-≡ r≡ (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-≡ (tra★▷-≡ r≡) τ)
  tra★-≡ r≡ Unit = refl
  tra★-≡ r≡ ι = refl
  tra★-≡ r≡ (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra★-≡ r≡ η) (tra★-≡ r≡ σ) (tra★-≡ r≡ ε)
  tra★-≡ r≡ ⟨ τ ⟩ = cong ⟨_⟩ (tra★-≡ r≡ τ)
  tra★-≡ r≡ (Name τ) = cong Name (tra★-≡ r≡ τ)
  tra★-≡ r≡ (dynamic ℓ) = cong dynamic (ren∈ᴸ-irr ℓ)
```

```
module _ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ where
  tra★▷-id : ∀ {κ} → tra★▷ {L = L} {Δ = Δ} {K = K} →★-id {κ} ≡[↠★] →★-id {K = K}
  tra∈★-≡ tra★▷-id Z = refl
  tra∈★-≡ tra★▷-id (S α) = trans (to★-wkn★ GK _) (trans (cong weaken★ (to★-from★ GK α)) (sym (to★-from★ GK (S α))))

  tra★-id : ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ) → tra★ (→★-id {K = K}) τ ≡ τ

  tra★-id▷ : ∀ {κ} (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ) → tra★ (tra★▷ →★-id) τ ≡ τ
  tra★-id▷ τ = trans (tra★-≡ tra★▷-id τ) (tra★-id τ)

  tra★-id (Var α) = to★-from★ GK α
  tra★-id (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ / τ₂) = cong₂ _/_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-id▷ τ)
  tra★-id (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-id▷ τ)
  tra★-id Unit = refl
  tra★-id ι = refl
  tra★-id (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra★-id η) (tra★-id σ) (tra★-id ε)
  tra★-id ⟨ τ ⟩ = cong ⟨_⟩ (tra★-id τ)
  tra★-id (Name τ) = cong Name (tra★-id τ)
  tra★-id (dynamic ℓ) = refl
```

```
_∘[↠★]_ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_}
  → ⦅ K′ ⦆ LΔ′ →★ LΔ″ → ⦅ K ⦆ LΔ →★ LΔ′ → ⦅ K⊢★ ⦆ LΔ →★ LΔ″
relabel (r′ ∘[↠★] r) = relabel r′ ∘ᴸ relabel r
tra∈★ (r′ ∘[↠★] r) α = tra★ r′ (tra∈★′ r α)
```

```
record Kit★-∘ {_#_ : _} {_%_ : _} (K : Kit★ _#_) (K′ : Kit★ _%_) : Set where
  no-eta-equality
  field
    tra★∘wkn★ : ∀ {r′ : ⦅ K′ ⦆ LΔ →★ LΔ′} {κ} (t : LΔ # κ) → tra★ (tra★▷ r′ {κ′}) (to-⊢★ K (wkn★ K t)) ≡ weaken★ (tra★ r′ (to-⊢★ K t))

open Kit★-∘ ⦃ ... ⦄
```

```
module _ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄ where
  tra★▷-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → (tra★▷ r′ {κ} ∘[↠★] tra★▷ r) ≡[↠★] tra★▷ (r′ ∘[↠★] r)
  tra∈★-≡ tra★▷-∘ Z = trans (cong (tra★ (tra★▷ _)) (to★-from★ GK Z)) (to★-from★ GK′ Z)
  tra∈★-≡ (tra★▷-∘ {r′ = r′} {r}) (S α) = tra★∘wkn★ {r′ = r′} (tra∈★ r α)

  tra★-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ)
    → tra★ r′ (tra★ r τ) ≡ tra★ (r′ ∘[↠★] r) τ

  tra★-∘▷ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ {κ} (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
    → tra★ (tra★▷ r′) (tra★ (tra★▷ r) τ) ≡ tra★ (tra★▷ (r′ ∘[↠★] r)) τ
  tra★-∘▷ τ = trans (tra★-∘ τ) (tra★-≡ tra★▷-∘ τ)

  tra★-∘ (Var x) = refl
  tra★-∘ (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ / τ₂) = cong₂ _/_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-∘▷ τ)
  tra★-∘ (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-∘▷ τ)
  tra★-∘ Unit = refl
  tra★-∘ ι = refl
  tra★-∘ (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra★-∘ η) (tra★-∘ σ) (tra★-∘ ε)
  tra★-∘ ⟨ τ ⟩ = cong ⟨_⟩ (tra★-∘ τ)
  tra★-∘ (Name τ) = cong Name (tra★-∘ τ)
  tra★-∘ {r′ = (rᴸ′ , _)} (dynamic ℓ) = cong dynamic (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ)
```

```
instance
  K-∘-∈★-∈★ : Kit★-∘ K∈★ K∈★
  K-∘-∈★-∈★ = record {
    tra★∘wkn★ = λ α → refl }
```

```
instance
  K-∘-∈★-⊢★ : Kit★-∘ K∈★ K⊢★
  K-∘-∈★-⊢★ = record {
    tra★∘wkn★ = λ α → refl }
```

```
→★-S★-∘ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ (r : ⦅ K ⦆ LΔ →★ LΔ′)
  → (→★-S★ {K = K∈★} {κ = κ} ∘[↠★] r) ≡[↠★] (tra★▷ r ∘[↠★] →★-S★ {K = K∈★})
tra∈★-≡ (→★-S★-∘ ⦃ GK = GK ⦄ r) = λ α → sym (to★-wkn★ GK (tra∈★ r α))
```

```
weaken★∘tra★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ {κ′ = κ′} (tra★ r τ) ≡ tra★ (tra★▷ r) (weaken★ τ)
weaken★∘tra★ {r = r} τ = begin
  weaken★ (tra★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-S★ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-S★-∘ r) τ ⟩
  sub★ (tra★▷ r ∘[↠★] →★-S★ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  tra★ (tra★▷ r) (weaken★ τ) ∎
 where open ≡-Reasoning
```

```
instance
  K-∘-⊢★-∈★ : Kit★-∘ K⊢★ K∈★
  K-∘-⊢★-∈★ = record {
    tra★∘wkn★ = sym ∘ weaken★∘tra★ }
```

```
weaken★∘sub★ : {r : LΔ ↠★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ {κ′ = κ′} (sub★ r τ) ≡ sub★ (tra★▷ r) (weaken★ τ)
weaken★∘sub★ {r = r} τ = begin
  weaken★ (sub★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-S★ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-S★-∘ r) τ ⟩
  sub★ (tra★▷ r ∘[↠★] →★-S★ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  sub★ (tra★▷ r) (weaken★ τ) ∎
 where open ≡-Reasoning
```

```
instance
  K-∘-⊢★-⊢★ : Kit★-∘ K⊢★ K⊢★
  K-∘-⊢★-⊢★ = record {
    tra★∘wkn★ = sym ∘ weaken★∘tra★ }
```

```
weaken★∘tra★∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {τ : L ⨟ Δ ⊢ᵀ κ} {κ′ : Kind} {τ′ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ}
  → weaken★ $ τ ≡ τ′
  → weaken★ $ tra★ r★ τ ≡ tra★ (tra★▷ r★) τ′
weaken★∘tra★∙ {τ = τ} (` wkn-τ≡) = ` trans (weaken★∘tra★ τ) (cong (tra★ _) wkn-τ≡)
```

```
weaken★∘ren★L∙ : ∀ {rᴸ : L →ᴸ L′} {τ : L ⨟ Δ ⊢ᵀ κ} {κ′} {τ′ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ}
  → weaken★ $ τ ≡ τ′
  → weaken★ $ ren★L rᴸ τ ≡ ren★L rᴸ τ′
weaken★∘ren★L∙ {τ = τ} (` wkn≡) = ` trans (tra★-∘ τ) (trans (tra★-≡ (_,_ λ α → refl) τ) (trans (sym (tra★-∘ τ)) (cong (ren★L _) wkn≡)))
```

```
ren★L-∘ᴸ : ∀ (rᴸ : L →ᴸ L′) (rᴸ′ : L′ →ᴸ L″) (rᴸ″ : L →ᴸ L″)
  → ∀ (τ : L ⨟ Δ ⊢ᵀ κ)
  → ren★L rᴸ′ (ren★L rᴸ τ) ≡ ren★L rᴸ″ τ
ren★L-∘ᴸ rᴸ rᴸ′ rᴸ″ τ = trans (tra★-∘ τ) (tra★-≡ (record { tra∈★-≡ = λ α → refl }) τ)
```

```
∘-[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (r★ ∘[↠★] -[ σ ]★) ≡[↠★] (-[ tra★ r★ σ ]★ ∘[↠★] tra★▷ r★)
∘-[]★ {K = K} ⦃ GK ⦄ ⦃ K-∘′ ⦄ {r★ = r★} {σ = σ} = record {
  tra∈★-≡ = λ{
    Z → cong (tra★ _) (sym (to★-from★ GK Z)) ;
    (S α) → sym (trans (cong (tra★ _) (to★-wkn★ GK (tra∈★ r★ α))) (trans (tra★-∘ (to-⊢★ K (tra∈★ r★ α))) (tra★-id {K = K⊢★} _))) } }
```

```
tra★∘[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → tra★ r★ (τ [ σ ]★) ≡ tra★ (tra★▷ r★) τ [ tra★ r★ σ ]★
tra★∘[]★ ⦃ GK ⦄ τ = trans (tra★-∘ τ) (trans (tra★-≡ ∘-[]★ τ) (sym (tra★-∘ τ)))
```

```
tra★∘[]★∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′} {τ[σ]}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → τ [ σ ]★ ≡ τ[σ]
  → tra★ (tra★▷ r★) τ [ tra★ r★ σ ]★ ≡ tra★ r★ τ[σ]
tra★∘[]★∙ τ τ[σ]≡ = trans (sym (tra★∘[]★ τ)) (cong (tra★ _) τ[σ]≡)
```

```
[]★∘weaken★ : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (σ : L ⨟ Δ ⊢ᵀ κ′)
  → weaken★ τ [ σ ]★ ≡ τ
[]★∘weaken★ τ σ = trans (tra★-∘ τ) (trans (tra★-≡ ≡[↠★]-refl τ) (tra★-id τ))
```

```
[]★∘weaken★∙ : ∀ {τ : L ⨟ Δ ⊢ᵀ κ} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → weaken★ $ τ ≡ τ′
  → τ′ [ σ ]★ ≡ τ
[]★∘weaken★∙ {τ = τ} {σ} (` w≡) = trans (cong (_[ σ ]★) (sym w≡)) (trans (tra★-∘ τ) (trans (tra★-≡ ≡[↠★]-refl τ) (tra★-id τ)))
```

```
-[]★∘[↠★] : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (-[ tra★ s★ σ ]★ ∘[↠★] tra★▷ s★) ≡[↠★] (s★ ∘[↠★] -[ σ ]★)
tra∈★-≡ -[]★∘[↠★] Z = refl
tra∈★-≡ -[]★∘[↠★] (S α) = []★∘weaken★ _ _
```

```
sub★∘[]★ : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → sub★ (tra★▷ s★) τ [ sub★ s★ σ ]★ ≡ sub★ s★ (τ [ σ ]★)
sub★∘[]★ τ = trans (tra★-∘ τ) (trans (tra★-≡ -[]★∘[↠★] τ) (sym (tra★-∘ τ)))
```

```
sub∘[]★∙ : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′} {τ[σ]}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → τ [ σ ]★ ≡ τ[σ]
  → sub★ (tra★▷ s★) τ [ sub★ s★ σ ]★ ≡ sub★ s★ τ[σ]
sub∘[]★∙ τ τ[σ]≡ = tra★∘[]★∙ τ τ[σ]≡
```

### Subtyping

```
infix 3 _≡[∈ᵉ]_
data _≡[∈ᵉ]_ (η∈ᵉ : η ⦂ σ ∈ᵉ ε) : η′ ⦂ σ′ ∈ᵉ ε′ → Set where
  refl : η∈ᵉ ≡[∈ᵉ] η∈ᵉ
```

```
infix 3 _≡[⊑]_
data _≡[⊑]_ (ε⊑ : ε ⊑ ε₁) : ε′ ⊑ ε₁′ → Set where
  refl : ε⊑ ≡[⊑] ε⊑
```

```
≡[⊑]-ι : ε ≡ ε′ → ι {ε′ = ε} ≡[⊑] ι {ε′ = ε′}
≡[⊑]-ι refl = refl
```

```
≡[⊑]-∷ : {η∈ : η ⦂ σ ∈ᵉ ε₁} {η∈′ : η′ ⦂ σ′ ∈ᵉ ε₁′} {ε⊑ : ε ⊑ ε₁} {ε⊑′ : ε′ ⊑ ε₁′} → η∈ ≡[∈ᵉ] η∈′ → ε⊑ ≡[⊑] ε⊑′ → η∈ ∷ ε⊑ ≡[⊑] η∈′ ∷ ε⊑′
≡[⊑]-∷ refl refl = refl
```

```
≡[⊑]-trans : {ε⊑ : ε ⊑ ε₁} {ε⊑′ : ε′ ⊑ ε₁′} {ε⊑″ : ε″ ⊑ ε₁″}
  → ε⊑ ≡[⊑] ε⊑′ → ε⊑′ ≡[⊑] ε⊑″ → ε⊑ ≡[⊑] ε⊑″
≡[⊑]-trans refl refl = refl
```


```
≡[∈ᵉ]-Z : η ≡ η′ → σ ≡ σ′ → ε ≡ ε′ → Z {η = η} {σ = σ} {ε = ε} ≡[∈ᵉ] Z {η = η′} {σ = σ′} {ε = ε′}
≡[∈ᵉ]-Z refl refl refl = refl
```

```
≡[∈ᵉ]-S : {η∈ε : η ⦂ σ ∈ᵉ ε} {η∈ε′ : η′ ⦂ σ′ ∈ᵉ ε′} → σ₁ ≡ σ₁′ → η₁ ≡ η₁′ → η∈ε ≡[∈ᵉ] η∈ε′ → S_ {σ′ = σ₁} {η′ = η₁} η∈ε ≡[∈ᵉ] S_ {σ′ = σ₁′} {η′ = η₁′} η∈ε′
≡[∈ᵉ]-S refl refl refl = refl
```

```
tra∈ᵉ : ∀ {_#_} {K : Kit★ _#_} → (r★ : ⦅ K ⦆ LΔ →★ LΔ′) → ∀ {η} {σ} {ε} → η ⦂ σ ∈ᵉ ε → tra★ r★ η ⦂ tra★ r★ σ ∈ᵉ tra★ r★ ε
tra∈ᵉ r★ Z = Z
tra∈ᵉ r★ (S η∈ε) = S (tra∈ᵉ r★ η∈ε)
```

```
tra⊑ : ∀ {_#_} {K : Kit★ _#_} (r★ : ⦅ K ⦆ LΔ →★ LΔ′) → ∀ {ε} {ε′} → ε ⊑ ε′ → tra★ r★ ε ⊑ tra★ r★ ε′
tra⊑ r★ ι = ι
tra⊑ r★ (η∈ε′ ∷ ε⊑) = tra∈ᵉ r★ η∈ε′ ∷ tra⊑ r★ ε⊑
```

```
tra∈ᵉ-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ (η∈ : η ⦂ σ ∈ᵉ ε) → tra∈ᵉ {K = K} →★-id η∈ ≡[∈ᵉ] η∈
tra∈ᵉ-id Z = ≡[∈ᵉ]-Z (tra★-id _) (tra★-id _) (tra★-id _)
tra∈ᵉ-id (S η∈) = ≡[∈ᵉ]-S (tra★-id _) (tra★-id _) (tra∈ᵉ-id η∈)
```

```
tra∈ᵉ-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′ → (η∈ : η ⦂ σ ∈ᵉ ε) → tra∈ᵉ r★ η∈ ≡[∈ᵉ] tra∈ᵉ r★′ η∈
tra∈ᵉ-≡ {η = η} {σ = σ} r≡ (Z {ε = ε}) = ≡[∈ᵉ]-Z (tra★-≡ r≡ η) (tra★-≡ r≡ σ) (tra★-≡ r≡ ε)
tra∈ᵉ-≡ r≡ (S_ {σ′ = σ′} {η′ = η′} η∈) = ≡[∈ᵉ]-S (tra★-≡ r≡ σ′) (tra★-≡ r≡ η′) (tra∈ᵉ-≡ r≡ η∈)
```

```
tra∈ᵉ-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r★ : ⦅ K ⦆ LΔ →★ LΔ′}
  → (η∈ : η ⦂ σ ∈ᵉ ε)
  → tra∈ᵉ r★′ (tra∈ᵉ r★ η∈) ≡[∈ᵉ] tra∈ᵉ (r★′ ∘[↠★] r★) η∈
tra∈ᵉ-∘ {η = η} {σ = σ} (Z {ε = ε}) = ≡[∈ᵉ]-Z (tra★-∘ η) (tra★-∘ σ) (tra★-∘ ε)
tra∈ᵉ-∘ (S_ {σ′ = σ′} {η′ = η′} η∈) = ≡[∈ᵉ]-S (tra★-∘ σ′) (tra★-∘ η′) (tra∈ᵉ-∘ η∈)
```

```
tra⊑-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → (ε⊑ : ε ⊑ ε₁)
  → tra⊑ {K = K} →★-id ε⊑ ≡[⊑] ε⊑
tra⊑-id ι = ≡[⊑]-ι (tra★-id _)
tra⊑-id (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-id _) (tra⊑-id _)
```

```
tra⊑-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′ → (ε⊑ : ε ⊑ ε′) → tra⊑ r★ ε⊑ ≡[⊑] tra⊑ r★′ ε⊑
tra⊑-≡ {ε′ = ε′} r≡ ι = ≡[⊑]-ι (tra★-≡ r≡ ε′)
tra⊑-≡ r≡ (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-≡ r≡ η∈) (tra⊑-≡ r≡ ε⊑)
```

```
tra⊑-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r★ : ⦅ K ⦆ LΔ →★ LΔ′}
  → (ε⊑ : ε ⊑ ε′)
  → tra⊑ r★′ (tra⊑ r★ ε⊑) ≡[⊑] tra⊑ (r★′ ∘[↠★] r★) ε⊑
tra⊑-∘ {ε′ = ε′} ι = ≡[⊑]-ι (tra★-∘ ε′)
tra⊑-∘ (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-∘ η∈) (tra⊑-∘ ε⊑)
```

```
data _≡[∉]_ {ℓ} (ℓ∉ε : ℓ ∉ ε) : ℓ′ ∉ ε′ → Set where
  refl : ℓ∉ε ≡[∉] ℓ∉ε
```

```
≡[∉]-ι :
    ℓ ≡ ℓ′
  → ι {Δ = Δ} {ℓ = ℓ} ≡[∉] ι {Δ = Δ} {ℓ = ℓ′}
≡[∉]-ι refl = refl
```

```
≡[∉]-∷ :
    .{ℓ≢ : ℓ₁ ≢ ℓ} .{ℓ≢′ : ℓ₁′ ≢ ℓ′}
  → {ℓ∉ : ℓ ∉ ε} {ℓ∉′ : ℓ′ ∉ ε′}
  → ℓ₁ ≡ ℓ₁′
  → σ ≡ σ′
  → ℓ∉ ≡[∉] ℓ∉′
  → _∷_ {σ = σ} ℓ≢ ℓ∉ ≡[∉] _∷_ {σ = σ′} ℓ≢′ ℓ∉′
≡[∉]-∷ refl refl refl = refl
```

```
tra∉ : ∀ {_#_} {K : Kit★ _#_} {ℓ} {ε} → (r★@(rᴸ , rᴰ) : ⦅ K ⦆ LΔ →★ LΔ′) → ℓ ∉ ε → ren∈ᴸ rᴸ ℓ ∉ tra★ r★ ε
tra∉ r★ ι = ι
tra∉ {ℓ = ℓ} r★@(rᴸ , _) (≢ℓ ∷ ℓ∉ε) = (≢ℓ ∘ injective-ren∈ᴸ rᴸ) ∷ tra∉ r★ ℓ∉ε
```

```
tra∉-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ r★ ℓ∉ ≡[∉] tra∉ r★′ ℓ∉
tra∉-≡ {ℓ = ℓ} r★≡ ι = ≡[∉]-ι (ren∈ᴸ-irr ℓ)
tra∉-≡ {ℓ = ℓ} r★≡ (_∷_ {ℓ′ = ℓ′} {σ = σ} _ ℓ∉) = ≡[∉]-∷ (ren∈ᴸ-irr ℓ′) (tra★-≡ r★≡ σ) (tra∉-≡ r★≡ ℓ∉)
```

```
tra∉-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ {K = K} →★-id ℓ∉ ≡[∉] ℓ∉
tra∉-id ι = ≡[∉]-ι refl
tra∉-id (_∷_ {σ = σ} _ ℓ∉) = ≡[∉]-∷ refl (tra★-id σ) (tra∉-id ℓ∉)
```

```
tra∉-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″}
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ r★′ (tra∉ r★ ℓ∉) ≡[∉] tra∉ (r★′ ∘[↠★] r★) ℓ∉
tra∉-∘ {ℓ = ℓ} {r★′ = (rᴸ′ , _)} ι = ≡[∉]-ι (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ)
tra∉-∘ {r★′ = (rᴸ′ , _)} (_∷_ {ℓ′ = ℓ′} {σ = σ} _ ℓ∉) = ≡[∉]-∷ (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ′) (tra★-∘ σ) (tra∉-∘ ℓ∉)
```

```
ren∉ᴸ : ∀ {ℓ} → (rᴸ : L →ᴸ L′) → ℓ ∉ ε → ren∈ᴸ rᴸ ℓ ∉ ren★L rᴸ ε
ren∉ᴸ rᴸ = tra∉ (→ᴸ⇒→★ rᴸ)
```

```
ren⊑ᴸ : (rᴸ : L →ᴸ L′) → ε ⊑ ε′ → ren★L rᴸ ε ⊑ ren★L rᴸ ε′
ren⊑ᴸ rᴸ = tra⊑ (→ᴸ⇒→★ rᴸ)
```

### Term equality

```
infix 2 _≡[∈]_
data _≡[∈]_ {Γ : Context L Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} (x : τ ∈ Γ) : ∀ {Γ′ : Context L′ Δ′} {τ′ : L′ ⨟ Δ′ ⊢ᵀ TYP} → τ′ ∈ Γ′ → Set where
  refl : x ≡[∈] x
```

```
≡[∈]-sym : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} → x ≡[∈] x′ → x′ ≡[∈] x
≡[∈]-sym refl = refl
```

```
≡[∈]-trans : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} {x″ : τ″ ∈ Γ″} → x ≡[∈] x′ → x′ ≡[∈] x″ → x ≡[∈] x″
≡[∈]-trans refl refl = refl
```

```
≡[∈]-subst : (τ≡ : τ ≡ τ′) → {x : τ ∈ Γ} → subst (_∈ _) τ≡ x ≡[∈] x
≡[∈]-subst refl = refl
```

```
≡[∈]-S : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} → τ₁ ≡ τ₁′ → x ≡[∈] x′ → S_ {τ₂ = τ₁} x ≡[∈] S_ {τ₂ = τ₁′} x′
≡[∈]-S refl refl = refl
```

```
≡[∈]-S★ : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → {wkn≡ : weaken★ $ τ ≡ τ₁} {wkn≡′ : weaken★ $ τ′ ≡ τ₂}
  → S★_ {κ = κ} x wkn≡ ≡[∈] S★_ {κ = κ} x′ wkn≡′
≡[∈]-S★ refl {wkn≡ = `refl} {wkn≡′ = `refl} = refl
```

```
≡[∈]-Z : Γ ≡ Γ′ → τ ≡ τ′ → Z {τ = τ} {Γ = Γ} ≡[∈] Z {τ = τ′} {Γ = Γ′}
≡[∈]-Z refl refl = refl
```

```
module ≡[∈]-Reasoning where

  infix 3 _∎
  infixr 2 _≡⟨_⟩_

  _∎ : (x : τ ∈ Γ) → x ≡[∈] x
  x ∎ = refl

  _≡⟨_⟩_ : (x : τ ∈ Γ) {x′ : τ′ ∈ Γ′} {x″ : τ″ ∈ Γ″} → x ≡[∈] x′ → x′ ≡[∈] x″ → x ≡[∈] x″
  _≡⟨_⟩_ x = ≡[∈]-trans
```

```
infix 2 _≡ᴹ_
data _≡ᴹ_ {Γ : Context L Δ} {τ : L ⨟ Δ ⊢ᵀ κ} (M : Γ ⊢ τ) : ∀ {Γ′ : Context L′ Δ} {τ′ : L′ ⨟ Δ ⊢ᵀ κ} → Γ′ ⊢ τ′ → Set where
  refl : M ≡ᴹ M
```

```
≡ᴹ-refl : ∀ {M : Γ ⊢ τ} {M′ : Γ ⊢ τ} → M ≡ M′ → M ≡ᴹ M′
≡ᴹ-refl refl = refl
```

```
≡ᴹ-sym : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} → M ≡ᴹ M′ → M′ ≡ᴹ M
≡ᴹ-sym refl = refl
```

```
≡ᴹ-trans : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} {M″ : Γ″ ⊢ τ″} → M ≡ᴹ M′ → M′ ≡ᴹ M″ → M ≡ᴹ M″
≡ᴹ-trans refl refl = refl
```

```
≡ᴹ-subst : (τ≡ : τ ≡ τ′) (M : Γ ⊢ τ) → subst (_ ⊢_) τ≡ M ≡ᴹ M
≡ᴹ-subst refl _ = refl
```

```
module ≡ᴹ-Reasoning where

  infix 3 _∎
  infixr 2 _≡⟨_⟩_

  _∎ : (M : Γ ⊢ τ) → M ≡ᴹ M
  M ∎ = refl

  _≡⟨_⟩_ : (M : Γ ⊢ τ) {M′ : Γ′ ⊢ τ′} {M″ : Γ″ ⊢ τ″} → M ≡ᴹ M′ → M′ ≡ᴹ M″ → M ≡ᴹ M″
  _≡⟨_⟩_ M = ≡ᴹ-trans
```

```
⦅_>>=_⦆ : ∀ {M : Γ ⊢ τ₁ / ε} {M′ : Γ′ ⊢ τ₁′ / ε′} {N : Γ ▷ τ₁ ⊢ τ₂ / ε} {N′ : Γ′ ▷ τ₁′ ⊢ τ₂′ / ε′}
  → M ≡ᴹ M′
  → N ≡ᴹ N′
  → M >>= N ≡ᴹ M′ >>= N′
⦅ refl >>= refl ⦆ = refl
```

```
≡ᴹ-handle′ : ∀ {ℓ} {M : Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε} {M′ : Γ′ ⊢ τ′ / dynamic ℓ′ ⦂ σ′ ∷ ε′} {H : Γ ⊢ σ ⇒ʰ τ / ε} {H′ : Γ′ ⊢ σ′ ⇒ʰ τ′ / ε′}
  → ∀ {ℓ∉ε} {ℓ∉ε′}
  → ℓ∉ε ≡[∉] ℓ∉ε′
  → M ≡ᴹ M′
  → H ≡ᴹ H′
  → handle′ ℓ∉ε M H ≡ᴹ handle′ ℓ∉ε′ M′ H′
≡ᴹ-handle′ refl refl refl = refl
```

```
≡ᴹ-subeffect : ∀ {M : Γ ⊢ τ / ε} {M′ : Γ′ ⊢ τ′ / ε′}
  → ∀ {ε⊑ : ε ⊑ ε₁} {ε′⊑ : ε′ ⊑ ε₁′}
  → ε⊑ ≡[⊑] ε′⊑
  → M ≡ᴹ M′
  → subeffect ε⊑ M ≡ᴹ subeffect ε′⊑ M′
≡ᴹ-subeffect refl refl = refl
```

```
≡ᴹ-return : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → ε ≡ ε′
  → M ≡ᴹ M′
  → return {ε = ε} M ≡ᴹ return {ε = ε′} M′
≡ᴹ-return refl refl = refl
```

```
≡ᴹ-appᵛ : ∀ {V : Γ ⊢ τ₁ ⇒ τ₂} {V′ : Γ′ ⊢ τ₁′ ⇒ τ₂′} {W} {W′}
  → V ≡ᴹ V′
  → W ≡ᴹ W′
  → appᵛ V W ≡ᴹ appᵛ V′ W′
≡ᴹ-appᵛ refl refl = refl
```

```
≡ᴹ-appᵀ : ∀ {σ} {σ′} {σ[τ]} {σ[τ′]} {V : Γ ⊢ Forallᵀ κ σ} {V′ : Γ′ ⊢ Forallᵀ κ σ′}
  → V ≡ᴹ V′
  → τ ≡ τ′
  → {σ[τ]≡ : σ [ τ ]★ ≡ σ[τ]}
  → {σ[τ′]≡ : σ′ [ τ′ ]★ ≡ σ[τ′]}
  → appᵀ V τ σ[τ]≡ ≡ᴹ appᵀ V′ τ′ σ[τ′]≡
≡ᴹ-appᵀ refl refl {refl} {refl} = refl
```

```
≡ᴹ-var : ∀ {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → Var x ≡ᴹ Var x′
≡ᴹ-var refl = refl
```

```
≡ᴹ-ƛ : {M : Γ ▷ τ₁ ⊢ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ τ₂′}
  → M ≡ᴹ M′ → ƛ M ≡ᴹ ƛ M′
≡ᴹ-ƛ refl = refl
```

```
≡ᴹ-Λ : {M : Γ ▷★ κ ⊢ τ₂} {M′ : Γ′ ▷★ κ ⊢ τ₂′}
  → M ≡ᴹ M′ → Λ M ≡ᴹ Λ M′
≡ᴹ-Λ refl = refl
```

```
≡ᴹ-ƛʰ : {M : Γ ▷ τ₁ ⊢ σ ⇒ʰ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ σ′ ⇒ʰ τ₂′}
  → M ≡ᴹ M′ → ƛʰ M ≡ᴹ ƛʰ M′
≡ᴹ-ƛʰ refl = refl
```

```
≡ᴹ-name : Γ ≡ Γ′ → ℓ ≡ ℓ′ → name {Γ = Γ} ℓ ≡ᴹ name {Γ = Γ′} ℓ′
≡ᴹ-name refl refl = refl
```

```
≡ᴹ-, : {M : Γ ⊢ τ₁} {M′ : Γ′ ⊢ τ₁′} {N : Γ ⊢ τ₂ ⇒ᵖ τ} {N′ : Γ′ ⊢ τ₂′ ⇒ᵖ τ′}
  → M ≡ᴹ M′
  → N ≡ᴹ N′
  → (M , N) ≡ᴹ (M′ , N′)
≡ᴹ-, refl refl = refl
```

```
≡ᴹ-tt : Γ ≡ Γ′ → tt {Γ = Γ} ≡ᴹ tt {Γ = Γ′}
≡ᴹ-tt refl = refl
```

```
≡ᴹ-,ᵀ : ∀ {σ} {σ′} {σ[τ]} {σ[τ′]} {W : Γ ⊢ σ[τ] ⇒ᵖ τ₁} {W′ : Γ′ ⊢ σ[τ′] ⇒ᵖ τ₁′}
  → σ ≡ σ′
  → τ ≡ τ′
  → W ≡ᴹ W′
  → {σ[τ]≡ : σ [ τ ]★ ≡ σ[τ]}
  → {σ[τ′]≡ : σ′ [ τ′ ]★ ≡ σ[τ′]}
  → (_,ᵀ_⦅_⦆ {σ = σ} τ W σ[τ]≡) ≡ᴹ (_,ᵀ_⦅_⦆ {σ = σ′} τ′ W′ σ[τ′]≡)
≡ᴹ-,ᵀ refl refl refl {refl} {refl} = refl
```

```
uip-⊢★ : (p q : τ ≡ τ′) → p ≡ q
uip-⊢★ refl refl = refl
```

```
≡ᴹ-handle : ∀ {Γ : Context L Δ} {Γ′ : Context L′ Δ} {M : _} {M′ : _} {H : Γ ⊢ _} {H′ : Γ′ ⊢ _}
  → M ≡ᴹ M′
  → H ≡ᴹ H′
  → {wkn≡³ : weaken★ $ τ ≡ τ₁ × weaken★ $ σ ≡ σ₁ × weaken★ $ ε ≡ ε₁}
  → {wkn≡³′ : weaken★ $ τ′ ≡ τ₁′ × weaken★ $ σ′ ≡ σ₁′ × weaken★ $ ε′ ≡ ε₁′}
  → handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³ ≡ᴹ handle {τ = τ′} {σ = σ′} {ε = ε′} M′ H′ wkn≡³′
≡ᴹ-handle refl refl {`refl , `refl , `refl} {` wknτ≡ , ` wknσ≡ , ` wknε≡} with uip-⊢★ refl wknτ≡ | uip-⊢★ refl wknσ≡ | uip-⊢★ refl wknε≡
... | refl | refl | refl = refl
```

```
≡ᴹ-perform : ∀ {N : Γ ⊢ Name η} {N′ : Γ′ ⊢ Name η′} {V : Γ ⊢ σ ⇒ᵖ τ} {V′ : Γ′ ⊢ σ′ ⇒ᵖ τ′}
  → ε ≡ ε′
  → N ≡ᴹ N′
  → V ≡ᴹ V′
  → perform {ε = ε} N V ≡ᴹ perform {ε = ε′} N′ V′
≡ᴹ-perform refl refl refl = refl
```

```
≡ᴹ-Λʰ : ∀ {Γ : Context L Δ} {Γ′ : Context L Δ} {M : Γ ▷★ κ ⊢ σ ⇒ʰ τ₁} {M′ : Γ′ ▷★ κ ⊢ σ′ ⇒ʰ τ₁′}
  → τ ≡ τ′
  → M ≡ᴹ M′
  → {wkn≡ : weaken★ $ τ ≡ τ₁}
  → {wkn≡′ : weaken★ $ τ′ ≡ τ₁′}
  → _≡ᴹ_ {τ = _ ⇒ʰ τ} (Λʰ_ M wkn≡) {τ′ = _ ⇒ʰ τ′} (Λʰ_ M′ wkn≡′)
≡ᴹ-Λʰ refl refl {`refl} {`refl} = refl
```

```
≡ᴹ-Handler : ∀ {M : Γ ▷ (τ₁ ⇒ τ₂) ⊢ τ₂} {M′ : Γ′ ▷ (τ₁′ ⇒ τ₂′) ⊢ τ₂′}
  → M ≡ᴹ M′
  → Handler M ≡ᴹ Handler M′
≡ᴹ-Handler refl = refl
```

```
≡ᴹ-⦅⦆ : Γ ≡ Γ′ → τ ≡ τ′ → ⦅⦆ {Γ = Γ} {τ = τ} ≡ᴹ ⦅⦆ {Γ = Γ′} {τ = τ′}
≡ᴹ-⦅⦆ refl refl = refl
```


### Renaming and substitution of terms

```
private variable
  _#★_ _#★′_ _#★″_ : Labels × Contextᴷ → Kind → Set
  _#_ _#′_ _#″_ :  ∀ {L Δ} → Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Set
```

```
record PerfectKit★ (K : Kit★ _#★_) : Set where
  field
    ⦃ GK★ ⦄ : GoodKit★ K
    ⦃ K★-∘-∈ ⦄ : Kit★-∘ K K∈★
    ⦃ K★-∈-∘ ⦄ : Kit★-∘ K∈★ K
    ⦃ K★-∘-⊢ ⦄ : Kit★-∘ K K⊢★
    ⦃ K★-⊢-∘ ⦄ : Kit★-∘ K⊢★ K
```

```
instance
  PK∈★ : PerfectKit★ K∈★
  PK∈★ = record {}

  PK⊢★ : PerfectKit★ K⊢★
  PK⊢★ = record {}
```

```
record Kit (_#★_ : Labels × Contextᴷ → Kind → Set) (_#_ : ∀ {L Δ} → Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Set) : Set where
  no-eta-equality
  field
    K★ : Kit★ _#★_
    from-∈ : τ ∈ Γ → Γ # τ
    to-⊢ : Γ # τ → Γ ⊢ τ
    wkn : ∀ {τ′} → Γ # τ → (Γ ▷ τ′) # τ
    wknᵀ : ∀ {τ : L ⨟ Δ ⊢ᵀ TYP} → Γ # τ → (Γ ▷★ κ) # weaken★ τ

open Kit using (K★; from-∈; to-⊢; wkn; wknᵀ)
```

```
K∈ : Kit _∋★_ (λ Γ τ → τ ∈ Γ)
K∈ = record {
  K★ = K∈★ ;
  from-∈ = λ x → x ;
  to-⊢ = Var ;
  wkn = S_ ;
  wknᵀ = λ x → S★_ x `refl }
```

```
infix 1 ⦅_⦆_↠_
record ⦅_⦆_↠_ (K : Kit _#★_ _#_) (Γ : Context L Δ) (Γ′ : Context L′ Δ′) : Set where
  constructor _,_
  field
    var★ : ⦅ K★ K ⦆ (L , Δ) →★ (L′ , Δ′)
    tra∈ : τ ∈ Γ → Γ′ # tra★ var★ τ

open ⦅_⦆_↠_
```

```
tra▷ : {K : Kit _#★_ _#_}
  → (r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′)
  → ∀ {τ} → ⦅ K ⦆ Γ ▷ τ ↠ Γ′ ▷ tra★ r★ τ
var★ (tra▷ r) = var★ r
tra∈ (tra▷ {K = K} r) Z = from-∈ K Z
tra∈ (tra▷ {K = K} r) (S x) = wkn K (tra∈ r x)

tra▷★ : {K : Kit _#★_ _#_} ⦃ PK★ : PerfectKit★ (K★ K) ⦄
  → ⦅ K ⦆ Γ ↠ Γ′
  → ∀ {κ} → ⦅ K ⦆ Γ ▷★ κ ↠ Γ′ ▷★ κ
var★ (tra▷★ r) = tra★▷ (var★ r)
tra∈ (tra▷★ {_#_ = _#_} {K = K} r) (S★_ {τ = τ} x wkn≡) = subst (_ #_) (_$_≡_.unwrap (weaken★∘tra★∙ {τ = τ} wkn≡)) (wknᵀ K (tra∈ r x))
```

```
tra : {K : Kit _#★_ _#_} ⦃ PK★ : PerfectKit★ (K★ K) ⦄
  → (r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′)
  → Γ ⊢ τ → Γ′ ⊢ tra★ r★ τ
```

```
subst⊢ : Γ ≡ Γ′ → τ ≡ τ′ → Γ ⊢ τ → Γ′ ⊢ τ′
subst⊢ refl refl M = M
```

```
tra-tra▷★▷Z : ∀ {K : Kit _#★_ _#_} ⦃ PK : PerfectKit★ (K★ K) ⦄ {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → (r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′)
  → ∀ {τ} {σ} {ε}
  → Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′
  → (weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → Γ′ ▷★ SCO ▷ Name (Var Z) ⊢ weaken★ (tra★ r★ τ) / Var Z ⦂ weaken★ (tra★ r★ σ) ∷ weaken★ (tra★ r★ ε)
tra-tra▷★▷Z {K = K} {Γ′ = Γ′} r {τ} {σ} {ε} M wkn≡³ = subst⊢ eq2 (eq wkn≡³) (tra (tra▷ (tra▷★ r)) M)
  where
    eq2 : Γ′ ▷★ SCO ▷ Name (to-⊢★ (K★ K) (from-∈★ (K★ K) Z)) ≡ Γ′ ▷★ SCO ▷ Name (Var Z)
    eq2 = cong (Γ′ ▷★ SCO ▷_) (cong Name (to★-from★ it Z))
    eq : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′
      → tra★ (tra★▷ (var★ r)) τ′ / (to-⊢★ (K★ K) (from-∈★ (K★ K) Z) ⦂ tra★ (tra★▷ (var★ r)) σ′ ∷ tra★ (tra★▷ (var★ r)) ε′)
        ≡ weaken★ (tra★ (var★ r) τ) / Var Z ⦂ weaken★ (tra★ (var★ r) σ) ∷ weaken★ (tra★ (var★ r) ε)
    eq (`refl , `refl , `refl) = cong₂ _/_ (sym (weaken★∘tra★ τ)) (cong₃ _⦂_∷_ (to★-from★ it Z) (sym (weaken★∘tra★ σ)) (sym (weaken★∘tra★ ε)))
```

```
tra {K = K} r (Var x) = to-⊢ K (tra∈ r x)
tra r (ƛ M) = ƛ tra (tra▷ r) M
tra r (Λ V) = Λ tra (tra▷★ r) V
tra r tt = tt
tra r (return V) = return (tra r V)
tra r@((rᴸ , rᴰ) , _) (name ℓ) = name (ren∈ᴸ rᴸ ℓ)
tra r (M >>= N) = tra r M >>= tra (tra▷ r) N
tra r (appᵛ W V) = appᵛ (tra r W) (tra r V)
tra r@(r★ , _) (appᵀ {σ = τ} V σ τ[σ]≡) = appᵀ (tra r V) (tra★ r★ σ) (tra★∘[]★∙ τ τ[σ]≡)
tra r (perform ℓ V) = perform (tra r ℓ) (tra r V)
tra r (handle M H wkn≡³) = handle (tra-tra▷★▷Z r M wkn≡³) (tra r H) (`refl , `refl , `refl)
tra r (handle′ ℓ∉ M H) = handle′ (tra∉ _ ℓ∉) (tra r M) (tra r H)
tra r (subeffect ε⊑ε′ M) = subeffect (tra⊑ _ ε⊑ε′) (tra r M)
tra r (V , W) = tra r V , tra r W
tra r ⦅⦆ = ⦅⦆
tra r@(r★@(rᴸ , rᴰ) , _) (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = tra★ r★ τ ,ᵀ tra r M ⦅ tra★∘[]★∙ σ σ[τ]≡ ⦆
tra r (Λʰ_ H wkn≡) = Λʰ_ (tra (tra▷★ r) H) (weaken★∘tra★∙ wkn≡)
tra r (ƛʰ H) = ƛʰ (tra (tra▷ r) H)
tra r (Handler H) = Handler (tra (tra▷ r) H)
```

```
infix 2 _→ᵍ_
_→ᵍ_ : (Γ : Context L Δ) (Γ′ : Context L′ Δ′) → Set
Γ →ᵍ Γ′ = ⦅ K∈ ⦆ Γ ↠ Γ′
```

```
↠-id : Γ →ᵍ Γ
var★ ↠-id = →★-id
tra∈ ↠-id x = subst (_∈ _) (sym (tra★-id _)) x
```

```
↠-S : Γ →ᵍ Γ ▷ τ
var★ ↠-S = →★-id
tra∈ ↠-S x = tra∈ ↠-id (S x)
```

```
weaken : Γ ⊢ τ → Γ ▷ τ′ ⊢ τ
weaken M = subst (_ ⊢_) (tra★-id _) (tra ↠-S M)
```

```
↠-S★ : Γ →ᵍ Γ ▷★ κ
var★ ↠-S★ = →★-S★
tra∈ ↠-S★ x = S★_ x `refl
```

```
weakenᵀ : ∀ {κ} → Γ ⊢ τ → Γ ▷★ κ ⊢ weaken★ τ
weakenᵀ = tra ↠-S★
```

```
K⊢ : Kit _⊢★_ (λ Γ τ → Γ ⊢ τ)
K⊢ = record {
  K★ = K⊢★ ;
  from-∈ = Var ;
  to-⊢ = λ M → M ;
  wkn = weaken ;
  wknᵀ = weakenᵀ }
```

```
infix 2 _↠_
_↠_ : (Γ : Context L Δ) (Γ′ : Context L′ Δ′) → Set
Γ ↠ Γ′ = ⦅ K⊢ ⦆ Γ ↠ Γ′
```

```
renᵍᴸ : (rᴸ : L →ᴸ L′) → Context L Δ → Context L′ Δ
renᵍᴸ rᴸ ∅ = ∅
renᵍᴸ rᴸ (Γ ▷ τ) = renᵍᴸ rᴸ Γ ▷ ren★L rᴸ τ
renᵍᴸ rᴸ (Γ ▷★ κ) = renᵍᴸ rᴸ Γ ▷★ κ

weakenᵍᴸ : Context L Δ → Context (L ▷-) Δ
weakenᵍᴸ = renᵍᴸ →ᴸ-S
```

```
→ᴸ⇒→ᵍ : (rᴸ : L →ᴸ L′) → ∀ {Γ : Context L Δ} → Γ →ᵍ renᵍᴸ rᴸ Γ
var★ (→ᴸ⇒→ᵍ rᴸ) = →ᴸ⇒→★ rᴸ
tra∈ (→ᴸ⇒→ᵍ rᴸ) Z = Z
tra∈ (→ᴸ⇒→ᵍ rᴸ) (S x) = S (tra∈ (→ᴸ⇒→ᵍ rᴸ) x)
tra∈ (→ᴸ⇒→ᵍ rᴸ) (S★_ {τ = τ} x wkn≡) = S★_ (tra∈ (→ᴸ⇒→ᵍ rᴸ) x) (weaken★∘ren★L∙ {τ = τ} wkn≡)

renᴸ : (rᴸ : L →ᴸ L′) → Γ ⊢ τ → renᵍᴸ rᴸ Γ ⊢ ren★L rᴸ τ
renᴸ rᴸ = tra (→ᴸ⇒→ᵍ rᴸ)

weakenᴸ : Γ ⊢ τ → weakenᵍᴸ Γ ⊢ weaken★L τ
weakenᴸ = renᴸ →ᴸ-S
```

```
infix 3 _≡[↠]_

record _≡[↠]_ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} {Γ : Context L Δ} {Γ′ Γ″ : Context L′ Δ′} (r : ⦅ K ⦆ Γ ↠ Γ′) (r′ : ⦅ K′ ⦆ Γ ↠ Γ″) : Set where
  constructor [_,_,_]
  field
    ≡[↠]⇒≡ᵍ : Γ′ ≡ Γ″
    ≡[↠]⇒≡[↠★] : var★ r ≡[↠★] var★ r′
    tra∈-≡ : ∀ {τ} (x : τ ∈ Γ) → to-⊢ K (tra∈ r x) ≡ᴹ to-⊢ K′ (tra∈ r′ x)

open _≡[↠]_
```

```
≡-weaken-S : {x : τ ∈ Γ} → weaken {τ′ = τ′} (Var x) ≡ Var (S_ {τ₂ = τ′} x)
≡-weaken-S {τ = τ} = aux (tra★-id τ)
  where
    aux : ∀ {Γ : Context L Δ} {τ} {τ′} (τ≡ : τ ≡ τ′) {x : τ′ ∈ Γ} → subst (Γ ⊢_) τ≡ (Var (subst (_∈ Γ) (sym τ≡) x)) ≡ Var x
    aux refl = refl
```

```
open Kit

record GoodKit (K : Kit _#★_ _#_) : Set where
  field
    ⦃ PK★ ⦄ : PerfectKit★ (K★ K)
    to-from : ∀ (x : τ ∈ Γ) → to-⊢ K (from-∈ K x) ≡ Var x
    to-wkn : ∀ {τ′} (x : Γ # τ) → to-⊢ K (wkn K {τ′ = τ′} x) ≡ weaken (to-⊢ K x)
    to-wknᵀ : ∀ (x : Γ # τ) → to-⊢ K (wknᵀ K x) ≡ weakenᵀ {κ = κ} (to-⊢ K x)

open GoodKit ⦃ ... ⦄
```

```
instance
  GK∈ : GoodKit K∈
  GK∈ = record {
    to-from = λ x → refl ;
    to-wkn = λ x → sym ≡-weaken-S ;
    to-wknᵀ = λ x → refl }

  GK⊢ : GoodKit K⊢
  GK⊢ = record {
    to-from = λ x → refl ;
    to-wkn = λ x → refl ;
    to-wknᵀ = λ x → refl }
```

```
to-from-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → to-⊢ K (from-∈ K x) ≡ᴹ to-⊢ K′ (from-∈ K′ x′)
to-from-≡ {x = x} {x′} x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-from x)) (≡ᴹ-trans (≡ᴹ-var x≡) (≡ᴹ-refl (sym (to-from x′))))
```

```
≡ᴹ-weaken : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → τ₁ ≡ τ₁′
  → M ≡ᴹ M′
  → weaken {τ′ = τ₁} M ≡ᴹ weaken {τ′ = τ₁′} M′
≡ᴹ-weaken refl refl = refl
```

```
≡ᴹ-weakenᵀ : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → M ≡ᴹ M′ → weakenᵀ {κ = κ} M ≡ᴹ weakenᵀ M′
≡ᴹ-weakenᵀ refl = refl
```

```
to-wkn-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : Γ # τ} {x′ : Γ′ #′ τ′}
  → τ₁ ≡ τ₂
  → to-⊢ K x ≡ᴹ to-⊢ K′ x′
  → to-⊢ K (wkn K {τ′ = τ₁} x) ≡ᴹ to-⊢ K′ (wkn K′ {τ′ = τ₂} x′)
to-wkn-≡ {x = x} {x′} τ≡ x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-wkn x)) (≡ᴹ-trans (≡ᴹ-weaken τ≡ x≡) (≡ᴹ-refl (sym (to-wkn x′))))
```

```
to-wknᵀ-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : Γ # τ} {x′ : Γ′ #′ τ′}
  → τ₁ ≡ τ₂
  → to-⊢ K x ≡ᴹ to-⊢ K′ x′
  → to-⊢ K (wknᵀ K {κ = κ} x) ≡ᴹ to-⊢ K′ (wknᵀ K′ x′)
to-wknᵀ-≡ {x = x} {x′} τ≡ x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-wknᵀ x)) (≡ᴹ-trans (≡ᴹ-weakenᵀ x≡) (≡ᴹ-refl (sym (to-wknᵀ x′))))
```

```
tra▷-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {τ} → tra▷ r {τ} ≡[↠] tra▷ r′
≡[↠]⇒≡ᵍ (tra▷-≡ r≡ {τ}) = cong₂ _▷_ (≡[↠]⇒≡ᵍ r≡) (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ)
≡[↠]⇒≡[↠★] (tra▷-≡ r≡) = ≡[↠]⇒≡[↠★] r≡
tra∈-≡ (tra▷-≡ r≡ {τ}) Z = to-from-≡ (≡[∈]-Z (≡[↠]⇒≡ᵍ r≡) (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ))
tra∈-≡ (tra▷-≡ r≡ {τ}) (S x) = to-wkn-≡ (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ) (tra∈-≡ r≡ x)
```

```
to-subst : ∀ {K : Kit _#★_ _#_} {τ τ′ : L ⨟ Δ ⊢ᵀ TYP}
  → (τ≡ : τ ≡ τ′)
  → (x : Γ # τ)
  → to-⊢ K (subst (Γ #_) τ≡ x) ≡ᴹ to-⊢ K x
to-subst refl x = refl
```

```
tra▷★-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {κ} → tra▷★ r {κ} ≡[↠] tra▷★ r′
≡[↠]⇒≡ᵍ (tra▷★-≡ r≡ {κ}) = cong (_▷★ κ) (≡[↠]⇒≡ᵍ r≡)
≡[↠]⇒≡[↠★] (tra▷★-≡ r≡) = tra★▷-≡ (≡[↠]⇒≡[↠★] r≡)
tra∈-≡ (tra▷★-≡ {K = K} {K′} {r = r} {r′} r≡ {κ}) (S★_ x wkn≡) =
  ≡ᴹ-trans (≡ᴹ-trans (to-subst {K = K} _ (wknᵀ K (tra∈ r x))) (≡ᴹ-refl (to-wknᵀ (tra∈ r x))))
    (≡ᴹ-trans (≡ᴹ-weakenᵀ (tra∈-≡ r≡ x))
      (≡ᴹ-sym (≡ᴹ-trans (to-subst {K = K′} _ (wknᵀ K′ (tra∈ r′ x))) (≡ᴹ-refl (to-wknᵀ (tra∈ r′ x)))))) -- to-wkn-≡ (tra★-≡ (≡[↠]⇒≡[↠★] r≡) κ) (tra∈-≡ r≡ x)
```

```
≡ᴹ-subst⊢ :
    (Γ≡ : Γ ≡ Γ′)
    (τ≡ : τ ≡ τ′)
    (M : Γ ⊢ τ)
  → subst⊢ Γ≡ τ≡ M ≡ᴹ M
≡ᴹ-subst⊢ refl refl M = refl
```

```
tra-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {κ} {τ : _ ⨟ _ ⊢ᵀ κ} (M : Γ ⊢ τ)
  → tra r M ≡ᴹ tra r′ M
```

```
tra-tra▷★▷Z-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK : GoodKit K′ ⦄
  → {Γ : Context L Δ} {Γ′ Γ″ : Context L′ Δ′}
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {τ σ ε τ′ σ′ ε′} (M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
  → (wkn≡³ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → tra-tra▷★▷Z r {τ = τ} {σ} {ε} M wkn≡³ ≡ᴹ tra-tra▷★▷Z r′ {τ = τ} {σ} {ε} M wkn≡³
tra-tra▷★▷Z-≡ {r = r} {r′} r≡ M wkn≡³ =
  subst⊢ _ _ (tra (tra▷ (tra▷★ r)) M) ≡⟨ ≡ᴹ-subst⊢ _ _ (tra _ M) ⟩
  tra (tra▷ (tra▷★ r)) M ≡⟨ tra-≡ (tra▷-≡ (tra▷★-≡ r≡)) M ⟩
  tra (tra▷ (tra▷★ r′)) M ≡⟨ ≡ᴹ-sym (≡ᴹ-subst⊢ _ _ (tra _ M)) ⟩
  subst⊢ _ _ (tra (tra▷ (tra▷★ r′)) M) ∎
  where open ≡ᴹ-Reasoning
```

```
tra-≡ r≡ (Var x) = tra∈-≡ r≡ x
tra-≡ r≡ (ƛ M) = ≡ᴹ-ƛ (tra-≡ (tra▷-≡ r≡) M)
tra-≡ r≡ (Λ M) = ≡ᴹ-Λ (tra-≡ (tra▷★-≡ r≡) M)
tra-≡ r≡@([ Γ≡ , _ , _ ]) tt = ≡ᴹ-tt Γ≡
tra-≡ r≡@([ Γ≡ , _ , _ ]) (name ℓ) = ≡ᴹ-name Γ≡ (ren∈ᴸ-irr ℓ)
tra-≡ r≡@([ Γ≡ , r★≡ , _ ]) (⦅⦆ {τ = τ}) = ≡ᴹ-⦅⦆ Γ≡ (tra★-≡ r★≡ τ)
tra-≡ r≡ (V , W) = ≡ᴹ-, (tra-≡ r≡ V) (tra-≡ r≡ W)
tra-≡ r≡@([ _ , r★≡ , _ ]) (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = ≡ᴹ-,ᵀ (tra★-≡ (tra★▷-≡ r★≡) σ) (tra★-≡ r★≡ τ) (tra-≡ r≡ M)
tra-≡ r≡@([ _ , r★≡ , _ ]) (return {ε = ε} M) = ≡ᴹ-return (tra★-≡ r★≡ ε) (tra-≡ r≡ M)
tra-≡ r≡ (M >>= N) = ⦅ tra-≡ r≡ M >>= tra-≡ (tra▷-≡ r≡) N ⦆
tra-≡ r≡ (appᵛ V W) = ≡ᴹ-appᵛ (tra-≡ r≡ V) (tra-≡ r≡ W)
tra-≡ r≡@([ _ , r★≡ , _ ]) (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-≡ r≡ V) (tra★-≡ r★≡ τ)
tra-≡ r≡@([ _ , r★≡ , _ ]) (perform {ε = ε} N V) = ≡ᴹ-perform (tra★-≡ r★≡ ε) (tra-≡ r≡ N) (tra-≡ r≡ V)
tra-≡ r≡ (handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³) = ≡ᴹ-handle (tra-tra▷★▷Z-≡ r≡ {τ = τ} {σ} {ε} M wkn≡³) (tra-≡ r≡ H)
tra-≡ r≡@([ _ , r★≡ , _ ]) (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-≡ r★≡ ℓ∉) (tra-≡ r≡ M) (tra-≡ r≡ H)
tra-≡ r≡@([ _ , r★≡ , _ ]) (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-≡ r★≡ ε⊑) (tra-≡ r≡ M)
tra-≡ r≡@([ _ , r★≡ , _ ]) (Λʰ_ {τ/ε = τ/ε} H wkn≡) = ≡ᴹ-Λʰ (tra★-≡ r★≡ τ/ε) (tra-≡ (tra▷★-≡ r≡) H)
tra-≡ r≡ (ƛʰ H) = ≡ᴹ-ƛʰ (tra-≡ (tra▷-≡ r≡) H)
tra-≡ r≡ (Handler H) = ≡ᴹ-Handler (tra-≡ (tra▷-≡ r≡) H)
```

```
tra▷-id : ∀ {τ} → tra▷ (↠-id {Γ = Γ}) {τ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ tra▷-id = cong (_ ▷_) (tra★-id _)
≡[↠]⇒≡[↠★] tra▷-id = ≡[↠★]-refl
tra∈-≡ tra▷-id Z = to-from-≡ {K = K∈} {K′ = K∈} (≡[∈]-trans (≡[∈]-Z refl (tra★-id _)) (≡[∈]-sym (≡[∈]-subst _)))
tra∈-≡ (tra▷-id {τ = τ}) (S x) =
  to-⊢ K∈ (wkn K∈ (from-∈ K∈ (subst (_∈ _) _ x))) ≡⟨ ≡ᴹ-refl (to-wkn _) ⟩
  weaken (to-⊢ K∈ (from-∈ K∈ (subst (_∈ _) _ x))) ≡⟨ ≡ᴹ-weaken refl (≡ᴹ-refl (to-from {K = K∈} _)) ⟩
  weaken (Var (subst (_∈ _) _ x)) ≡⟨ ≡ᴹ-refl ≡-weaken-S ⟩
  Var (S (subst (_∈ _) _ x)) ≡⟨ ≡ᴹ-var (≡[∈]-S (tra★-id τ) (≡[∈]-subst _)) ⟩
  Var (S x) ≡⟨ ≡ᴹ-var (≡[∈]-sym (≡[∈]-subst _)) ⟩
  Var (subst (_∈ _) _ (S x)) ≡⟨ ≡ᴹ-sym (≡ᴹ-refl (to-from {K = K∈} _)) ⟩
  to-⊢ K∈ (from-∈ K∈ (subst (_∈ _) _ (S x))) ∎
  where open ≡ᴹ-Reasoning
```

```
subst# : (K : Kit _#★_ _#_)
  → {Γ : Context L Δ} {τ τ′ : L ⨟ Δ ⊢ᵀ TYP}
  → {τ≡ : τ ≡ τ′}
  → (M : Γ # τ)
  → to-⊢ K (subst (_ #_) τ≡ M) ≡ᴹ to-⊢ K M
subst# K {τ≡ = refl} M = refl
```

```
tra∈-id :
    {Γ : Context L Δ}
  → {x : τ ∈ Γ}
  → to-⊢ K∈ (tra∈ ↠-id x) ≡ᴹ Var x
tra∈-id = ≡ᴹ-trans (≡ᴹ-refl (to-from {K = K∈} _)) (≡ᴹ-var (≡[∈]-subst _))
```

```
tra▷★-id : ∀ {κ} → tra▷★ (↠-id {Γ = Γ}) {κ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ tra▷★-id = refl
≡[↠]⇒≡[↠★] tra▷★-id = tra★▷-id
tra∈-≡ tra▷★-id (S★_ x wkn≡) =
  to-⊢ K∈ (tra∈ (tra▷★ ↠-id) ((S★ x) wkn≡)) ≡⟨ subst# K∈ _ ⟩
  to-⊢ K∈ (wknᵀ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-refl (to-wknᵀ {K = K∈} _) ⟩
  weakenᵀ (to-⊢ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-weakenᵀ tra∈-id ⟩
  Var (S★_ x `refl)               ≡⟨ ≡ᴹ-var (≡[∈]-S★ refl) ⟩
  Var (S★_ x wkn≡)                ≡⟨ ≡ᴹ-sym tra∈-id ⟩
  to-⊢ K∈ (tra∈ ↠-id ((S★ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
≡[↠]-trans : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} {K″ : Kit _#★″_ _#″_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ GK″ : GoodKit K″ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K ⦆ Γ ↠ Γ″} {r″ : ⦅ K ⦆ Γ ↠ Γ³}
  → r ≡[↠] r′
  → r′ ≡[↠] r″
  → r ≡[↠] r″
≡[↠]-trans [ Γ≡ , r★≡ , tra∈≡ ] [ Γ′≡ , r★′≡ , tra∈′≡ ] = [ trans Γ≡ Γ′≡ , ≡[↠★]-trans r★≡ r★′≡ , (λ x → ≡ᴹ-trans (tra∈≡ x) (tra∈′≡ x)) ]
```

```
tra-id : ∀ (M : Γ ⊢ τ)
  → tra ↠-id M ≡ᴹ M
```

```
tra-id▷ : ∀ (M : Γ ▷ τ′ ⊢ τ) → tra (tra▷ ↠-id) M ≡ᴹ M
tra-id▷ M = ≡ᴹ-trans (tra-≡ tra▷-id M) (tra-id M)
```

```
tra-id▷★ : ∀ (M : Γ ▷★ κ ⊢ τ) → tra (tra▷★ ↠-id) M ≡ᴹ M
tra-id▷★ M = ≡ᴹ-trans (tra-≡ tra▷★-id M) (tra-id M)
```

```
tra-tra▷★▷Z-id : ∀ {τ} {σ} {ε}
  → ∀ (M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
  → (wkn≡³ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → tra-tra▷★▷Z ↠-id {τ = τ} {σ} {ε} M wkn≡³ ≡ᴹ M
tra-tra▷★▷Z-id M wkn≡³ = ≡ᴹ-trans (≡ᴹ-subst⊢ _ _ (tra _ M)) (≡ᴹ-trans (tra-≡ (≡[↠]-trans {K′ = K∈} {K″ = K∈} (tra▷-≡ tra▷★-id) tra▷-id) M) (tra-id M))
```

```
tra-id (Var x) = ≡ᴹ-trans (≡ᴹ-refl (to-from {K = K∈} _)) (≡ᴹ-var (≡[∈]-subst _))
tra-id (ƛ M) = ≡ᴹ-ƛ (tra-id▷ M)
tra-id (Λ M) = ≡ᴹ-Λ (tra-id▷★ M)
tra-id tt = refl
tra-id (name ℓ) = refl
tra-id ⦅⦆ = ≡ᴹ-⦅⦆ refl (tra★-id _)
tra-id (V , W) = ≡ᴹ-, (tra-id V) (tra-id W)
tra-id (τ ,ᵀ M ⦅ σ[τ]≡ ⦆) = ≡ᴹ-,ᵀ (tra★-id▷ _) (tra★-id τ) (tra-id M)
tra-id (return M) = ≡ᴹ-return (tra★-id _) (tra-id M)
tra-id (M >>= N) = ⦅ tra-id M >>= tra-id▷ N ⦆
tra-id (appᵛ V W) = ≡ᴹ-appᵛ (tra-id V) (tra-id W)
tra-id (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-id V) (tra★-id τ)
tra-id (perform N V) = ≡ᴹ-perform (tra★-id _) (tra-id N) (tra-id V)
tra-id (handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³) = ≡ᴹ-handle (tra-tra▷★▷Z-id M wkn≡³) (tra-id H)
tra-id (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-id ℓ∉) (tra-id M) (tra-id H)
tra-id (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-id ε⊑) (tra-id M)
tra-id ((Λʰ H) wkn≡) = ≡ᴹ-Λʰ (tra★-id _) (tra-id▷★ H)
tra-id (ƛʰ H) = ≡ᴹ-ƛʰ (tra-id▷ H)
tra-id (Handler M) = ≡ᴹ-Handler (tra-id▷ M)
```

```
infix 5 _∘[↠]_
_∘[↠]_ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ _ : GoodKit K ⦄ ⦃ _ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄
  → (r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″) (r : ⦅ K ⦆ Γ ↠ Γ′)
  → ⦅ K⊢ ⦆ Γ ↠ Γ″
var★ (r′ ∘[↠] r) = var★ r′ ∘[↠★] var★ r
tra∈ (_∘[↠]_ {K = K} r′ r) {τ = τ} x = subst (_ ⊢_) (tra★-∘ {r′ = var★ r′} {r = var★ r} τ) (tra r′ (to-⊢ K (tra∈ r x)))
```

```
tra∈-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ _ : GoodKit K ⦄ ⦃ _ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′} {Γ″ : Context L″ Δ″}
  → (r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″) (r : ⦅ K ⦆ Γ ↠ Γ′)
  → ∀ (x : τ ∈ Γ)
  → tra∈ (r′ ∘[↠] r) x ≡ᴹ tra r′ (to-⊢ K (tra∈ r x))
tra∈-∘ r′ r x = ≡ᴹ-subst _ _
```

```
≡ᴹ-tra : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → {M : Γ ⊢ τ} {M′ : Γ ⊢ τ′}
  → M ≡ᴹ M′
  → tra r M ≡ᴹ tra r M′
≡ᴹ-tra refl = refl
```

```
K⊢-to-⊢ : ∀ {M : Γ ⊢ τ} → to-⊢ K⊢ M ≡ᴹ M
K⊢-to-⊢ = ≡ᴹ-subst _ _
```

```
record Kit-∘ (K : Kit _#★_ _#_) (K′ : Kit _#★′_ _#′_) ⦃ _ : PerfectKit★ (K★ K′) ⦄ : Set where
  field
    ⦃ K★-∘ ⦄ : Kit★-∘ (K★ K) (K★ K′)
    tra∘wkn : ∀ {r′ : ⦅ K′ ⦆ Γ ↠ Γ′} (m : Γ # τ) → tra (tra▷ r′ {τ′}) (to-⊢ K (wkn K m)) ≡ weaken (tra r′ (to-⊢ K m))
    tra∘wknᵀ : ∀ {r′ : ⦅ K′ ⦆ Γ ↠ Γ′} (m : Γ # τ) → tra (tra▷★ r′ {κ}) (to-⊢ K (wknᵀ K m)) ≡ᴹ weakenᵀ (tra r′ (to-⊢ K m))

open Kit-∘ ⦃ ... ⦄
```

```
tra▷-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″} 
  → ∀ {τ}
  → tra▷ r′ ∘[↠] tra▷ r {τ} ≡[↠] tra▷ (r′ ∘[↠] r)
≡[↠]⇒≡ᵍ (tra▷-∘ {τ = τ}) = cong (_ ▷_) (tra★-∘ τ)
≡[↠]⇒≡[↠★] tra▷-∘ = ≡[↠★]-refl
tra∈-≡ (tra▷-∘ {r = r} {r′} {τ = τ}) Z = ≡ᴹ-trans (≡ᴹ-subst (tra★-∘ τ) _) (≡ᴹ-trans (≡ᴹ-tra {r = tra▷ r′} (≡ᴹ-refl (to-from Z))) (≡ᴹ-trans (≡ᴹ-refl (to-from Z)) (≡ᴹ-var (≡[∈]-Z refl (tra★-∘ τ)))))
tra∈-≡ (tra▷-∘ {K = K} {r = r} {r′} {τ = τ}) (S x) =
  to-⊢ K⊢ (tra∈ (tra▷ r′ ∘[↠] tra▷ r {τ}) (S x)) ≡⟨ K⊢-to-⊢ ⟩
  tra∈ (tra▷ r′ ∘[↠] tra▷ r {τ}) (S x) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra (tra▷ r′) (to-⊢ K (wkn K (tra∈ r x))) ≡⟨ ≡ᴹ-refl (tra∘wkn (tra∈ r x)) ⟩
  weaken (tra r′ (to-⊢ K (tra∈ r x))) ≡⟨ ≡ᴹ-weaken (tra★-∘ τ) (
    tra r′ (to-⊢ K (tra∈ r x)) ≡⟨ ≡ᴹ-sym (tra∈-∘ r′ r x) ⟩
    tra∈ (r′ ∘[↠] r) x ∎) ⟩
  tra∈ (tra▷ (r′ ∘[↠] r) {τ}) (S x) ≡⟨ ≡ᴹ-sym K⊢-to-⊢ ⟩
  to-⊢ K⊢ (tra∈ (tra▷ (r′ ∘[↠] r) {τ}) (S x)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra▷★-S★ : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (x : τ ∈ Γ)
  → (wkn≡ : weaken★ {κ′ = κ} $ τ ≡ τ′)
  → to-⊢ K (tra∈ (tra▷★ r) (S★_ x wkn≡)) ≡ᴹ to-⊢ K (wknᵀ K (tra∈ r x))
tra▷★-S★ {K = K} x wkn≡ = subst# K _
```

```
tra▷★-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″} 
  → ∀ {κ}
  → tra▷★ r′ ∘[↠] tra▷★ r {κ} ≡[↠] tra▷★ (r′ ∘[↠] r)
≡[↠]⇒≡ᵍ tra▷★-∘ = refl
≡[↠]⇒≡[↠★] tra▷★-∘ = tra★▷-∘
tra∈-≡ (tra▷★-∘ {K = K} {K′ = K′} {r = r} {r′} {κ = κ}) (S★_ x wkn≡) =
  to-⊢ K⊢ (tra∈ (tra▷★ r′ ∘[↠] tra▷★ r) ((S★ x) wkn≡)) ≡⟨ K⊢-to-⊢ ⟩
  tra∈ (tra▷★ r′ ∘[↠] tra▷★ r) ((S★ x) wkn≡) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra (tra▷★ r′) (to-⊢ K (tra∈ (tra▷★ r) (S★_ x wkn≡))) ≡⟨ ≡ᴹ-tra {r = tra▷★ r′} (tra▷★-S★ {r = r} x wkn≡) ⟩
  tra (tra▷★ r′) (to-⊢ K (wknᵀ K (tra∈ r x))) ≡⟨ tra∘wknᵀ (tra∈ r x) ⟩
  weakenᵀ (tra r′ (to-⊢ K (tra∈ r x))) ≡⟨ ≡ᴹ-weakenᵀ (≡ᴹ-sym (tra∈-∘ r′ r x)) ⟩
  weakenᵀ (tra∈ (r′ ∘[↠] r) x) ≡⟨ ≡ᴹ-sym (tra▷★-S★ {r = r′ ∘[↠] r} x wkn≡) ⟩
  tra∈ (tra▷★ (r′ ∘[↠] r)) ((S★ x) wkn≡) ≡⟨ ≡ᴹ-sym K⊢-to-⊢ ⟩
  to-⊢ K⊢ (tra∈ (tra▷★ (r′ ∘[↠] r)) ((S★ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ⊢ τ)
  → tra r′ (tra r M) ≡ᴹ tra (r′ ∘[↠] r) M

tra-∘▷ :  ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ▷ τ′ ⊢ τ)
  → tra (tra▷ r′) (tra (tra▷ r) M) ≡ᴹ tra (tra▷ (r′ ∘[↠] r)) M
tra-∘▷ {r = r} {r′} M = ≡ᴹ-trans (tra-∘ M) (tra-≡ tra▷-∘ M)

tra-∘▷★ :  ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ▷★ κ ⊢ τ)
  → tra (tra▷★ r′) (tra (tra▷★ r) M) ≡ᴹ tra (tra▷★ (r′ ∘[↠] r)) M
tra-∘▷★ M = ≡ᴹ-trans (tra-∘ M) (tra-≡ tra▷★-∘ M)

≡ᵍ⇒↠ : Γ ≡ Γ′ → ⦅ K∈ ⦆ Γ ↠ Γ′
≡ᵍ⇒↠ refl = ↠-id

subst⊢⇒tra :
    (Γ≡ : Γ ≡ Γ′)
  → (τ≡ : τ ≡ τ′)
  → (M : Γ ⊢ τ)
  → subst⊢ Γ≡ τ≡ M ≡ᴹ tra (≡ᵍ⇒↠ Γ≡) M
subst⊢⇒tra refl refl M = ≡ᴹ-sym (tra-id M)

tra-subst⊢-cong : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (τ₁≡ : τ₁ ≡ τ₁′)
  → (τ≡ : τ ≡ τ′)
  → (M : Γ ▷ τ₁ ⊢ τ)
  → tra (tra▷ r) (subst⊢ (cong (_ ▷_) τ₁≡) τ≡ M) ≡ᴹ tra (tra▷ r) M
tra-subst⊢-cong refl refl M = refl

tra-tra▷★▷Z-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′} {Γ″ : Context L″ Δ″}
  → {r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ {τ} {σ} {ε}
  → ∀ (M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
  → (wkn≡³ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → tra-tra▷★▷Z r′ {τ = tra★ r★ τ} {σ = tra★ r★ σ} {ε = tra★ r★ ε} (tra-tra▷★▷Z r {τ = τ} {σ} {ε} M wkn≡³) (`refl , `refl , `refl) ≡ᴹ tra-tra▷★▷Z (r′ ∘[↠] r) {τ = τ} {σ} {ε} M wkn≡³
tra-tra▷★▷Z-∘ {K′ = K′} {r = r@(r★ , _)} {r′} {τ = τ} {σ} {ε} M wkn≡³ =
  tra-tra▷★▷Z r′ {τ = tra★ r★ τ} {σ = tra★ r★ σ} {ε = tra★ r★ ε} (tra-tra▷★▷Z r {τ = τ} {σ} {ε} M wkn≡³) (`refl , `refl , `refl) ≡⟨ ≡ᴹ-subst⊢ _ _ _ ⟩
  tra (tra▷ (tra▷★ r′)) (tra-tra▷★▷Z r {τ = τ} {σ} {ε} M wkn≡³) ≡⟨ tra-subst⊢-cong (cong Name (to★-from★ it Z)) _ (tra _ M) ⟩
  tra (tra▷ (tra▷★ r′)) (tra (tra▷ (tra▷★ r)) M) ≡⟨ tra-∘ M ⟩
  tra (tra▷ (tra▷★ r′) ∘[↠] tra▷ (tra▷★ r)) M ≡⟨ tra-≡ (≡[↠]-trans {K′ = K⊢} {K″ = K⊢} tra▷-∘ (tra▷-≡ tra▷★-∘)) M ⟩
  tra (tra▷ (tra▷★ (r′ ∘[↠] r))) M ≡⟨ ≡ᴹ-sym (≡ᴹ-subst⊢ _ _ _) ⟩
  tra-tra▷★▷Z (r′ ∘[↠] r) {τ = τ} {σ} {ε} M wkn≡³ ∎
  where
    open ≡ᴹ-Reasoning

tra-∘ {r = r} {r′} (Var x) = ≡ᴹ-sym (tra∈-∘ r′ r x)
tra-∘ (ƛ M) = ≡ᴹ-ƛ (tra-∘▷ M)
tra-∘ (Λ M) = ≡ᴹ-Λ (tra-∘▷★ M)
tra-∘ tt = refl
tra-∘ {r′ = ((rᴸ′ , _) , _)} (name ℓ) = ≡ᴹ-name refl (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ)
tra-∘ (⦅⦆ {τ = τ}) = ≡ᴹ-⦅⦆ refl (tra★-∘ τ)
tra-∘ (V , W) = ≡ᴹ-, (tra-∘ V) (tra-∘ W)
tra-∘ (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = ≡ᴹ-,ᵀ (tra★-∘▷ σ) (tra★-∘ τ) (tra-∘ M)
tra-∘ (return {ε = ε} M) = ≡ᴹ-return (tra★-∘ ε) (tra-∘ M)
tra-∘ (M >>= N) = ⦅ tra-∘ M >>= tra-∘▷ N ⦆
tra-∘ (appᵛ V W) = ≡ᴹ-appᵛ (tra-∘ V) (tra-∘ W)
tra-∘ (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-∘ V) (tra★-∘ τ)
tra-∘ (perform {ε = ε} N V) = ≡ᴹ-perform (tra★-∘ ε) (tra-∘ N) (tra-∘ V)
tra-∘ (handle M H wkn≡³) = ≡ᴹ-handle (tra-tra▷★▷Z-∘ M wkn≡³) (tra-∘ H)
tra-∘ (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-∘ ℓ∉) (tra-∘ M) (tra-∘ H)
tra-∘ (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-∘ ε⊑) (tra-∘ M)
tra-∘ (Λʰ_ {τ/ε = τ/ε} H wkn≡) = ≡ᴹ-Λʰ (tra★-∘ τ/ε) (tra-∘▷★ H)
tra-∘ (ƛʰ H) = ≡ᴹ-ƛʰ (tra-∘▷ H)
tra-∘ (Handler M) = ≡ᴹ-Handler (tra-∘▷ M)
```

```
-[_] : Γ ⊢ τ → Γ ▷ τ ↠ Γ
var★ -[ V ] = →★-id
tra∈ -[ V ] Z = subst (_ ⊢_) (sym (tra★-id _)) V
tra∈ -[ V ] (S x) = subst (_ ⊢_) (sym (tra★-id _)) (Var x)

-[_]ᵀ : {Γ : Context L Δ} → (τ : L ⨟ Δ ⊢ᵀ κ) → Γ ▷★ κ ↠ Γ
var★ -[ τ ]ᵀ = -[ τ ]★
tra∈ -[ τ ]ᵀ ((S★ x) (` wkn≡)) = subst (_ ⊢_) (trans (sym ([]★∘weaken★ _ _)) (cong (sub★ _) wkn≡)) (Var x)
```

```
_[_] : ∀ {κ₂} {τ₁} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → Γ ▷ τ₁ ⊢ τ₂ → Γ ⊢ τ₁ → Γ ⊢ τ₂
M [ V ] = subst (_ ⊢_) (tra★-id _) (tra -[ V ] M)

_[_]ᵀ : ∀ {κ₁} {κ₂} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → Γ ▷★ κ₁ ⊢ τ₂ → (τ₁ : L ⨟ Δ ⊢ᵀ κ₁) → Γ ⊢ τ₂ [ τ₁ ]★
M [ τ ]ᵀ = tra -[ τ ]ᵀ M
```

```
weaken₀→ᵍ : {Γ : Context L ∅} → ∅ →ᵍ Γ
weaken₀→ᵍ = →★-id , λ()

weaken₀ : ∅ ⊢ τ → Γ ⊢ τ
weaken₀ M = subst (_ ⊢_) (tra★-id _) (tra weaken₀→ᵍ M)

weaken₁→ᵍ : ∅ ▷ τ′ →ᵍ Γ ▷ τ′
var★ weaken₁→ᵍ = →★-id
tra∈ weaken₁→ᵍ Z = subst (_∈ _) (sym (tra★-id _)) Z

weaken₁ : ∅ ▷ τ′ ⊢ τ → Γ ▷ τ′ ⊢ τ
weaken₁ M = subst (_ ⊢_) (tra★-id _) (tra weaken₁→ᵍ M)
```

```
instance
  Kit-∘-∈-∈ : Kit-∘ K∈ K∈
  Kit-∘-∈-∈ = record {
    tra∘wkn = λ x → sym ≡-weaken-S ;
    tra∘wknᵀ = λ x → ≡ᴹ-var (≡[∈]-trans (≡[∈]-subst _ {x = S★_ _ _}) refl) }
```

```
≡ᴹ⇒≡ : ∀ {M M′ : Γ ⊢ τ} → M ≡ᴹ M′ → M ≡ M′
≡ᴹ⇒≡ refl = refl
```

```
tra-S-Var : ∀ {x : τ ∈ Γ} → tra (↠-S {τ = τ′}) (Var x) ≡ᴹ Var (S_ {τ₂ = τ′} x)
tra-S-Var = ≡ᴹ-var (≡[∈]-subst _)
```

```
ren▷∘↠-S : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} → tra▷ r {τ′} ∘[↠] ↠-S ≡[↠] ↠-S {τ = tra★ r★ τ′} ∘[↠] r
≡[↠]⇒≡ᵍ ren▷∘↠-S = refl
≡[↠]⇒≡[↠★] ren▷∘↠-S = _,_ λ α → refl
tra∈-≡ (ren▷∘↠-S {τ′ = τ′} {r = r}) x =
  tra∈ (tra▷ r ∘[↠] ↠-S) x ≡⟨ tra∈-∘ (tra▷ r) ↠-S x ⟩
  tra (tra▷ r {τ′}) (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = tra▷ r} (tra∈-id {x = S x}) ⟩
  tra (tra▷ r {τ′}) (Var (S x)) ≡⟨ refl ⟩
  Var (S (tra∈ r x)) ≡⟨ ≡ᴹ-sym tra-S-Var ⟩
  tra ↠-S (Var (tra∈ r x)) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S r x) ⟩
  tra∈ (↠-S ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
ren∘weaken′ : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r {τ′}) (tra ↠-S M) ≡ᴹ tra (↠-S {τ = tra★ r★ τ′}) (tra r M)
ren∘weaken′ M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ ren▷∘↠-S M) (≡ᴹ-sym (tra-∘ M)))
```

```
ren∘weaken : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r) (weaken {τ′ = τ′} M) ≡ᴹ weaken {τ′ = tra★ r★ τ′} (tra r M)
ren∘weaken {τ = τ} {τ′ = τ′} {r = r@(r★ , _)} M =
  tra (tra▷ r) (weaken {τ′ = τ′} M) ≡⟨ ≡ᴹ-tra {τ = τ} {r = tra▷ r} (≡ᴹ-subst (tra★-id _) (tra ↠-S M)) ⟩
  tra (tra▷ r) (tra ↠-S M) ≡⟨ ren∘weaken′ M ⟩
  tra ↠-S (tra r M) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst _ _) ⟩
  weaken {τ′ = tra★ r★ τ′} (tra r M) ∎
  where open ≡ᴹ-Reasoning
```

```
ren▷★∘↠-S★ : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} → tra▷★ r {κ} ∘[↠] ↠-S★ ≡[↠] ↠-S★ ∘[↠] r
≡[↠]⇒≡ᵍ ren▷★∘↠-S★ = refl
≡[↠]⇒≡[↠★] ren▷★∘↠-S★ = _,_ λ α → refl
tra∈-≡ (ren▷★∘↠-S★ {r = r}) x =
  tra∈ (tra▷★ r ∘[↠] ↠-S★) x ≡⟨ tra∈-∘ (tra▷★ r) ↠-S★ x ⟩
  tra (tra▷★ r) (Var (S★_ x `refl)) ≡⟨ ≡ᴹ-var (≡[∈]-subst _) ⟩
  Var (S★_ (tra∈ r x) `refl) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S★ r x) ⟩
  tra∈ (↠-S★ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
ren∘weakenᵀ : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷★ r) (weakenᵀ {κ = κ} M) ≡ᴹ weakenᵀ (tra r M)
ren∘weakenᵀ M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ ren▷★∘↠-S★ M) (≡ᴹ-sym (tra-∘ M)))
```

```
instance
  Kit-∘-⊢-∈ : Kit-∘ K⊢ K∈
  Kit-∘-⊢-∈ = record {
    tra∘wkn = λ M → ≡ᴹ⇒≡ (ren∘weaken M) ;
    tra∘wknᵀ = ren∘weakenᵀ }
```

```
instance
  Kit-∘-∈-⊢ : Kit-∘ K∈ K⊢
  Kit-∘-∈-⊢ = record {
    tra∘wkn = λ x → refl ;
    tra∘wknᵀ = λ x → ≡ᴹ-subst _ _ }
```

```
sub▷∘↠-S : ∀ {r@(r★ , _) : Γ ↠ Γ′} → tra▷ r {τ′} ∘[↠] ↠-S ≡[↠] ↠-S {τ = tra★ r★ τ′} ∘[↠] r
≡[↠]⇒≡ᵍ sub▷∘↠-S = refl
≡[↠]⇒≡[↠★] (sub▷∘↠-S {r = (r★ , _)}) = _,_ (λ α → sym (tra★-id _))
tra∈-≡ (sub▷∘↠-S {τ′ = τ′} {r = r}) x =
  tra∈ (tra▷ r ∘[↠] ↠-S) x ≡⟨ tra∈-∘ (tra▷ r) ↠-S x ⟩
  tra (tra▷ r {τ′}) (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = tra▷ r} (tra∈-id {x = S x}) ⟩
  tra (tra▷ r {τ′}) (Var (S x)) ≡⟨ refl ⟩
  weaken (tra∈ r x) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra ↠-S (tra∈ r x) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S r x) ⟩
  tra∈ (↠-S ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
sub∘weaken′ : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r {τ′}) (tra ↠-S M) ≡ᴹ tra (↠-S {τ = tra★ r★ τ′}) (tra r M)
sub∘weaken′ {r = r} M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ (sub▷∘↠-S {r = r}) M) (≡ᴹ-sym (tra-∘ M)))
```

```
sub∘weaken : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r) (weaken {τ′ = τ′} M) ≡ᴹ weaken {τ′ = tra★ r★ τ′} (tra r M)
sub∘weaken {τ = τ} {τ′ = τ′} {r = r@(r★ , _)} M =
  tra (tra▷ r) (weaken {τ′ = τ′} M) ≡⟨ ≡ᴹ-tra {τ = τ} {r = tra▷ r} (≡ᴹ-subst (tra★-id _) (tra ↠-S M)) ⟩
  tra (tra▷ r) (tra ↠-S M) ≡⟨ sub∘weaken′ M ⟩
  tra ↠-S (tra r M) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst _ _) ⟩
  weaken {τ′ = tra★ r★ τ′} (tra r M) ∎
  where open ≡ᴹ-Reasoning
```

```
sub▷★∘↠-S★ : ∀ {r@(r★ , _) : Γ ↠ Γ′} → tra▷★ r {κ} ∘[↠] ↠-S★ ≡[↠] ↠-S★ ∘[↠] r
≡[↠]⇒≡ᵍ sub▷★∘↠-S★ = refl
≡[↠]⇒≡[↠★] sub▷★∘↠-S★ = _,_ λ α → refl
tra∈-≡ (sub▷★∘↠-S★ {r = r}) x =
  tra∈ (tra▷★ r ∘[↠] ↠-S★) x ≡⟨ tra∈-∘ (tra▷★ r) ↠-S★ x ⟩
  tra (tra▷★ r) (Var (S★_ x `refl)) ≡⟨ ≡ᴹ-subst _ _ ⟩
  weakenᵀ (tra∈ r x) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S★ r x) ⟩
  tra∈ (↠-S★ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning

sub∘weakenᵀ : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷★ r) (weakenᵀ {κ = κ} M) ≡ᴹ weakenᵀ (tra r M)
sub∘weakenᵀ {r = r} M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ (sub▷★∘↠-S★ {r = r}) M) (≡ᴹ-sym (tra-∘ M)))
```

```
instance
  Kit-∘-⊢-⊢ : Kit-∘ K⊢ K⊢
  Kit-∘-⊢-⊢ = record {
    tra∘wkn = λ M → ≡ᴹ⇒≡ (sub∘weaken M) ;
    tra∘wknᵀ = sub∘weakenᵀ }
```

```
⦅appᵛ_,_[_]⦆ :
    (V₁ : Γ ▷ τ ⊢ τ₁ ⇒ τ₂)
    (V₂ : Γ ▷ τ ⊢ τ₁)
    (W : Γ ⊢ τ)
  → appᵛ V₁ V₂ [ W ] ≡ᴹ appᵛ (V₁ [ W ]) (V₂ [ W ])
⦅appᵛ V₁ , V₂ [ W ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) (≡ᴹ-sym (≡ᴹ-appᵛ (≡ᴹ-subst (tra★-id _) (tra -[ W ] V₁)) (≡ᴹ-subst (tra★-id _) (tra -[ W ] V₂))))
```

```
unfold:_[_] : ∀ {κ₂} {τ₁} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → (M : Γ ▷ τ₁ ⊢ τ₂) → (V : Γ ⊢ τ₁)
  → M [ V ] ≡ᴹ tra -[ V ] M
unfold: M [ V ] = ≡ᴹ-subst (tra★-id _) (tra -[ V ] M)
```

```
renᵍᴸ-id : renᵍᴸ →ᴸ-id Γ ≡ Γ
renᵍᴸ-id {Γ = ∅} = refl
renᵍᴸ-id {Γ = Γ ▷ x} = cong₂ _▷_ renᵍᴸ-id (tra★-id _)
renᵍᴸ-id {Γ = Γ ▷★ κ} = cong (_▷★ _) renᵍᴸ-id
```

```
→ᴸ⇒→-id-≡[∈] : (x : τ ∈ Γ)
  → tra∈ (→ᴸ⇒→ᵍ →ᴸ-id) x ≡[∈] x
→ᴸ⇒→-id-≡[∈] Z = ≡[∈]-Z renᵍᴸ-id (tra★-id _)
→ᴸ⇒→-id-≡[∈] (S x) = ≡[∈]-S (tra★-id _) (→ᴸ⇒→-id-≡[∈] x)
→ᴸ⇒→-id-≡[∈] ((S★ x) wkn≡) = ≡[∈]-S★ (→ᴸ⇒→-id-≡[∈] x)

→ᴸ⇒→-id : →ᴸ⇒→ᵍ →ᴸ-id {Γ = Γ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ →ᴸ⇒→-id = renᵍᴸ-id
≡[↠]⇒≡[↠★] →ᴸ⇒→-id = ≡[↠★]-refl
tra∈-≡ →ᴸ⇒→-id x = ≡ᴹ-trans (≡ᴹ-var (→ᴸ⇒→-id-≡[∈] x)) (≡ᴹ-var (≡[∈]-sym (≡[∈]-subst _)))
```

```
renᴸ-id : {M : Γ ⊢ τ} → renᴸ →ᴸ-id M ≡ᴹ M
renᴸ-id {M = M} = ≡ᴹ-trans (tra-≡ →ᴸ⇒→-id M) (tra-id _) --  →ᴸ⇒→-id
```

```
→ᴸ⇒→★-≡ : {rᴸ rᴸ′ : L →ᴸ L′} → →ᴸ⇒→★ {Δ = Δ} rᴸ ≡[↠★] →ᴸ⇒→★ rᴸ′
tra∈★-≡ →ᴸ⇒→★-≡ = λ α → refl
```

```
→ᴸ⇒→★-∘ : ∀ (rᴸ′ : L′ →ᴸ L″) (rᴸ : L →ᴸ L′)
  → (→ᴸ⇒→★ rᴸ′ ∘[↠★] →ᴸ⇒→★ rᴸ) ≡[↠★] →ᴸ⇒→★ {Δ = Δ} (rᴸ′ ∘ᴸ rᴸ)
tra∈★-≡ (→ᴸ⇒→★-∘ _ _) = λ α → refl
```

```
renᵍᴸ-∘ : ∀ {rᴸ′ : L′ →ᴸ L″} {rᴸ : L →ᴸ L′} {Γ : Context L Δ} → renᵍᴸ rᴸ′ (renᵍᴸ rᴸ Γ) ≡ renᵍᴸ (rᴸ′ ∘ᴸ rᴸ) Γ
renᵍᴸ-∘ {Γ = ∅} = refl
renᵍᴸ-∘ {rᴸ′ = rᴸ′} {rᴸ = rᴸ} {Γ = Γ ▷ τ} = cong₂ _▷_ renᵍᴸ-∘ (trans (tra★-∘ τ) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ))
renᵍᴸ-∘ {Γ = Γ ▷★ κ} = cong (_▷★ _) renᵍᴸ-∘
```

```
≡-renᵍᴸ : {Γ : Context L Δ} → renᵍᴸ rᴸ Γ ≡ renᵍᴸ rᴸ′ Γ
≡-renᵍᴸ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} = cong (λ rᴸ → renᵍᴸ rᴸ _) (unique-→ᴸ rᴸ rᴸ′)
```

```
→ᴸ⇒→ᵍ-∘-≡[∈] : (x : τ ∈ Γ)
  → tra∈ (→ᴸ⇒→ᵍ rᴸ′) (tra∈ (→ᴸ⇒→ᵍ rᴸ) x) ≡[∈] tra∈ (→ᴸ⇒→ᵍ (rᴸ′ ∘ᴸ rᴸ)) x
→ᴸ⇒→ᵍ-∘-≡[∈] {rᴸ′ = rᴸ′} {rᴸ = rᴸ} (Z {τ = τ}) = ≡[∈]-Z renᵍᴸ-∘ (trans (tra★-∘ τ) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ))
→ᴸ⇒→ᵍ-∘-≡[∈] {rᴸ′ = rᴸ′} {rᴸ = rᴸ} (S_ {τ₂ = τ₂} x) = ≡[∈]-S (trans (tra★-∘ τ₂) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ₂)) (→ᴸ⇒→ᵍ-∘-≡[∈] x)
→ᴸ⇒→ᵍ-∘-≡[∈] ((S★ x) x₁) = ≡[∈]-S★ (→ᴸ⇒→ᵍ-∘-≡[∈] x)
```

```
≡ᴹ⇒≡ᵀ : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} → M ≡ᴹ M′ → τ ≡ τ′
≡ᴹ⇒≡ᵀ refl = refl
```

```
-- TODO: don't use K (equality is decidable)
≡ᵀ-unique : (p q : τ ≡ τ′) → p ≡ q
≡ᵀ-unique refl refl = refl
```

```
-[]∘S : (V : Γ ⊢ τ)
  → (-[ V ] ∘[↠] ↠-S) ≡[↠] ↠-id
≡[↠]⇒≡ᵍ (-[]∘S V) = refl
≡[↠]⇒≡[↠★] (-[]∘S V) = _,_ λ α → refl
tra∈-≡ (-[]∘S V) x =
  tra∈ (-[ V ] ∘[↠] ↠-S) x ≡⟨ tra∈-∘ -[ V ] ↠-S x ⟩
  tra -[ V ] (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = -[ V ]} (tra∈-id {x = (S x)}) ⟩
  tra -[ V ] (Var (S x)) ≡⟨ ≡ᴹ-subst _ _ ⟩
  Var x ≡⟨ ≡ᴹ-sym tra∈-id ⟩
  Var (tra∈ ↠-id x) ∎
  where open ≡ᴹ-Reasoning
```

```
⦅weaken_[_]′⦆ : ∀ (M : Γ ⊢ τ) (V : Γ ⊢ τ′)
  → tra -[ V ] (weaken M) ≡ᴹ M
⦅weaken_[_]′⦆ {τ = τ} M V =
  tra -[ V ] (weaken M) ≡⟨ ≡ᴹ-tra {r = -[ V ]} (≡ᴹ-subst (tra★-id τ) (tra ↠-S M)) ⟩
  tra -[ V ] (tra ↠-S M) ≡⟨ tra-∘ M ⟩
  tra (-[ V ] ∘[↠] ↠-S) M ≡⟨ tra-≡ (-[]∘S V) M ⟩
  tra ↠-id M ≡⟨ tra-id M ⟩
  M ∎
  where open ≡ᴹ-Reasoning
```

```
⦅weaken_[_]⦆ : ∀ (M : Γ ⊢ τ) (V : Γ ⊢ τ′)
  → weaken M [ V ] ≡ᴹ M
⦅weaken M [ V ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) ⦅weaken M [ V ]′⦆
```

```
⦅VarZ[_]⦆ : ∀ (V : Γ ⊢ τ)
  → Var Z [ V ] ≡ᴹ V
⦅VarZ[ V ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) (≡ᴹ-subst (sym (tra★-id _)) _)
```

```
⦅ƛ_[_]ᵀ⦆ : ∀ (M : Γ ▷★ κ ▷ τ₂ ⊢ τ₃) (τ : L ⨟ Δ ⊢ᵀ κ)
  → (ƛ M) [ τ ]ᵀ ≡ᴹ ƛ (tra (tra▷ -[ τ ]ᵀ) M)
⦅ƛ M [ τ ]ᵀ⦆ = refl
```

```
-[]ᵀ∘-[]′ : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁) (x : τ′ ∈ Γ ▷★ κ ▷ τ₁)
  → tra -[ V [ τ ]ᵀ ] (tra∈ (tra▷ -[ τ ]ᵀ) x) ≡ᴹ tra -[ τ ]ᵀ (tra∈ -[ V ] x)
-[]ᵀ∘-[]′ {τ₁ = τ₁} τ V Z = ≡ᴹ-trans (≡ᴹ-subst _ (tra -[ τ ]ᵀ V)) (≡ᴹ-tra {τ = τ₁} {τ′ = tra★ _ τ₁} {r = -[ τ ]ᵀ} (≡ᴹ-sym (≡ᴹ-subst (sym (tra★-id _)) V)))
-[]ᵀ∘-[]′ {τ₁ = τ₁} {τ′ = τ′} τ V (S x) =
  tra -[ V [ τ ]ᵀ ] (weaken (tra∈ -[ τ ]ᵀ x)) ≡⟨ ⦅weaken _ [ V [ τ ]ᵀ ]′⦆ ⟩
  tra∈ -[ τ ]ᵀ x ≡⟨ ≡ᴹ-tra {r = -[ τ ]ᵀ} (≡ᴹ-sym (≡ᴹ-subst (sym (tra★-id _)) (Var x))) ⟩
  tra -[ τ ]ᵀ (tra∈ -[ V ] (S x)) ∎
  where open ≡ᴹ-Reasoning

-[]ᵀ∘-[] : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁)
  → (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) ≡[↠] (-[ τ ]ᵀ ∘[↠] -[ V ])
≡[↠]⇒≡ᵍ (-[]ᵀ∘-[] τ V) = refl
tra∈★-≡ (≡[↠]⇒≡[↠★] (-[]ᵀ∘-[] τ V)) = λ α → tra★-id _
tra∈-≡ (-[]ᵀ∘-[] τ V) x =
  tra∈ (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) x ≡⟨ tra∈-∘ -[ V [ τ ]ᵀ ] (tra▷ -[ τ ]ᵀ) x ⟩
  tra -[ V [ τ ]ᵀ ] (tra∈ (tra▷ -[ τ ]ᵀ) x) ≡⟨ -[]ᵀ∘-[]′ τ V x ⟩
  tra -[ τ ]ᵀ (tra∈ -[ V ] x) ≡⟨ ≡ᴹ-sym (tra∈-∘ -[ τ ]ᵀ -[ V ] x) ⟩
  tra∈ (-[ τ ]ᵀ ∘[↠] -[ V ]) x ∎
  where open ≡ᴹ-Reasoning
```

In the translation BBD2FCN, name abstraction `Λᴺ M` is translated to
a type abstraction followed by a term abstraction `Λ ƛ M′`.
By β reduction, name application `(Λᴺ M) n` reduces to a substitution `M [ n ]`,
which translates to a simultaneous type- and term- substitution `M′ [ τ′ , V′ ]`
(where `τ′` and `V′` are the type- and term-level translations of `n`).
By contrast, an application of the translated abstraction `(Λ ƛ M′) τ′ V′`
β reduces to two successive substitutions, `M′ ▷[ τ′ ] [ V′ ]`.

```
⦅_▷[_]ᵀ[_]⦆ : ∀ (M : Γ ▷★ κ ▷ τ₁ ⊢ τ₂) (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁)
  → tra (tra▷ -[ τ ]ᵀ) M [ V [ τ ]ᵀ ] ≡ᴹ M [ V ] [ τ ]ᵀ
⦅_▷[_]ᵀ[_]⦆ M τ V =
  (tra (tra▷ -[ τ ]ᵀ) M) [ V [ τ ]ᵀ ] ≡⟨ unfold: tra _ M [ V [ τ ]ᵀ ] ⟩
  tra -[ V [ τ ]ᵀ ] (tra (tra▷ -[ τ ]ᵀ) M) ≡⟨ tra-∘ M ⟩
  tra (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) M ≡⟨ tra-≡ (-[]ᵀ∘-[] τ V) M ⟩
  tra (-[ τ ]ᵀ ∘[↠] -[ V ]) M ≡⟨ ≡ᴹ-sym (tra-∘ M) ⟩
  (tra -[ V ] M) [ τ ]ᵀ ≡⟨ ≡ᴹ-sym (≡ᴹ-tra {r = -[ τ ]ᵀ} (unfold: M [ V ])) ⟩
  (M [ V ] [ τ ]ᵀ) ∎
  where open ≡ᴹ-Reasoning
```

```
appᵀ-irr : ∀ {Δ} {Γ : Context L Δ} {κ} {σ} {σ[τ] σ[τ]′}
  → {N : Γ ⊢ Forallᵀ κ σ}
  → {τ : L ⨟ Δ ⊢ᵀ κ}
  → {σ[τ]≡ : _ ≡ σ[τ]}
  → {σ[τ]′≡ : _ ≡ σ[τ]′}
  → appᵀ N τ σ[τ]≡ ≡ᴹ appᵀ N τ σ[τ]′≡
appᵀ-irr {σ[τ]≡ = refl} {σ[τ]′≡ = refl} = refl
```

```
wrap :
    (M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
  → ∀ {τ σ ε}
  → (wkn≡ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → Γ ▷★ SCO ▷ Name (Var Z) ⊢ weaken★ τ / Var Z ⦂ weaken★ σ ∷ weaken★ ε
wrap M (`refl , `refl , `refl) = M
```

```
unfold:wrap : {Γ : Context L Δ}
  → (M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
  → (wkn≡ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′)
  → wrap M wkn≡ ≡ᴹ M
unfold:wrap M (`refl , `refl , `refl) = refl
```

```
handle-irr : ∀ {Γ : Context L Δ} {σ} {τ} {ε}
    {M : Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′}
    {H : Γ ⊢ σ ⇒ʰ τ / ε}
    {wkn≡ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′}
  → handle M H wkn≡ ≡ᴹ handle (wrap M wkn≡) H (`refl , `refl , `refl)
handle-irr {wkn≡ = (`refl , `refl , `refl)} = refl
```

```
≡ᴹ-[] : {M : Γ ▷ τ₁ ⊢ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ τ₂′}
    {V : Γ ⊢ τ₁} {V′ : Γ′ ⊢ τ₁′}
  → M ≡ᴹ M′
  → V ≡ᴹ V′
  → M [ V ] ≡ᴹ M′ [ V′ ]
≡ᴹ-[] refl refl = refl
```

```
[]∘return : ∀ (M : Γ ▷ τ₁ ⊢ τ₂) (V : Γ ⊢ τ₁)
  → return {ε = ε} M [ V ] ≡ᴹ return {ε = ε} (M [ V ])
[]∘return M V = ≡ᴹ-trans (unfold: return M [ V ]) (≡ᴹ-return (tra★-id _) (≡ᴹ-sym (unfold: M [ V ])))
```

```
lift★ : ∀ {_#_} {K : Kit★ _#_}
  → ⦅ K ⦆ LΔ →★ LΔ′
  → LΔ ↠★ LΔ′
relabel (lift★ r) = relabel r
tra∈★ (lift★ r) = tra∈★′ r
```

```
lift★≡ : ∀ {_#_} {K : Kit★ _#_} ⦃ _ : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → lift★ r ≡[↠★] r
lift★≡ = _,_ (λ α → refl)
```

```
tra★-lift : ∀ {_#_} {K : Kit★ _#_} ⦃ _ : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → (τ : LΔ ⊢★ κ)
  → tra★ (lift★ r) τ ≡ tra★ r τ
tra★-lift = tra★-≡ lift★≡
```

```
lift : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄
  → ⦅ K ⦆ Γ ↠ Γ′ → Γ ↠ Γ′
var★ (lift r) = lift★ (var★ r)
tra∈ (lift {K = K} r) {τ = τ} x = subst (_ ⊢_) (sym (tra★-lift τ)) (to-⊢ K (tra∈ r x))
```

```
lift≡ : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄ → {r : ⦅ K ⦆ Γ ↠ Γ′} → lift r ≡[↠] r
≡[↠]⇒≡ᵍ lift≡ = refl
≡[↠]⇒≡[↠★] lift≡ = lift★≡
tra∈-≡ lift≡ x = ≡ᴹ-subst _ _
```

```
tra-lift : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (M : Γ ⊢ τ)
  → tra (lift r) M ≡ᴹ tra r M
tra-lift = tra-≡ lift≡
```

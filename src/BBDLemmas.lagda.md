```
module BBDLemmas where

open import Common using (⊥; ⊥-elim; ⦅_⦆_≡_; _⦅_⦆≡_)
open import Labels
open import BindersByDay
open import BBDSubstitution
open import BBDSemantics

open import Data.Unit using (⊤; tt)
open import Data.Product using (∃-syntax; _×_; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; sym)
```

```
private variable
  κ κ′ : Kind
  L L′ : Labels
  rᴸ : L →ᴸ L′
  ℓ : -∈ᴸ L
  ℓ′ : -∈ᴸ L′
  Δ Δ′ : Contextᴷ
  Γ : Context L Δ
  Γ′ : Context L′ Δ′
  a a′ b : -∈ᴺ Δ
  ε ε₁ ε₂ : L ⨟ Δ ⊢ᵀ EFF
  ε′ ε₁′ ε₂′ : L′ ⨟ Δ′ ⊢ᵀ EFF
  σ σ′ : L ⨟ Δ ⊢ᵀ SIG
  τ τ₁ τ₂ τ₃ τ′ τ₁′ τ₂′ : L ⨟ Δ ⊢ᵀ κ
  n : -∈ᴺ L ⨟ Δ
  τ/ε τ/ε′ τ/ε₀ τ/ε₀′ τ/ε₁ τ/ε₁′ τ/ε₂ τ/ε₂′ τ/ε₃ τ/ε₃′ : L ⨟ Δ ⊢ᵀ COM
  τ≈ : ⦅ ren★L rᴸ ⦆ τ ≡ τ′
  ε≈ : ⦅ ren★L rᴸ ⦆ ε ≡ ε′
  τ/ε≈ : ⦅ ren★L rᴸ ⦆ τ/ε ≡ τ/ε′
  τ/ε₁≈ : ⦅ ren★L rᴸ ⦆ τ/ε₁ ≡ τ/ε₁′
  τ/ε₂≈ : ⦅ ren★L rᴸ ⦆ τ/ε₂ ≡ τ/ε₂′
  τ/ε₃≈ : ⦅ ren★L rᴸ ⦆ τ/ε₃ ≡ τ/ε₃′
```

```
data ⦅_>>=_≡_>>=_⦆ (M : Γ ⊢ τ / ε) (N : Γ ▷ τ ⊢ τ₁ / ε) : (M′ : Γ ⊢ τ′ / ε) (N′ : Γ ▷ τ′ ⊢ τ₁ / ε) → Set where
  _>>=refl : ∀ {M′} → M ≡ M′ → ⦅ M >>= N ≡ M′ >>= N ⦆

inv-≡->>= : ∀ {M : Γ ⊢ τ / ε} {M′ : Γ ⊢ τ′ / ε} {N : Γ ▷ τ ⊢ τ₁ / ε} {N′ : Γ ▷ τ′ ⊢ τ₁ / ε}
  → M >>= N ≡ M′ >>= N′
  → ⦅ M >>= N ≡ M′ >>= N′ ⦆
inv-≡->>= refl = refl >>=refl
```

```
data ⦅handle′_[_]_≡handle′_[_]_⦆ (ℓ∉ : ℓ ∉ ε) (M : Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε) (H : Γ ⊢ σ ⇒ʰ τ / ε) : ℓ′ ∉ ε → Γ ⊢ τ / dynamic ℓ′ ⦂ σ′ ∷ ε → Γ ⊢ σ′ ⇒ʰ τ / ε → Set where
  handle′-[_]- : ∀ {M′} → M ≡ M′ → ⦅handle′ ℓ∉ [ M ] H ≡handle′ ℓ∉ [ M′ ] H ⦆

inv-≡-handle′ : ∀ {ℓ∉ : ℓ ∉ ε} {M : Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε} {H : Γ ⊢ σ ⇒ʰ τ / ε} {ℓ′∉ : ℓ′ ∉ ε} {M′ : Γ ⊢ τ / dynamic ℓ′ ⦂ σ′ ∷ ε} {H′ : Γ ⊢ σ′ ⇒ʰ τ / ε}
  → handle′ ℓ∉ M H ≡ handle′ ℓ′∉ M′ H′
  → ⦅handle′ ℓ∉ [ M ] H ≡handle′ ℓ′∉ [ M′ ] H′ ⦆
inv-≡-handle′ refl = handle′-[ refl ]-
```

```
data ⦅subeffect_,_≡subeffect_,_⦆ (ε⊑ : ε₁ ⊑ ε₂) (M : Γ ⊢ τ / ε₁) : ε₁′ ⊑ ε₂ → Γ ⊢ τ / ε₁′ → Set where
  subeffect-[_] : ∀ {M′} → M ≡ M′ → ⦅subeffect ε⊑ , M ≡subeffect ε⊑ , M′ ⦆

inv-≡-subeffect : ∀ {ε⊑ : ε₁ ⊑ ε₂} {M : Γ ⊢ τ / ε₁} {ε′⊑ : ε₁′ ⊑ ε₂} {M′ : Γ ⊢ τ / ε₁′}
  → subeffect ε⊑ M ≡ subeffect ε′⊑ M′
  → ⦅subeffect ε⊑ , M ≡subeffect ε′⊑ , M′ ⦆
inv-≡-subeffect refl = subeffect-[ refl ]
```

```
data Inv-E[M/□] (E : □: τ/ε ⊢ᴱ τ/ε₁) M : ∀ {τ/ε′} (E′ : □: τ/ε′ ⊢ᴱ τ/ε₁) M′
  → (p : E [ M /□] ≡ E′ [ M′ /□])
  → Set where
  refl : Inv-E[M/□] E M E M refl

inv-E[perform] : ∀ {E : □: τ / dynamic ℓ ⦂ σ ∷ ε ⊢ᴱ τ/ε} {V} {E′ : □: τ′ / dynamic ℓ′ ⦂ σ′ ∷ ε′ ⊢ᴱ τ/ε} {V′}
  → (p : E [ perform (dynamic ℓ) V /□] ≡ E′ [ perform (dynamic ℓ′) V′ /□])
  → Inv-E[M/□] E (perform (dynamic ℓ) V) E′ (perform (dynamic ℓ′) V′) p
inv-E[perform] {E = □} {E′ = □} refl = refl
inv-E[perform] {E = [ E ]>>= x} {E′ = [ E′ ]>>= x₁} p with inv-≡->>= p
... | p′ >>=refl with inv-E[perform] p′ | p
...   | refl | refl = refl
inv-E[perform] {E = handle′ x [ E ] x₁} {E′ = handle′ x₂ [ E′ ] x₃} p with inv-≡-handle′ p
... | handle′-[ p′ ]- with inv-E[perform] p′ | p
...   | refl | refl = refl
inv-E[perform] {E = subeffect′ x [ E ]} {E′ = subeffect′ x₁ [ E′ ]} p with inv-≡-subeffect p
... | subeffect-[ p′ ] with inv-E[perform] p′ | p
...   | refl | refl = refl
```

```
contra-□-[]>>= : ∀ {L₁ L₁′} {τ₀} {τ/ε₀′} {M′ N₀}
  → (E′ : □: _ ⊢ᴱ τ₀ / ε)
  → ∀ {τ/ε₁} {τ/ε₁′} {N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ / ε ≡ τ/ε₁}
  → {τ/ε₀′≈ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀′ ≡ τ/ε₁′}
  → ((E′ [ M′ /□]) >>= N₀) ↦ N  ⦅ τ/ε₀≈ ⦆
  → M′ ↦ N′ ⦅ τ/ε₀′≈ ⦆
  → ⊥
contra-□-[]>>= □ (step₀ (β->>= V N₀)) (step₀ ())
contra-□-[]>>= □ (step₀ (β->>= V N₀)) (step₁ ())
```

```
return≢E[perform] : ∀ {V} {W}
  → {E : □: τ′ / n ⦂ σ ∷ ε′ ⊢ᴱ τ / ε}
  → return V ≢ E [ perform n W /□]
return≢E[perform] {E = □} ()
return≢E[perform] {E = [ E ]>>= x} ()
return≢E[perform] {E = handle′ x [ E ] x₁} ()
return≢E[perform] {E = subeffect′ x [ E ]} ()
```

```
return↦-contra : ∀ {V} {N}
  → return V ↦ N ⦅ τ/ε≈ ⦆
  → ⊥
return↦-contra (step₀ ())
return↦-contra (step₁ ())
```

```
return⟶-contra : ∀ {V} {N}
  → return V ⟶ N ⦅ τ/ε≈ ⦆
  → ⊥
return⟶-contra (ξ (step₀ ()) □ E′ x₁ refl x₂)
return⟶-contra (ξ (step₁ ()) □ E′ x₁ refl x₂)
return⟶-contra (ξ x ([ E ]>>= x₂) E′ x₁ () x₃)
return⟶-contra (ξ x (handle′ x₂ [ E ] x₄) E′ x₁ () x₃)
return⟶-contra (ξ x subeffect′ x₂ [ E ] E′ x₁ () x₃)
```

```
perform↦-contra : ∀ {V} {N}
  → perform n V ↦ N ⦅ τ/ε≈ ⦆
  → ⊥
perform↦-contra (step₀ ())
perform↦-contra (step₁ ())
```

```
E[perform]↦-contra : ∀ {N′} {V}
  → {E : □: τ / dynamic ℓ ⦂ σ ∷ ε ⊢ᴱ _}
  → ℓ ∉ᴱ E
  → E [ perform (dynamic ℓ) V /□] ↦ N′ ⦅ τ/ε₁≈ ⦆
  → ⊥
E[perform]↦-contra □ M↦N = perform↦-contra M↦N
E[perform]↦-contra ([ □ ]>>= N) (step₀ ())
E[perform]↦-contra ([ [ ℓ∉E ]>>= N₁ ]>>= N) (step₀ ())
E[perform]↦-contra ([ handle′ x [ ℓ∉E ] H ]>>= N) (step₀ ())
E[perform]↦-contra ([ subeffect′ ε⊑ε′ [ ℓ∉E ] ]>>= N) (step₀ ())
E[perform]↦-contra (handle′ ℓ≢ℓ [ □ ] H) (step₀ (hop x₁ V .H x₂)) = ⊥-elim (ℓ≢ℓ refl)
E[perform]↦-contra {E = handle′ _ [ E′ ] _} (handle′ ℓ≢ℓ [ [ ℓ∉E ]>>= N ] H) (step₀ (hop ℓ∉E′ V .H E[perform]≡)) with inv-E[perform] {E′ = E′} E[perform]≡
... | refl = ⊥-elim (ℓ≢ℓ refl)
E[perform]↦-contra {E = handle′ _ [ E′ ] _} (handle′ ℓ≢ℓ [ handle′ x₁ [ ℓ∉E ] H₁ ] H) (step₀ (hop ℓ∉E′ V .H E[perform]≡)) with inv-E[perform] {E′ = E′} E[perform]≡
... | refl = ⊥-elim (ℓ≢ℓ refl)
E[perform]↦-contra {E = handle′ _ [ E′ ] _} (handle′ ℓ≢ℓ [ subeffect′ ε⊑ε′ [ ℓ∉E ] ] H) (step₀ (hop ℓ∉E′ V .H E[perform]≡)) with inv-E[perform] {E′ = E′} E[perform]≡
... | refl = ⊥-elim (ℓ≢ℓ refl)
E[perform]↦-contra subeffect′ ε⊑ε′ [ □ ] (step₀ ())
E[perform]↦-contra subeffect′ ε⊑ε′ [ [ ℓ∉E ]>>= N ] (step₀ ())
E[perform]↦-contra subeffect′ ε⊑ε′ [ handle′ x [ ℓ∉E ] H ] (step₀ ())
E[perform]↦-contra subeffect′ ε⊑ε′ [ subeffect′ ε⊑ε′₁ [ ℓ∉E ] ] (step₀ ())
```

```
E[perform]⟶-contra′ : ∀ {M′} {N′} {V}
  → (E′ : □: τ/ε₁ ⊢ᴱ τ/ε)
  → {E : □: τ / dynamic ℓ ⦂ σ ∷ ε ⊢ᴱ _}
  → ℓ ∉ᴱ E
  → E′ [ M′ /□] ≡ E [ perform (dynamic ℓ) V /□]
  → M′ ↦ N′ ⦅ τ/ε₁≈ ⦆
  → ⊥
E[perform]⟶-contra′ □ ℓ∉E refl M↦N = E[perform]↦-contra ℓ∉E M↦N
E[perform]⟶-contra′ ([ E′ ]>>= x) ([ ℓ∉E ]>>= N) F[E][M]≡ M↦N with inv-≡->>= F[E][M]≡
... | E[M]≡ >>=refl = E[perform]⟶-contra′ E′ ℓ∉E E[M]≡ M↦N
E[perform]⟶-contra′ (handle′ x [ E′ ] x₁) (handle′ x₂ [ ℓ∉E ] H) F[E][M]≡ M↦N with inv-≡-handle′ F[E][M]≡
... | handle′-[ E[M]≡ ]- = E[perform]⟶-contra′ E′ ℓ∉E E[M]≡ M↦N
E[perform]⟶-contra′ subeffect′ x [ E′ ] subeffect′ ε⊑ε′ [ ℓ∉E ] F[E][M]≡ M↦N with inv-≡-subeffect F[E][M]≡
... | subeffect-[ E[M]≡ ] = E[perform]⟶-contra′ E′ ℓ∉E E[M]≡ M↦N
```

```
E[perform]⟶-contra : ∀ {V} {N}
  → {E : □: τ / dynamic ℓ ⦂ σ ∷ ε ⊢ᴱ _}
  → ℓ ∉ᴱ E
  → E [ perform (dynamic ℓ) V /□] ⟶ N ⦅ τ/ε₁≈ ⦆
  → ⊥
E[perform]⟶-contra ℓ∉E₀ (ξ M↦M′ E E′ E≡E′ E[M]≡ E′[M′]≡) = E[perform]⟶-contra′ E ℓ∉E₀ E[M]≡ M↦M′
```

```
contra-□-handle[] : ∀ {L₁ L₁′} {τ/ε₀′} {M′ ℓ∉ H E′[M′]}
  → (E′ : □: _ ⊢ᴱ τ / dynamic ℓ ⦂ σ ∷ ε)
  → ∀ {τ/ε₁} {τ/ε₁′} {N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ / ε ≡ τ/ε₁}
  → {τ/ε₀′≈ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀′ ≡ τ/ε₁′}
  → E′ [ M′ /□] ≡ E′[M′]
  → handle′ ℓ∉ E′[M′] H ↦ N  ⦅ τ/ε₀≈ ⦆
  → M′ ↦ N′ ⦅ τ/ε₀′≈ ⦆
  → ⊥
contra-□-handle[] E′ E′[M′]≡ (step₀ (hop ℓ∉E V _ refl)) M′↦ = E[perform]⟶-contra′ E′ ℓ∉E E′[M′]≡ M′↦
contra-□-handle[] □ refl (step₀ (hret V _)) M′↦ = return↦-contra M′↦
contra-□-handle[] ([ E′ ]>>= x) () (step₀ (hret V _)) M′↦
contra-□-handle[] (handle′ x [ E′ ] x₁) () (step₀ (hret V _)) M′↦
contra-□-handle[] subeffect′ x [ E′ ] () (step₀ (hret V _)) M′↦

contra-□-subeffect[] : ∀ {L₁ L₁′} {τ/ε₀′} {M′ ε⊑ E′[M′]}
  → (E′ : □: _ ⊢ᴱ τ / ε)
  → ∀ {τ/ε₁} {τ/ε₁′} {N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ / ε′ ≡ τ/ε₁}
  → {τ/ε₀′≈ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀′ ≡ τ/ε₁′}
  → E′ [ M′ /□] ≡ E′[M′]
  → subeffect ε⊑ E′[M′] ↦ N  ⦅ τ/ε₀≈ ⦆
  → M′ ↦ N′ ⦅ τ/ε₀′≈ ⦆
  → ⊥
contra-□-subeffect[] □ refl (step₀ (subret _ V)) M′↦ = return↦-contra M′↦
contra-□-subeffect[] ([ E′ ]>>= x) () (step₀ (subret _ V)) M′↦
contra-□-subeffect[] (handle′ x [ E′ ] x₁) () (step₀ (subret _ V)) M′↦
contra-□-subeffect[] subeffect′ x [ E′ ] () (step₀ (subret _ V)) M′↦
contra-□-subeffect[] E′ E′[M′]≡ (step₁ ()) M′↦
```

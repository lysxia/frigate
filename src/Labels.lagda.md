```
module Labels where

open import Common using (⊥; ⊥-elim)
open import Function.Base using (_∘_)
open import Relation.Binary.PropositionalEquality as ≡ using (_≡_; refl; cong; trans)
```

- Sets of labels `L : Labels`
- Labels `ℓ : L`

```
infixl 4 _▷-

data Labels : Set where
  ∅ : Labels
  _▷- : Labels → Labels
```

```
infix 3 -∈ᴸ_

data -∈ᴸ_ : Labels → Set where
  Z : ∀ {L} → -∈ᴸ L ▷-
  S_ : ∀ {L} → -∈ᴸ L → -∈ᴸ L ▷-
```

```
private variable
  L L′ L″ : Labels
  ℓ ℓ′ : -∈ᴸ L
```

```
Injective : {A B : Set} → (A → B) → Set
Injective {A} f = ∀ {a a′ : A} → f a ≡ f a′ → a ≡ a′
```

```
infix 3 _→ᴸ_

data _→ᴸ_ L : Labels → Set where
  Z : L →ᴸ L
  S_ : L →ᴸ L′ → L →ᴸ L′ ▷-

ren∈ᴸ : L →ᴸ L′ → -∈ᴸ L → -∈ᴸ L′
ren∈ᴸ Z ℓ = ℓ
ren∈ᴸ (S rᴸ) ℓ = S (ren∈ᴸ rᴸ ℓ)
```

```
pred-≡ : ∀ {L : Labels} {ℓ ℓ′ : -∈ᴸ L} → S ℓ ≡ S ℓ′ → ℓ ≡ ℓ′
pred-≡ refl = refl
```

```
injective-ren∈ᴸ : (rᴸ : L →ᴸ L′) → Injective (ren∈ᴸ rᴸ)
injective-ren∈ᴸ Z refl = refl
injective-ren∈ᴸ (S rᴸ) eq = injective-ren∈ᴸ rᴸ (pred-≡ eq)
```

```
private variable
  rᴸ : L →ᴸ L′
  rᴸ′ : L′ →ᴸ L″
  rᴸ″ : L →ᴸ L″
```

```
→ᴸ-id : {L : Labels}
    ------
  → L →ᴸ L
→ᴸ-id = Z
```

```
→ᴸ-S : {L : Labels}
    ---------
  → L →ᴸ L ▷-
→ᴸ-S = S Z
```

```
renᴸ▷ : {L L′ : Labels}
  → L →ᴸ L′
    -------------
  → L ▷- →ᴸ L′ ▷-
renᴸ▷ Z = Z
renᴸ▷ (S rᴸ) = S (renᴸ▷ rᴸ)
```

```
_∘ᴸ_ : L′ →ᴸ L″ → L →ᴸ L′ → L →ᴸ L″
Z ∘ᴸ rᴸ = rᴸ
(S rᴸ′) ∘ᴸ rᴸ = S (rᴸ′ ∘ᴸ rᴸ)
```

```
∘ᴸ-idᴸ : →ᴸ-id ∘ᴸ rᴸ ≡ rᴸ
∘ᴸ-idᴸ = refl
```

```
∘ᴸ-idᴿ : rᴸ ∘ᴸ →ᴸ-id ≡ rᴸ
∘ᴸ-idᴿ {rᴸ = Z} = refl
∘ᴸ-idᴿ {rᴸ = S rᴸ} = cong S_ (∘ᴸ-idᴿ {rᴸ = rᴸ})
```

```
pred : L ▷- →ᴸ L′ → L →ᴸ L′
pred Z = S Z
pred (S rᴸ) = S (pred rᴸ)
```

```
no-pred : (L ▷- →ᴸ L) → ⊥
no-pred {L = ∅} ()
no-pred {L = (L ▷-)} (S rᴸ) = no-pred (pred rᴸ)
```

```
unique-→ᴸ : (rᴸ rᴸ′ : L →ᴸ L′) → rᴸ ≡ rᴸ′
unique-→ᴸ Z Z = refl
unique-→ᴸ (S rᴸ) (S rᴸ′) = cong S_ (unique-→ᴸ rᴸ rᴸ′)
unique-→ᴸ Z (S rᴸ′) = ⊥-elim (no-pred rᴸ′)
unique-→ᴸ (S rᴸ) Z  = ⊥-elim (no-pred rᴸ)
```

```
ren∈ᴸ-irr : {rᴸ rᴸ′ : L →ᴸ L′} → (ℓ : -∈ᴸ L) → ren∈ᴸ rᴸ ℓ ≡ ren∈ᴸ rᴸ′ ℓ
ren∈ᴸ-irr {rᴸ = rᴸ} {rᴸ′ = rᴸ′} ℓ = cong (λ r → ren∈ᴸ r ℓ) (unique-→ᴸ rᴸ rᴸ′)
```

```
ren∈ᴸ-∘ : (ℓ : -∈ᴸ L) → ren∈ᴸ rᴸ′ (ren∈ᴸ rᴸ ℓ) ≡ ren∈ᴸ (rᴸ′ ∘ᴸ rᴸ) ℓ
ren∈ᴸ-∘ {rᴸ′ = Z} ℓ = refl
ren∈ᴸ-∘ {rᴸ′ = S rᴸ′} ℓ = cong S_ (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ)
```

```
ren∈ᴸ-∘′ : (ℓ : -∈ᴸ L) → ren∈ᴸ rᴸ′ (ren∈ᴸ rᴸ ℓ) ≡ ren∈ᴸ rᴸ″ ℓ
ren∈ᴸ-∘′ {rᴸ′ = rᴸ′} ℓ = trans (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ) (ren∈ᴸ-irr ℓ)
```

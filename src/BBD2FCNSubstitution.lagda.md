
```
module BBD2FCNSubstitution where

open import Common using (cong₃)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; cong; cong₂; subst)
```

```
open import Labels
open import BindersByDay as B
open import BBDSubstitution as B
open import FirstClassNames as F
open import FCNSubstitution as F
open import BBD2FCN
```

```
private
  variable
    L L′ : Labels
    rᴸ : L →ᴸ L′
    ℓ ℓ′ : -∈ᴸ L
    κ κ′ κ₁ κ₂ : B.Kind
    Δ Δ′ : B.Contextᴷ
    Γ Γ′ : B.Context L Δ
    n n′ : B.-∈ᴺ L ⨟ Δ
    τ τ₁ τ₂ τ₃ τ′ τ₁′ τ₂′ σ σ′ : L B.⨟ Δ ⊢ᵀ κ
    ε ε′ ε₁ ε₁′ ε₂′ : L B.⨟ Δ ⊢ᵀ EFF
    τ/ε τ/ε′ : L B.⨟ Δ ⊢ᵀ COM
```

```
open B.⦅_⦆_→★_
open F.⦅_⦆_→★_
open B.Kit★
open F.Kit★
open _≈[→★]_

private variable
  _#★_ : Labels × B.Contextᴷ → B.Kind → Set
  _#_ : ∀ {L Δ} → B.Context L Δ → L B.⨟ Δ ⊢ᵀ TYP → Set
  _#★′_ : Labels × F.Contextᴷ → F.Kind → Set
  _#′_ : ∀ {L Δ} → F.Context L Δ → L F.⨟ Δ ⊢ᵀ TYP → Set
  K★ : B.Kit★ _#★_
  K★′ : F.Kit★ _#★′_
  K : B.Kit _#★_ _#_
  K′ : F.Kit _#★′_ _#′_
```

```
⟦_⟧≡ : ∀ {M : Γ B.⊢ τ} {M′ : Γ′ B.⊢ τ′} → M B.≡ᴹ M′ → ⟦ M ⟧ F.≡ᴹ ⟦ M′ ⟧
⟦ refl ⟧≡ = refl
```

```
postulate ⟦⟧∘ren⊑ᴸ : ∀ {ε⊑ : ε B.⊑ ε₁} → ⟦ B.ren⊑ᴸ rᴸ ε⊑ ⟧⊑ F.≡[⊑] F.ren⊑ᴸ rᴸ ⟦ ε⊑ ⟧⊑
postulate ⟦⟧∘renᴸ : ∀ {M : Γ B.⊢ τ} → ⟦ B.renᴸ rᴸ M ⟧ F.≡ᴹ F.renᴸ rᴸ ⟦ M ⟧
```

```
postulate ⟦_[_]⟧≡ : ∀ (N : Γ ▷ τ₁ B.⊢ τ₂) (M : Γ B.⊢ τ₁) → ⟦ N B.[ M ] ⟧ F.≡ᴹ ⟦ N ⟧ F.[ ⟦ M ⟧ ]
postulate ⟦_[_]ᵀ⟧≡ : ∀ (N : Γ ▷★ κ B.⊢ σ) (τ : L B.⨟ Δ ⊢ᵀ κ) → ⟦ N B.[ τ ]ᵀ ⟧ F.≡ᴹ ⟦ N ⟧ F.[ ⟦ τ ⟧ᵀ ]ᵀ
```

```
postulate
 ⟦_[_]ᴺ⟧ : (N : Γ ▷ᴺ- B.⊢ τ) (n : -∈ᴺ L ⨟ Δ)
  → ⟦ N [ n ]ᴺ ⟧ F.≡ᴹ (F.tra (F.tra▷ F.-[ ⟦ n ⟧∈ᶻ ]ᵀ) ⟦ N ⟧) F.[ ⟦ n ⟧N ]
-- ⟦ N [ n ]ᴺ⟧ = ?
```

```
open B.⦅_⦆_↠_
open F.⦅_⦆_↠_
open B.Kit
open F.Kit
```

```
record _≈[↠]_ {K : B.Kit _#★_ _#_} {K′ : F.Kit _#★′_ _#′_} (s : B.⦅ K ⦆ Γ ↠ Γ′) (s′ : F.⦅ K′ ⦆ ⟦ Γ ⟧ᵍ ↠ ⟦ Γ′ ⟧ᵍ) : Set where
  field
    ≈[↠]⇒≈[↠★] : var★ s ≈[→★] var★ s′
    ≈-tra∈ : (x : τ B.∈ Γ) → ⟦ to-⊢ K (tra∈ s x) ⟧ F.≡ᴹ to-⊢ K′ (tra∈ s′ ⟦ x ⟧∈)
```

```
record ⟦Kit⟧ (K : B.Kit _#★_ _#_) (K′ : F.Kit _#★′_ _#′_) : Set where
  field
    ⦃ GK ⦄ : B.GoodKit K
    ⦃ GK′ ⦄ : F.GoodKit K′

open ⟦Kit⟧ ⦃ ... ⦄
```

```
postulate
 ⟦tra⟧ : ∀ ⦃ _ : ⟦Kit⟧ K K′ ⦄ {s : B.⦅ K ⦆ Γ ↠ Γ′} {s′ : F.⦅ K′ ⦆ ⟦ Γ ⟧ᵍ ↠ ⟦ Γ′ ⟧ᵍ}
  → s ≈[↠] s′
  → (M : Γ B.⊢ τ)
  → ⟦ B.tra s M ⟧ F.≡ᴹ F.tra s′ ⟦ M ⟧
-- ⟦sub⟧ s≡s′ (Var x) = s≡s′ x
-- ⟦sub⟧ s≡s′ (ƛ M) = {! !}
-- ⟦sub⟧ s≡s′ (ƛᴺ M) = {! !}
-- ⟦sub⟧ s≡s′ (Λ M) = {! !}
-- ⟦sub⟧ s≡s′ tt = {! !}
-- ⟦sub⟧ s≡s′ ⦅⦆ = {! !}
-- ⟦sub⟧ s≡s′ (M , M₁) = {! !}
-- ⟦sub⟧ s≡s′ (τ ,ᵀ M ⦅ x ⦆) = {! !}
-- ⟦sub⟧ s≡s′ (a ,ᴺ M ⦅ x ⦆) = {! !}
-- ⟦sub⟧ s≡s′ ((Λʰ M) x) = {! !}
-- ⟦sub⟧ s≡s′ ((ƛʰᴺ M) x) = {! !}
-- ⟦sub⟧ s≡s′ (ƛʰ M) = {! !}
-- ⟦sub⟧ s≡s′ (Handler M) = {! !}
-- ⟦sub⟧ s≡s′ (return M) = {! !}
-- ⟦sub⟧ s≡s′ (M >>= M₁) = {! !}
-- ⟦sub⟧ s≡s′ (appᵛ M M₁) = {! !}
-- ⟦sub⟧ s≡s′ (appᴺ M b x) = {! !}
-- ⟦sub⟧ s≡s′ (appᵀ M σ x) = {! !}
-- ⟦sub⟧ s≡s′ (perform a M) = {! !}
-- ⟦sub⟧ s≡s′ (handle M M₁ x) = {! !}
-- ⟦sub⟧ s≡s′ (handle′ x M M₁) = {! !}
-- ⟦sub⟧ s≡s′ (subeffect x M) = {! !}
```

```
instance
  w∈ : ⟦Kit⟧ B.K∈ F.K∈
  w∈ = record {}
  w⊢ : ⟦Kit⟧ B.K⊢ F.K⊢
  w⊢ = record {}
```

```
postulate ⟦weakenᴸ⟧ : ∀ (M : Γ B.⊢ τ) → ⟦ B.weakenᴸ M ⟧ F.≡ᴹ F.weakenᴸ ⟦ M ⟧
postulate ⟦weaken⟧ : ∀ (M : Γ B.⊢ τ) → ⟦ B.weaken {τ′ = τ′} M ⟧ F.≡ᴹ F.weaken {τ′ = ⟦ τ′ ⟧ᵀ} ⟦ M ⟧
postulate ⟦weaken₀⟧ : ∀ (M : ∅ B.⊢ τ) → ⟦ B.weaken₀ {Γ = Γ} M ⟧ F.≡ᴹ F.weaken₀ {Γ = ⟦ Γ ⟧ᵍ} ⟦ M ⟧
postulate ⟦weaken₁⟧ : ∀ (M : ∅ ▷ τ′ B.⊢ τ) → ⟦ B.weaken₁ {Γ = Γ} M ⟧ F.≡ᴹ F.weaken₁ {Γ = ⟦ Γ ⟧ᵍ} ⟦ M ⟧
```

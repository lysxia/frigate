```
module BBDDeterminism where

open import Common using (⊥-elim; ⦅_⦆_≡_; `_; `refl)
open import Labels
open import BindersByDay
open import BBDSubstitution
open import BBDSemantics
open import BBDLemmas

open import Data.Unit using (⊤; tt)
open import Data.Product using (∃-syntax; _×_; _,_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym)
```

```
private variable
  κ κ′ : Kind
  L L′ : Labels
  rᴸ : L →ᴸ L′
  ℓ : -∈ᴸ L
  ℓ′ : -∈ᴸ L′
  Δ Δ′ : Contextᴷ
  Γ : Context L Δ
  Γ′ : Context L′ Δ′
  a a′ b : -∈ᴺ Δ
  ε ε₁ ε₂ : L ⨟ Δ ⊢ᵀ EFF
  ε′ ε₁′ ε₂′ : L′ ⨟ Δ′ ⊢ᵀ EFF
  σ σ′ : L ⨟ Δ ⊢ᵀ SIG
  τ τ₁ τ₂ τ₃ τ′ τ₁′ τ₂′ : L ⨟ Δ ⊢ᵀ κ
  n : -∈ᴺ L ⨟ Δ
  τ/ε τ/ε′ τ/ε₀ τ/ε₀′ τ/ε₁ τ/ε₁′ τ/ε₂ τ/ε₂′ τ/ε₃ τ/ε₃′ : L ⨟ Δ ⊢ᵀ COM
  τ≈ : ⦅ ren★L rᴸ ⦆ τ ≡ τ′
  ε≈ : ⦅ ren★L rᴸ ⦆ ε ≡ ε′
  τ/ε≈ : ⦅ ren★L rᴸ ⦆ τ/ε ≡ τ/ε′
  τ/ε₁≈ : ⦅ ren★L rᴸ ⦆ τ/ε₁ ≡ τ/ε₁′
  τ/ε₂≈ : ⦅ ren★L rᴸ ⦆ τ/ε₂ ≡ τ/ε₂′
  τ/ε₃≈ : ⦅ ren★L rᴸ ⦆ τ/ε₃ ≡ τ/ε₃′
```

```
pre-det↦ : ∀ {L₁ L₁′} {τ/ε₀} {τ/ε₀′} {M M′}
  → (E : □: τ/ε₀ ⊢ᴱ τ/ε) (E′ : _)
  → ∀ {τ/ε₁} {τ/ε₁′} {N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ/ε₀ ≡ τ/ε₁}
  → {τ/ε₀′≈ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀′ ≡ τ/ε₁′}
  → M  ↦ N  ⦅ τ/ε₀≈ ⦆
  → M′ ↦ N′ ⦅ τ/ε₀′≈ ⦆
  → (p : E [ M /□] ≡ E′ [ M′ /□])
  → Inv-E[M/□] E M E′ M′ p
pre-det↦ □ □ M↦ M′↦ refl = refl
pre-det↦ □ ([ E′ ]>>= N)         M↦ M′↦ refl = ⊥-elim (contra-□-[]>>= E′ M↦ M′↦)
pre-det↦ □ (handle′ x [ E′ ] x₁) M↦ M′↦ refl = ⊥-elim (contra-□-handle[] E′ refl M↦ M′↦)
pre-det↦ □ subeffect′ x [ E′ ]   M↦ M′↦ refl = ⊥-elim (contra-□-subeffect[] E′ refl M↦ M′↦)
pre-det↦ ([ E ]>>= N) □ M↦ M′↦ refl = ⊥-elim (contra-□-[]>>= E M′↦ M↦)
pre-det↦ (handle′ x [ E ] x₁) □ M↦ M′↦ refl = ⊥-elim (contra-□-handle[] E refl M′↦ M↦)
pre-det↦ subeffect′ x [ E ] □ M↦ M′↦ refl = ⊥-elim (contra-□-subeffect[] E refl M′↦ M↦)

pre-det↦ ([ E ]>>= N) ([ E′ ]>>= N′) M↦ M′↦ E>>=N[M]≡ with inv-≡->>= E>>=N[M]≡
... | E[M]≡ >>=refl with pre-det↦ E E′ M↦ M′↦ E[M]≡ | E>>=N[M]≡
...   | refl | refl = refl

pre-det↦ (handle′ x [ E ] x₁) (handle′ x₂ [ E′ ] x₃) M↦ M′↦ handle-E[M]≡ with inv-≡-handle′ handle-E[M]≡
... | handle′-[ E[M]≡ ]- with pre-det↦ E E′ M↦ M′↦ E[M]≡ | handle-E[M]≡
...   | refl | refl = refl

pre-det↦ subeffect′ x [ E ] subeffect′ x₁ [ E′ ] M↦ M′↦ subeffect-E[M]≡ with inv-≡-subeffect subeffect-E[M]≡
... | subeffect-[ E[M]≡ ] with pre-det↦ E E′ M↦ M′↦ E[M]≡ | subeffect-E[M]≡
...   | refl | refl = refl
```

```
data _≡[↦]_ {L₁}  {rᴸ : L →ᴸ L₁} {τ/ε₀} {τ/ε} {M} {N}  {≈τ/ε : ⦅ ren★L rᴸ ⦆ τ/ε₀ ≡ τ/ε} (M↦N : M ↦ N ⦅ ≈τ/ε ⦆) :
          ∀ {L₁′} {rᴸ′ : L →ᴸ L₁′}      {τ/ε′}    {N′} {≈τ/ε′ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀ ≡ τ/ε′} → M ↦ N′ ⦅ ≈τ/ε′ ⦆ → Set where
  refl : M↦N ≡[↦] M↦N

det↦ : ∀ {L₁ L₁′} {τ/ε₀ τ/ε τ/ε′} {M N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ/ε₀ ≡ τ/ε}
  → {τ/ε₀≈′ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀ ≡ τ/ε′}
  → (M↦N : M ↦ N  ⦅ τ/ε₀≈ ⦆)
  → (M↦N′ : M ↦ N′ ⦅ τ/ε₀≈′ ⦆)
  → M↦N ≡[↦] M↦N′
det↦ (step₀ (β N V)) (step₀ (β .N .V)) = refl
det↦ (step₀ (βᵀ N τ₁)) (step₀ (βᵀ .N .τ₁)) = refl
det↦ (step₀ (βᴺ N n)) (step₀ (βᴺ .N .n)) = refl
det↦ (step₀ (β->>= V N)) (step₀ (β->>= .V .N)) = refl
det↦ (step₀ (hop ℓ∉E V H p)) (step₀ (hop ℓ∉E′ V′ .H q)) with p
... | refl with inv-E[perform] q
...   | refl with ∉ᴱ-unique ℓ∉E ℓ∉E′
...     | refl = refl
det↦ (step₀ (hop □ V H ())) (step₀ (hret V₁ .H))
det↦ (step₀ (hop ([ E ]>>= x) V H ())) (step₀ (hret V₁ .H))
det↦ (step₀ (hop (handle′ x [ E ] x₁) V H ())) (step₀ (hret V₁ .H))
det↦ (step₀ (hop subeffect′ x [ E ] V H ())) (step₀ (hret V₁ .H))
det↦ (step₀ (hret V H)) (step₀ (hop □ V₁ .H ()))
det↦ (step₀ (hret V H)) (step₀ (hop ([ E ]>>= x) V₁ .H ()))
det↦ (step₀ (hret V H)) (step₀ (hop (handle′ x [ E ] x₁) V₁ .H ()))
det↦ (step₀ (hret V H)) (step₀ (hop subeffect′ x [ E ] V₁ .H ()))
det↦ (step₀ (hret V H)) (step₀ (hret .V .H)) = refl
det↦ (step₀ (subret ε⊑ε′ V)) (step₀ (subret .ε⊑ε′ .V)) = refl
det↦ (step₁ (generate _ _)) (step₀ ())
det↦ (step₁ (generate M H)) (step₁ (generate .M .H)) = refl
```

```
data _≡[⟶]_ {L₁}  {rᴸ : L →ᴸ L₁} {τ/ε₀} {τ/ε} {M} {N}  {≈τ/ε : ⦅ ren★L rᴸ ⦆ τ/ε₀ ≡ τ/ε} (M⟶N : M ⟶ N ⦅ ≈τ/ε ⦆) :
          ∀ {L₁′} {rᴸ′ : L →ᴸ L₁′}      {τ/ε′}    {N′} {≈τ/ε′ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀ ≡ τ/ε′} → M ⟶ N′ ⦅ ≈τ/ε′ ⦆ → Set where
  refl : M⟶N ≡[⟶] M⟶N

det⟶ : ∀ {L₁ L₁′} {τ/ε₀ τ/ε τ/ε′} {M N N′}
  → {rᴸ : L →ᴸ L₁}
  → {rᴸ′ : L →ᴸ L₁′}
  → {τ/ε₀≈ : ⦅ ren★L rᴸ ⦆ τ/ε₀ ≡ τ/ε}
  → {τ/ε₀≈′ : ⦅ ren★L rᴸ′ ⦆ τ/ε₀ ≡ τ/ε′}
  → (M⟶N : M ⟶ N  ⦅ τ/ε₀≈ ⦆)
  → (M⟶N′ : M ⟶ N′ ⦅ τ/ε₀≈′ ⦆)
  → M⟶N ≡[⟶] M⟶N′
det⟶ (ξ N↦N₁ E E₁ E≡E₁ E[N]≡ E₁[N₁]≡) (ξ N↦N₁′ E′ E₁′ E′≡E₁′ refl E₁′[N₁]≡) with pre-det↦ E E′ N↦N₁ N↦N₁′ E[N]≡
... | refl with det↦ N↦N₁ N↦N₁′
... | refl with E≡E₁ | E′≡E₁′ | E₁[N₁]≡ | E₁′[N₁]≡
... | ` refl | ` refl | refl | refl = refl
```

```
determinism : (M : ∅ ⊢ τ / ε) → (p q : Progress M) → p ≡ q
determinism M (step M⟶M₁) (step M⟶M₁′) with det⟶ M⟶M₁ M⟶M₁′
... | refl = refl
determinism M (step M⟶) (value refl) = ⊥-elim (return⟶-contra M⟶)
determinism M (step M⟶) (op ℓ∉E refl) = ⊥-elim (E[perform]⟶-contra ℓ∉E M⟶)
determinism M (value return≡E[perform]) (op E refl) = ⊥-elim (return≢E[perform] return≡E[perform])
determinism M (value refl) (step M⟶) = ⊥-elim (return⟶-contra M⟶)
determinism M (value refl) (value refl) = refl
determinism M (op ℓ∉E q) (step M⟶) with q   -- delay match so that Agda can see this is disjoint from the last clause
... | refl = ⊥-elim (E[perform]⟶-contra ℓ∉E M⟶)
determinism M (op ℓ∉E refl) (value return≡E[perform]) = ⊥-elim (return≢E[perform] return≡E[perform])
determinism M (op ℓ∉E₁ E₁[perform]≡) (op ℓ∉E₂ refl) with inv-E[perform] E₁[perform]≡
... | refl with ∉ᴱ-unique ℓ∉E₁ ℓ∉E₂
... | refl = refl
```

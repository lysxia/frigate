# Equivalence between Binders By Day and First Class Names

- `BindersByDay`: syntax of Binders By Day.
  TODO: this is to be removed, after switching `BBD2FCN` to depend on `BindersByDay2`.
- `BindersByDay2`: syntax of a variant of Binders By Day, where maps from handler names to signatures appear in types instead of in the context.
- `FirstClassNames`: syntax of First Class Names.
- `BBD2FCN`: translation from `BindersByDay` to `FirstClassNames`
- `FCN2BBD`: translation from `FirstClassNames` to `BindersByDay2`

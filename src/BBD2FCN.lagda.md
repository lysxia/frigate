Embedding of the syntax of "Binders by day, labels by night" (BindersByDay.agda)
into "First-class names for effect handlers" (FirstClassNames.agda)

Main idea: translate dependent name abstraction
   ⟦ λᴺ a . M ⟧
to two System-F abstractions
   Λ (η : ★) . λ (a : Name η) . ⟦ M ⟧

```
module BBD2FCN where

open import Common using (cong₃; _$_≡_; `_; `refl)
open import Function.Base using (it)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; cong; cong₂; subst)
```

```
open import Labels
open import BindersByDay as B
open import BBDSubstitution as B
open import FirstClassNames as F
open import FCNSubstitution as F
```

```
private
  variable
    L L′ : Labels
    rᴸ : L →ᴸ L′
    ℓ ℓ′ : -∈ᴸ L
    κ κ′ κ₁ κ₂ : B.Kind
    Δ Δ′ : B.Contextᴷ
    Γ Γ′ : B.Context L Δ
    n n′ : B.-∈ᴺ L ⨟ Δ
    τ τ₁ τ₂ τ₃ τ′ τ₁′ τ₂′ σ σ′ : L B.⨟ Δ ⊢ᵀ κ
    ε ε′ ε₁ ε₁′ ε₂′ : L B.⨟ Δ ⊢ᵀ EFF
    τ/ε τ/ε′ : L B.⨟ Δ ⊢ᵀ COM
```

## Translate types

Translate kinds.

```
⟦_⟧K : B.Kind → F.Kind
⟦ TYP ⟧K = TYP
⟦ EFF ⟧K = EFF
⟦ SIG ⟧K = SIG
⟦ COM ⟧K = COM
```

Translate type contexts.

```
⟦_⟧ᴷ : B.Contextᴷ → F.Contextᴷ
⟦ ∅ ⟧ᴷ = ∅
⟦ Γ ▷ᴺ- ⟧ᴷ = ⟦ Γ ⟧ᴷ ▷ SCO
⟦ Γ ▷ κ ⟧ᴷ = ⟦ Γ ⟧ᴷ ▷ ⟦ κ ⟧K
```

Translate type variables and handler names.

```
⟦_⟧∈★ : κ B.∈★ Δ → ⟦ κ ⟧K F.∈★ ⟦ Δ ⟧ᴷ
⟦ Z ⟧∈★ = Z
⟦ S x ⟧∈★ = S ⟦ x ⟧∈★
⟦ Sᴺ x ⟧∈★ = S ⟦ x ⟧∈★
```

```
⟦_⟧∈ᴺ : B.-∈ᴺ Δ → SCO F.∈★ ⟦ Δ ⟧ᴷ
⟦ Z ⟧∈ᴺ = Z
⟦ S a ⟧∈ᴺ = S ⟦ a ⟧∈ᴺ
⟦ Sᴺ x ⟧∈ᴺ = S ⟦ x ⟧∈ᴺ
```

```
⟦_⟧∈ᶻ : B.-∈ᴺ L ⨟ Δ → L F.⨟ ⟦ Δ ⟧ᴷ ⊢ᵀ SCO
⟦ static η ⟧∈ᶻ = Var ⟦ η ⟧∈ᴺ
⟦ dynamic ℓ ⟧∈ᶻ = dynamic ℓ
```

### Translate types

Two intermediate definitions before `⟦_⟧ᵀ`.

Translate type `τ` under type context `Γ` and handler context `𝓼`: `⟦ Γ , 𝓼 ∣ τ ⟧ᵀ`.

```
⟦_⟧ᵀ : L B.⨟ Δ ⊢ᵀ κ → L F.⨟ ⟦ Δ ⟧ᴷ ⊢ᵀ ⟦ κ ⟧K
⟦ Var x ⟧ᵀ = Var ⟦ x ⟧∈★
⟦ τ ⇒ τ′ ⟧ᵀ = ⟦ τ ⟧ᵀ ⇒ ⟦ τ′ ⟧ᵀ
⟦ τ ⇒ʰ τ′ ⟧ᵀ = ⟦ τ ⟧ᵀ ⇒ʰ ⟦ τ′ ⟧ᵀ
⟦ τ ⇒ᵖ τ′ ⟧ᵀ = ⟦ τ ⟧ᵀ ⇒ᵖ ⟦ τ′ ⟧ᵀ
⟦ Forallᵀ κ τ ⟧ᵀ = Forallᵀ ⟦ κ ⟧K ⟦ τ ⟧ᵀ
⟦ Forallᴺ τ ⟧ᵀ = Forallᵀ SCO (Name (Var Z) ⇒ ⟦ τ ⟧ᵀ / ι)
⟦ Forallˢᴺ τ ⟧ᵀ = Forallˢ SCO (Name (Var Z) ⇒ˢ ⟦ τ ⟧ᵀ)
⟦ ⟨ τ ⟩ ⟧ᵀ = ⟨ ⟦ τ ⟧ᵀ ⟩
⟦ ι ⟧ᵀ = ι
⟦ a ⦂ σ ∷ ε ⟧ᵀ = ⟦ a ⟧∈ᶻ ⦂ ⟦ σ ⟧ᵀ ∷ ⟦ ε ⟧ᵀ
⟦ σ ⇒ˢ σ′ ⟧ᵀ = ⟦ σ ⟧ᵀ ⇒ˢ ⟦ σ′ ⟧ᵀ
⟦ Forallˢ κ σ ⟧ᵀ = Forallˢ ⟦ κ ⟧K ⟦ σ ⟧ᵀ
⟦ τ / ε ⟧ᵀ = ⟦ τ ⟧ᵀ / ⟦ ε ⟧ᵀ
⟦ Unit ⟧ᵀ = Unit
```

Translate subtyping

```
⟦_⟧∈ᵉ : n B.⦂ σ ∈ᵉ ε → ⟦ n ⟧∈ᶻ F.⦂ ⟦ σ ⟧ᵀ ∈ᵉ ⟦ ε ⟧ᵀ
⟦ Z ⟧∈ᵉ = Z
⟦ S n∈ε ⟧∈ᵉ = S ⟦ n∈ε ⟧∈ᵉ
```

```
⟦_⟧⊑ : ε B.⊑ ε′ → ⟦ ε ⟧ᵀ F.⊑ ⟦ ε′ ⟧ᵀ
⟦ ι ⟧⊑ = ι
⟦ n∈ε′ ∷ ε⊑ε′ ⟧⊑ = ⟦ n∈ε′ ⟧∈ᵉ ∷ ⟦ ε⊑ε′ ⟧⊑
```

```
⟦_⟧∉ : ℓ B.∉ ε → ℓ F.∉ ⟦ ε ⟧ᵀ
⟦ ι ⟧∉ = ι
⟦ ≢ℓ ∷ ℓ∉ε ⟧∉ = ≢ℓ ∷ ⟦ ℓ∉ε ⟧∉
```

## Substitution lemmas

```
open B.⦅_⦆_→★_
open F.⦅_⦆_→★_
open B.Kit★
open F.Kit★

private variable
  _#_ : Labels × B.Contextᴷ → B.Kind → Set
  _#′_ : Labels × F.Contextᴷ → F.Kind → Set
  K : B.Kit★ _#_
  K′ : F.Kit★ _#′_

⦅_,_⦆_≡[#]_ : (K : B.Kit★ _#_) (K′ : F.Kit★ _#′_) → (L , Δ) # κ → (L , ⟦ Δ ⟧ᴷ) #′ ⟦ κ ⟧K → Set
⦅ K , K′ ⦆ α ≡[#] α′ = ⟦ to-⊢★ K α ⟧ᵀ ≡ to-⊢★ K′ α′

record _≈[→★]_ {K : B.Kit★ _#_} {K′ : F.Kit★ _#′_} (r★ : B.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)) (⟦r★⟧ : F.⦅ K′ ⦆ (L , ⟦ Δ ⟧ᴷ) →★ (L′ , ⟦ Δ′ ⟧ᴷ)) : Set where
  constructor _,_
  field
    ≈-→ᴰ : ∀ (α : κ B.∈★ Δ) → ⦅ K , K′ ⦆ (tra∈★ r★ α) ≡[#] (tra∈★ ⟦r★⟧ ⟦ α ⟧∈★)
    ≈-→ᴺ : ∀ (n :  -∈ᴺ Δ) → ⟦ tra∈ᴺ r★ n ⟧∈ᶻ ≡ to-⊢★ K′ (tra∈★ ⟦r★⟧ ⟦ n ⟧∈ᴺ)

open _≈[→★]_

record ⟦Kit★⟧ (K : B.Kit★ _#_) (K′ : F.Kit★ _#′_) (⟦_⟧# : ∀ {L Δ κ} → (L , Δ) # κ → (L , ⟦ Δ ⟧ᴷ) #′ ⟦ κ ⟧K) : Set where
  field
    ⦃ GK ⦄ : B.GoodKit★ K
    ⦃ GK′ ⦄ : F.GoodKit★ K′
    ⟦wkn★⟧ : ∀ {LΔ} {α : LΔ # κ} {α′} → ⦅ K , K′ ⦆ α ≡[#] α′ → ⟦ to-⊢★ K (wkn★ K {κ′ = κ′} α) ⟧ᵀ ≡ to-⊢★ K′ (wkn★ K′ α′)
    ⟦wkn★ᴺ⟧ : ∀ {LΔ} {α : LΔ # κ} {α′} → ⦅ K , K′ ⦆ α ≡[#] α′ → ⟦ to-⊢★ K (wkn★ᴺ K α) ⟧ᵀ ≡ to-⊢★ K′ (wkn★ K′ α′)

open ⟦Kit★⟧ ⦃ ... ⦄

⟦tra∈ᶻ⟧ : ∀ {r★ : B.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : F.⦅ K′ ⦆ _ →★ _}
  → r★ ≈[→★] ⟦r★⟧
  → ∀ (n : -∈ᴺ L ⨟ Δ) → ⟦ B.tra∈ᶻ r★ n ⟧∈ᶻ ≡ F.tra★ ⟦r★⟧ ⟦ n ⟧∈ᶻ
⟦tra∈ᶻ⟧ r★≈ (dynamic ℓ) = cong dynamic (ren∈ᴸ-irr ℓ)
⟦tra∈ᶻ⟧ r★≈ (static n) = ≈-→ᴺ r★≈ n

⟦wknᶻ⟧ : ∀ (n : -∈ᴺ L ⨟ Δ) → ⟦ B.weakenᶻ {κ = κ} n ⟧∈ᶻ ≡ F.weaken★ ⟦ n ⟧∈ᶻ
⟦wknᶻ⟧ (dynamic x) = refl
⟦wknᶻ⟧ (static x) = refl

⟦wknᶻᴺ⟧ : ∀ (n : -∈ᴺ L ⨟ Δ) → ⟦ B.weakenᶻᴺ n ⟧∈ᶻ ≡ F.weaken★ ⟦ n ⟧∈ᶻ
⟦wknᶻᴺ⟧ (dynamic x) = refl
⟦wknᶻᴺ⟧ (static x) = refl

⟦tra★▷⟧ : ∀ {⟦_⟧# : ∀ {L Δ κ} → (L , Δ) # κ → (L , ⟦ Δ ⟧ᴷ) #′ ⟦ κ ⟧K} {K : B.Kit★ _#_} {K′ : F.Kit★ _#′_}
    ⦃ _ : ⟦Kit★⟧ K K′ ⟦_⟧# ⦄ {r★ : B.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : F.⦅ K′ ⦆ _ →★ _}
  → r★ ≈[→★] ⟦r★⟧
  → B.tra★▷ r★ {κ = κ} ≈[→★] F.tra★▷ ⟦r★⟧
⟦tra★▷⟧ r★≈ .≈-→ᴰ Z = ≡.trans (cong ⟦_⟧ᵀ (B.GoodKit★.to★-from★ it _)) (sym (F.GoodKit★.to★-from★ it _))
⟦tra★▷⟧ r★≈ .≈-→ᴰ (S α) = ⟦wkn★⟧ (≈-→ᴰ r★≈ α)
⟦tra★▷⟧ {r★ = r★} r★≈ .≈-→ᴺ (S n) = ≡.trans (⟦wknᶻ⟧ (tra∈ᴺ r★ n)) (≡.trans (cong F.weaken★ (≈-→ᴺ r★≈ n)) (sym (F.GoodKit★.to★-wkn★ it _)))

⟦tra★▷ᴺ⟧ : ∀ {⟦_⟧# : ∀ {L Δ κ} → (L , Δ) # κ → (L , ⟦ Δ ⟧ᴷ) #′ ⟦ κ ⟧K} {K : B.Kit★ _#_} {K′ : F.Kit★ _#′_}
    ⦃ _ : ⟦Kit★⟧ K K′ ⟦_⟧# ⦄ {r★ : B.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : F.⦅ K′ ⦆ _ →★ _}
  → r★ ≈[→★] ⟦r★⟧
  → B.tra★▷ᴺ r★ ≈[→★] F.tra★▷ ⟦r★⟧
⟦tra★▷ᴺ⟧ r★≈ .≈-→ᴰ (Sᴺ α) = ⟦wkn★ᴺ⟧ (≈-→ᴰ r★≈ α)
⟦tra★▷ᴺ⟧ r★≈ .≈-→ᴺ Z = sym (F.GoodKit★.to★-from★ it _)
⟦tra★▷ᴺ⟧ {r★ = r★} r★≈ .≈-→ᴺ (Sᴺ n) = ≡.trans (⟦wknᶻᴺ⟧ (tra∈ᴺ r★ n)) (≡.trans (cong F.weaken★ (≈-→ᴺ r★≈ n)) (sym (F.GoodKit★.to★-wkn★ it _)))

⟦tra★⟧ : ∀ {⟦_⟧# : ∀ {L Δ κ} → (L , Δ) # κ → (L , ⟦ Δ ⟧ᴷ) #′ ⟦ κ ⟧K}  {K : B.Kit★ _#_} {K′ : F.Kit★ _#′_}
    ⦃ ⟦K⟧ : ⟦Kit★⟧ K K′ ⟦_⟧# ⦄ {r★ : B.⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {⟦r★⟧ : F.⦅ K′ ⦆ _ →★ _}
  → r★ ≈[→★] ⟦r★⟧
  → ∀ (τ : L B.⨟ Δ ⊢ᵀ κ) → ⟦ B.tra★ r★ τ ⟧ᵀ ≡ F.tra★ ⟦r★⟧ ⟦ τ ⟧ᵀ
⟦tra★⟧ r★≈ ι = refl
⟦tra★⟧ r★≈ Unit = refl
⟦tra★⟧ r★≈ (Var α) = ≈-→ᴰ r★≈ α
⟦tra★⟧ r★≈ ⟨ τ ⟩ = cong ⟨_⟩ (⟦tra★⟧ r★≈ τ)
⟦tra★⟧ r★≈ (τ / τ₁) = cong₂ _/_ (⟦tra★⟧ r★≈ τ) (⟦tra★⟧ r★≈ τ₁)
⟦tra★⟧ r★≈ (τ₁ ⇒ τ₂) = cong₂ _⇒_ (⟦tra★⟧ r★≈ τ₁) (⟦tra★⟧ r★≈ τ₂)
⟦tra★⟧ r★≈ (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (⟦tra★⟧ r★≈ τ₁) (⟦tra★⟧ r★≈ τ₂)
⟦tra★⟧ r★≈ (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (⟦tra★⟧ r★≈ τ₁) (⟦tra★⟧ r★≈ τ₂)
⟦tra★⟧ r★≈ (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (⟦tra★⟧ r★≈ τ₁) (⟦tra★⟧ r★≈ τ₂)
⟦tra★⟧ r★≈ (Forallᵀ κ τ) = cong (Forallᵀ ⟦ κ ⟧K) (⟦tra★⟧ (⟦tra★▷⟧ r★≈) τ)
⟦tra★⟧ r★≈ (Forallˢ κ τ) = cong (Forallˢ ⟦ κ ⟧K) (⟦tra★⟧ (⟦tra★▷⟧ r★≈) τ)
⟦tra★⟧ r★≈ (Forallᴺ τ) = cong₂ (λ τ₁ τ₂ → Forallᵀ SCO (Name τ₁ ⇒ τ₂ / ι)) (sym (F.GoodKit★.to★-from★ it Z)) (⟦tra★⟧ (⟦tra★▷ᴺ⟧ r★≈) τ)
⟦tra★⟧ r★≈ (Forallˢᴺ τ) = cong₂ (λ τ₁ τ₂ → Forallˢ SCO (Name τ₁ ⇒ˢ τ₂)) (sym (F.GoodKit★.to★-from★ it Z)) (⟦tra★⟧ (⟦tra★▷ᴺ⟧ r★≈) τ)
⟦tra★⟧ r★≈ (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (⟦tra∈ᶻ⟧ r★≈ η) (⟦tra★⟧ r★≈ σ) (⟦tra★⟧ r★≈ ε)
```

```
≈-→★-ᴸ : (rᴸ : L →ᴸ L′) → B.→ᴸ⇒→★ {Δ = Δ} rᴸ ≈[→★] F.→ᴸ⇒→★ rᴸ
≈-→★-ᴸ rᴸ .≈-→ᴰ α = refl
≈-→★-ᴸ rᴸ .≈-→ᴺ α = refl

≈-→★-Sᴺ : B.→★-Sᴺ {L = L} {Δ = Δ} {K = B.K∈★} ≈[→★] F.→★-S★ {K = F.K∈★}
≈-→★-Sᴺ .≈-→ᴰ α = refl
≈-→★-Sᴺ .≈-→ᴺ α = refl

≈-→★-S : B.→★-S★ {L = L} {Δ = Δ} {K = B.K∈★} {κ = κ} ≈[→★] F.→★-S★ {K = F.K∈★}
≈-→★-S .≈-→ᴰ α = refl
≈-→★-S .≈-→ᴺ α = refl
```

```
instance
  ⟦K∈★⟧ : ⟦Kit★⟧ B.K∈★ F.K∈★ ⟦_⟧∈★
  ⟦K∈★⟧ = record {
    ⟦wkn★⟧ = λ{ refl → refl } ;
    ⟦wkn★ᴺ⟧ = λ{ refl → refl } }

  ⟦K⊢★⟧ : ⟦Kit★⟧ B.K⊢★ F.K⊢★ ⟦_⟧ᵀ
  ⟦K⊢★⟧ = record {
    ⟦wkn★⟧ = λ{ {α = τ} refl → ⟦tra★⟧ ≈-→★-S τ } ;
    ⟦wkn★ᴺ⟧ = λ{ {α = τ} refl → ⟦tra★⟧ ≈-→★-Sᴺ τ } }
```

```
⟦ren★L⟧ : ⟦ B.ren★L rᴸ τ ⟧ᵀ ≡ F.ren★L rᴸ ⟦ τ ⟧ᵀ
⟦ren★L⟧ {rᴸ = rᴸ} {τ = τ} = ⟦tra★⟧ ⦃ ⟦K⟧ = ⟦K∈★⟧ ⦄ (≈-→★-ᴸ rᴸ) τ
```

```
⟦weaken★ᴺ⟧ : ⟦ B.weaken★ᴺ τ ⟧ᵀ ≡ F.weaken★ ⟦ τ ⟧ᵀ
⟦weaken★ᴺ⟧ {τ = τ} = ⟦tra★⟧ ≈-→★-Sᴺ τ

⟦weaken★⟧ : ⟦ B.weaken★ {κ′ = κ′} τ ⟧ᵀ ≡ F.weaken★ ⟦ τ ⟧ᵀ
⟦weaken★⟧ {τ = τ} = ⟦tra★⟧ ≈-→★-S τ

⟦weakenᶻ⟧ : ⟦ B.weakenᶻ {κ = κ} n ⟧∈ᶻ ≡ F.weaken★ ⟦ n ⟧∈ᶻ
⟦weakenᶻ⟧ {n = dynamic ℓ} = refl
⟦weakenᶻ⟧ {n = static n} = refl

⟦weakenᶻᴺ⟧ : ⟦ B.weakenᶻᴺ n ⟧∈ᶻ ≡ F.weaken★ ⟦ n ⟧∈ᶻ
⟦weakenᶻᴺ⟧ {n = dynamic ℓ} = refl
⟦weakenᶻᴺ⟧ {n = static n} = refl

⟦weaken★ᴺ⟧∙ : B.weaken★ᴺ $ τ ≡ τ′ → F.weaken★ $ ⟦ τ ⟧ᵀ ≡ ⟦ τ′ ⟧ᵀ
⟦weaken★ᴺ⟧∙ {τ = τ} `refl = ` sym (⟦weaken★ᴺ⟧ {τ = τ})

⟦weaken★⟧∙ : B.weaken★ $ τ ≡ τ′ → F.weaken★ $ ⟦ τ ⟧ᵀ ≡ ⟦ τ′ ⟧ᵀ
⟦weaken★⟧∙ {τ = τ} `refl = ` sym (⟦weaken★⟧ {τ = τ})

⟦weakenᶻᴺ⟧∙ : B.weakenᶻᴺ $ n ≡ n′ → F.weaken★ $ ⟦ n ⟧∈ᶻ ≡ ⟦ n′ ⟧∈ᶻ
⟦weakenᶻᴺ⟧∙ {n = n} `refl = ` sym (⟦weakenᶻᴺ⟧ {n = n})

⟦weakenᶻ⟧∙ : B.weakenᶻ $ n ≡ n′ → F.weaken★ $ ⟦ n ⟧∈ᶻ ≡ ⟦ n′ ⟧∈ᶻ
⟦weakenᶻ⟧∙ {n = n} `refl = ` sym (⟦weakenᶻ⟧ {n = n})
```

## Translate terms

Translate contexts

```
⟦_⟧ᵍ : ∀ (Γ : B.Context L Δ) → F.Context L ⟦ Δ ⟧ᴷ
⟦ ∅ ⟧ᵍ = ∅
⟦ Γ ▷ τ ⟧ᵍ = ⟦ Γ ⟧ᵍ ▷ ⟦ τ ⟧ᵀ
⟦ Γ ▷ᴺ- ⟧ᵍ = ⟦ Γ ⟧ᵍ ▷★ SCO ▷ Name (Var Z)
⟦ Γ ▷★ κ ⟧ᵍ = ⟦ Γ ⟧ᵍ ▷★ ⟦ κ ⟧K
```

Translate variables.

```
⟦_⟧∈ : τ B.∈ Γ → ⟦ τ ⟧ᵀ F.∈ ⟦ Γ ⟧ᵍ
⟦ Z ⟧∈ = Z
⟦ S x ⟧∈ = S ⟦ x ⟧∈
⟦ Sᴺ_ {τ = τ} x wkn≡ ⟧∈ = S S★_ ⟦ x ⟧∈ (⟦weaken★ᴺ⟧∙ {τ = τ} wkn≡)
⟦ S★_ {τ = τ} x wkn≡ ⟧∈ = S★_ ⟦ x ⟧∈ (⟦weaken★⟧∙ {τ = τ} wkn≡)
```

Translate BBD handler names `a : σ B.∈ Γ` to:
1. scope variables `⟦ a ⟧η`
2. FCN handler names `⟦ a ⟧Name` (whose type is indexed by scope variables)

```
⟦_∣_⟧Name : (Γ : B.Context L Δ) → (n : -∈ᴺ Δ) → Name (Var ⟦ n ⟧∈ᴺ) F.∈ ⟦ Γ ⟧ᵍ
⟦ Γ ▷ᴺ- ∣ Z ⟧Name = Z
⟦ Γ ▷ _ ∣ n ⟧Name = S ⟦ Γ ∣ n ⟧Name
⟦ Γ ▷ᴺ- ∣ Sᴺ n ⟧Name = S (S★_ ⟦ Γ ∣ n ⟧Name `refl)
⟦ Γ ▷★ κ ∣ S n ⟧Name = S★_ ⟦ Γ ∣ n ⟧Name `refl
```

```
⟦_⟧N : {Γ : B.Context L Δ} → (n : -∈ᴺ L ⨟ Δ) → ⟦ Γ ⟧ᵍ F.⊢ Name ⟦ n ⟧∈ᶻ
⟦ static n ⟧N = Var ⟦ _ ∣ n ⟧Name
⟦ dynamic ℓ ⟧N = name ℓ
```

More substitution lemmas.

```
⟦-[_]★ᵀ⟧ : (τ : L B.⨟ Δ ⊢ᵀ κ) → B.-[ τ ]★ ≈[→★] F.-[ ⟦ τ ⟧ᵀ ]★
≈-→ᴰ ⟦-[ τ ]★ᵀ⟧ Z = refl
≈-→ᴰ ⟦-[ τ ]★ᵀ⟧ (S α) = refl
≈-→ᴺ ⟦-[ τ ]★ᵀ⟧ (S α) = refl

⟦-[_]★ᴺ⟧ : (n : B.-∈ᴺ L ⨟ Δ) → B.-[ n ]★ᴺ ≈[→★] F.-[ ⟦ n ⟧∈ᶻ ]★
≈-→ᴰ ⟦-[ n ]★ᴺ⟧ (Sᴺ α) = refl
≈-→ᴺ ⟦-[ n ]★ᴺ⟧ Z = refl
≈-→ᴺ ⟦-[ n ]★ᴺ⟧ (Sᴺ α) = refl


⟦_[_]★ᵀ⟧≡ : (σ : L B.⨟ Δ ▷ κ₂ ⊢ᵀ κ₁) (τ : L B.⨟ Δ ⊢ᵀ κ₂) → ⟦ σ B.[ τ ]★ ⟧ᵀ ≡ ⟦ σ ⟧ᵀ F.[ ⟦ τ ⟧ᵀ ]★
⟦ σ [ τ ]★ᵀ⟧≡ = ⟦tra★⟧ ⟦-[ τ ]★ᵀ⟧ σ

⟦_[_]★ᴺ⟧≡ : (τ : L B.⨟ Δ ▷ᴺ- ⊢ᵀ κ) (n : B.-∈ᴺ L ⨟ Δ) → ⟦ τ [ n ]★ᴺ ⟧ᵀ ≡ ⟦ τ ⟧ᵀ F.[ ⟦ n ⟧∈ᶻ ]★
⟦ σ [ n ]★ᴺ⟧≡ = ⟦tra★⟧ ⟦-[ n ]★ᴺ⟧ σ

⟦_[_]★ᵀ⟧∙ : ∀ (σ : L B.⨟ Δ ▷ κ′ ⊢ᵀ κ) (τ : _) {σ[τ]} → σ B.[ τ ]★ ≡ σ[τ] → ⟦ σ ⟧ᵀ F.[ ⟦ τ ⟧ᵀ ]★ ≡ ⟦ σ[τ] ⟧ᵀ
⟦ σ [ τ ]★ᵀ⟧∙ ≡σ[τ] = ≡.trans (sym ⟦ σ [ τ ]★ᵀ⟧≡) (cong ⟦_⟧ᵀ ≡σ[τ])

⟦_[_]★ᴺ⟧∙ : ∀ (σ : L B.⨟ Δ ▷ᴺ- ⊢ᵀ κ) (n : _) {σ[n]} → σ B.[ n ]★ᴺ ≡ σ[n] → ⟦ σ ⟧ᵀ F.[ ⟦ n ⟧∈ᶻ ]★ ≡ ⟦ σ[n] ⟧ᵀ
⟦ σ [ n ]★ᴺ⟧∙ ≡σ[τ] = ≡.trans (sym ⟦ σ [ n ]★ᴺ⟧≡) (cong ⟦_⟧ᵀ ≡σ[τ])
```

### Translate terms

```
⟦_⟧ : Γ B.⊢ τ → ⟦ Γ ⟧ᵍ F.⊢ ⟦ τ ⟧ᵀ
```

Translate values.

```
⟦ tt ⟧ = tt
⟦ ⦅⦆ ⟧ = ⦅⦆
⟦ V , W ⟧ = ⟦ V ⟧ , ⟦ W ⟧
⟦ _,ᵀ_⦅_⦆ {σ = σ} τ W σ[τ]≡ ⟧ = ⟦ τ ⟧ᵀ ,ᵀ ⟦ W ⟧ ⦅ ⟦ σ [ τ ]★ᵀ⟧∙ σ[τ]≡ ⦆
⟦ _,ᴺ_⦅_⦆ {σ = σ} n W σ[n]≡ ⟧ = ⟦ n ⟧∈ᶻ ,ᵀ (⟦ n ⟧N , ⟦ W ⟧) ⦅ cong₂ _⇒ˢ_ refl (⟦ σ [ n ]★ᴺ⟧∙ σ[n]≡) ⦆
```

```
⟦ Var x ⟧ = Var ⟦ x ⟧∈
⟦ ƛ M ⟧ = ƛ ⟦ M ⟧
⟦ ƛᴺ M ⟧ = Λ (ƛ return ⟦ M ⟧)
⟦ Λ M ⟧ = Λ ⟦ M ⟧
```

Translate handlers.

```
⟦ ƛʰ M ⟧ = ƛʰ ⟦ M ⟧
⟦ ƛʰᴺ_ M wkn≡ ⟧ = Λʰ_ (ƛʰ ⟦ M ⟧) (⟦weaken★ᴺ⟧∙ wkn≡)
⟦ Λʰ_ H wkn≡ ⟧ = Λʰ_ ⟦ H ⟧ (⟦weaken★⟧∙ wkn≡)
⟦ Handler M ⟧ = Handler ⟦ M ⟧
```

Translate computations.

```
⟦ appᴺ {σ = σ} M n σ[n]≡ ⟧ = appᵀ ⟦ M ⟧ ⟦ n ⟧∈ᶻ ⟦σ[n]⟧≡ >>= appᵛ (Var Z) (F.weaken ⟦ n ⟧N)
  where ⟦σ[n]⟧≡ = cong₂ _⇒_ refl (cong₂ _/_ (⟦ σ [ n ]★ᴺ⟧∙ σ[n]≡) refl)
⟦ perform n V ⟧ = perform ⟦ n ⟧N ⟦ V ⟧
⟦ handle M H (wkn-τ≡ , wkn-σ≡ , wkn-ε≡) ⟧ = handle ⟦ M ⟧ ⟦ H ⟧ wkn≡³
  where wkn≡³ = (⟦weaken★ᴺ⟧∙ wkn-τ≡ , ⟦weaken★ᴺ⟧∙ wkn-σ≡ , ⟦weaken★ᴺ⟧∙ wkn-ε≡)
⟦ handle′ ℓ∉ M H ⟧ = handle′ ⟦ ℓ∉ ⟧∉ ⟦ M ⟧ ⟦ H ⟧
⟦ appᵛ N M ⟧ = appᵛ ⟦ N ⟧ ⟦ M ⟧
⟦ appᵀ {σ = σ} N τ σ[τ]≡ ⟧ = appᵀ ⟦ N ⟧ ⟦ τ ⟧ᵀ (⟦ σ [ τ ]★ᵀ⟧∙ σ[τ]≡)
⟦ return M ⟧ = return ⟦ M ⟧
⟦ M >>= N ⟧ = ⟦ M ⟧ >>= ⟦ N ⟧
⟦ subeffect ε⊑ε′ M ⟧ = subeffect ⟦ ε⊑ε′ ⟧⊑ ⟦ M ⟧
```

```
⟦ren★L⟧∙ : B.ren★L rᴸ τ ≡ τ′ → F.ren★L rᴸ ⟦ τ ⟧ᵀ ≡ ⟦ τ′ ⟧ᵀ
⟦ren★L⟧∙ {τ = τ} refl = sym (⟦ren★L⟧ {τ = τ})
```

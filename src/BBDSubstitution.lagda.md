```
module BBDSubstitution where

open import Common using (cong₃; _$_≡_; `_; `refl)
open import Labels
open import BindersByDay

open import Function.Base using (_∘_; it)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; sym; trans; cong; cong₂; subst; module ≡-Reasoning)
```

```
private variable
  κ κ′ : Kind
  L L′ L″ L₂′ : Labels
  Δ Δ′ Δ″ Δ₂′ : Contextᴷ
  LΔ LΔ′ LΔ″ : Labels × Contextᴷ
  ℓ ℓ′ ℓ₁ ℓ₁′ : -∈ᴸ L
  η η′ η₁ η₁′ : -∈ᴺ L ⨟ Δ
  rᴸ : L →ᴸ L′
  rᴸ′ : L′ →ᴸ L″
  rᴸ″ : L →ᴸ L″
  τ τ₁ τ₂ τ₁′ τ₂′ τ′ τ″ τ₃ : L ⨟ Δ ⊢ᵀ κ
  ε ε′ ε₁ ε₁′ ε″ ε₁″ : L ⨟ Δ ⊢ᵀ EFF
  σ σ′ σ₁ σ₁′ : L ⨟ Δ ⊢ᵀ SIG
  τ/ε τ/ε′ τ/ε₁ τ/ε₁′ : L ⨟ Δ ⊢ᵀ COM
  Γ Γ′ Γ″ Γ³ : Context L Δ
  r★ r★′ r★″ : L →ᴸ L′ × Δ →ᴰ Δ′
  sᴰ sᴰ′ : Δ ↠ᴰ L′ ⨟ Δ′
  s★ s★′ s★″ : L →ᴸ L′ × Δ ↠ᴰ L′ ⨟ Δ′
```

```
open ⦅_⦆_→★_
open Kit★
```

```
→ᴸ⇒→★ : (L →ᴸ L′) → (L , Δ) →★ (L′ , Δ)
→ᴸ⇒→★ rᴸ = rᴸ ; (λ x → x) ; static
```

```
ren★L : ∀ {L L′} → (L →ᴸ L′) → ∀ {Δ κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ ⊢ᵀ κ
ren★L rᴸ = ren★ (→ᴸ⇒→★ rᴸ)

weaken★L : ∀ {Δ κ} → L ⨟ Δ ⊢ᵀ κ → L ▷- ⨟ Δ ⊢ᵀ κ
weaken★L = ren★L →ᴸ-S
```

### Substitution lemmas

```
⦅_,_⦆_≡ᵀ_ : ∀ {_#_} {_%_} (K : Kit★ _#_) (K′ : Kit★ _%_) → ∀ {κ} → LΔ # κ → LΔ % κ → Set
⦅ K , K′ ⦆ t ≡ᵀ u = to-⊢★ K t ≡ to-⊢★ K′ u
```

```
record _≡[↠★]_ {_#_} {_%_} {K : Kit★ _#_} {K′ : Kit★ _%_} (r : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)) (r′ : ⦅ K′ ⦆ (L , Δ) →★ (L′ , Δ′)) : Set where
  constructor _,_
  field
    tra∈★-≡ : ∀ {κ} (α : κ ∈★ Δ) → ⦅ K , K′ ⦆ tra∈★ r α ≡ᵀ tra∈★ r′ α
    tra∈ᴺ-≡ : ∀ (n : -∈ᴺ Δ) → tra∈ᴺ r n ≡ tra∈ᴺ r′ n

open _≡[↠★]_
```

```
≡[↠★]-refl : ∀ {_#_} {K : Kit★ _#_} {s : ⦅ K ⦆ LΔ →★ LΔ′} → s ≡[↠★] s
≡[↠★]-refl = record {
  tra∈★-≡ = λ _ → refl ;
  tra∈ᴺ-≡ = λ _ → refl }
```

```
≡[↠★]-sym : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} {s : ⦅ K ⦆ LΔ →★ LΔ′} {s′ : ⦅ K′ ⦆ LΔ →★ LΔ′} → s ≡[↠★] s′ → s′ ≡[↠★] s
≡[↠★]-sym s≡ = record {
  tra∈★-≡ = λ α → sym (tra∈★-≡ s≡ α) ;
  tra∈ᴺ-≡ = λ n → sym (tra∈ᴺ-≡ s≡ n) }
```

```
≡[↠★]-trans : ∀ {_#_ _%_ _&_} {K : Kit★ _#_} {K′ : Kit★ _%_} {K″ : Kit★ _&_}
  → ∀ {s : ⦅ K ⦆ LΔ →★ LΔ′} {s′ : ⦅ K′ ⦆ LΔ →★ LΔ′} {s″ : ⦅ K″ ⦆ LΔ →★ LΔ′}
  → s ≡[↠★] s′ → s′ ≡[↠★] s″ → s ≡[↠★] s″
≡[↠★]-trans s≡ s≡′ = record {
  tra∈★-≡ = λ α → trans (tra∈★-≡ s≡ α) (tra∈★-≡ s≡′ α) ;
  tra∈ᴺ-≡ = λ n → trans (tra∈ᴺ-≡ s≡ n) (tra∈ᴺ-≡ s≡′ n) }
```

```
record GoodKit★ {_#_ : Labels × Contextᴷ → Kind → Set} (K : Kit★ _#_) : Set where
  no-eta-equality
  field
    to★-from★ : ∀ {κ} (α : κ ∈★ Δ) → to-⊢★ K (from-∈★ K {L = L} α) ≡ Var α
    to★-wkn★ : ∀ {κ} (t : LΔ # κ) → to-⊢★ K (wkn★ K t) ≡ weaken★ {κ′ = κ′} (to-⊢★ K t)
    to★-wkn★ᴺ : ∀ {κ} (t : LΔ # κ) → to-⊢★ K (wkn★ᴺ K t) ≡ weaken★ᴺ (to-⊢★ K t)

open GoodKit★
```

```
instance
  GK∈★ : GoodKit★ K∈★
  GK∈★ = record {
    to★-from★ = λ α → refl ;
    to★-wkn★ = λ α → refl ;
    to★-wkn★ᴺ = λ n → refl }
```

```
instance
  GK⊢★ : GoodKit★ K⊢★
  GK⊢★ = record {
    to★-from★ = λ α → refl ;
    to★-wkn★ = λ τ → refl ;
    to★-wkn★ᴺ = λ n → refl }
```

```
module _ {_#_ _%_ : _} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ where
  tra★▷-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → tra★▷ r ≡[↠★] tra★▷ r′ {κ}
  tra∈★-≡ (tra★▷-≡ r≡) Z = trans (to★-from★ GK Z) (sym (to★-from★ GK′ Z))
  tra∈★-≡ (tra★▷-≡ {r = r} {r′} r≡) (S α) = trans (to★-wkn★ GK (tra∈★ r α)) (trans (cong weaken★ (tra∈★-≡ r≡ α)) (sym (to★-wkn★ GK′ (tra∈★ r′ α))))
  tra∈ᴺ-≡ (tra★▷-≡ r≡) (S n) = cong weakenᶻ (tra∈ᴺ-≡ r≡ n)

  tra★▷ᴺ-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → tra★▷ᴺ r ≡[↠★] tra★▷ᴺ r′
  tra∈★-≡ (tra★▷ᴺ-≡ {r = r} {r′} r≡) (Sᴺ α) = trans (to★-wkn★ᴺ GK (tra∈★ r α)) (trans (cong weaken★ᴺ (tra∈★-≡ r≡ α)) (sym (to★-wkn★ᴺ GK′ (tra∈★ r′ α))))
  tra∈ᴺ-≡ (tra★▷ᴺ-≡ r≡) Z = refl
  tra∈ᴺ-≡ (tra★▷ᴺ-≡ r≡) (Sᴺ n) = cong weakenᶻᴺ (tra∈ᴺ-≡ r≡ n)

  tra∈ᶻ-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → ∀ (n : -∈ᴺ L ⨟ Δ)
    → tra∈ᶻ r n ≡ tra∈ᶻ r′ n
  tra∈ᶻ-≡ r≡ (dynamic ℓ) = cong dynamic (ren∈ᴸ-irr ℓ)
  tra∈ᶻ-≡ r≡ (static n) = tra∈ᴺ-≡ r≡ n

  tra★-≡ : {r : ⦅ K ⦆ (L , Δ) →★ LΔ′} {r′ : ⦅ K′ ⦆ (L , Δ) →★ LΔ′}
    → r ≡[↠★] r′
    → ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ)
    → tra★ r τ ≡ tra★ r′ τ
  tra★-≡ r≡ (Var α) = tra∈★-≡ r≡ α
  tra★-≡ r≡ (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (τ₁ / τ₂) = cong₂ _/_ (tra★-≡ r≡ τ₁) (tra★-≡ r≡ τ₂)
  tra★-≡ r≡ (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-≡ (tra★▷-≡ r≡) τ)
  tra★-≡ r≡ (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-≡ (tra★▷-≡ r≡) τ)
  tra★-≡ r≡ (Forallˢᴺ τ) = cong Forallˢᴺ (tra★-≡ (tra★▷ᴺ-≡ r≡) τ)
  tra★-≡ r≡ (Forallᴺ τ) = cong Forallᴺ (tra★-≡ (tra★▷ᴺ-≡ r≡) τ)
  tra★-≡ r≡ Unit = refl
  tra★-≡ r≡ ι = refl
  tra★-≡ r≡ (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra∈ᶻ-≡ r≡ η) (tra★-≡ r≡ σ) (tra★-≡ r≡ ε)
  tra★-≡ r≡ ⟨ τ ⟩ = cong ⟨_⟩ (tra★-≡ r≡ τ)
```

```
module _ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ where
  tra∈ᶻ-id : ∀ (n : -∈ᴺ L ⨟ Δ) → tra∈ᶻ (→★-id {K = K}) n ≡ n
  tra∈ᶻ-id (dynamic ℓ) = refl
  tra∈ᶻ-id (static n) = refl

  tra★▷-id : ∀ {κ} → tra★▷ {L = L} {Δ = Δ} {K = K} →★-id {κ} ≡[↠★] →★-id {K = K}
  tra∈★-≡ tra★▷-id Z = refl
  tra∈★-≡ tra★▷-id (S α) = trans (to★-wkn★ GK _) (trans (cong weaken★ (to★-from★ GK α)) (sym (to★-from★ GK (S α))))
  tra∈ᴺ-≡ tra★▷-id (S n) = refl

  tra★▷ᴺ-id : tra★▷ᴺ {L = L} {Δ = Δ} {K = K} →★-id ≡[↠★] →★-id {K = K}
  tra∈★-≡ tra★▷ᴺ-id (Sᴺ α) = trans (to★-wkn★ᴺ GK _) (trans (cong weaken★ᴺ (to★-from★ GK α)) (sym (to★-from★ GK (Sᴺ α))))
  tra∈ᴺ-≡ tra★▷ᴺ-id Z = refl
  tra∈ᴺ-≡ tra★▷ᴺ-id (Sᴺ n) = refl

  tra★-id : ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ) → tra★ (→★-id {K = K}) τ ≡ τ

  tra★-id▷ : ∀ {κ} (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ) → tra★ (tra★▷ →★-id) τ ≡ τ
  tra★-id▷ τ = trans (tra★-≡ tra★▷-id τ) (tra★-id τ)

  tra★-id▷ᴺ : ∀ (τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ) → tra★ (tra★▷ᴺ →★-id) τ ≡ τ
  tra★-id▷ᴺ τ = trans (tra★-≡ tra★▷ᴺ-id τ) (tra★-id τ)

  tra★-id (Var α) = to★-from★ GK α
  tra★-id (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (τ₁ / τ₂) = cong₂ _/_ (tra★-id τ₁) (tra★-id τ₂)
  tra★-id (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-id▷ τ)
  tra★-id (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-id▷ τ)
  tra★-id (Forallˢᴺ τ) = cong Forallˢᴺ (tra★-id▷ᴺ τ)
  tra★-id (Forallᴺ τ) = cong Forallᴺ (tra★-id▷ᴺ τ)
  tra★-id Unit = refl
  tra★-id ι = refl
  tra★-id (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra∈ᶻ-id η) (tra★-id σ) (tra★-id ε)
  tra★-id ⟨ τ ⟩ = cong ⟨_⟩ (tra★-id τ)
```

```
lift★ : ∀ {_#_} {K : Kit★ _#_}
  → ⦅ K ⦆ LΔ →★ LΔ′
  → LΔ ↠★ LΔ′
relabel (lift★ r) = relabel r
tra∈★ (lift★ r) = tra∈★′ r
tra∈ᴺ (lift★ r) = tra∈ᴺ r
```

```
lift★≡ : ∀ {_#_} {K : Kit★ _#_} ⦃ _ : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → lift★ r ≡[↠★] r
lift★≡ = ((λ α → refl) , (λ n → refl))
```

```
tra★-lift : ∀ {_#_} {K : Kit★ _#_} ⦃ _ : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → (τ : LΔ ⊢★ κ)
  → tra★ (lift★ r) τ ≡ tra★ r τ
tra★-lift = tra★-≡ lift★≡
```

```
_∘[↠★]_ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_}
  → ⦅ K′ ⦆ LΔ′ →★ LΔ″ → ⦅ K ⦆ LΔ →★ LΔ′ → ⦅ K⊢★ ⦆ LΔ →★ LΔ″
relabel (r′ ∘[↠★] r) = relabel r′ ∘ᴸ relabel r
tra∈★ (r′ ∘[↠★] r) α = tra★ r′ (tra∈★′ r α)
tra∈ᴺ (r′ ∘[↠★] r) n = tra∈ᶻ r′ (tra∈ᴺ r n)
```

```
record Kit★-∘ {_#_ : _} {_%_ : _} (K : Kit★ _#_) (K′ : Kit★ _%_) : Set where
  no-eta-equality
  field
    tra★∘wkn★ : ∀ {r′ : ⦅ K′ ⦆ LΔ →★ LΔ′} {κ} (t : LΔ # κ) → tra★ (tra★▷ r′ {κ′}) (to-⊢★ K (wkn★ K t)) ≡ weaken★ (tra★ r′ (to-⊢★ K t))
    tra★∘wkn★ᴺ : ∀ {r′ : ⦅ K′ ⦆ LΔ →★ LΔ′} {κ} (t : LΔ # κ) → tra★ (tra★▷ᴺ r′) (to-⊢★ K (wkn★ᴺ K t)) ≡ weaken★ᴺ (tra★ r′ (to-⊢★ K t))

open Kit★-∘ ⦃ ... ⦄
```

```
tra∈ᶻ∘weakenᶻ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → (n : -∈ᴺ proj₁ LΔ ⨟ proj₂ LΔ)
  → tra∈ᶻ (tra★▷ r {κ}) (weakenᶻ n) ≡ weakenᶻ (tra∈ᶻ r n)
tra∈ᶻ∘weakenᶻ (static n) = refl
tra∈ᶻ∘weakenᶻ (dynamic ℓ) = refl
```

```
tra∈ᶻ∘weakenᶻᴺ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → (n : -∈ᴺ proj₁ LΔ ⨟ proj₂ LΔ)
  → tra∈ᶻ (tra★▷ᴺ r) (weakenᶻᴺ n) ≡ weakenᶻᴺ (tra∈ᶻ r n)
tra∈ᶻ∘weakenᶻᴺ (static n) = refl
tra∈ᶻ∘weakenᶻᴺ (dynamic ℓ) = refl
```

```
module _ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄ where
  tra★▷-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → (tra★▷ r′ {κ} ∘[↠★] tra★▷ r) ≡[↠★] tra★▷ (r′ ∘[↠★] r)
  tra∈★-≡ tra★▷-∘ Z = trans (cong (tra★ (tra★▷ _)) (to★-from★ GK Z)) (to★-from★ GK′ Z)
  tra∈★-≡ (tra★▷-∘ {r′ = r′} {r}) (S α) = tra★∘wkn★ {r′ = r′} (tra∈★ r α)
  tra∈ᴺ-≡ (tra★▷-∘ {r = r}) (S n) = tra∈ᶻ∘weakenᶻ (tra∈ᴺ r n)

  tra★▷ᴺ-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → (tra★▷ᴺ r′ ∘[↠★] tra★▷ᴺ r) ≡[↠★] tra★▷ᴺ (r′ ∘[↠★] r)
  tra∈★-≡ (tra★▷ᴺ-∘ {r′ = r′} {r}) (Sᴺ α) = tra★∘wkn★ᴺ {r′ = r′} (tra∈★ r α)
  tra∈ᴺ-≡ tra★▷ᴺ-∘ Z = refl
  tra∈ᴺ-≡ (tra★▷ᴺ-∘ {r = r}) (Sᴺ n) = tra∈ᶻ∘weakenᶻᴺ (tra∈ᴺ r n)

  tra∈ᶻ-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ (n : -∈ᴺ L ⨟ Δ)
    → tra∈ᶻ r′ (tra∈ᶻ r n) ≡ tra∈ᶻ (r′ ∘[↠★] r) n
  tra∈ᶻ-∘ {r′ = r′} (dynamic ℓ) = cong dynamic (ren∈ᴸ-∘ {rᴸ′ = relabel r′} ℓ)
  tra∈ᶻ-∘ (static n) = refl

  tra★-∘ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ)
    → tra★ r′ (tra★ r τ) ≡ tra★ (r′ ∘[↠★] r) τ

  tra★-∘▷ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ {κ} (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
    → tra★ (tra★▷ r′) (tra★ (tra★▷ r) τ) ≡ tra★ (tra★▷ (r′ ∘[↠★] r)) τ
  tra★-∘▷ τ = trans (tra★-∘ τ) (tra★-≡ tra★▷-∘ τ)

  tra★-∘▷ᴺ : {r′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r : ⦅ K ⦆ (L , Δ) →★ LΔ′}
    → ∀ (τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ)
    → tra★ (tra★▷ᴺ r′) (tra★ (tra★▷ᴺ r) τ) ≡ tra★ (tra★▷ᴺ (r′ ∘[↠★] r)) τ
  tra★-∘▷ᴺ τ = trans (tra★-∘ τ) (tra★-≡ tra★▷ᴺ-∘ τ)

  tra★-∘ (Var x) = refl
  tra★-∘ (τ₁ ⇒ τ₂) = cong₂ _⇒_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ˢ τ₂) = cong₂ _⇒ˢ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ʰ τ₂) = cong₂ _⇒ʰ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ ⇒ᵖ τ₂) = cong₂ _⇒ᵖ_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (τ₁ / τ₂) = cong₂ _/_ (tra★-∘ τ₁) (tra★-∘ τ₂)
  tra★-∘ (Forallˢ κ τ) = cong (Forallˢ κ) (tra★-∘▷ τ)
  tra★-∘ (Forallᵀ κ τ) = cong (Forallᵀ κ) (tra★-∘▷ τ)
  tra★-∘ (Forallˢᴺ τ) = cong Forallˢᴺ (tra★-∘▷ᴺ τ)
  tra★-∘ (Forallᴺ τ) = cong Forallᴺ (tra★-∘▷ᴺ τ)
  tra★-∘ Unit = refl
  tra★-∘ ι = refl
  tra★-∘ (η ⦂ σ ∷ ε) = cong₃ _⦂_∷_ (tra∈ᶻ-∘ η) (tra★-∘ σ) (tra★-∘ ε)
  tra★-∘ ⟨ τ ⟩ = cong ⟨_⟩ (tra★-∘ τ)
```

```
instance
  K-∘-∈★-∈★ : Kit★-∘ K∈★ K∈★
  K-∘-∈★-∈★ = record {
    tra★∘wkn★ = λ α → refl ;
    tra★∘wkn★ᴺ = λ α → refl }
```

```
instance
  K-∘-∈★-⊢★ : Kit★-∘ K∈★ K⊢★
  K-∘-∈★-⊢★ = record {
    tra★∘wkn★ = λ α → refl ;
    tra★∘wkn★ᴺ = λ α → refl }
```

```
→★-S★-∘ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ (r : ⦅ K ⦆ LΔ →★ LΔ′)
  → (→★-S★ {K = K∈★} {κ = κ} ∘[↠★] r) ≡[↠★] (tra★▷ r ∘[↠★] →★-S★ {K = K∈★})
tra∈★-≡ (→★-S★-∘ ⦃ GK = GK ⦄ r) = λ α → sym (to★-wkn★ GK (tra∈★ r α))
tra∈ᴺ-≡ (→★-S★-∘ r) n = refl
```

```
→★-Sᴺ-∘ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ (r : ⦅ K ⦆ LΔ →★ LΔ′)
  → (→★-Sᴺ {K = K∈★} ∘[↠★] r) ≡[↠★] (tra★▷ᴺ r ∘[↠★] →★-Sᴺ {K = K∈★})
tra∈★-≡ (→★-Sᴺ-∘ ⦃ GK = GK ⦄ r) = λ α → sym (to★-wkn★ᴺ GK (tra∈★ r α))
tra∈ᴺ-≡ (→★-Sᴺ-∘ r) n = refl
```

```
weaken★∘tra★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ {κ′ = κ′} (tra★ r τ) ≡ tra★ (tra★▷ r) (weaken★ τ)
weaken★∘tra★ {r = r} τ = begin
  weaken★ (tra★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-S★ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-S★-∘ r) τ ⟩
  sub★ (tra★▷ r ∘[↠★] →★-S★ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  tra★ (tra★▷ r) (weaken★ τ) ∎
 where open ≡-Reasoning
```

```
weaken★ᴺ∘tra★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r : ⦅ K ⦆ LΔ →★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ᴺ (tra★ r τ) ≡ tra★ (tra★▷ᴺ r) (weaken★ᴺ τ)
weaken★ᴺ∘tra★ {r = r} τ = begin
  weaken★ᴺ (tra★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-Sᴺ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-Sᴺ-∘ r) τ ⟩
  sub★ (tra★▷ᴺ r ∘[↠★] →★-Sᴺ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  tra★ (tra★▷ᴺ r) (weaken★ᴺ τ) ∎
 where open ≡-Reasoning
```

```
instance
  K-∘-⊢★-∈★ : Kit★-∘ K⊢★ K∈★
  K-∘-⊢★-∈★ = record {
    tra★∘wkn★ = sym ∘ weaken★∘tra★ ;
    tra★∘wkn★ᴺ = sym ∘ weaken★ᴺ∘tra★ }
```

```
weaken★∘sub★ : {r : LΔ ↠★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ {κ′ = κ′} (sub★ r τ) ≡ sub★ (tra★▷ r) (weaken★ τ)
weaken★∘sub★ {r = r} τ = begin
  weaken★ (sub★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-S★ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-S★-∘ r) τ ⟩
  sub★ (tra★▷ r ∘[↠★] →★-S★ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  sub★ (tra★▷ r) (weaken★ τ) ∎
 where open ≡-Reasoning
```

```
weaken★ᴺ∘sub★ : {r : LΔ ↠★ LΔ′}
  → ∀ {κ} (τ : LΔ ⊢★ κ)
  → weaken★ᴺ (sub★ r τ) ≡ sub★ (tra★▷ᴺ r) (weaken★ᴺ τ)
weaken★ᴺ∘sub★ {r = r} τ = begin
  weaken★ᴺ (sub★ r τ) ≡⟨ tra★-∘ τ ⟩
  sub★ (→★-Sᴺ {K = K∈★} ∘[↠★] r) τ ≡⟨ tra★-≡ (→★-Sᴺ-∘ r) τ ⟩
  sub★ (tra★▷ᴺ r ∘[↠★] →★-Sᴺ {K = K∈★}) τ ≡⟨ sym (tra★-∘ τ) ⟩
  sub★ (tra★▷ᴺ r) (weaken★ᴺ τ) ∎
 where open ≡-Reasoning
```

```
instance
  K-∘-⊢★-⊢★ : Kit★-∘ K⊢★ K⊢★
  K-∘-⊢★-⊢★ = record {
    tra★∘wkn★ = sym ∘ weaken★∘tra★ ;
    tra★∘wkn★ᴺ = sym ∘ weaken★ᴺ∘tra★ }
```

```
weaken★∘tra★∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {τ : L ⨟ Δ ⊢ᵀ κ} {κ′ : Kind} {τ′ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ}
  → weaken★ $ τ ≡ τ′
  → weaken★ $ tra★ r★ τ ≡ tra★ (tra★▷ r★) τ′
weaken★∘tra★∙ {τ = τ} (` wkn-τ≡) = ` trans (weaken★∘tra★ τ) (cong (tra★ _) wkn-τ≡)
```

```
weaken★ᴺ∘tra★∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K∈★ ⦄ ⦃ K-∘′ : Kit★-∘ K∈★ K ⦄
  → {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {τ : L ⨟ Δ ⊢ᵀ κ} {τ′ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ}
  → weaken★ᴺ $ τ ≡ τ′
  → weaken★ᴺ $ tra★ r★ τ ≡ tra★ (tra★▷ᴺ r★) τ′
weaken★ᴺ∘tra★∙ {τ = τ} (` wkn-τ≡) = ` trans (weaken★ᴺ∘tra★ τ) (cong (tra★ _) wkn-τ≡)
```

```
weaken★∘ren★L∙ : ∀ {rᴸ : L →ᴸ L′} {τ : L ⨟ Δ ⊢ᵀ κ} {κ′} {τ′ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ}
  → weaken★ $ τ ≡ τ′
  → weaken★ $ ren★L rᴸ τ ≡ ren★L rᴸ τ′
weaken★∘ren★L∙ {τ = τ} (` wkn≡) = ` trans (tra★-∘ τ) (trans (tra★-≡ ((λ α → refl) , (λ n → refl)) τ) (trans (sym (tra★-∘ τ)) (cong (ren★L _) wkn≡)))
```

```
weaken★ᴺ∘ren★L∙ : ∀ {rᴸ : L →ᴸ L′} {τ : L ⨟ Δ ⊢ᵀ κ} {τ′ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ}
  → weaken★ᴺ $ τ ≡ τ′
  → weaken★ᴺ $ ren★L rᴸ τ ≡ ren★L rᴸ τ′
weaken★ᴺ∘ren★L∙ {τ = τ} (` wkn≡) = ` trans (tra★-∘ τ) (trans (tra★-≡ ((λ α → refl) , (λ n → refl)) τ) (trans (sym (tra★-∘ τ)) (cong (ren★L _) wkn≡)))
```

```
→ᴸ⇒→★-≡ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L →ᴸ L′}
  → →ᴸ⇒→★ rᴸ ≡[↠★] →ᴸ⇒→★ {Δ = Δ} rᴸ′
tra∈★-≡ →ᴸ⇒→★-≡ = λ α → refl
tra∈ᴺ-≡ →ᴸ⇒→★-≡ = λ n → refl
```

```
→ᴸ⇒→★-∘ : ∀ (rᴸ′ : L′ →ᴸ L″) (rᴸ : L →ᴸ L′)
  → (→ᴸ⇒→★ rᴸ′ ∘[↠★] →ᴸ⇒→★ rᴸ) ≡[↠★] →ᴸ⇒→★ {Δ = Δ} (rᴸ′ ∘ᴸ rᴸ)
tra∈★-≡ (→ᴸ⇒→★-∘ _ _) = λ α → refl
tra∈ᴺ-≡ (→ᴸ⇒→★-∘ _ _) = λ n → refl
```

```
ren★L-∘ᴸ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L′ →ᴸ L″} {rᴸ″ : L →ᴸ L″}
  → ∀ (τ : L ⨟ Δ ⊢ᵀ κ)
  → ren★L rᴸ′ (ren★L rᴸ τ) ≡ ren★L rᴸ″ τ
ren★L-∘ᴸ {rᴸ = rᴸ} {rᴸ′} τ = trans (tra★-∘ τ) (tra★-≡ (≡[↠★]-trans (→ᴸ⇒→★-∘ rᴸ′ rᴸ) →ᴸ⇒→★-≡) τ)
```

```
tra∈★-∘-[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → ∀ {κ} (x : κ ∈★ Δ ▷ κ′)
  → tra∈★ (r★ ∘[↠★] -[ σ ]★) x ≡ tra∈★ (-[ tra★ r★ σ ]★ ∘[↠★] tra★▷ r★) x
tra∈★-∘-[]★ ⦃ GK ⦄ Z = cong (tra★ _) (sym (to★-from★ GK Z))
tra∈★-∘-[]★ {K = K} ⦃ GK ⦄ {r★ = r★} (S α) = sym (trans (cong (tra★ _) (to★-wkn★ GK (tra∈★ r★ α))) (trans (tra★-∘ (to-⊢★ K (tra∈★ r★ α))) (tra★-id {K = K⊢★} _)))
```

```
tra∈ᴺ-∘-[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → ∀ n
  → tra∈ᴺ (r★ ∘[↠★] -[ σ ]★) n ≡ tra∈ᴺ (-[ tra★ r★ σ ]★ ∘[↠★] tra★▷ r★) n
tra∈ᴺ-∘-[]★ {r★ = r★} (S n) with tra∈ᴺ r★ n
... | dynamic ℓ = refl
... | static n = refl
```

```
∘-[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (r★ ∘[↠★] -[ σ ]★) ≡[↠★] (-[ tra★ r★ σ ]★ ∘[↠★] tra★▷ r★)
∘-[]★ {K = K} ⦃ GK ⦄ ⦃ K-∘′ ⦄ {r★ = r★} {σ = σ} = record {
  tra∈★-≡ = tra∈★-∘-[]★ {σ = σ} ;
  tra∈ᴺ-≡ = tra∈ᴺ-∘-[]★ {σ = σ} }
```

```
tra★∘[]★ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → tra★ r★ (τ [ σ ]★) ≡ tra★ (tra★▷ r★) τ [ tra★ r★ σ ]★
tra★∘[]★ ⦃ GK ⦄ τ = trans (tra★-∘ τ) (trans (tra★-≡ ∘-[]★ τ) (sym (tra★-∘ τ)))
```

```
tra★∘[]★∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′} {τ[σ]}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → τ [ σ ]★ ≡ τ[σ]
  → tra★ (tra★▷ r★) τ [ tra★ r★ σ ]★ ≡ tra★ r★ τ[σ]
tra★∘[]★∙ τ τ[σ]≡ = trans (sym (tra★∘[]★ τ)) (cong (tra★ _) τ[σ]≡)
```

```
[]★∘weaken★ : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (σ : L ⨟ Δ ⊢ᵀ κ′)
  → weaken★ τ [ σ ]★ ≡ τ
[]★∘weaken★ τ σ = trans (tra★-∘ τ) (trans (tra★-≡ ≡[↠★]-refl τ) (tra★-id τ))
```

```
[]★∘weaken★∙ : ∀ {τ : L ⨟ Δ ⊢ᵀ κ} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → weaken★ $ τ ≡ τ′
  → τ′ [ σ ]★ ≡ τ
[]★∘weaken★∙ {τ = τ} {σ} (` w≡) = trans (cong (_[ σ ]★) (sym w≡)) (trans (tra★-∘ τ) (trans (tra★-≡ ≡[↠★]-refl τ) (tra★-id τ)))
```

```
-[]★∘[↠★] : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (-[ tra★ s★ σ ]★ ∘[↠★] tra★▷ s★) ≡[↠★] (s★ ∘[↠★] -[ σ ]★)
tra∈★-≡ -[]★∘[↠★] Z = refl
tra∈★-≡ -[]★∘[↠★] (S α) = []★∘weaken★ _ _
tra∈ᴺ-≡ (-[]★∘[↠★] {s★ = s★}) (S n) with (tra∈ᴺ s★ n)
... | dynamic ℓ = refl
... | static n = refl
```

```
sub★∘[]★ : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → sub★ (tra★▷ s★) τ [ sub★ s★ σ ]★ ≡ sub★ s★ (τ [ σ ]★)
sub★∘[]★ τ = trans (tra★-∘ τ) (trans (tra★-≡ -[]★∘[↠★] τ) (sym (tra★-∘ τ)))
```

```
sub∘[]★∙ : ∀ {s★ : (L , Δ) ↠★ (L′ , Δ′)} {σ : L ⨟ Δ ⊢ᵀ κ′} {τ[σ]}
  → (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ)
  → τ [ σ ]★ ≡ τ[σ]
  → sub★ (tra★▷ s★) τ [ sub★ s★ σ ]★ ≡ sub★ s★ τ[σ]
sub∘[]★∙ τ τ[σ]≡ = trans (sub★∘[]★ τ) (cong (sub★ _) τ[σ]≡)
```

```
tra∈★-∘-[]★ᴺ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {n : -∈ᴺ L ⨟ Δ}
  → ∀ {κ} (x : κ ∈★ Δ ▷ᴺ-)
  → tra∈★ (r★ ∘[↠★] -[ n ]★ᴺ) x ≡ tra∈★ (-[ tra∈ᶻ r★ n ]★ᴺ ∘[↠★] tra★▷ᴺ r★) x
tra∈★-∘-[]★ᴺ {K = K} ⦃ GK ⦄ {r★ = r★} (Sᴺ α) = sym (trans (cong (tra★ _) (to★-wkn★ᴺ GK (tra∈★ r★ α))) (trans (tra★-∘ (to-⊢★ K (tra∈★ r★ α))) (tra★-id {K = K⊢★} _)))
```

```
tra∈ᴺ-∘-[]★ᴺ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {n : -∈ᴺ L ⨟ Δ}
  → ∀ m
  → tra∈ᴺ (r★ ∘[↠★] -[ n ]★ᴺ) m ≡ tra∈ᴺ (-[ tra∈ᶻ r★ n ]★ᴺ ∘[↠★] tra★▷ᴺ r★) m
tra∈ᴺ-∘-[]★ᴺ Z = refl
tra∈ᴺ-∘-[]★ᴺ {r★ = r★} (Sᴺ n) with tra∈ᴺ r★ n
... | dynamic ℓ = refl
... | static n = refl
```

```
∘-[]★ᴺ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {n : -∈ᴺ L ⨟ Δ}
  → (r★ ∘[↠★] -[ n ]★ᴺ) ≡[↠★] (-[ tra∈ᶻ r★ n ]★ᴺ ∘[↠★] tra★▷ᴺ r★)
∘-[]★ᴺ {K = K} ⦃ GK ⦄ ⦃ K-∘′ ⦄ {r★ = r★} {n = n} = record {
  tra∈★-≡ = tra∈★-∘-[]★ᴺ {n = n} ;
  tra∈ᴺ-≡ = tra∈ᴺ-∘-[]★ᴺ {n = n} }
```

```
tra★∘[]★ᴺ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {n : -∈ᴺ L ⨟ Δ}
  → (τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ)
  → tra★ r★ (τ [ n ]★ᴺ) ≡ tra★ (tra★▷ᴺ r★) τ [ tra∈ᶻ r★ n ]★ᴺ
tra★∘[]★ᴺ ⦃ GK ⦄ τ = trans (tra★-∘ τ) (trans (tra★-≡ ∘-[]★ᴺ τ) (sym (tra★-∘ τ)))
```

```
tra★∘[]★ᴺ∙ : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ ⦃ K-∘ : Kit★-∘ K K⊢★ ⦄ ⦃ K-∘′ : Kit★-∘ K⊢★ K ⦄
  → ∀ {r★ : ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)} {n : -∈ᴺ L ⨟ Δ} {τ[n]}
  → (τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ)
  → τ [ n ]★ᴺ ≡ τ[n]
  → tra★ (tra★▷ᴺ r★) τ [ tra∈ᶻ r★ n ]★ᴺ ≡ tra★ r★ τ[n]
tra★∘[]★ᴺ∙ τ τ[n]≡ = trans (sym (tra★∘[]★ᴺ τ)) (cong (tra★ _) τ[n]≡)
```

### Subtyping

```
infix 3 _≡[∈ᵉ]_
data _≡[∈ᵉ]_ (η∈ᵉ : η ⦂ σ ∈ᵉ ε) : η′ ⦂ σ′ ∈ᵉ ε′ → Set where
  refl : η∈ᵉ ≡[∈ᵉ] η∈ᵉ
```

```
infix 3 _≡[⊑]_
data _≡[⊑]_ (ε⊑ : ε ⊑ ε₁) : ε′ ⊑ ε₁′ → Set where
  refl : ε⊑ ≡[⊑] ε⊑
```

```
≡[⊑]-ι : ε ≡ ε′ → ι {ε′ = ε} ≡[⊑] ι {ε′ = ε′}
≡[⊑]-ι refl = refl
```

```
≡[⊑]-∷ : {η∈ : η ⦂ σ ∈ᵉ ε₁} {η∈′ : η′ ⦂ σ′ ∈ᵉ ε₁′} {ε⊑ : ε ⊑ ε₁} {ε⊑′ : ε′ ⊑ ε₁′} → η∈ ≡[∈ᵉ] η∈′ → ε⊑ ≡[⊑] ε⊑′ → η∈ ∷ ε⊑ ≡[⊑] η∈′ ∷ ε⊑′
≡[⊑]-∷ refl refl = refl
```

```
≡[⊑]-trans : {ε⊑ : ε ⊑ ε₁} {ε⊑′ : ε′ ⊑ ε₁′} {ε⊑″ : ε″ ⊑ ε₁″}
  → ε⊑ ≡[⊑] ε⊑′ → ε⊑′ ≡[⊑] ε⊑″ → ε⊑ ≡[⊑] ε⊑″
≡[⊑]-trans refl refl = refl
```


```
≡[∈ᵉ]-Z : η ≡ η′ → σ ≡ σ′ → ε ≡ ε′ → Z {η = η} {σ = σ} {ε = ε} ≡[∈ᵉ] Z {η = η′} {σ = σ′} {ε = ε′}
≡[∈ᵉ]-Z refl refl refl = refl
```

```
≡[∈ᵉ]-S : {η∈ε : η ⦂ σ ∈ᵉ ε} {η∈ε′ : η′ ⦂ σ′ ∈ᵉ ε′} → σ₁ ≡ σ₁′ → η₁ ≡ η₁′ → η∈ε ≡[∈ᵉ] η∈ε′ → S_ {η′ = η₁} {σ′ = σ₁} η∈ε ≡[∈ᵉ] S_ {η′ = η₁′} {σ′ = σ₁′} η∈ε′
≡[∈ᵉ]-S refl refl refl = refl
```

```
tra∈ᵉ : ∀ {_#_} {K : Kit★ _#_} → (r★ : ⦅ K ⦆ LΔ →★ LΔ′) → ∀ {η} {σ} {ε} → η ⦂ σ ∈ᵉ ε → tra∈ᶻ r★ η ⦂ tra★ r★ σ ∈ᵉ tra★ r★ ε
tra∈ᵉ r★ Z = Z
tra∈ᵉ r★ (S η∈ε) = S (tra∈ᵉ r★ η∈ε)
```

```
tra⊑ : ∀ {_#_} {K : Kit★ _#_} (r★ : ⦅ K ⦆ LΔ →★ LΔ′) → ∀ {ε} {ε′} → ε ⊑ ε′ → tra★ r★ ε ⊑ tra★ r★ ε′
tra⊑ r★ ι = ι
tra⊑ r★ (η∈ε′ ∷ ε⊑) = tra∈ᵉ r★ η∈ε′ ∷ tra⊑ r★ ε⊑
```

```
tra∈ᵉ-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄ (η∈ : η ⦂ σ ∈ᵉ ε) → tra∈ᵉ {K = K} →★-id η∈ ≡[∈ᵉ] η∈
tra∈ᵉ-id Z = ≡[∈ᵉ]-Z (tra∈ᶻ-id _) (tra★-id _) (tra★-id _)
tra∈ᵉ-id (S η∈) = ≡[∈ᵉ]-S (tra★-id _) (tra∈ᶻ-id _) (tra∈ᵉ-id η∈)
```

```
tra∈ᵉ-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′ → (η∈ : η ⦂ σ ∈ᵉ ε) → tra∈ᵉ r★ η∈ ≡[∈ᵉ] tra∈ᵉ r★′ η∈
tra∈ᵉ-≡ {η = η} {σ = σ} r≡ (Z {ε = ε}) = ≡[∈ᵉ]-Z (tra∈ᶻ-≡ r≡ η) (tra★-≡ r≡ σ) (tra★-≡ r≡ ε)
tra∈ᵉ-≡ r≡ (S_ {η′ = η′} {σ′ = σ′} η∈) = ≡[∈ᵉ]-S (tra★-≡ r≡ σ′) (tra∈ᶻ-≡ r≡ η′) (tra∈ᵉ-≡ r≡ η∈)
```

```
tra∈ᵉ-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r★ : ⦅ K ⦆ LΔ →★ LΔ′}
  → (η∈ : η ⦂ σ ∈ᵉ ε)
  → tra∈ᵉ r★′ (tra∈ᵉ r★ η∈) ≡[∈ᵉ] tra∈ᵉ (r★′ ∘[↠★] r★) η∈
tra∈ᵉ-∘ {η = η} {σ = σ} (Z {ε = ε}) = ≡[∈ᵉ]-Z (tra∈ᶻ-∘ η) (tra★-∘ σ) (tra★-∘ ε)
tra∈ᵉ-∘ (S_ {η′ = η′} {σ′ = σ′} η∈) = ≡[∈ᵉ]-S (tra★-∘ σ′) (tra∈ᶻ-∘ η′) (tra∈ᵉ-∘ η∈)
```

```
tra⊑-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → (ε⊑ : ε ⊑ ε₁)
  → tra⊑ {K = K} →★-id ε⊑ ≡[⊑] ε⊑
tra⊑-id ι = ≡[⊑]-ι (tra★-id _)
tra⊑-id (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-id _) (tra⊑-id _)
```

```
tra⊑-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′ → (ε⊑ : ε ⊑ ε′) → tra⊑ r★ ε⊑ ≡[⊑] tra⊑ r★′ ε⊑
tra⊑-≡ {ε′ = ε′} r≡ ι = ≡[⊑]-ι (tra★-≡ r≡ ε′)
tra⊑-≡ r≡ (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-≡ r≡ η∈) (tra⊑-≡ r≡ ε⊑)
```

```
tra⊑-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″} {r★ : ⦅ K ⦆ LΔ →★ LΔ′}
  → (ε⊑ : ε ⊑ ε′)
  → tra⊑ r★′ (tra⊑ r★ ε⊑) ≡[⊑] tra⊑ (r★′ ∘[↠★] r★) ε⊑
tra⊑-∘ {ε′ = ε′} ι = ≡[⊑]-ι (tra★-∘ ε′)
tra⊑-∘ (η∈ ∷ ε⊑) = ≡[⊑]-∷ (tra∈ᵉ-∘ η∈) (tra⊑-∘ ε⊑)
```

```
data _≡[∉]_ {ℓ} (ℓ∉ε : ℓ ∉ ε) : ℓ′ ∉ ε′ → Set where
  refl : ℓ∉ε ≡[∉] ℓ∉ε
```

```
≡[∉]-ι :
    ℓ ≡ ℓ′
  → ι {Δ = Δ} {ℓ = ℓ} ≡[∉] ι {Δ = Δ} {ℓ = ℓ′}
≡[∉]-ι refl = refl
```

```
≡[∉]-∷ :
    .{ℓ≢ : ℓ₁ ≢ ℓ} .{ℓ≢′ : ℓ₁′ ≢ ℓ′}
  → {ℓ∉ : ℓ ∉ ε} {ℓ∉′ : ℓ′ ∉ ε′}
  → ℓ₁ ≡ ℓ₁′
  → σ ≡ σ′
  → ℓ∉ ≡[∉] ℓ∉′
  → _∷_ {σ = σ} ℓ≢ ℓ∉ ≡[∉] _∷_ {σ = σ′} ℓ≢′ ℓ∉′
≡[∉]-∷ refl refl refl = refl
```

```
tra∉ : ∀ {_#_} {K : Kit★ _#_} {ℓ} {ε} → (r★@(rᴸ ; _ ; _) : ⦅ K ⦆ LΔ →★ LΔ′) → ℓ ∉ ε → ren∈ᴸ rᴸ ℓ ∉ tra★ r★ ε
tra∉ r★ ι = ι
tra∉ {ℓ = ℓ} r★@(rᴸ ; _ ; _) (≢ℓ ∷ ℓ∉ε) = (≢ℓ ∘ injective-ren∈ᴸ rᴸ) ∷ tra∉ r★ ℓ∉ε
```

```
tra∉-≡ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ →★ LΔ′}
  → r★ ≡[↠★] r★′
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ r★ ℓ∉ ≡[∉] tra∉ r★′ ℓ∉
tra∉-≡ {ℓ = ℓ} r★≡ ι = ≡[∉]-ι (ren∈ᴸ-irr ℓ)
tra∉-≡ {ℓ = ℓ} r★≡ (_∷_ {ℓ′ = ℓ′} {σ = σ} _ ℓ∉) = ≡[∉]-∷ (ren∈ᴸ-irr ℓ′) (tra★-≡ r★≡ σ) (tra∉-≡ r★≡ ℓ∉)
```

```
tra∉-id : ∀ {_#_} {K : Kit★ _#_} ⦃ GK : GoodKit★ K ⦄
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ {K = K} →★-id ℓ∉ ≡[∉] ℓ∉
tra∉-id ι = ≡[∉]-ι refl
tra∉-id (_∷_ {σ = σ} _ ℓ∉) = ≡[∉]-∷ refl (tra★-id σ) (tra∉-id ℓ∉)
```

```
tra∉-∘ : ∀ {_#_ _%_} {K : Kit★ _#_} {K′ : Kit★ _%_} ⦃ GK : GoodKit★ K ⦄ ⦃ GK′ : GoodKit★ K′ ⦄ ⦃ K-∘ : Kit★-∘ K K′ ⦄
    {r★ : ⦅ K ⦆ LΔ →★ LΔ′} {r★′ : ⦅ K′ ⦆ LΔ′ →★ LΔ″}
  → (ℓ∉ : ℓ ∉ ε)
  → tra∉ r★′ (tra∉ r★ ℓ∉) ≡[∉] tra∉ (r★′ ∘[↠★] r★) ℓ∉
tra∉-∘ {ℓ = ℓ} {r★′ = (rᴸ′ ; _ ; _)} ι = ≡[∉]-ι (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ)
tra∉-∘ {r★′ = (rᴸ′ ; _ ; _)} (_∷_ {ℓ′ = ℓ′} {σ = σ} _ ℓ∉) = ≡[∉]-∷ (ren∈ᴸ-∘ {rᴸ′ = rᴸ′} ℓ′) (tra★-∘ σ) (tra∉-∘ ℓ∉)
```

```
ren∉ᴸ : ∀ {ℓ} → (rᴸ : L →ᴸ L′) → ℓ ∉ ε → ren∈ᴸ rᴸ ℓ ∉ ren★L rᴸ ε
ren∉ᴸ rᴸ = tra∉ (→ᴸ⇒→★ rᴸ)
```

```
ren⊑ᴸ : (rᴸ : L →ᴸ L′) → ε ⊑ ε′ → ren★L rᴸ ε ⊑ ren★L rᴸ ε′
ren⊑ᴸ rᴸ = tra⊑ (→ᴸ⇒→★ rᴸ)
```

### Term equality

```
infix 2 _≡[∈]_
data _≡[∈]_ {Γ : Context L Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} (x : τ ∈ Γ) : ∀ {Γ′ : Context L′ Δ′} {τ′ : L′ ⨟ Δ′ ⊢ᵀ TYP} → τ′ ∈ Γ′ → Set where
  refl : x ≡[∈] x
```

```
≡[∈]-sym : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} → x ≡[∈] x′ → x′ ≡[∈] x
≡[∈]-sym refl = refl
```

```
≡[∈]-trans : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} {x″ : τ″ ∈ Γ″} → x ≡[∈] x′ → x′ ≡[∈] x″ → x ≡[∈] x″
≡[∈]-trans refl refl = refl
```

```
≡[∈]-subst : (τ≡ : τ ≡ τ′) → {x : τ ∈ Γ} → subst (_∈ _) τ≡ x ≡[∈] x
≡[∈]-subst refl = refl
```

```
≡[∈]-S : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′} → τ₁ ≡ τ₁′ → x ≡[∈] x′ → S_ {τ′ = τ₁} x ≡[∈] S_ {τ′ = τ₁′} x′
≡[∈]-S refl refl = refl
```

```
≡[∈]-S★ : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → {wkn≡ : weaken★ $ τ ≡ τ₁} {wkn≡′ : weaken★ $ τ′ ≡ τ₂}
  → S★_ {κ = κ} x wkn≡ ≡[∈] S★_ {κ = κ} x′ wkn≡′
≡[∈]-S★ refl {wkn≡ = `refl} {wkn≡′ = `refl} = refl
```

```
≡[∈]-Sᴺ : {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → {wkn≡ : weaken★ᴺ $ τ ≡ τ₁} {wkn≡′ : weaken★ᴺ $ τ′ ≡ τ₂}
  → Sᴺ_ x wkn≡ ≡[∈] Sᴺ_ x′ wkn≡′
≡[∈]-Sᴺ refl {wkn≡ = `refl} {wkn≡′ = `refl} = refl
```

```
≡[∈]-Z : Γ ≡ Γ′ → τ ≡ τ′ → Z {τ = τ} {Γ = Γ} ≡[∈] Z {τ = τ′} {Γ = Γ′}
≡[∈]-Z refl refl = refl
```

```
module ≡[∈]-Reasoning where

  infix 3 _∎
  infixr 2 _≡⟨_⟩_

  _∎ : (x : τ ∈ Γ) → x ≡[∈] x
  x ∎ = refl

  _≡⟨_⟩_ : (x : τ ∈ Γ) {x′ : τ′ ∈ Γ′} {x″ : τ″ ∈ Γ″} → x ≡[∈] x′ → x′ ≡[∈] x″ → x ≡[∈] x″
  _≡⟨_⟩_ x = ≡[∈]-trans
```

```
infix 2 _≡ᴹ_
data _≡ᴹ_ {Γ : Context L Δ} {τ : L ⨟ Δ ⊢ᵀ κ} (M : Γ ⊢ τ) : ∀ {Γ′ : Context L′ Δ′} {τ′ : L′ ⨟ Δ′ ⊢ᵀ κ} → Γ′ ⊢ τ′ → Set where
  refl : M ≡ᴹ M
```

```
≡ᴹ-refl : ∀ {M : Γ ⊢ τ} {M′ : Γ ⊢ τ} → M ≡ M′ → M ≡ᴹ M′
≡ᴹ-refl refl = refl
```

```
≡ᴹ-sym : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} → M ≡ᴹ M′ → M′ ≡ᴹ M
≡ᴹ-sym refl = refl
```

```
≡ᴹ-trans : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} {M″ : Γ″ ⊢ τ″} → M ≡ᴹ M′ → M′ ≡ᴹ M″ → M ≡ᴹ M″
≡ᴹ-trans refl refl = refl
```

```
≡ᴹ-subst : (τ≡ : τ ≡ τ′) (M : Γ ⊢ τ) → subst (_ ⊢_) τ≡ M ≡ᴹ M
≡ᴹ-subst refl _ = refl
```

```
≡ᴹ⇒≡ : ∀ {M M′ : Γ ⊢ τ} → M ≡ᴹ M′ → M ≡ M′
≡ᴹ⇒≡ refl = refl
```

```
module ≡ᴹ-Reasoning where

  infix 3 _∎
  infixr 2 _≡⟨_⟩_

  _∎ : (M : Γ ⊢ τ) → M ≡ᴹ M
  M ∎ = refl

  _≡⟨_⟩_ : (M : Γ ⊢ τ) {M′ : Γ′ ⊢ τ′} {M″ : Γ″ ⊢ τ″} → M ≡ᴹ M′ → M′ ≡ᴹ M″ → M ≡ᴹ M″
  _≡⟨_⟩_ M = ≡ᴹ-trans
```

```
≡ᴹ->>= ⦅_>>=_⦆ : ∀ {M : Γ ⊢ τ₁ / ε} {M′ : Γ′ ⊢ τ₁′ / ε′} {N : Γ ▷ τ₁ ⊢ τ₂ / ε} {N′ : Γ′ ▷ τ₁′ ⊢ τ₂′ / ε′}
  → M ≡ᴹ M′
  → N ≡ᴹ N′
  → M >>= N ≡ᴹ M′ >>= N′
≡ᴹ->>= refl refl = refl
⦅ refl >>= refl ⦆ = refl
```

```
≡ᴹ-handle′ : ∀ {ℓ} {M : Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε} {M′ : Γ′ ⊢ τ′ / dynamic ℓ′ ⦂ σ′ ∷ ε′} {H : Γ ⊢ σ ⇒ʰ τ / ε} {H′ : Γ′ ⊢ σ′ ⇒ʰ τ′ / ε′}
  → ∀ {ℓ∉ε} {ℓ∉ε′}
  → ℓ∉ε ≡[∉] ℓ∉ε′
  → M ≡ᴹ M′
  → H ≡ᴹ H′
  → handle′ ℓ∉ε M H ≡ᴹ handle′ ℓ∉ε′ M′ H′
≡ᴹ-handle′ refl refl refl = refl
```

```
≡ᴹ-subeffect : ∀ {M : Γ ⊢ τ / ε} {M′ : Γ′ ⊢ τ′ / ε′}
  → ∀ {ε⊑ : ε ⊑ ε₁} {ε′⊑ : ε′ ⊑ ε₁′}
  → ε⊑ ≡[⊑] ε′⊑
  → M ≡ᴹ M′
  → subeffect ε⊑ M ≡ᴹ subeffect ε′⊑ M′
≡ᴹ-subeffect refl refl = refl
```

```
≡ᴹ-return : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → ε ≡ ε′
  → M ≡ᴹ M′
  → return {ε = ε} M ≡ᴹ return {ε = ε′} M′
≡ᴹ-return refl refl = refl
```

```
≡ᴹ-appᵛ : ∀ {V : Γ ⊢ τ₁ ⇒ τ₂} {V′ : Γ′ ⊢ τ₁′ ⇒ τ₂′} {W} {W′}
  → V ≡ᴹ V′
  → W ≡ᴹ W′
  → appᵛ V W ≡ᴹ appᵛ V′ W′
≡ᴹ-appᵛ refl refl = refl
```

```
≡ᴹ-appᵀ : ∀ {σ} {σ′} {σ[τ]} {σ[τ′]} {V : Γ ⊢ Forallᵀ κ σ} {V′ : Γ′ ⊢ Forallᵀ κ σ′}
  → V ≡ᴹ V′
  → τ ≡ τ′
  → {σ[τ]≡ : σ [ τ ]★ ≡ σ[τ]}
  → {σ[τ′]≡ : σ′ [ τ′ ]★ ≡ σ[τ′]}
  → appᵀ V τ σ[τ]≡ ≡ᴹ appᵀ V′ τ′ σ[τ′]≡
≡ᴹ-appᵀ refl refl {refl} {refl} = refl
```

```
≡ᴹ-appᴺ : ∀ {σ} {σ′} {σ[η]} {σ[η′]} {V : Γ ⊢ Forallᴺ σ} {V′ : Γ′ ⊢ Forallᴺ σ′}
  → V ≡ᴹ V′
  → η ≡ η′
  → {σ[η]≡ : σ [ η ]★ᴺ ≡ σ[η]}
  → {σ[η′]≡ : σ′ [ η′ ]★ᴺ ≡ σ[η′]}
  → appᴺ {σ = σ} V η σ[η]≡ ≡ᴹ appᴺ {σ = σ′} V′ η′ σ[η′]≡
≡ᴹ-appᴺ refl refl {refl} {refl} = refl
```

```
≡ᴹ-var : ∀ {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → Var x ≡ᴹ Var x′
≡ᴹ-var refl = refl
```

```
≡ᴹ-ƛ : {M : Γ ▷ τ₁ ⊢ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ τ₂′}
  → M ≡ᴹ M′ → ƛ M ≡ᴹ ƛ M′
≡ᴹ-ƛ refl = refl
```

```
≡ᴹ-ƛᴺ : {M : Γ ▷ᴺ- ⊢ τ} {M′ : Γ′ ▷ᴺ- ⊢ τ′}
  → M ≡ᴹ M′ → ƛᴺ M ≡ᴹ ƛᴺ M′
≡ᴹ-ƛᴺ refl = refl
```

```
≡ᴹ-Λ : {M : Γ ▷★ κ ⊢ τ₂} {M′ : Γ′ ▷★ κ ⊢ τ₂′}
  → M ≡ᴹ M′ → Λ M ≡ᴹ Λ M′
≡ᴹ-Λ refl = refl
```

```
≡ᴹ-ƛʰ : {M : Γ ▷ τ₁ ⊢ σ ⇒ʰ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ σ′ ⇒ʰ τ₂′}
  → M ≡ᴹ M′ → ƛʰ M ≡ᴹ ƛʰ M′
≡ᴹ-ƛʰ refl = refl
```

```
≡ᴹ-ƛʰᴺ : {M : Γ ▷ᴺ- ⊢ σ ⇒ʰ τ} {M′ : Γ′ ▷ᴺ- ⊢ σ′ ⇒ʰ τ′}
  → τ₁ ≡ τ₁′
  → M ≡ᴹ M′
  → {wkn≡ : weaken★ᴺ $ τ₁ ≡ τ}
  → {wkn≡′ : weaken★ᴺ $ τ₁′ ≡ τ′}
  → ƛʰᴺ_ {τ/ε = τ₁} M wkn≡ ≡ᴹ ƛʰᴺ_ {τ/ε = τ₁′} M′ wkn≡′
≡ᴹ-ƛʰᴺ refl refl {wkn≡ = `refl} {wkn≡′ = `refl} = refl
```

```
≡ᴹ-, : {M : Γ ⊢ τ₁} {M′ : Γ′ ⊢ τ₁′} {N : Γ ⊢ τ₂ ⇒ᵖ τ} {N′ : Γ′ ⊢ τ₂′ ⇒ᵖ τ′}
  → M ≡ᴹ M′
  → N ≡ᴹ N′
  → (M , N) ≡ᴹ (M′ , N′)
≡ᴹ-, refl refl = refl
```

```
≡ᴹ-tt : Γ ≡ Γ′ → tt {Γ = Γ} ≡ᴹ tt {Γ = Γ′}
≡ᴹ-tt refl = refl
```

```
≡ᴹ-,ᵀ : ∀ {σ} {σ′} {σ[τ]} {σ[τ′]} {W : Γ ⊢ σ[τ] ⇒ᵖ τ₁} {W′ : Γ′ ⊢ σ[τ′] ⇒ᵖ τ₁′}
  → σ ≡ σ′
  → τ ≡ τ′
  → W ≡ᴹ W′
  → {σ[τ]≡ : σ [ τ ]★ ≡ σ[τ]}
  → {σ[τ′]≡ : σ′ [ τ′ ]★ ≡ σ[τ′]}
  → (_,ᵀ_⦅_⦆ {σ = σ} τ W σ[τ]≡) ≡ᴹ (_,ᵀ_⦅_⦆ {σ = σ′} τ′ W′ σ[τ′]≡)
≡ᴹ-,ᵀ refl refl refl {refl} {refl} = refl
```

```
≡ᴹ-,ᴺ : ∀ {σ} {σ′} {σ[η]} {σ[η′]} {W : Γ ⊢ σ[η] ⇒ᵖ τ₁} {W′ : Γ′ ⊢ σ[η′] ⇒ᵖ τ₁′}
  → σ ≡ σ′
  → η ≡ η′
  → W ≡ᴹ W′
  → {σ[η]≡ : σ [ η ]★ᴺ ≡ σ[η]}
  → {σ[η′]≡ : σ′ [ η′ ]★ᴺ ≡ σ[η′]}
  → (_,ᴺ_⦅_⦆ {σ = σ} η W σ[η]≡) ≡ᴹ (_,ᴺ_⦅_⦆ {σ = σ′} η′ W′ σ[η′]≡)
≡ᴹ-,ᴺ refl refl refl {refl} {refl} = refl
```

```
uip-⊢★ : (p q : τ ≡ τ′) → p ≡ q
uip-⊢★ refl refl = refl
```

```
≡ᴹ-handle : ∀ {Γ : Context L Δ} {Γ′ : Context L′ Δ} {M : _} {M′ : _} {H : Γ ⊢ _} {H′ : Γ′ ⊢ _}
  → M ≡ᴹ M′
  → H ≡ᴹ H′
  → {wkn≡³ : weaken★ᴺ $ τ ≡ τ₁ × weaken★ᴺ $ σ ≡ σ₁ × weaken★ᴺ $ ε ≡ ε₁}
  → {wkn≡³′ : weaken★ᴺ $ τ′ ≡ τ₁′ × weaken★ᴺ $ σ′ ≡ σ₁′ × weaken★ᴺ $ ε′ ≡ ε₁′}
  → handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³ ≡ᴹ handle {τ = τ′} {σ = σ′} {ε = ε′} M′ H′ wkn≡³′
≡ᴹ-handle refl refl {`refl , `refl , `refl} {` wknτ≡ , ` wknσ≡ , ` wknε≡} with uip-⊢★ refl wknτ≡ | uip-⊢★ refl wknσ≡ | uip-⊢★ refl wknε≡
... | refl | refl | refl = refl
```

```
≡ᴹ-perform : {V : Γ ⊢ σ ⇒ᵖ τ} {V′ : Γ′ ⊢ σ′ ⇒ᵖ τ′}
  → ε ≡ ε′
  → η ≡ η′
  → V ≡ᴹ V′
  → perform {ε = ε} η V ≡ᴹ perform {ε = ε′} η′ V′
≡ᴹ-perform refl refl refl = refl
```

```
≡ᴹ-Λʰ : ∀ {Γ : Context L Δ} {Γ′ : Context L Δ} {M : Γ ▷★ κ ⊢ σ ⇒ʰ τ₁} {M′ : Γ′ ▷★ κ ⊢ σ′ ⇒ʰ τ₁′}
  → τ ≡ τ′
  → M ≡ᴹ M′
  → {wkn≡ : weaken★ $ τ ≡ τ₁}
  → {wkn≡′ : weaken★ $ τ′ ≡ τ₁′}
  → _≡ᴹ_ {τ = _ ⇒ʰ τ} (Λʰ_ M wkn≡) {τ′ = _ ⇒ʰ τ′} (Λʰ_ M′ wkn≡′)
≡ᴹ-Λʰ refl refl {`refl} {`refl} = refl
```

```
≡ᴹ-Handler : ∀ {M : Γ ▷ (τ₁ ⇒ τ₂) ⊢ τ₂} {M′ : Γ′ ▷ (τ₁′ ⇒ τ₂′) ⊢ τ₂′}
  → M ≡ᴹ M′
  → Handler M ≡ᴹ Handler M′
≡ᴹ-Handler refl = refl
```

```
≡ᴹ-⦅⦆ : Γ ≡ Γ′ → τ ≡ τ′ → ⦅⦆ {Γ = Γ} {τ = τ} ≡ᴹ ⦅⦆ {Γ = Γ′} {τ = τ′}
≡ᴹ-⦅⦆ refl refl = refl
```

### Renaming and substitution of terms

```
private variable
  _#★_ _#★′_ _#★″_ : Labels × Contextᴷ → Kind → Set
  _#_ _#′_ _#″_ :  ∀ {L Δ} → Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Set
```

```
record PerfectKit★ (K : Kit★ _#★_) : Set where
  field
    ⦃ GK★ ⦄ : GoodKit★ K
    ⦃ K★-∘-∈ ⦄ : Kit★-∘ K K∈★
    ⦃ K★-∈-∘ ⦄ : Kit★-∘ K∈★ K
    ⦃ K★-∘-⊢ ⦄ : Kit★-∘ K K⊢★
    ⦃ K★-⊢-∘ ⦄ : Kit★-∘ K⊢★ K
```

```
instance
  PK∈★ : PerfectKit★ K∈★
  PK∈★ = record {}

  PK⊢★ : PerfectKit★ K⊢★
  PK⊢★ = record {}
```

```
record Kit (_#★_ : Labels × Contextᴷ → Kind → Set) (_#_ : ∀ {L Δ} → Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Set) : Set where
  no-eta-equality
  field
    K★ : Kit★ _#★_
    from-∈ : τ ∈ Γ → Γ # τ
    to-⊢ : Γ # τ → Γ ⊢ τ
    wkn : ∀ {τ′} → Γ # τ → (Γ ▷ τ′) # τ
    wknᵀ : ∀ {τ : L ⨟ Δ ⊢ᵀ TYP} → Γ # τ → (Γ ▷★ κ) # weaken★ τ
    wknᴺ : ∀ {τ : L ⨟ Δ ⊢ᵀ TYP} → Γ # τ → (Γ ▷ᴺ-) # weaken★ᴺ τ

open Kit
```

```
K∈ : Kit _∋★_ (λ Γ τ → τ ∈ Γ)
K∈ = record {
  K★ = K∈★ ;
  from-∈ = λ x → x ;
  to-⊢ = Var ;
  wkn = S_ ;
  wknᵀ = λ x → S★_ x `refl ;
  wknᴺ = λ x → Sᴺ_ x `refl }
```

```
infix 1 ⦅_⦆_↠_
record ⦅_⦆_↠_ (K : Kit _#★_ _#_) (Γ : Context L Δ) (Γ′ : Context L′ Δ′) : Set where
  constructor _,_
  field
    var★ : ⦅ K★ K ⦆ (L , Δ) →★ (L′ , Δ′)
    tra∈ : τ ∈ Γ → Γ′ # tra★ var★ τ

open ⦅_⦆_↠_
```

```
tra∈′ : {K : Kit _#★_ _#_} → (r : ⦅ K ⦆ Γ ↠ Γ′) → τ ∈ Γ → Γ′ ⊢ tra★ (var★ r) τ
tra∈′ {K = K} r M = to-⊢ K (tra∈ r M)
```

```
tra▷ : {K : Kit _#★_ _#_}
  → (r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′)
  → ∀ {τ} → ⦅ K ⦆ Γ ▷ τ ↠ Γ′ ▷ tra★ r★ τ
var★ (tra▷ r) = var★ r
tra∈ (tra▷ {K = K} r) Z = from-∈ K Z
tra∈ (tra▷ {K = K} r) (S x) = wkn K (tra∈ r x)
```

```
tra▷★ : {K : Kit _#★_ _#_} ⦃ PK★ : PerfectKit★ (K★ K) ⦄
  → ⦅ K ⦆ Γ ↠ Γ′
  → ∀ {κ} → ⦅ K ⦆ Γ ▷★ κ ↠ Γ′ ▷★ κ
var★ (tra▷★ r) = tra★▷ (var★ r)
tra∈ (tra▷★ {_#_ = _#_} {K = K} r) (S★_ {τ = τ} x wkn≡) = subst (_ #_) (_$_≡_.unwrap (weaken★∘tra★∙ {τ = τ} wkn≡)) (wknᵀ K (tra∈ r x))
```

```
tra▷ᴺ : {K : Kit _#★_ _#_} ⦃ PK★ : PerfectKit★ (K★ K) ⦄
  → ⦅ K ⦆ Γ ↠ Γ′
  → ⦅ K ⦆ Γ ▷ᴺ- ↠ Γ′ ▷ᴺ-
var★ (tra▷ᴺ r) = tra★▷ᴺ (var★ r)
tra∈ (tra▷ᴺ {_#_ = _#_} {K = K} r) (Sᴺ_ {τ = τ} x wkn≡) = subst (_ #_) (_$_≡_.unwrap (weaken★ᴺ∘tra★∙ {τ = τ} wkn≡)) (wknᴺ K (tra∈ r x))
```

```
tra : {K : Kit _#★_ _#_} ⦃ PK★ : PerfectKit★ (K★ K) ⦄
  → (r@(r★ , _) : ⦅ K ⦆ Γ ↠ Γ′)
  → Γ ⊢ τ → Γ′ ⊢ tra★ r★ τ
```

```
subst⊢ : Γ ≡ Γ′ → τ ≡ τ′ → Γ ⊢ τ → Γ′ ⊢ τ′
subst⊢ refl refl M = M
```

```
tra {K = K} r (Var x) = to-⊢ K (tra∈ r x)
tra r (ƛ M) = ƛ tra (tra▷ r) M
tra r (ƛᴺ V) = ƛᴺ tra (tra▷ᴺ r) V
tra r (Λ V) = Λ tra (tra▷★ r) V
tra r tt = tt
tra r (return V) = return (tra r V)
tra r (M >>= N) = tra r M >>= tra (tra▷ r) N
tra r (appᵛ W V) = appᵛ (tra r W) (tra r V)
tra r@(r★ , _) (appᵀ {σ = τ} V σ τ[σ]≡) = appᵀ (tra r V) (tra★ r★ σ) (tra★∘[]★∙ τ τ[σ]≡)
tra r@(r★ , _) (appᴺ {σ = τ} V n τ[n]≡) = appᴺ {σ = tra★ (tra★▷ᴺ r★) τ} (tra r V) (tra∈ᶻ r★ n) (tra★∘[]★ᴺ∙ τ τ[n]≡)
tra r@(r★ , _) (perform n V) = perform (tra∈ᶻ r★ n) (tra r V)
tra r@(r★ , _) (handle M H (wτ≡ , wσ≡ , wε≡)) = handle (tra (tra▷ᴺ r) M) (tra r H) wkn≡
  where wkn≡ = (weaken★ᴺ∘tra★∙ wτ≡ , weaken★ᴺ∘tra★∙ wσ≡ , weaken★ᴺ∘tra★∙ wε≡)
tra r (handle′ ℓ∉ M H) = handle′ (tra∉ _ ℓ∉) (tra r M) (tra r H)
tra r (subeffect ε⊑ε′ M) = subeffect (tra⊑ _ ε⊑ε′) (tra r M)
tra r (V , W) = tra r V , tra r W
tra r ⦅⦆ = ⦅⦆
tra r@(r★ , _) (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = tra★ r★ τ ,ᵀ tra r M ⦅ tra★∘[]★∙ σ σ[τ]≡ ⦆
tra r@(r★ , _) (_,ᴺ_⦅_⦆ {σ = σ} n M σ[n]≡) = tra∈ᶻ r★ n ,ᴺ tra r M ⦅ tra★∘[]★ᴺ∙ σ σ[n]≡ ⦆
tra r (Λʰ_ H wkn≡) = Λʰ_ (tra (tra▷★ r) H) (weaken★∘tra★∙ wkn≡)
tra r (ƛʰ H) = ƛʰ (tra (tra▷ r) H)
tra r (ƛʰᴺ_ H wkn≡) = ƛʰᴺ_ (tra (tra▷ᴺ r) H) (weaken★ᴺ∘tra★∙ wkn≡)
tra r (Handler H) = Handler (tra (tra▷ r) H)
```

```
infix 2 _→ᵍ_
_→ᵍ_ : (Γ : Context L Δ) (Γ′ : Context L′ Δ′) → Set
Γ →ᵍ Γ′ = ⦅ K∈ ⦆ Γ ↠ Γ′
```

```
↠-id : Γ →ᵍ Γ
var★ ↠-id = →★-id
tra∈ ↠-id x = subst (_∈ _) (sym (tra★-id _)) x
```

```
↠-S : Γ →ᵍ Γ ▷ τ
var★ ↠-S = →★-id
tra∈ ↠-S x = tra∈ ↠-id (S x)
```

```
weaken : Γ ⊢ τ → Γ ▷ τ′ ⊢ τ
weaken M = subst (_ ⊢_) (tra★-id _) (tra ↠-S M)
```

```
↠-S★ : Γ →ᵍ Γ ▷★ κ
var★ ↠-S★ = →★-S★
tra∈ ↠-S★ x = S★_ x `refl
```

```
↠-Sᴺ : Γ →ᵍ Γ ▷ᴺ-
var★ ↠-Sᴺ = →★-Sᴺ
tra∈ ↠-Sᴺ x = Sᴺ_ x `refl
```

```
weakenᵀ : ∀ {κ} → Γ ⊢ τ → Γ ▷★ κ ⊢ weaken★ τ
weakenᵀ = tra ↠-S★
```

```
weakenᴺ : Γ ⊢ τ → Γ ▷ᴺ- ⊢ weaken★ᴺ τ
weakenᴺ = tra ↠-Sᴺ
```

```
K⊢ : Kit _⊢★_ (λ Γ τ → Γ ⊢ τ)
K⊢ = record {
  K★ = K⊢★ ;
  from-∈ = Var ;
  to-⊢ = λ M → M ;
  wkn = weaken ;
  wknᵀ = weakenᵀ ;
  wknᴺ = weakenᴺ }
```

```
infix 2 _↠_
_↠_ : (Γ : Context L Δ) (Γ′ : Context L′ Δ′) → Set
Γ ↠ Γ′ = ⦅ K⊢ ⦆ Γ ↠ Γ′
```

```
renᵍᴸ : (rᴸ : L →ᴸ L′) → Context L Δ → Context L′ Δ
renᵍᴸ rᴸ ∅ = ∅
renᵍᴸ rᴸ (Γ ▷ τ) = renᵍᴸ rᴸ Γ ▷ ren★L rᴸ τ
renᵍᴸ rᴸ (Γ ▷★ κ) = renᵍᴸ rᴸ Γ ▷★ κ
renᵍᴸ rᴸ (Γ ▷ᴺ-) = renᵍᴸ rᴸ Γ ▷ᴺ-

weakenᵍᴸ : Context L Δ → Context (L ▷-) Δ
weakenᵍᴸ = renᵍᴸ →ᴸ-S
```

```
→ᴸ⇒→ᵍ : (rᴸ : L →ᴸ L′) → ∀ {Γ : Context L Δ} → Γ →ᵍ renᵍᴸ rᴸ Γ
var★ (→ᴸ⇒→ᵍ rᴸ) = →ᴸ⇒→★ rᴸ
tra∈ (→ᴸ⇒→ᵍ rᴸ) Z = Z
tra∈ (→ᴸ⇒→ᵍ rᴸ) (S x) = S (tra∈ (→ᴸ⇒→ᵍ rᴸ) x)
tra∈ (→ᴸ⇒→ᵍ rᴸ) (S★_ {τ = τ} x wkn≡) = S★_ (tra∈ (→ᴸ⇒→ᵍ rᴸ) x) (weaken★∘ren★L∙ {τ = τ} wkn≡)
tra∈ (→ᴸ⇒→ᵍ rᴸ) (Sᴺ_ {τ = τ} x wkn≡) = Sᴺ_ (tra∈ (→ᴸ⇒→ᵍ rᴸ) x) (weaken★ᴺ∘ren★L∙ {τ = τ} wkn≡)

renᴸ : (rᴸ : L →ᴸ L′) → Γ ⊢ τ → renᵍᴸ rᴸ Γ ⊢ ren★L rᴸ τ
renᴸ rᴸ = tra (→ᴸ⇒→ᵍ rᴸ)

weakenᴸ : Γ ⊢ τ → weakenᵍᴸ Γ ⊢ weaken★L τ
weakenᴸ = renᴸ →ᴸ-S
```

```
infix 3 _≡[↠]_

record _≡[↠]_ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} {Γ : Context L Δ} {Γ′ Γ″ : Context L′ Δ′} (r : ⦅ K ⦆ Γ ↠ Γ′) (r′ : ⦅ K′ ⦆ Γ ↠ Γ″) : Set where
  constructor [_,_,_]
  field
    ≡[↠]⇒≡ᵍ : Γ′ ≡ Γ″
    ≡[↠]⇒≡[↠★] : var★ r ≡[↠★] var★ r′
    tra∈-≡ : ∀ {τ} (x : τ ∈ Γ) → to-⊢ K (tra∈ r x) ≡ᴹ to-⊢ K′ (tra∈ r′ x)

open _≡[↠]_
```

```
≡-weaken-S : {x : τ ∈ Γ} → weaken {τ′ = τ′} (Var x) ≡ Var (S_ {τ′ = τ′} x)
≡-weaken-S {τ = τ} = aux (tra★-id τ)
  where
    aux : ∀ {Γ : Context L Δ} {τ} {τ′} (τ≡ : τ ≡ τ′) {x : τ′ ∈ Γ} → subst (Γ ⊢_) τ≡ (Var (subst (_∈ Γ) (sym τ≡) x)) ≡ Var x
    aux refl = refl
```

```
record GoodKit (K : Kit _#★_ _#_) : Set where
  field
    ⦃ PK★ ⦄ : PerfectKit★ (K★ K)
    to-from : ∀ (x : τ ∈ Γ) → to-⊢ K (from-∈ K x) ≡ Var x
    to-wkn : ∀ {τ′} (x : Γ # τ) → to-⊢ K (wkn K {τ′ = τ′} x) ≡ weaken (to-⊢ K x)
    to-wknᵀ : ∀ (x : Γ # τ) → to-⊢ K (wknᵀ K x) ≡ weakenᵀ {κ = κ} (to-⊢ K x)
    to-wknᴺ : ∀ (x : Γ # τ) → to-⊢ K (wknᴺ K x) ≡ weakenᴺ (to-⊢ K x)

open GoodKit ⦃ ... ⦄
```

```
instance
  GK∈ : GoodKit K∈
  GK∈ = record {
    to-from = λ x → refl ;
    to-wkn = λ x → sym ≡-weaken-S ;
    to-wknᵀ = λ x → refl ;
    to-wknᴺ = λ x → refl }

  GK⊢ : GoodKit K⊢
  GK⊢ = record {
    to-from = λ x → refl ;
    to-wkn = λ x → refl ;
    to-wknᵀ = λ x → refl ;
    to-wknᴺ = λ x → refl }
```

```
to-from-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : τ ∈ Γ} {x′ : τ′ ∈ Γ′}
  → x ≡[∈] x′
  → to-⊢ K (from-∈ K x) ≡ᴹ to-⊢ K′ (from-∈ K′ x′)
to-from-≡ {x = x} {x′} x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-from x)) (≡ᴹ-trans (≡ᴹ-var x≡) (≡ᴹ-refl (sym (to-from x′))))
```

```
≡ᴹ-weaken : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → τ₁ ≡ τ₁′
  → M ≡ᴹ M′
  → weaken {τ′ = τ₁} M ≡ᴹ weaken {τ′ = τ₁′} M′
≡ᴹ-weaken refl refl = refl
```

```
≡ᴹ-weakenᵀ : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → M ≡ᴹ M′ → weakenᵀ {κ = κ} M ≡ᴹ weakenᵀ M′
≡ᴹ-weakenᵀ refl = refl
```

```
≡ᴹ-weakenᴺ : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → M ≡ᴹ M′ → weakenᴺ M ≡ᴹ weakenᴺ M′
≡ᴹ-weakenᴺ refl = refl
```

```
to-wkn-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : Γ # τ} {x′ : Γ′ #′ τ′}
  → τ₁ ≡ τ₂
  → to-⊢ K x ≡ᴹ to-⊢ K′ x′
  → to-⊢ K (wkn K {τ′ = τ₁} x) ≡ᴹ to-⊢ K′ (wkn K′ {τ′ = τ₂} x′)
to-wkn-≡ {x = x} {x′} τ≡ x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-wkn x)) (≡ᴹ-trans (≡ᴹ-weaken τ≡ x≡) (≡ᴹ-refl (sym (to-wkn x′))))
```

```
to-wknᵀ-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → ∀ {Γ Γ′ : Context L Δ}
  → {x : Γ # τ} {x′ : Γ′ #′ τ′}
  → τ₁ ≡ τ₂
  → to-⊢ K x ≡ᴹ to-⊢ K′ x′
  → to-⊢ K (wknᵀ K {κ = κ} x) ≡ᴹ to-⊢ K′ (wknᵀ K′ x′)
to-wknᵀ-≡ {x = x} {x′} τ≡ x≡ = ≡ᴹ-trans (≡ᴹ-refl (to-wknᵀ x)) (≡ᴹ-trans (≡ᴹ-weakenᵀ x≡) (≡ᴹ-refl (sym (to-wknᵀ x′))))
```

```
tra▷-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {τ} → tra▷ r {τ} ≡[↠] tra▷ r′
≡[↠]⇒≡ᵍ (tra▷-≡ r≡ {τ}) = cong₂ _▷_ (≡[↠]⇒≡ᵍ r≡) (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ)
≡[↠]⇒≡[↠★] (tra▷-≡ r≡) = ≡[↠]⇒≡[↠★] r≡
tra∈-≡ (tra▷-≡ r≡ {τ}) Z = to-from-≡ (≡[∈]-Z (≡[↠]⇒≡ᵍ r≡) (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ))
tra∈-≡ (tra▷-≡ r≡ {τ}) (S x) = to-wkn-≡ (tra★-≡ (≡[↠]⇒≡[↠★] r≡) τ) (tra∈-≡ r≡ x)
```

```
to-subst : ∀ {K : Kit _#★_ _#_} {τ τ′ : L ⨟ Δ ⊢ᵀ TYP}
  → (τ≡ : τ ≡ τ′)
  → (x : Γ # τ)
  → to-⊢ K (subst (Γ #_) τ≡ x) ≡ᴹ to-⊢ K x
to-subst refl x = refl
```

```
tra▷★-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {κ} → tra▷★ r {κ} ≡[↠] tra▷★ r′
≡[↠]⇒≡ᵍ (tra▷★-≡ r≡ {κ}) = cong (_▷★ κ) (≡[↠]⇒≡ᵍ r≡)
≡[↠]⇒≡[↠★] (tra▷★-≡ r≡) = tra★▷-≡ (≡[↠]⇒≡[↠★] r≡)
tra∈-≡ (tra▷★-≡ {K = K} {K′} {r = r} {r′} r≡ {κ}) (S★_ x wkn≡) =
  ≡ᴹ-trans (≡ᴹ-trans (to-subst {K = K} _ (wknᵀ K (tra∈ r x))) (≡ᴹ-refl (to-wknᵀ (tra∈ r x))))
    (≡ᴹ-trans (≡ᴹ-weakenᵀ (tra∈-≡ r≡ x))
      (≡ᴹ-sym (≡ᴹ-trans (to-subst {K = K′} _ (wknᵀ K′ (tra∈ r′ x))) (≡ᴹ-refl (to-wknᵀ (tra∈ r′ x)))))) -- to-wkn-≡ (tra★-≡ (≡[↠]⇒≡[↠★] r≡) κ) (tra∈-≡ r≡ x)
```

```
tra▷ᴺ-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → tra▷ᴺ r ≡[↠] tra▷ᴺ r′
≡[↠]⇒≡ᵍ (tra▷ᴺ-≡ r≡) = cong _▷ᴺ- (≡[↠]⇒≡ᵍ r≡)
≡[↠]⇒≡[↠★] (tra▷ᴺ-≡ r≡) = tra★▷ᴺ-≡ (≡[↠]⇒≡[↠★] r≡)
tra∈-≡ (tra▷ᴺ-≡ {K = K} {K′} {r = r} {r′} r≡) (Sᴺ_ x wkn≡) =
  ≡ᴹ-trans (≡ᴹ-trans (to-subst {K = K} _ (wknᴺ K (tra∈ r x))) (≡ᴹ-refl (to-wknᴺ (tra∈ r x))))
    (≡ᴹ-trans (≡ᴹ-weakenᴺ (tra∈-≡ r≡ x))
      (≡ᴹ-sym (≡ᴹ-trans (to-subst {K = K′} _ (wknᴺ K′ (tra∈ r′ x))) (≡ᴹ-refl (to-wknᴺ (tra∈ r′ x)))))) -- to-wkn-≡ (tra★-≡ (≡[↠]⇒≡[↠★] r≡) κ) (tra∈-≡ r≡ x)
```

```
≡ᴹ-subst⊢ :
    (Γ≡ : Γ ≡ Γ′)
    (τ≡ : τ ≡ τ′)
    (M : Γ ⊢ τ)
  → subst⊢ Γ≡ τ≡ M ≡ᴹ M
≡ᴹ-subst⊢ refl refl M = refl
```

```
tra-≡ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK : GoodKit K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ ↠ Γ″}
  → r ≡[↠] r′
  → ∀ {κ} {τ : _ ⨟ _ ⊢ᵀ κ} (M : Γ ⊢ τ)
  → tra r M ≡ᴹ tra r′ M
```

```
tra-≡ r≡ (Var x) = tra∈-≡ r≡ x
tra-≡ r≡ (ƛ M) = ≡ᴹ-ƛ (tra-≡ (tra▷-≡ r≡) M)
tra-≡ r≡ (Λ M) = ≡ᴹ-Λ (tra-≡ (tra▷★-≡ r≡) M)
tra-≡ r≡ (ƛᴺ M) = ≡ᴹ-ƛᴺ (tra-≡ (tra▷ᴺ-≡ r≡) M)
tra-≡ r≡@([ Γ≡ , _ , _ ]) tt = ≡ᴹ-tt Γ≡
tra-≡ r≡@([ Γ≡ , r★≡ , _ ]) (⦅⦆ {τ = τ}) = ≡ᴹ-⦅⦆ Γ≡ (tra★-≡ r★≡ τ)
tra-≡ r≡ (V , W) = ≡ᴹ-, (tra-≡ r≡ V) (tra-≡ r≡ W)
tra-≡ r≡@([ _ , r★≡ , _ ]) (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = ≡ᴹ-,ᵀ (tra★-≡ (tra★▷-≡ r★≡) σ) (tra★-≡ r★≡ τ) (tra-≡ r≡ M)
tra-≡ r≡@([ _ , r★≡ , _ ]) (_,ᴺ_⦅_⦆ {σ = σ} n M σ[n]≡) = ≡ᴹ-,ᴺ (tra★-≡ (tra★▷ᴺ-≡ r★≡) σ) (tra∈ᶻ-≡ r★≡ n) (tra-≡ r≡ M)
tra-≡ r≡@([ _ , r★≡ , _ ]) (return {ε = ε} M) = ≡ᴹ-return (tra★-≡ r★≡ ε) (tra-≡ r≡ M)
tra-≡ r≡ (M >>= N) = ⦅ tra-≡ r≡ M >>= tra-≡ (tra▷-≡ r≡) N ⦆
tra-≡ r≡ (appᵛ V W) = ≡ᴹ-appᵛ (tra-≡ r≡ V) (tra-≡ r≡ W)
tra-≡ r≡@([ _ , r★≡ , _ ]) (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-≡ r≡ V) (tra★-≡ r★≡ τ)
tra-≡ r≡@([ _ , r★≡ , _ ]) (appᴺ {σ = σ} V n σ[n]≡) = ≡ᴹ-appᴺ (tra-≡ r≡ V) (tra∈ᶻ-≡ r★≡ n)
tra-≡ r≡@([ _ , r★≡ , _ ]) (perform {ε = ε} n V) = ≡ᴹ-perform (tra★-≡ r★≡ ε) (tra∈ᶻ-≡ r★≡ n) (tra-≡ r≡ V)
tra-≡ r≡ (handle M H wkn≡³) = ≡ᴹ-handle (tra-≡ (tra▷ᴺ-≡ r≡) M) (tra-≡ r≡ H)
tra-≡ r≡@([ _ , r★≡ , _ ]) (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-≡ r★≡ ℓ∉) (tra-≡ r≡ M) (tra-≡ r≡ H)
tra-≡ r≡@([ _ , r★≡ , _ ]) (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-≡ r★≡ ε⊑) (tra-≡ r≡ M)
tra-≡ r≡@([ _ , r★≡ , _ ]) (Λʰ_ {τ/ε = τ/ε} H wkn≡) = ≡ᴹ-Λʰ (tra★-≡ r★≡ τ/ε) (tra-≡ (tra▷★-≡ r≡) H)
tra-≡ r≡ (ƛʰ H) = ≡ᴹ-ƛʰ (tra-≡ (tra▷-≡ r≡) H)
tra-≡ r≡@([ _ , r★≡ , _ ]) (ƛʰᴺ_ {τ/ε = τ/ε} M wkn≡) = ≡ᴹ-ƛʰᴺ (tra★-≡ r★≡ τ/ε) (tra-≡ (tra▷ᴺ-≡ r≡) M)
tra-≡ r≡ (Handler H) = ≡ᴹ-Handler (tra-≡ (tra▷-≡ r≡) H)
```

```
tra▷-id : ∀ {τ} → tra▷ (↠-id {Γ = Γ}) {τ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ tra▷-id = cong (_ ▷_) (tra★-id _)
≡[↠]⇒≡[↠★] tra▷-id = ≡[↠★]-refl
tra∈-≡ tra▷-id Z = to-from-≡ {K = K∈} {K′ = K∈} (≡[∈]-trans (≡[∈]-Z refl (tra★-id _)) (≡[∈]-sym (≡[∈]-subst _)))
tra∈-≡ (tra▷-id {τ = τ}) (S x) =
  to-⊢ K∈ (wkn K∈ (from-∈ K∈ (subst (_∈ _) _ x))) ≡⟨ ≡ᴹ-refl (to-wkn _) ⟩
  weaken (to-⊢ K∈ (from-∈ K∈ (subst (_∈ _) _ x))) ≡⟨ ≡ᴹ-weaken refl (≡ᴹ-refl (to-from {K = K∈} _)) ⟩
  weaken (Var (subst (_∈ _) _ x)) ≡⟨ ≡ᴹ-refl ≡-weaken-S ⟩
  Var (S (subst (_∈ _) _ x)) ≡⟨ ≡ᴹ-var (≡[∈]-S (tra★-id τ) (≡[∈]-subst _)) ⟩
  Var (S x) ≡⟨ ≡ᴹ-var (≡[∈]-sym (≡[∈]-subst _)) ⟩
  Var (subst (_∈ _) _ (S x)) ≡⟨ ≡ᴹ-sym (≡ᴹ-refl (to-from {K = K∈} _)) ⟩
  to-⊢ K∈ (from-∈ K∈ (subst (_∈ _) _ (S x))) ∎
  where open ≡ᴹ-Reasoning
```

```
subst# : (K : Kit _#★_ _#_)
  → {Γ : Context L Δ} {τ τ′ : L ⨟ Δ ⊢ᵀ TYP}
  → {τ≡ : τ ≡ τ′}
  → (M : Γ # τ)
  → to-⊢ K (subst (_ #_) τ≡ M) ≡ᴹ to-⊢ K M
subst# K {τ≡ = refl} M = refl
```

```
tra∈-id :
    {Γ : Context L Δ}
  → {x : τ ∈ Γ}
  → to-⊢ K∈ (tra∈ ↠-id x) ≡ᴹ Var x
tra∈-id = ≡ᴹ-trans (≡ᴹ-refl (to-from {K = K∈} _)) (≡ᴹ-var (≡[∈]-subst _))
```

```
tra▷★-id : ∀ {κ} → tra▷★ (↠-id {Γ = Γ}) {κ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ tra▷★-id = refl
≡[↠]⇒≡[↠★] tra▷★-id = tra★▷-id
tra∈-≡ tra▷★-id (S★_ x wkn≡) =
  to-⊢ K∈ (tra∈ (tra▷★ ↠-id) ((S★ x) wkn≡)) ≡⟨ subst# K∈ _ ⟩
  to-⊢ K∈ (wknᵀ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-refl (to-wknᵀ {K = K∈} _) ⟩
  weakenᵀ (to-⊢ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-weakenᵀ tra∈-id ⟩
  Var (S★_ x `refl) ≡⟨ ≡ᴹ-var (≡[∈]-S★ refl) ⟩
  Var (S★_ x wkn≡) ≡⟨ ≡ᴹ-sym tra∈-id ⟩
  to-⊢ K∈ (tra∈ ↠-id ((S★ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra▷ᴺ-id : tra▷ᴺ (↠-id {Γ = Γ}) ≡[↠] ↠-id
≡[↠]⇒≡ᵍ tra▷ᴺ-id = refl
≡[↠]⇒≡[↠★] tra▷ᴺ-id = tra★▷ᴺ-id
tra∈-≡ tra▷ᴺ-id (Sᴺ_ x wkn≡) =
  to-⊢ K∈ (tra∈ (tra▷ᴺ ↠-id) (Sᴺ_ x wkn≡)) ≡⟨ subst# K∈ _ ⟩
  to-⊢ K∈ (wknᴺ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-refl (to-wknᴺ {K = K∈} _) ⟩
  weakenᴺ (to-⊢ K∈ (tra∈ ↠-id x)) ≡⟨ ≡ᴹ-weakenᴺ tra∈-id ⟩
  Var (Sᴺ_ x `refl) ≡⟨ ≡ᴹ-var (≡[∈]-Sᴺ refl) ⟩
  Var (Sᴺ_ x wkn≡) ≡⟨ ≡ᴹ-sym tra∈-id ⟩
  to-⊢ K∈ (tra∈ ↠-id ((Sᴺ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
≡[↠]-trans : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} {K″ : Kit _#★″_ _#″_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ GK″ : GoodKit K″ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K ⦆ Γ ↠ Γ″} {r″ : ⦅ K ⦆ Γ ↠ Γ³}
  → r ≡[↠] r′
  → r′ ≡[↠] r″
  → r ≡[↠] r″
≡[↠]-trans [ Γ≡ , r★≡ , tra∈≡ ] [ Γ′≡ , r★′≡ , tra∈′≡ ] = [ trans Γ≡ Γ′≡ , ≡[↠★]-trans r★≡ r★′≡ , (λ x → ≡ᴹ-trans (tra∈≡ x) (tra∈′≡ x)) ]
```

```
tra-id : ∀ (M : Γ ⊢ τ)
  → tra ↠-id M ≡ᴹ M
```

```
tra-id▷ : ∀ (M : Γ ▷ τ′ ⊢ τ) → tra (tra▷ ↠-id) M ≡ᴹ M
tra-id▷ M = ≡ᴹ-trans (tra-≡ tra▷-id M) (tra-id M)
```

```
tra-id▷★ : ∀ (M : Γ ▷★ κ ⊢ τ) → tra (tra▷★ ↠-id) M ≡ᴹ M
tra-id▷★ M = ≡ᴹ-trans (tra-≡ tra▷★-id M) (tra-id M)
```

```
tra-id▷ᴺ : ∀ (M : Γ ▷ᴺ- ⊢ τ) → tra (tra▷ᴺ ↠-id) M ≡ᴹ M
tra-id▷ᴺ M = ≡ᴹ-trans (tra-≡ tra▷ᴺ-id M) (tra-id M)
```

```
tra-id (Var x) = ≡ᴹ-trans (≡ᴹ-refl (to-from {K = K∈} _)) (≡ᴹ-var (≡[∈]-subst _))
tra-id (ƛ M) = ≡ᴹ-ƛ (tra-id▷ M)
tra-id (ƛᴺ M) = ≡ᴹ-ƛᴺ (tra-id▷ᴺ M)
tra-id (Λ M) = ≡ᴹ-Λ (tra-id▷★ M)
tra-id tt = refl
tra-id ⦅⦆ = ≡ᴹ-⦅⦆ refl (tra★-id _)
tra-id (V , W) = ≡ᴹ-, (tra-id V) (tra-id W)
tra-id (τ ,ᵀ M ⦅ σ[τ]≡ ⦆) = ≡ᴹ-,ᵀ (tra★-id▷ _) (tra★-id τ) (tra-id M)
tra-id (n ,ᴺ M ⦅ σ[n]≡ ⦆) = ≡ᴹ-,ᴺ (tra★-id▷ᴺ _) (tra∈ᶻ-id n) (tra-id M)
tra-id (return M) = ≡ᴹ-return (tra★-id _) (tra-id M)
tra-id (M >>= N) = ⦅ tra-id M >>= tra-id▷ N ⦆
tra-id (appᵛ V W) = ≡ᴹ-appᵛ (tra-id V) (tra-id W)
tra-id (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-id V) (tra★-id τ)
tra-id (appᴺ V n σ[n]≡) = ≡ᴹ-appᴺ (tra-id V) (tra∈ᶻ-id n)
tra-id (perform n V) = ≡ᴹ-perform (tra★-id _) (tra∈ᶻ-id n) (tra-id V)
tra-id (handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³) = ≡ᴹ-handle (tra-id▷ᴺ M) (tra-id H)
tra-id (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-id ℓ∉) (tra-id M) (tra-id H)
tra-id (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-id ε⊑) (tra-id M)
tra-id ((Λʰ H) wkn≡) = ≡ᴹ-Λʰ (tra★-id _) (tra-id▷★ H)
tra-id (ƛʰ H) = ≡ᴹ-ƛʰ (tra-id▷ H)
tra-id (ƛʰᴺ_ H wkn≡) = ≡ᴹ-ƛʰᴺ (tra★-id _) (tra-id▷ᴺ H)
tra-id (Handler M) = ≡ᴹ-Handler (tra-id▷ M)
```

```
lift : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄
  → ⦅ K ⦆ Γ ↠ Γ′ → Γ ↠ Γ′
var★ (lift r) = lift★ (var★ r)
tra∈ (lift r) {τ = τ} x = subst (_ ⊢_) (sym (tra★-lift τ)) (tra∈′ r x)
```

```
lift≡ : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄ → {r : ⦅ K ⦆ Γ ↠ Γ′} → lift r ≡[↠] r
≡[↠]⇒≡ᵍ lift≡ = refl
≡[↠]⇒≡[↠★] lift≡ = lift★≡
tra∈-≡ lift≡ x = ≡ᴹ-subst _ _
```

```
tra-lift : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (M : Γ ⊢ τ)
  → tra (lift r) M ≡ᴹ tra r M
tra-lift = tra-≡ lift≡
```

```
infix 5 _∘[↠]_
_∘[↠]_ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ _ : GoodKit K ⦄ ⦃ _ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄
  → (r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″) (r : ⦅ K ⦆ Γ ↠ Γ′)
  → ⦅ K⊢ ⦆ Γ ↠ Γ″
var★ (r′ ∘[↠] r) = var★ r′ ∘[↠★] var★ r
tra∈ (_∘[↠]_ {K = K} r′ r) {τ = τ} x = subst (_ ⊢_) (tra★-∘ {r′ = var★ r′} {r = var★ r} τ) (tra r′ (to-⊢ K (tra∈ r x)))
```

```
tra∈-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ _ : GoodKit K ⦄ ⦃ _ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′} {Γ″ : Context L″ Δ″}
  → (r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″) (r : ⦅ K ⦆ Γ ↠ Γ′)
  → ∀ (x : τ ∈ Γ)
  → tra∈ (r′ ∘[↠] r) x ≡ᴹ tra r′ (to-⊢ K (tra∈ r x))
tra∈-∘ r′ r x = ≡ᴹ-subst _ _
```

```
≡ᴹ-tra : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → {M : Γ ⊢ τ} {M′ : Γ ⊢ τ′}
  → M ≡ᴹ M′
  → tra r M ≡ᴹ tra r M′
≡ᴹ-tra refl = refl
```

```
K⊢-to-⊢ : ∀ {M : Γ ⊢ τ} → to-⊢ K⊢ M ≡ᴹ M
K⊢-to-⊢ = ≡ᴹ-subst _ _
```

```
record Kit-∘ (K : Kit _#★_ _#_) (K′ : Kit _#★′_ _#′_) ⦃ _ : PerfectKit★ (K★ K′) ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ : Set where
  field
    tra∘wkn : ∀ {r′ : ⦅ K′ ⦆ Γ ↠ Γ′} (m : Γ # τ) → tra (tra▷ r′ {τ′}) (to-⊢ K (wkn K m)) ≡ weaken (tra r′ (to-⊢ K m))
    tra∘wknᵀ : ∀ {r′ : ⦅ K′ ⦆ Γ ↠ Γ′} (m : Γ # τ) → tra (tra▷★ r′ {κ}) (to-⊢ K (wknᵀ K m)) ≡ᴹ weakenᵀ {κ = κ} (tra r′ (to-⊢ K m))
    tra∘wknᴺ : ∀ {r′ : ⦅ K′ ⦆ Γ ↠ Γ′} (m : Γ # τ) → tra (tra▷ᴺ r′) (to-⊢ K (wknᴺ K m)) ≡ᴹ weakenᴺ (tra r′ (to-⊢ K m))

open Kit-∘ ⦃ ... ⦄
```

```
tra▷-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ {τ}
  → tra▷ r′ ∘[↠] tra▷ r {τ} ≡[↠] tra▷ (r′ ∘[↠] r)
≡[↠]⇒≡ᵍ (tra▷-∘ {τ = τ}) = cong (_ ▷_) (tra★-∘ τ)
≡[↠]⇒≡[↠★] tra▷-∘ = ≡[↠★]-refl
tra∈-≡ (tra▷-∘ {r = r} {r′} {τ = τ}) Z = ≡ᴹ-trans (≡ᴹ-subst (tra★-∘ τ) _) (≡ᴹ-trans (≡ᴹ-tra {r = tra▷ r′} (≡ᴹ-refl (to-from Z))) (≡ᴹ-trans (≡ᴹ-refl (to-from Z)) (≡ᴹ-var (≡[∈]-Z refl (tra★-∘ τ)))))
tra∈-≡ (tra▷-∘ {K = K} {r = r} {r′} {τ = τ}) (S x) =
  to-⊢ K⊢ (tra∈ (tra▷ r′ ∘[↠] tra▷ r {τ}) (S x)) ≡⟨ K⊢-to-⊢ ⟩
  tra∈ (tra▷ r′ ∘[↠] tra▷ r {τ}) (S x) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra (tra▷ r′) (to-⊢ K (wkn K (tra∈ r x))) ≡⟨ ≡ᴹ-refl (tra∘wkn (tra∈ r x)) ⟩
  weaken (tra r′ (to-⊢ K (tra∈ r x))) ≡⟨ ≡ᴹ-weaken (tra★-∘ τ) (
    tra r′ (to-⊢ K (tra∈ r x)) ≡⟨ ≡ᴹ-sym (tra∈-∘ r′ r x) ⟩
    tra∈ (r′ ∘[↠] r) x ∎) ⟩
  tra∈ (tra▷ (r′ ∘[↠] r) {τ}) (S x) ≡⟨ ≡ᴹ-sym K⊢-to-⊢ ⟩
  to-⊢ K⊢ (tra∈ (tra▷ (r′ ∘[↠] r) {τ}) (S x)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra▷★-S★ : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (x : τ ∈ Γ)
  → (wkn≡ : weaken★ {κ′ = κ} $ τ ≡ τ′)
  → to-⊢ K (tra∈ (tra▷★ r) (S★_ x wkn≡)) ≡ᴹ to-⊢ K (wknᵀ K (tra∈ r x))
tra▷★-S★ {K = K} x wkn≡ = subst# K _
```

```
tra▷★-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ {κ}
  → tra▷★ r′ ∘[↠] tra▷★ r {κ} ≡[↠] tra▷★ (r′ ∘[↠] r)
≡[↠]⇒≡ᵍ tra▷★-∘ = refl
≡[↠]⇒≡[↠★] tra▷★-∘ = tra★▷-∘
tra∈-≡ (tra▷★-∘ {K = K} {K′ = K′} {r = r} {r′} {κ = κ}) (S★_ x wkn≡) =
  to-⊢ K⊢ (tra∈ (tra▷★ r′ ∘[↠] tra▷★ r) ((S★ x) wkn≡)) ≡⟨ K⊢-to-⊢ ⟩
  tra∈ (tra▷★ r′ ∘[↠] tra▷★ r) ((S★ x) wkn≡) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra (tra▷★ r′) (to-⊢ K (tra∈ (tra▷★ r) (S★_ x wkn≡))) ≡⟨ ≡ᴹ-tra {r = tra▷★ r′} (tra▷★-S★ {r = r} x wkn≡) ⟩
  tra (tra▷★ r′) (to-⊢ K (wknᵀ K (tra∈ r x))) ≡⟨ tra∘wknᵀ (tra∈ r x) ⟩
  weakenᵀ (tra r′ (to-⊢ K (tra∈ r x))) ≡⟨ ≡ᴹ-weakenᵀ (≡ᴹ-sym (tra∈-∘ r′ r x)) ⟩
  weakenᵀ (tra∈ (r′ ∘[↠] r) x) ≡⟨ ≡ᴹ-sym (tra▷★-S★ {r = r′ ∘[↠] r} x wkn≡) ⟩
  tra∈ (tra▷★ (r′ ∘[↠] r)) ((S★ x) wkn≡) ≡⟨ ≡ᴹ-sym K⊢-to-⊢ ⟩
  to-⊢ K⊢ (tra∈ (tra▷★ (r′ ∘[↠] r)) ((S★ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra▷ᴺ-Sᴺ : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (x : τ ∈ Γ)
  → (wkn≡ : weaken★ᴺ $ τ ≡ τ′)
  → to-⊢ K (tra∈ (tra▷ᴺ r) (Sᴺ_ x wkn≡)) ≡ᴹ to-⊢ K (wknᴺ K (tra∈ r x))
tra▷ᴺ-Sᴺ {K = K} x wkn≡ = subst# K _
```

```
tra▷ᴺ-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → tra▷ᴺ r′ ∘[↠] tra▷ᴺ r ≡[↠] tra▷ᴺ (r′ ∘[↠] r)
≡[↠]⇒≡ᵍ tra▷ᴺ-∘ = refl
≡[↠]⇒≡[↠★] tra▷ᴺ-∘ = tra★▷ᴺ-∘
tra∈-≡ (tra▷ᴺ-∘ {K = K} {K′ = K′} {r = r} {r′}) (Sᴺ_ x wkn≡) =
  to-⊢ K⊢ (tra∈ (tra▷ᴺ r′ ∘[↠] tra▷ᴺ r) ((Sᴺ x) wkn≡)) ≡⟨ K⊢-to-⊢ ⟩
  tra∈ (tra▷ᴺ r′ ∘[↠] tra▷ᴺ r) ((Sᴺ x) wkn≡) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra (tra▷ᴺ r′) (to-⊢ K (tra∈ (tra▷ᴺ r) (Sᴺ_ x wkn≡))) ≡⟨ ≡ᴹ-tra {r = tra▷ᴺ r′} (tra▷ᴺ-Sᴺ {r = r} x wkn≡) ⟩
  tra (tra▷ᴺ r′) (to-⊢ K (wknᴺ K (tra∈ r x))) ≡⟨ tra∘wknᴺ (tra∈ r x) ⟩
  weakenᴺ (tra r′ (to-⊢ K (tra∈ r x))) ≡⟨ ≡ᴹ-weakenᴺ (≡ᴹ-sym (tra∈-∘ r′ r x)) ⟩
  weakenᴺ (tra∈ (r′ ∘[↠] r) x) ≡⟨ ≡ᴹ-sym (tra▷ᴺ-Sᴺ {r = r′ ∘[↠] r} x wkn≡) ⟩
  tra∈ (tra▷ᴺ (r′ ∘[↠] r)) ((Sᴺ x) wkn≡) ≡⟨ ≡ᴹ-sym K⊢-to-⊢ ⟩
  to-⊢ K⊢ (tra∈ (tra▷ᴺ (r′ ∘[↠] r)) ((Sᴺ x) wkn≡)) ∎
  where open ≡ᴹ-Reasoning
```

```
tra-∘ : ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ⊢ τ)
  → tra r′ (tra r M) ≡ᴹ tra (r′ ∘[↠] r) M

tra-∘▷ :  ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ▷ τ′ ⊢ τ)
  → tra (tra▷ r′) (tra (tra▷ r) M) ≡ᴹ tra (tra▷ (r′ ∘[↠] r)) M
tra-∘▷ {r = r} {r′} M = ≡ᴹ-trans (tra-∘ M) (tra-≡ tra▷-∘ M)

tra-∘▷★ :  ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ▷★ κ ⊢ τ)
  → tra (tra▷★ r′) (tra (tra▷★ r) M) ≡ᴹ tra (tra▷★ (r′ ∘[↠] r)) M
tra-∘▷★ M = ≡ᴹ-trans (tra-∘ M) (tra-≡ tra▷★-∘ M)

tra-∘▷ᴺ :  ∀ {K : Kit _#★_ _#_} {K′ : Kit _#★′_ _#′_} ⦃ GK : GoodKit K ⦄ ⦃ GK′ : GoodKit K′ ⦄ ⦃ _ : Kit★-∘ (K★ K) (K★ K′) ⦄ ⦃ _ : Kit-∘ K K′ ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′} {r′ : ⦅ K′ ⦆ Γ′ ↠ Γ″}
  → ∀ (M : Γ ▷ᴺ- ⊢ τ)
  → tra (tra▷ᴺ r′) (tra (tra▷ᴺ r) M) ≡ᴹ tra (tra▷ᴺ (r′ ∘[↠] r)) M
tra-∘▷ᴺ M = ≡ᴹ-trans (tra-∘ M) (tra-≡ tra▷ᴺ-∘ M)

≡ᵍ⇒↠ : Γ ≡ Γ′ → ⦅ K∈ ⦆ Γ ↠ Γ′
≡ᵍ⇒↠ refl = ↠-id

subst⊢⇒tra :
    (Γ≡ : Γ ≡ Γ′)
  → (τ≡ : τ ≡ τ′)
  → (M : Γ ⊢ τ)
  → subst⊢ Γ≡ τ≡ M ≡ᴹ tra (≡ᵍ⇒↠ Γ≡) M
subst⊢⇒tra refl refl M = ≡ᴹ-sym (tra-id M)

tra-subst⊢-cong : ∀ {K : Kit _#★_ _#_} ⦃ _ : PerfectKit★ (K★ K) ⦄
  → {r : ⦅ K ⦆ Γ ↠ Γ′}
  → (τ₁≡ : τ₁ ≡ τ₁′)
  → (τ≡ : τ ≡ τ′)
  → (M : Γ ▷ τ₁ ⊢ τ)
  → tra (tra▷ r) (subst⊢ (cong (_ ▷_) τ₁≡) τ≡ M) ≡ᴹ tra (tra▷ r) M
tra-subst⊢-cong refl refl M = refl

tra-∘ {r = r} {r′} (Var x) = ≡ᴹ-sym (tra∈-∘ r′ r x)
tra-∘ (ƛ M) = ≡ᴹ-ƛ (tra-∘▷ M)
tra-∘ (ƛᴺ M) = ≡ᴹ-ƛᴺ (tra-∘▷ᴺ M)
tra-∘ (Λ M) = ≡ᴹ-Λ (tra-∘▷★ M)
tra-∘ tt = refl
tra-∘ (⦅⦆ {τ = τ}) = ≡ᴹ-⦅⦆ refl (tra★-∘ τ)
tra-∘ (V , W) = ≡ᴹ-, (tra-∘ V) (tra-∘ W)
tra-∘ (_,ᵀ_⦅_⦆ {σ = σ} τ M σ[τ]≡) = ≡ᴹ-,ᵀ (tra★-∘▷ σ) (tra★-∘ τ) (tra-∘ M)
tra-∘ (_,ᴺ_⦅_⦆ {σ = σ} n M σ[n]≡) = ≡ᴹ-,ᴺ (tra★-∘▷ᴺ σ) (tra∈ᶻ-∘ n) (tra-∘ M)
tra-∘ (return {ε = ε} M) = ≡ᴹ-return (tra★-∘ ε) (tra-∘ M)
tra-∘ (M >>= N) = ⦅ tra-∘ M >>= tra-∘▷ N ⦆
tra-∘ (appᵛ V W) = ≡ᴹ-appᵛ (tra-∘ V) (tra-∘ W)
tra-∘ (appᵀ V τ σ[τ]≡) = ≡ᴹ-appᵀ (tra-∘ V) (tra★-∘ τ)
tra-∘ (appᴺ V n σ[n]≡) = ≡ᴹ-appᴺ (tra-∘ V) (tra∈ᶻ-∘ n)
tra-∘ (perform {ε = ε} n V) = ≡ᴹ-perform (tra★-∘ ε) (tra∈ᶻ-∘ n) (tra-∘ V)
tra-∘ (handle {τ = τ} {σ = σ} {ε = ε} M H wkn≡³) = ≡ᴹ-handle (tra-∘▷ᴺ M) (tra-∘ H)
tra-∘ (handle′ ℓ∉ M H) = ≡ᴹ-handle′ (tra∉-∘ ℓ∉) (tra-∘ M) (tra-∘ H)
tra-∘ (subeffect ε⊑ M) = ≡ᴹ-subeffect (tra⊑-∘ ε⊑) (tra-∘ M)
tra-∘ (Λʰ_ {τ/ε = τ/ε} H wkn≡) = ≡ᴹ-Λʰ (tra★-∘ τ/ε) (tra-∘▷★ H)
tra-∘ (ƛʰ H) = ≡ᴹ-ƛʰ (tra-∘▷ H)
tra-∘ (ƛʰᴺ_ {τ/ε = τ/ε} H wkn≡) = ≡ᴹ-ƛʰᴺ (tra★-∘ τ/ε) (tra-∘▷ᴺ H)
tra-∘ (Handler M) = ≡ᴹ-Handler (tra-∘▷ M)
```

```
-[_] : Γ ⊢ τ → Γ ▷ τ ↠ Γ
var★ -[ V ] = →★-id
tra∈ -[ V ] Z = subst (_ ⊢_) (sym (tra★-id _)) V
tra∈ -[ V ] (S x) = subst (_ ⊢_) (sym (tra★-id _)) (Var x)

-[_]ᵀ : {Γ : Context L Δ} → (τ : L ⨟ Δ ⊢ᵀ κ) → Γ ▷★ κ ↠ Γ
var★ -[ τ ]ᵀ = -[ τ ]★
tra∈ -[ τ ]ᵀ ((S★ x) (` wkn≡)) = subst (_ ⊢_) (trans (sym ([]★∘weaken★ _ _)) (cong (sub★ _) wkn≡)) (Var x)
```

```
_[_] : ∀ {κ₂} {τ₁} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → Γ ▷ τ₁ ⊢ τ₂ → Γ ⊢ τ₁ → Γ ⊢ τ₂
M [ V ] = subst (_ ⊢_) (tra★-id _) (tra -[ V ] M)

_[_]ᵀ : ∀ {κ₁} {κ₂} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → Γ ▷★ κ₁ ⊢ τ₂ → (τ₁ : L ⨟ Δ ⊢ᵀ κ₁) → Γ ⊢ τ₂ [ τ₁ ]★
M [ τ ]ᵀ = tra -[ τ ]ᵀ M
```

```
weaken₀→ᵍ : {Γ : Context L ∅} → ∅ →ᵍ Γ
weaken₀→ᵍ = →★-id , λ()

weaken₀ : ∅ ⊢ τ → Γ ⊢ τ
weaken₀ M = subst (_ ⊢_) (tra★-id _) (tra weaken₀→ᵍ M)

weaken₁→ᵍ : ∅ ▷ τ′ →ᵍ Γ ▷ τ′
var★ weaken₁→ᵍ = →★-id
tra∈ weaken₁→ᵍ Z = subst (_∈ _) (sym (tra★-id _)) Z

weaken₁ : ∅ ▷ τ′ ⊢ τ → Γ ▷ τ′ ⊢ τ
weaken₁ M = subst (_ ⊢_) (tra★-id _) (tra weaken₁→ᵍ M)
```

```
instance
  Kit-∘-∈-∈ : Kit-∘ K∈ K∈
  Kit-∘-∈-∈ = record {
    tra∘wkn = λ x → sym ≡-weaken-S ;
    tra∘wknᵀ = λ x → ≡ᴹ-var (≡[∈]-trans (≡[∈]-subst _ {x = S★_ _ _}) refl) ;
    tra∘wknᴺ = λ x → ≡ᴹ-var (≡[∈]-trans (≡[∈]-subst _ {x = Sᴺ_ _ _}) refl) }
```

```
tra-S-Var : ∀ {x : τ ∈ Γ} → tra (↠-S {τ = τ′}) (Var x) ≡ᴹ Var (S_ {τ′ = τ′} x)
tra-S-Var = ≡ᴹ-var (≡[∈]-subst _)
```

```
ren▷∘↠-S : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} → tra▷ r {τ′} ∘[↠] ↠-S ≡[↠] ↠-S {τ = tra★ r★ τ′} ∘[↠] r
≡[↠]⇒≡ᵍ ren▷∘↠-S = refl
≡[↠]⇒≡[↠★] (ren▷∘↠-S {r = (r★ , _)}) = (λ α → refl) , (λ n → sym (tra∈ᶻ-id (tra∈ᴺ r★ n)))
tra∈-≡ (ren▷∘↠-S {τ′ = τ′} {r = r}) x =
  tra∈ (tra▷ r ∘[↠] ↠-S) x ≡⟨ tra∈-∘ (tra▷ r) ↠-S x ⟩
  tra (tra▷ r {τ′}) (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = tra▷ r} (tra∈-id {x = S x}) ⟩
  tra (tra▷ r {τ′}) (Var (S x)) ≡⟨ refl ⟩
  Var (S (tra∈ r x)) ≡⟨ ≡ᴹ-sym tra-S-Var ⟩
  tra ↠-S (Var (tra∈ r x)) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S r x) ⟩
  tra∈ (↠-S ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
ren∘weaken′ : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r {τ′}) (tra ↠-S M) ≡ᴹ tra (↠-S {τ = tra★ r★ τ′}) (tra r M)
ren∘weaken′ M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ ren▷∘↠-S M) (≡ᴹ-sym (tra-∘ M)))
```

```
ren∘weaken : ∀ {r@(r★ , _) : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r) (weaken {τ′ = τ′} M) ≡ᴹ weaken {τ′ = tra★ r★ τ′} (tra r M)
ren∘weaken {τ = τ} {τ′ = τ′} {r = r@(r★ , _)} M =
  tra (tra▷ r) (weaken {τ′ = τ′} M) ≡⟨ ≡ᴹ-tra {τ = τ} {r = tra▷ r} (≡ᴹ-subst (tra★-id _) (tra ↠-S M)) ⟩
  tra (tra▷ r) (tra ↠-S M) ≡⟨ ren∘weaken′ M ⟩
  tra ↠-S (tra r M) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst _ _) ⟩
  weaken {τ′ = tra★ r★ τ′} (tra r M) ∎
  where open ≡ᴹ-Reasoning
```

```
ren▷★∘↠-S★ : ∀ {r : Γ →ᵍ Γ′} → tra▷★ r {κ} ∘[↠] ↠-S★ ≡[↠] ↠-S★ ∘[↠] r
≡[↠]⇒≡ᵍ ren▷★∘↠-S★ = refl
≡[↠]⇒≡[↠★] ren▷★∘↠-S★ = (λ α → refl) , (λ n → refl)
tra∈-≡ (ren▷★∘↠-S★ {r = r}) x =
  tra∈ (tra▷★ r ∘[↠] ↠-S★) x ≡⟨ tra∈-∘ (tra▷★ r) ↠-S★ x ⟩
  tra (tra▷★ r) (Var (S★_ x `refl)) ≡⟨ ≡ᴹ-var (≡[∈]-subst _) ⟩
  Var (S★_ (tra∈ r x) `refl) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S★ r x) ⟩
  tra∈ (↠-S★ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
ren∘weakenᵀ : ∀ {r : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷★ r) (weakenᵀ {κ = κ} M) ≡ᴹ weakenᵀ (tra r M)
ren∘weakenᵀ M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ ren▷★∘↠-S★ M) (≡ᴹ-sym (tra-∘ M)))
```

```
ren▷ᴺ∘↠-Sᴺ : ∀ {r : Γ →ᵍ Γ′} → tra▷ᴺ r ∘[↠] ↠-Sᴺ ≡[↠] ↠-Sᴺ ∘[↠] r
≡[↠]⇒≡ᵍ ren▷ᴺ∘↠-Sᴺ = refl
≡[↠]⇒≡[↠★] ren▷ᴺ∘↠-Sᴺ = (λ α → refl) , (λ n → refl)
tra∈-≡ (ren▷ᴺ∘↠-Sᴺ {r = r}) x =
  tra∈ (tra▷ᴺ r ∘[↠] ↠-Sᴺ) x ≡⟨ tra∈-∘ (tra▷ᴺ r) ↠-Sᴺ x ⟩
  tra (tra▷ᴺ r) (Var (Sᴺ_ x `refl)) ≡⟨ ≡ᴹ-var (≡[∈]-subst _) ⟩
  Var (Sᴺ_ (tra∈ r x) `refl) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-Sᴺ r x) ⟩
  tra∈ (↠-Sᴺ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
ren∘weakenᴺ : ∀ {r : Γ →ᵍ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ᴺ r) (weakenᴺ M) ≡ᴹ weakenᴺ (tra r M)
ren∘weakenᴺ M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ ren▷ᴺ∘↠-Sᴺ M) (≡ᴹ-sym (tra-∘ M)))
```

```
instance
  Kit-∘-⊢-∈ : Kit-∘ K⊢ K∈
  Kit-∘-⊢-∈ = record {
    tra∘wkn = λ M → ≡ᴹ⇒≡ (ren∘weaken M) ;
    tra∘wknᵀ = ren∘weakenᵀ ;
    tra∘wknᴺ = ren∘weakenᴺ }
```

```
instance
  Kit-∘-∈-⊢ : Kit-∘ K∈ K⊢
  Kit-∘-∈-⊢ = record {
    tra∘wkn = λ x → refl ;
    tra∘wknᵀ = λ x → ≡ᴹ-subst _ _ ;
    tra∘wknᴺ = λ x → ≡ᴹ-subst _ _ }
```

```
sub▷∘↠-S : ∀ {r@(r★ , _) : Γ ↠ Γ′} → tra▷ r {τ′} ∘[↠] ↠-S ≡[↠] ↠-S {τ = tra★ r★ τ′} ∘[↠] r
≡[↠]⇒≡ᵍ sub▷∘↠-S = refl
≡[↠]⇒≡[↠★] (sub▷∘↠-S {r = (r★ , _)}) = (λ α → sym (tra★-id _)) , (λ n → sym (tra∈ᶻ-id (tra∈ᴺ r★ n)))
tra∈-≡ (sub▷∘↠-S {τ′ = τ′} {r = r}) x =
  tra∈ (tra▷ r ∘[↠] ↠-S) x ≡⟨ tra∈-∘ (tra▷ r) ↠-S x ⟩
  tra (tra▷ r {τ′}) (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = tra▷ r} (tra∈-id {x = S x}) ⟩
  tra (tra▷ r {τ′}) (Var (S x)) ≡⟨ refl ⟩
  weaken (tra∈ r x) ≡⟨ ≡ᴹ-subst _ _ ⟩
  tra ↠-S (tra∈ r x) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S r x) ⟩
  tra∈ (↠-S ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning
```

```
sub∘weaken′ : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r {τ′}) (tra ↠-S M) ≡ᴹ tra (↠-S {τ = tra★ r★ τ′}) (tra r M)
sub∘weaken′ {r = r} M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ (sub▷∘↠-S {r = r}) M) (≡ᴹ-sym (tra-∘ M)))
```

```
sub∘weaken : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ r) (weaken {τ′ = τ′} M) ≡ᴹ weaken {τ′ = tra★ r★ τ′} (tra r M)
sub∘weaken {τ = τ} {τ′ = τ′} {r = r@(r★ , _)} M =
  tra (tra▷ r) (weaken {τ′ = τ′} M) ≡⟨ ≡ᴹ-tra {τ = τ} {r = tra▷ r} (≡ᴹ-subst (tra★-id _) (tra ↠-S M)) ⟩
  tra (tra▷ r) (tra ↠-S M) ≡⟨ sub∘weaken′ M ⟩
  tra ↠-S (tra r M) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst _ _) ⟩
  weaken {τ′ = tra★ r★ τ′} (tra r M) ∎
  where open ≡ᴹ-Reasoning
```

```
sub▷★∘↠-S★ : ∀ {r@(r★ , _) : Γ ↠ Γ′} → tra▷★ r {κ} ∘[↠] ↠-S★ ≡[↠] ↠-S★ ∘[↠] r
≡[↠]⇒≡ᵍ sub▷★∘↠-S★ = refl
≡[↠]⇒≡[↠★] sub▷★∘↠-S★ = (λ α → refl) , (λ n → refl)
tra∈-≡ (sub▷★∘↠-S★ {r = r}) x =
  tra∈ (tra▷★ r ∘[↠] ↠-S★) x ≡⟨ tra∈-∘ (tra▷★ r) ↠-S★ x ⟩
  tra (tra▷★ r) (Var (S★_ x `refl)) ≡⟨ ≡ᴹ-subst _ _ ⟩
  weakenᵀ (tra∈ r x) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-S★ r x) ⟩
  tra∈ (↠-S★ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning

sub∘weakenᵀ : ∀ {r@(r★ , _) : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷★ r) (weakenᵀ {κ = κ} M) ≡ᴹ weakenᵀ (tra r M)
sub∘weakenᵀ {r = r} M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ (sub▷★∘↠-S★ {r = r}) M) (≡ᴹ-sym (tra-∘ M)))
```

```
sub▷ᴺ∘↠-Sᴺ : ∀ {r : Γ ↠ Γ′} → tra▷ᴺ r ∘[↠] ↠-Sᴺ ≡[↠] ↠-Sᴺ ∘[↠] r
≡[↠]⇒≡ᵍ sub▷ᴺ∘↠-Sᴺ = refl
≡[↠]⇒≡[↠★] sub▷ᴺ∘↠-Sᴺ = (λ α → refl) , (λ n → refl)
tra∈-≡ (sub▷ᴺ∘↠-Sᴺ {r = r}) x =
  tra∈ (tra▷ᴺ r ∘[↠] ↠-Sᴺ) x ≡⟨ tra∈-∘ (tra▷ᴺ r) ↠-Sᴺ x ⟩
  tra (tra▷ᴺ r) (Var (Sᴺ_ x `refl)) ≡⟨ ≡ᴹ-subst _ _ ⟩
  weakenᴺ (tra∈ r x) ≡⟨ ≡ᴹ-sym (tra∈-∘ ↠-Sᴺ r x) ⟩
  tra∈ (↠-Sᴺ ∘[↠] r) x ∎
  where open ≡ᴹ-Reasoning

sub∘weakenᴺ : ∀ {r : Γ ↠ Γ′} (M : Γ ⊢ τ)
  → tra (tra▷ᴺ r) (weakenᴺ M) ≡ᴹ weakenᴺ (tra r M)
sub∘weakenᴺ {r = r} M = ≡ᴹ-trans (tra-∘ M) (≡ᴹ-trans (tra-≡ (sub▷ᴺ∘↠-Sᴺ {r = r}) M) (≡ᴹ-sym (tra-∘ M)))
```

```
instance
  Kit-∘-⊢-⊢ : Kit-∘ K⊢ K⊢
  Kit-∘-⊢-⊢ = record {
    tra∘wkn = λ M → ≡ᴹ⇒≡ (sub∘weaken M) ;
    tra∘wknᵀ = sub∘weakenᵀ ;
    tra∘wknᴺ = sub∘weakenᴺ }
```

```
⦅appᵛ_,_[_]⦆ :
    (V₁ : Γ ▷ τ ⊢ τ₁ ⇒ τ₂)
    (V₂ : Γ ▷ τ ⊢ τ₁)
    (W : Γ ⊢ τ)
  → appᵛ V₁ V₂ [ W ] ≡ᴹ appᵛ (V₁ [ W ]) (V₂ [ W ])
⦅appᵛ V₁ , V₂ [ W ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) (≡ᴹ-sym (≡ᴹ-appᵛ (≡ᴹ-subst (tra★-id _) (tra -[ W ] V₁)) (≡ᴹ-subst (tra★-id _) (tra -[ W ] V₂))))
```

```
unfold:_[_] : ∀ {κ₂} {τ₁} {τ₂ : _ ⨟ _ ⊢ᵀ κ₂} → (M : Γ ▷ τ₁ ⊢ τ₂) → (V : Γ ⊢ τ₁)
  → M [ V ] ≡ᴹ tra -[ V ] M
unfold: M [ V ] = ≡ᴹ-subst (tra★-id _) (tra -[ V ] M)
```

```
renᵍᴸ-id : renᵍᴸ →ᴸ-id Γ ≡ Γ
renᵍᴸ-id {Γ = ∅} = refl
renᵍᴸ-id {Γ = Γ ▷ x} = cong₂ _▷_ renᵍᴸ-id (tra★-id _)
renᵍᴸ-id {Γ = Γ ▷★ κ} = cong (_▷★ _) renᵍᴸ-id
renᵍᴸ-id {Γ = (Γ ▷ᴺ-)} = cong (_▷ᴺ-) renᵍᴸ-id
```

```
→ᴸ⇒→-id-≡[∈] : (x : τ ∈ Γ)
  → tra∈ (→ᴸ⇒→ᵍ →ᴸ-id) x ≡[∈] x
→ᴸ⇒→-id-≡[∈] Z = ≡[∈]-Z renᵍᴸ-id (tra★-id _)
→ᴸ⇒→-id-≡[∈] (S x) = ≡[∈]-S (tra★-id _) (→ᴸ⇒→-id-≡[∈] x)
→ᴸ⇒→-id-≡[∈] ((S★ x) x₁) = ≡[∈]-S★ (→ᴸ⇒→-id-≡[∈] x)
→ᴸ⇒→-id-≡[∈] ((Sᴺ x) x₁) = ≡[∈]-Sᴺ (→ᴸ⇒→-id-≡[∈] x)

→ᴸ⇒→-id : →ᴸ⇒→ᵍ →ᴸ-id {Γ = Γ} ≡[↠] ↠-id
≡[↠]⇒≡ᵍ →ᴸ⇒→-id = renᵍᴸ-id
≡[↠]⇒≡[↠★] →ᴸ⇒→-id = (λ α → refl) , (λ n → refl)
tra∈-≡ →ᴸ⇒→-id x = ≡ᴹ-trans (≡ᴹ-var (→ᴸ⇒→-id-≡[∈] x)) (≡ᴹ-var (≡[∈]-sym (≡[∈]-subst _)))
```

```
renᴸ-id : {M : Γ ⊢ τ} → renᴸ →ᴸ-id M ≡ᴹ M
renᴸ-id {M = M} = ≡ᴹ-trans (tra-≡ →ᴸ⇒→-id M) (tra-id _) --  →ᴸ⇒→-id
```

```
renᵍᴸ-∘ : ∀ {rᴸ′ : L′ →ᴸ L″} {rᴸ : L →ᴸ L′} {Γ : Context L Δ} → renᵍᴸ rᴸ′ (renᵍᴸ rᴸ Γ) ≡ renᵍᴸ (rᴸ′ ∘ᴸ rᴸ) Γ
renᵍᴸ-∘ {Γ = ∅} = refl
renᵍᴸ-∘ {rᴸ′ = rᴸ′} {rᴸ = rᴸ} {Γ = Γ ▷ τ} = cong₂ _▷_ renᵍᴸ-∘ (trans (tra★-∘ τ) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ))
renᵍᴸ-∘ {Γ = Γ ▷★ κ} = cong (_▷★ _) renᵍᴸ-∘
renᵍᴸ-∘ {Γ = (Γ ▷ᴺ-)} = cong (_▷ᴺ-) renᵍᴸ-∘
```

```
≡-renᵍᴸ : {Γ : Context L Δ} → renᵍᴸ rᴸ Γ ≡ renᵍᴸ rᴸ′ Γ
≡-renᵍᴸ {Γ = ∅} = refl
≡-renᵍᴸ {Γ = Γ ▷ τ} = cong₂ _▷_ ≡-renᵍᴸ (tra★-≡ ((λ α → refl) , (λ n → refl)) τ)
≡-renᵍᴸ {Γ = Γ ▷★ κ} = cong (_▷★ _) ≡-renᵍᴸ
≡-renᵍᴸ {Γ = (Γ ▷ᴺ-)} = cong _▷ᴺ- ≡-renᵍᴸ
```

```
→ᴸ⇒→ᵍ-∘-≡[∈] : (x : τ ∈ Γ)
  → tra∈ (→ᴸ⇒→ᵍ rᴸ′) (tra∈ (→ᴸ⇒→ᵍ rᴸ) x) ≡[∈] tra∈ (→ᴸ⇒→ᵍ (rᴸ′ ∘ᴸ rᴸ)) x
→ᴸ⇒→ᵍ-∘-≡[∈] {rᴸ′ = rᴸ′} {rᴸ = rᴸ} (Z {τ = τ}) = ≡[∈]-Z renᵍᴸ-∘ (trans (tra★-∘ τ) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ))
→ᴸ⇒→ᵍ-∘-≡[∈] {rᴸ′ = rᴸ′} {rᴸ = rᴸ} (S_ {τ′ = τ₂} x) = ≡[∈]-S (trans (tra★-∘ τ₂) (tra★-≡ (→ᴸ⇒→★-∘ rᴸ′ rᴸ) τ₂)) (→ᴸ⇒→ᵍ-∘-≡[∈] x)
→ᴸ⇒→ᵍ-∘-≡[∈] ((S★ x) x₁) = ≡[∈]-S★ (→ᴸ⇒→ᵍ-∘-≡[∈] x)
→ᴸ⇒→ᵍ-∘-≡[∈] ((Sᴺ x) x₁) = ≡[∈]-Sᴺ (→ᴸ⇒→ᵍ-∘-≡[∈] x)
```

```
≡ᴹ⇒≡ᵀ : {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′} → M ≡ᴹ M′ → τ ≡ τ′
≡ᴹ⇒≡ᵀ refl = refl
```

```
-- TODO: don't use K (equality is decidable)
≡ᵀ-unique : (p q : τ ≡ τ′) → p ≡ q
≡ᵀ-unique refl refl = refl
```

```
⦅weaken⦆ : ∀ {M : Γ ⊢ τ} {M′ : Γ′ ⊢ τ′}
  → τ₁ ≡ τ₁′
  → M ≡ᴹ M′
  → weaken {τ′ = τ₁} M ≡ᴹ weaken {τ′ = τ₁′} M′
⦅weaken⦆ refl refl = refl
```

```
-[]∘S : (V : Γ ⊢ τ)
  → (-[ V ] ∘[↠] ↠-S) ≡[↠] ↠-id
≡[↠]⇒≡ᵍ (-[]∘S V) = refl
≡[↠]⇒≡[↠★] (-[]∘S V) = (λ α → refl) , (λ n → refl)
tra∈-≡ (-[]∘S V) x =
  tra∈ (-[ V ] ∘[↠] ↠-S) x ≡⟨ tra∈-∘ -[ V ] ↠-S x ⟩
  tra -[ V ] (Var (tra∈ ↠-id (S x))) ≡⟨ ≡ᴹ-tra {r = -[ V ]} (tra∈-id {x = (S x)}) ⟩
  tra -[ V ] (Var (S x)) ≡⟨ ≡ᴹ-subst _ _ ⟩
  Var x ≡⟨ ≡ᴹ-sym tra∈-id ⟩
  Var (tra∈ ↠-id x) ∎
  where open ≡ᴹ-Reasoning
```

```
⦅weaken_[_]′⦆ : ∀ (M : Γ ⊢ τ) (V : Γ ⊢ τ′)
  → tra -[ V ] (weaken M) ≡ᴹ M
⦅weaken_[_]′⦆ {τ = τ} M V =
  tra -[ V ] (weaken M) ≡⟨ ≡ᴹ-tra {r = -[ V ]} (≡ᴹ-subst (tra★-id τ) (tra ↠-S M)) ⟩
  tra -[ V ] (tra ↠-S M) ≡⟨ tra-∘ M ⟩
  tra (-[ V ] ∘[↠] ↠-S) M ≡⟨ tra-≡ (-[]∘S V) M ⟩
  tra ↠-id M ≡⟨ tra-id M ⟩
  M ∎
  where open ≡ᴹ-Reasoning
```

```
⦅weaken_[_]⦆ : ∀ (M : Γ ⊢ τ) (V : Γ ⊢ τ′)
  → weaken M [ V ] ≡ᴹ M
⦅weaken M [ V ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) ⦅weaken M [ V ]′⦆
```

```
⦅VarZ[_]⦆ : ∀ (V : Γ ⊢ τ)
  → Var Z [ V ] ≡ᴹ V
⦅VarZ[ V ]⦆ = ≡ᴹ-trans (≡ᴹ-subst (tra★-id _) _) (≡ᴹ-subst (sym (tra★-id _)) _)
```

```
⦅ƛ_[_]ᵀ⦆ : ∀ (M : Γ ▷★ κ ▷ τ₂ ⊢ τ₃) (τ : L ⨟ Δ ⊢ᵀ κ)
  → (ƛ M) [ τ ]ᵀ ≡ᴹ ƛ (tra (tra▷ -[ τ ]ᵀ) M)
⦅ƛ M [ τ ]ᵀ⦆ = refl
```

```
-[]ᵀ∘-[]′ : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁) (x : τ′ ∈ Γ ▷★ κ ▷ τ₁)
  → tra -[ V [ τ ]ᵀ ] (tra∈ (tra▷ -[ τ ]ᵀ) x) ≡ᴹ tra -[ τ ]ᵀ (tra∈ -[ V ] x)
-[]ᵀ∘-[]′ {τ₁ = τ₁} τ V Z = ≡ᴹ-trans (≡ᴹ-subst _ (tra -[ τ ]ᵀ V)) (≡ᴹ-tra {τ = τ₁} {τ′ = tra★ _ τ₁} {r = -[ τ ]ᵀ} (≡ᴹ-sym (≡ᴹ-subst (sym (tra★-id _)) V)))
-[]ᵀ∘-[]′ {τ₁ = τ₁} {τ′ = τ′} τ V (S x) =
  tra -[ V [ τ ]ᵀ ] (weaken (tra∈ -[ τ ]ᵀ x)) ≡⟨ ⦅weaken _ [ V [ τ ]ᵀ ]′⦆ ⟩
  tra∈ -[ τ ]ᵀ x ≡⟨ ≡ᴹ-tra {r = -[ τ ]ᵀ} (≡ᴹ-sym (≡ᴹ-subst (sym (tra★-id _)) (Var x))) ⟩
  tra -[ τ ]ᵀ (tra∈ -[ V ] (S x)) ∎
  where open ≡ᴹ-Reasoning

-[]ᵀ∘-[] : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁)
  → (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) ≡[↠] (-[ τ ]ᵀ ∘[↠] -[ V ])
≡[↠]⇒≡ᵍ (-[]ᵀ∘-[] τ V) = refl
tra∈★-≡ (≡[↠]⇒≡[↠★] (-[]ᵀ∘-[] τ V)) = λ α → tra★-id _
tra∈ᴺ-≡ (≡[↠]⇒≡[↠★] (-[]ᵀ∘-[] τ V)) = λ n → tra∈ᶻ-id _
tra∈-≡ (-[]ᵀ∘-[] τ V) x =
  tra∈ (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) x ≡⟨ tra∈-∘ -[ V [ τ ]ᵀ ] (tra▷ -[ τ ]ᵀ) x ⟩
  tra -[ V [ τ ]ᵀ ] (tra∈ (tra▷ -[ τ ]ᵀ) x) ≡⟨ -[]ᵀ∘-[]′ τ V x ⟩
  tra -[ τ ]ᵀ (tra∈ -[ V ] x) ≡⟨ ≡ᴹ-sym (tra∈-∘ -[ τ ]ᵀ -[ V ] x) ⟩
  tra∈ (-[ τ ]ᵀ ∘[↠] -[ V ]) x ∎
  where open ≡ᴹ-Reasoning
```

In the translation BBD2FCN, name abstraction `Λᴺ M` is translated to
a type abstraction followed by a term abstraction `Λ ƛ M′`.
By β reduction, name application `(Λᴺ M) n` reduces to a substitution `M [ n ]`,
which translates to a simultaneous type- and term- substitution `M′ [ τ′ , V′ ]`
(where `τ′` and `V′` are the type- and term-level translations of `n`).
By contrast, an application of the translated abstraction `(Λ ƛ M′) τ′ V′`
β reduces to two successive substitutions, `M′ ▷[ τ′ ] [ V′ ]`.

```
⦅_▷[_]ᵀ[_]⦆ : ∀ (M : Γ ▷★ κ ▷ τ₁ ⊢ τ₂) (τ : L ⨟ Δ ⊢ᵀ κ) (V : Γ ▷★ κ ⊢ τ₁)
  → tra (tra▷ -[ τ ]ᵀ) M [ V [ τ ]ᵀ ] ≡ᴹ M [ V ] [ τ ]ᵀ
⦅_▷[_]ᵀ[_]⦆ M τ V =
  (tra (tra▷ -[ τ ]ᵀ) M) [ V [ τ ]ᵀ ] ≡⟨ unfold: tra _ M [ V [ τ ]ᵀ ] ⟩
  tra -[ V [ τ ]ᵀ ] (tra (tra▷ -[ τ ]ᵀ) M) ≡⟨ tra-∘ M ⟩
  tra (-[ V [ τ ]ᵀ ] ∘[↠] tra▷ -[ τ ]ᵀ) M ≡⟨ tra-≡ (-[]ᵀ∘-[] τ V) M ⟩
  tra (-[ τ ]ᵀ ∘[↠] -[ V ]) M ≡⟨ ≡ᴹ-sym (tra-∘ M) ⟩
  (tra -[ V ] M) [ τ ]ᵀ ≡⟨ ≡ᴹ-sym (≡ᴹ-tra {r = -[ τ ]ᵀ} (unfold: M [ V ])) ⟩
  (M [ V ] [ τ ]ᵀ) ∎
  where open ≡ᴹ-Reasoning
```

```
appᵀ-irr : ∀ {Δ} {Γ : Context L Δ} {κ} {σ} {σ[τ] σ[τ]′}
  → {N : Γ ⊢ Forallᵀ κ σ}
  → {τ : L ⨟ Δ ⊢ᵀ κ}
  → {σ[τ]≡ : _ ≡ σ[τ]}
  → {σ[τ]′≡ : _ ≡ σ[τ]′}
  → appᵀ N τ σ[τ]≡ ≡ᴹ appᵀ N τ σ[τ]′≡
appᵀ-irr {σ[τ]≡ = refl} {σ[τ]′≡ = refl} = refl
```

```
wrap :
    (M : Γ ▷ᴺ- ⊢ τ′ / static Z ⦂ σ′ ∷ ε′)
  → ∀ {τ σ ε}
  → (wkn≡ : weaken★ᴺ $ τ ≡ τ′ × weaken★ᴺ $ σ ≡ σ′ × weaken★ᴺ $ ε ≡ ε′)
  → Γ ▷ᴺ- ⊢ weaken★ᴺ τ / static Z ⦂ weaken★ᴺ σ ∷ weaken★ᴺ ε
wrap M (`refl , `refl , `refl) = M
```

```
unfold:wrap : {Γ : Context L Δ}
  → (M : Γ ▷ᴺ- ⊢ τ′ / static Z ⦂ σ′ ∷ ε′)
  → (wkn≡ : weaken★ᴺ $ τ ≡ τ′ × weaken★ᴺ $ σ ≡ σ′ × weaken★ᴺ $ ε ≡ ε′)
  → wrap M {τ = τ} {σ = σ} {ε = ε} wkn≡ ≡ᴹ M
unfold:wrap M (`refl , `refl , `refl) = refl
```

```
handle-irr : ∀ {Γ : Context L Δ} {σ} {τ} {ε}
    {M : Γ ▷ᴺ- ⊢ τ′ / static Z ⦂ σ′ ∷ ε′}
    {H : Γ ⊢ σ ⇒ʰ τ / ε}
    {wkn≡ : weaken★ᴺ $ τ ≡ τ′ × weaken★ᴺ $ σ ≡ σ′ × weaken★ᴺ $ ε ≡ ε′}
  → handle M H wkn≡ ≡ᴹ handle (wrap M {τ = τ} {σ = σ} {ε = ε} wkn≡) H (`refl , `refl , `refl)
handle-irr {wkn≡ = (`refl , `refl , `refl)} = refl
```

```
[]★ᴺ∘weaken★ᴺ : ∀ (τ : L ⨟ Δ ⊢ᵀ κ) (n : -∈ᴺ L ⨟ Δ)
  → weaken★ᴺ τ [ n ]★ᴺ ≡ τ
[]★ᴺ∘weaken★ᴺ τ σ = trans (tra★-∘ τ) (trans (tra★-≡ ≡[↠★]-refl τ) (tra★-id τ))
```

```
[]★ᴺ∘weaken★ᴺ∙ : ∀ {τ : L ⨟ Δ ⊢ᵀ κ} {n : -∈ᴺ L ⨟ Δ}
  → weaken★ᴺ $ τ ≡ τ′
  → τ′ [ n ]★ᴺ ≡ τ
[]★ᴺ∘weaken★ᴺ∙ {τ = τ} {n} (` w≡) = trans (cong (_[ n ]★ᴺ) (sym w≡)) ([]★ᴺ∘weaken★ᴺ τ n)
```

```
-[_]ᴺ : {Γ : Context L Δ} → (n : -∈ᴺ L ⨟ Δ) → Γ ▷ᴺ- ↠ Γ
var★ -[ n ]ᴺ = -[ n ]★ᴺ
tra∈ -[ n ]ᴺ ((Sᴺ x) (` wkn≡)) = subst (_ ⊢_) (trans (sym ([]★ᴺ∘weaken★ᴺ _ _)) (cong (sub★ _) wkn≡)) (Var x)

_[_]ᴺ : ∀ {τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ} → Γ ▷ᴺ- ⊢ τ → (n : -∈ᴺ L ⨟ Δ) → Γ ⊢ τ [ n ]★ᴺ
M [ n ]ᴺ = tra -[ n ]ᴺ M
```

```
⦅ƛ_[_]ᴺ⦆ : ∀ (M : Γ ▷ᴺ- ▷ τ₂ ⊢ τ₃) (n : -∈ᴺ L ⨟ Δ)
  → (ƛ M) [ n ]ᴺ ≡ᴹ ƛ (tra (tra▷ -[ n ]ᴺ) M)
⦅ƛ M [ τ ]ᴺ⦆ = refl
```

```
≡ᴹ-[] : {M : Γ ▷ τ₁ ⊢ τ₂} {M′ : Γ′ ▷ τ₁′ ⊢ τ₂′}
    {V : Γ ⊢ τ₁} {V′ : Γ′ ⊢ τ₁′}
  → M ≡ᴹ M′
  → V ≡ᴹ V′
  → M [ V ] ≡ᴹ M′ [ V′ ]
≡ᴹ-[] refl refl = refl
```

```
-∘[] : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄ ⦃ _ : Kit-∘ K⊢ K ⦄ {L L′ Δ Δ′} {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → {τ : L ⨟ Δ ⊢ᵀ TYP}
  → (r : ⦅ K ⦆ Γ ↠ Γ′)
  → (V : Γ ⊢ τ)
  → -[ tra r V ] ∘[↠] tra▷ r ≡[↠] r ∘[↠] -[ V ]
≡[↠]⇒≡ᵍ (-∘[] r V) = refl
≡[↠]⇒≡[↠★] (-∘[] r V) = (λ α → tra★-id _) , (λ n → tra∈ᶻ-id _)
tra∈-≡ (-∘[] {K = K} {τ = τ} r V) {.τ} Z =
  tra∈ (-[ tra r V ] ∘[↠] tra▷ r {τ = τ}) Z ≡⟨ ≡ᴹ-subst (tra★-∘ τ) _ ⟩
  tra -[ tra r V ] (to-⊢ K (from-∈ K Z)) ≡⟨ ≡ᴹ-tra (≡ᴹ-refl (to-from {K = K} Z)) ⟩
  tra -[ tra r V ] (Var Z) ≡⟨ ≡ᴹ-subst _ (tra r V) ⟩
  tra r V ≡⟨ ≡ᴹ-tra (≡ᴹ-sym (≡ᴹ-subst (sym (tra★-id τ)) V)) ⟩
  tra r (tra∈ -[ V ] Z) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst (tra★-∘ τ) _) ⟩
  tra∈ (r ∘[↠] -[ V ]) Z ∎
  where open ≡ᴹ-Reasoning
tra∈-≡ (-∘[] {K = K} {τ = τ} r V) {τ₁} (S x) =
  tra∈ (-[ tra r V ] ∘[↠] tra▷ r {τ = τ}) (S x) ≡⟨ ≡ᴹ-subst (tra★-∘ τ₁) _ ⟩
  tra -[ tra r V ] (to-⊢ K (wkn K (tra∈ r x))) ≡⟨ ≡ᴹ-tra (≡ᴹ-refl (to-wkn (tra∈ r x))) ⟩
  tra -[ tra r V ] (weaken (to-⊢ K (tra∈ r x))) ≡⟨ ⦅weaken (to-⊢ K (tra∈ r x)) [ tra r V ]′⦆ ⟩
  to-⊢ K (tra∈ r x) ≡⟨ ≡ᴹ-sym (≡ᴹ-tra (≡ᴹ-subst (sym (tra★-id τ₁)) (Var x))) ⟩
  tra r (subst (_⊢_ _) (sym (tra★-id τ₁)) (Var x)) ≡⟨ ≡ᴹ-sym (≡ᴹ-subst (tra★-∘ τ₁) _) ⟩
  tra∈ (r ∘[↠] -[ V ]) (S x) ∎
  where
    open ≡ᴹ-Reasoning
    eq = tra★-id {K = K★ K} (tra★ (var★ r) τ₁)

tra-[] : ∀ {K : Kit _#★_ _#_} ⦃ _ : GoodKit K ⦄ ⦃ _ : Kit-∘ K⊢ K ⦄ ⦃ _ : Kit-∘ K K⊢ ⦄
    {L L′ Δ Δ′} {κ} {Γ : Context L Δ} {Γ′ : Context L′ Δ′}
  → (r : ⦅ K ⦆ Γ ↠ Γ′)
  → {τ : L ⨟ Δ ⊢ᵀ TYP}
  → {τ₂ : L ⨟ Δ ⊢ᵀ κ}
  → (M : Γ ▷ τ ⊢ τ₂)
  → (V : Γ ⊢ τ)
  → tra (tra▷ r) M [ tra r V ] ≡ᴹ tra r (M [ V ])
tra-[] r M V =
  tra (tra▷ r) M [ tra r V ] ≡⟨ unfold: tra (tra▷ r) M [ tra r V ] ⟩
  tra -[ tra r V ] (tra (tra▷ r) M) ≡⟨ tra-∘ M ⟩
  tra (-[ tra r V ] ∘[↠] tra▷ r) M ≡⟨ tra-≡ (-∘[] r V) M ⟩
  tra (r ∘[↠] -[ V ]) M ≡⟨ ≡ᴹ-sym (tra-∘ M) ⟩
  tra r (tra -[ V ] M) ≡⟨ ≡ᴹ-sym (≡ᴹ-tra (unfold: M [ V ])) ⟩
  tra r (M [ V ]) ∎
  where open ≡ᴹ-Reasoning
```

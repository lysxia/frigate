```
module FCNSemantics where

open import Common using (⊥-elim; _$_≡_; `refl)
open import Labels
open import FirstClassNames
open import FCNSubstitution

open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Product using (∃-syntax; Σ-syntax; _×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; sym; cong; cong₂; subst; module ≡-Reasoning) renaming (trans to ≡-trans)
```

```
private variable
  L L′ L″ : Labels
  rᴸ : L →ᴸ L′
  rᴸ′ : L′ →ᴸ L″
  Δ Δ′ : Contextᴷ
  κ κ′ : Kind
  τ τ′ τ₁ τ₂ τ₃ τ₁′ τ₂′ τ₃′ τ″ τ₁″ τ₂″ σ σ′ : L ⨟ Δ ⊢ᵀ κ
  ε ε₁ ε₁′ ε′ ε″ : L ⨟ Δ ⊢ᵀ EFF
  τ/ε τ/ε₁ τ/ε₂ : L ⨟ Δ ⊢ᵀ COM
  τ/ε′ τ/ε₁′ τ/ε₂′ : L′ ⨟ Δ ⊢ᵀ COM
  τ/ε″ τ/ε₁″ τ/ε₂″ : L″ ⨟ Δ ⊢ᵀ COM
  η η′ : SCO ∈★ Δ
  Γ Γ′ : Context L Δ
```


Evaluation contexts

```
infix 3 □:_⊢ᴱ_

data □:_⊢ᴱ_ {L} (τ/ε : L ⨟ ∅ ⊢ᵀ COM) : L ⨟ ∅ ⊢ᵀ COM → Set where
  □ : □: τ/ε ⊢ᴱ τ/ε

  [_]>>=_ : ∀ {τ₁ τ₂ ε}
    → □: τ/ε ⊢ᴱ τ₁ / ε
    → ∅ ▷ τ₁ ⊢ τ₂ / ε
      ----------------
    → □: τ/ε ⊢ᴱ τ₂ / ε

  handle′_[_] : ∀ {σ τ ε} {ℓ : -∈ᴸ L}
    → ℓ ∉ ε
    → □: τ/ε ⊢ᴱ τ / dynamic ℓ ⦂ σ ∷ ε     -- M : handled computation
    → ∅ ⊢ σ ⇒ʰ τ / ε                 -- H : perform clause
      -------------------------------
    → □: τ/ε ⊢ᴱ τ / ε                  -- handle M with { H }

  subeffect′_[_] :
      ε ⊑ ε′
    → □: τ/ε ⊢ᴱ τ / ε
      ----------------
    → □: τ/ε ⊢ᴱ τ / ε′
```

```
private variable
  E : □: τ₁ ⊢ᴱ τ₂
  E′ : □: τ₁′ ⊢ᴱ τ₂′
  N : Γ ⊢ τ
  N′ : Γ′ ⊢ τ′
  H : Γ ⊢ σ ⇒ʰ τ/ε
  H′ : Γ′ ⊢ σ′ ⇒ʰ τ/ε′
```

```
_[_/□] : □: τ/ε ⊢ᴱ τ/ε′ → ∅ ⊢ τ/ε → ∅ ⊢ τ/ε′
□ [ M /□] = M
([ E ]>>= N) [ M /□] = (E [ M /□]) >>= N
(handle′ ℓ∉ [ E ] H) [ M /□] = handle′ ℓ∉ (E [ M /□]) H
(subeffect′ ε⊑ε′ [ E ]) [ M /□] = subeffect ε⊑ε′ (E [ M /□])
```

Evaluation context substitution under binders (used in resumptions for
the handler reduction rule, with `Γ = ∅ ▷ τ₂` (just one resumption argument)).
This technically generalizes `_[_/□]` above, but at the cost of extra
calls to `weaken₁` and `weaken₀` in its definition, which are
the identity when `Γ = ∅`, but not definitionally.

```
_[_/□]₁ : □: τ/ε ⊢ᴱ τ/ε′ → Γ ⊢ τ/ε → Γ ⊢ τ/ε′
□ [ M /□]₁ = M
([ E ]>>= N) [ M /□]₁ = (E [ M /□]₁) >>= weaken₁ N
(handle′ ℓ [ E ] H) [ M /□]₁ = handle′ ℓ (E [ M /□]₁) (weaken₀ H)
(subeffect′ ε⊑ε′ [ E ]) [ M /□]₁ = subeffect ε⊑ε′ (E [ M /□]₁)
```

```
_[_/□]ᴱ : ∀ {τ/ε′′} → □: τ/ε′ ⊢ᴱ τ/ε′′ → □: τ/ε ⊢ᴱ τ/ε′ → □: τ/ε ⊢ᴱ τ/ε′′
□ [ E′ /□]ᴱ = E′
([ E ]>>= N) [ E′ /□]ᴱ = [ E [ E′ /□]ᴱ ]>>= N
handle′ ℓ [ E ] H [ E′ /□]ᴱ = handle′ ℓ [ E [ E′ /□]ᴱ ] H
(subeffect′ ε⊑ε′ [ E ]) [ M /□]ᴱ = subeffect′ ε⊑ε′ [ E [ M /□]ᴱ ]
```

```
renᴱᴸ : (rᴸ : L →ᴸ L′) → □: τ/ε ⊢ᴱ τ/ε′ → □: ren★L rᴸ τ/ε ⊢ᴱ ren★L rᴸ τ/ε′
renᴱᴸ rᴸ □ = □
renᴱᴸ rᴸ ([ E ]>>= N) = [ renᴱᴸ rᴸ E ]>>= renᴸ rᴸ N
renᴱᴸ rᴸ (handle′ ℓ∉ [ E ] H) = handle′ (ren∉ᴸ rᴸ ℓ∉) [ renᴱᴸ rᴸ E ] (renᴸ rᴸ H)
renᴱᴸ rᴸ (subeffect′ ε⊑ε′ [ E ]) = subeffect′ (ren⊑ᴸ rᴸ ε⊑ε′) [ renᴱᴸ rᴸ E ]
```

```
infix 2 _≡ᴱ_

data _≡ᴱ_ (E : □: τ/ε₁ ⊢ᴱ τ/ε₂) : □: τ/ε₁′ ⊢ᴱ τ/ε₂′ → Set where
  refl : E ≡ᴱ E
```

```
□≡□ : τ/ε ≡ τ/ε′ → □ {τ/ε = τ/ε} ≡ᴱ □ {τ/ε = τ/ε′}
□≡□ refl = refl
```

```
⦅[_]>>=_⦆ : ∀ {E : □: τ/ε₁ ⊢ᴱ τ₂ / ε} {E′ : □: τ/ε₁′ ⊢ᴱ τ₂′ / ε′} {N : ∅ ▷ τ₂ ⊢ τ₃ / ε} {N′ : ∅ ▷ τ₂′ ⊢ τ₃′ / ε′}
  → E ≡ᴱ E′
  → N ≡ᴹ N′
  → [ E ]>>= N ≡ᴱ [ E′ ]>>= N′
⦅[ refl ]>>= refl ⦆ = refl
```

```
⦅handle′_[_]_⦆ : ∀ {ℓ ℓ′ : -∈ᴸ L} {E : □: τ/ε₁ ⊢ᴱ τ₂ / dynamic ℓ ⦂ σ ∷ ε} {E′ : □: τ/ε₁′ ⊢ᴱ τ₂′ / dynamic ℓ′ ⦂ σ′ ∷ ε′} {H : ∅ ⊢ σ ⇒ʰ τ₂ / ε} {H′ : ∅ ⊢ σ′ ⇒ʰ τ₂′ / ε′}
  → ℓ ≡ ℓ′
  → E ≡ᴱ E′
  → H ≡ᴹ H′
  → ∀ {ℓ∉ε} {ℓ′∉ε′}
  → handle′ ℓ∉ε [ E ] H ≡ᴱ handle′ ℓ′∉ε′ [ E′ ] H′
⦅handle′ refl [ refl ] refl ⦆ {ℓ∉ε} {ℓ∉ε′} with ∉-unique ℓ∉ε ℓ∉ε′
... | refl = refl
```


```
⦅subeffect′_[_]⦆ : ∀ {ε⊑ : ε ⊑ ε₁} {ε′⊑ : ε′ ⊑ ε₁′} {E : □: τ/ε₁ ⊢ᴱ τ₂ / ε} {E′ : □: τ/ε₁′ ⊢ᴱ τ₂′ / ε′}
  → ε⊑ ≡[⊑] ε′⊑
  → E ≡ᴱ E′
  → subeffect′ ε⊑ [ E ] ≡ᴱ subeffect′ ε′⊑ [ E′ ]
⦅subeffect′ refl [ refl ]⦆ = refl
```

```
renᴱᴸ-id : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} → renᴱᴸ →ᴸ-id E ≡ᴱ E
renᴱᴸ-id {E = □} = □≡□ (tra★-id _)
renᴱᴸ-id {E = [ E ]>>= N} = ⦅[ renᴱᴸ-id ]>>= renᴸ-id ⦆
renᴱᴸ-id {E = handle′ ℓ∉ε [ E ] H} = ⦅handle′ refl [ renᴱᴸ-id ] renᴸ-id ⦆
renᴱᴸ-id {E = subeffect′ ε⊑ [ E ]} = ⦅subeffect′ (tra⊑-id _) [ renᴱᴸ-id ]⦆
```

```
open ⦅_⦆_↠_
tra∈-→ᴸ⇒ᵍ-≡ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L′ →ᴸ L″} {rᴸ″ : L →ᴸ L″}
  → ∀ {τ} (x : τ ∈ Γ)
  → tra∈ (→ᴸ⇒→ᵍ (rᴸ′ ∘ᴸ rᴸ)) x ≡[∈] tra∈ (→ᴸ⇒→ᵍ rᴸ″) x
tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} {rᴸ″ = rᴸ″} {τ = τ} Z =
  ≡[∈]-Z ≡-renᵍᴸ (tra★-≡ {r = →ᴸ⇒→★ (rᴸ′ ∘ᴸ rᴸ)} {r′ = →ᴸ⇒→★ rᴸ″} (_,_ λ α → refl) τ)
tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} (S_ {τ₂ = τ₂} x) =
  ≡[∈]-S (tra★-≡ (_,_ λ α → refl) τ₂) (tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} x)
tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} ((S★ x) x₁) = ≡[∈]-S★ (tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} x)
```

```
→ᴸ⇒→ᵍ-∘ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L′ →ᴸ L″} {rᴸ″ : L →ᴸ L″}
  → _≡[↠]_ {Γ = Γ} (→ᴸ⇒→ᵍ rᴸ′ ∘[↠] →ᴸ⇒→ᵍ rᴸ) (→ᴸ⇒→ᵍ rᴸ″)
_≡[↠]_.≡[↠]⇒≡ᵍ (→ᴸ⇒→ᵍ-∘) = ≡-trans renᵍᴸ-∘ (≡-renᵍᴸ)
_≡[↠]_.≡[↠]⇒≡[↠★] (→ᴸ⇒→ᵍ-∘ {rᴸ = rᴸ} {rᴸ′ = rᴸ′}) = ≡[↠★]-trans (→ᴸ⇒→★-∘ rᴸ′ rᴸ) (→ᴸ⇒→★-≡)
_≡[↠]_.tra∈-≡ (→ᴸ⇒→ᵍ-∘ {rᴸ = rᴸ} {rᴸ′ = rᴸ′}) x =
  ≡ᴹ-trans (≡ᴹ-subst _ (Var _)) (≡ᴹ-var (≡[∈]-trans (→ᴸ⇒→ᵍ-∘-≡[∈] x) (tra∈-→ᴸ⇒ᵍ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} x)))
```

```
renᴸ-∘ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L′ →ᴸ L″} {rᴸ″ : L →ᴸ L″}
  → ∀ (M : Γ ⊢ τ)
  → renᴸ rᴸ′ (renᴸ rᴸ M) ≡ᴹ renᴸ rᴸ″ M
renᴸ-∘ M = ≡ᴹ-trans (tra-∘ M) (tra-≡ (→ᴸ⇒→ᵍ-∘) M)
```

```
renᴱᴸ-∘ᴸ : ∀ {rᴸ : L →ᴸ L′} {rᴸ′ : L′ →ᴸ L″} {rᴸ″ : L →ᴸ L″}
  → ∀ {E : □: τ ⊢ᴱ τ′}
  → renᴱᴸ rᴸ′ (renᴱᴸ rᴸ E) ≡ᴱ renᴱᴸ rᴸ″ E
renᴱᴸ-∘ᴸ {τ = τ} {E = □} = □≡□ (ren★L-∘ᴸ _ _ _ τ)
renᴱᴸ-∘ᴸ {E = [ E ]>>= N} = ⦅[ renᴱᴸ-∘ᴸ ]>>= renᴸ-∘ N ⦆
renᴱᴸ-∘ᴸ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} {rᴸ″ = rᴸ″} {E = handle′ ℓ∉ε [ E ] H} = ⦅handle′ (ren∈ᴸ-∘′ {rᴸ′ = rᴸ′} {rᴸ = rᴸ} {rᴸ″ = rᴸ″} _) [ renᴱᴸ-∘ᴸ ] renᴸ-∘ H ⦆
renᴱᴸ-∘ᴸ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} {E = subeffect′ ε⊑ [ E ]} =
  ⦅subeffect′ (≡[⊑]-trans (tra⊑-∘ _) (tra⊑-≡ (≡[↠★]-trans (→ᴸ⇒→★-∘ rᴸ′ rᴸ) →ᴸ⇒→★-≡) _)) [ renᴱᴸ-∘ᴸ ]⦆
```

```
≡ᴱ-refl : E ≡ E′ → E ≡ᴱ E′
≡ᴱ-refl refl = refl
```

```
≡ᴱ-sym : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′}
  → E ≡ᴱ E′ → E′ ≡ᴱ E
≡ᴱ-sym refl = refl
```

```
≡ᴱ-trans : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′} {E″ : □: τ/ε₁″ ⊢ᴱ τ/ε₂″}
  → E ≡ᴱ E′ → E′ ≡ᴱ E″ → E ≡ᴱ E″
≡ᴱ-trans refl refl = refl
```

```
data _∉ᴱ_ {L} (ℓ : -∈ᴸ L) {τ/ε : L ⨟ ∅ ⊢ᵀ COM} : □: τ/ε ⊢ᴱ τ/ε′ → Set where
  □ : ℓ ∉ᴱ □

  [_]>>=_ : ∀ {τ₁ τ₂ ε E}
    → ℓ ∉ᴱ E
    → (N : ∅ ▷ τ₁ ⊢ τ₂ / ε)
    → ℓ ∉ᴱ ([ E ]>>= N)

  handle′_[_] : ∀ {ℓ′ : -∈ᴸ L} {ℓ′∉ : ℓ′ ∉ ε} {E}
    → .(ℓ ≢ ℓ′)  -- For uniqueness
    → ℓ ∉ᴱ E
    → (H : ∅ ⊢ σ ⇒ʰ τ / ε)
    → ℓ ∉ᴱ (handle′ ℓ′∉ [ E ] H)

  subeffect′_[_] : ∀ {E : □: τ/ε ⊢ᴱ τ / ε}
    → (ε⊑ε′ : ε ⊑ ε′)
    → ℓ ∉ᴱ E
    → ℓ ∉ᴱ (subeffect′ ε⊑ε′ [ E ])
```

```
∉ᴱ-unique : ∀ {ℓ} {E : □: τ/ε ⊢ᴱ τ/ε′} (p q : ℓ ∉ᴱ E) → p ≡ q
∉ᴱ-unique □ □ = refl
∉ᴱ-unique ([ p ]>>= N) ([ q ]>>= .N) with ∉ᴱ-unique p q
... | refl = refl
∉ᴱ-unique (handle′ x [ p ] H) (handle′ x₁ [ q ] .H) with ∉ᴱ-unique p q
... | refl = refl
∉ᴱ-unique subeffect′ ε⊑ε′ [ p ] subeffect′ .ε⊑ε′ [ q ] with ∉ᴱ-unique p q
... | refl = refl
```

Note that when `ℓ ∉ ε`, these two cases are disjoint.
This is evidence that the semantics is deterministic.

```
split-∈ᵉ : ∀ {ℓ} {ℓ₁} {σ₁}
  → dynamic ℓ₁ ⦂ σ₁ ∈ᵉ dynamic ℓ ⦂ σ ∷ ε
  → (ℓ₁ ≡ ℓ × σ₁ ≡ σ) ⊎ (dynamic ℓ₁ ⦂ σ₁ ∈ᵉ ε)
split-∈ᵉ Z = inj₁ (refl , refl)
split-∈ᵉ (S ℓ₁∈ε) = inj₂ ℓ₁∈ε
```

```
∈ᵉ-∉-≢ : ∀ {ℓ} {ℓ₁} {σ₁} → dynamic ℓ₁ ⦂ σ₁ ∈ᵉ ε → ℓ ∉ ε → ℓ₁ ≢ ℓ
∈ᵉ-∉-≢ Z (dℓ₁≢dℓ ∷ ℓ∉ε) refl = ⊥-elim (dℓ₁≢dℓ refl)  -- eliminate irrelevant ⊥
∈ᵉ-∉-≢ (S ℓ₁∈ε) (dℓ′≢dℓ ∷ ℓ∉ε) = ∈ᵉ-∉-≢ ℓ₁∈ε ℓ∉ε
```

```
∉ᴱ-∈ᵉ : ∀ {ℓ} {σ₁} {ε₁}
  → {E : □: τ₁ / dynamic ℓ ⦂ σ₁ ∷ ε₁ ⊢ᴱ τ / ε}
  → ℓ ∉ᴱ E
  → dynamic ℓ ⦂ σ₁ ∈ᵉ ε
∉ᴱ-∈ᵉ □ = Z
∉ᴱ-∈ᵉ ([ ℓ∉E ]>>= N) = ∉ᴱ-∈ᵉ ℓ∉E
∉ᴱ-∈ᵉ (handle′ ℓ≢ℓ′ [ ℓ∉E ] H) with split-∈ᵉ (∉ᴱ-∈ᵉ ℓ∉E)
... | inj₁ (ℓ≡ℓ′ , _) = ⊥-elim (ℓ≢ℓ′ ℓ≡ℓ′)
... | inj₂ ℓ∈ε = ℓ∈ε
∉ᴱ-∈ᵉ subeffect′ ε⊑ε′ [ ℓ∉E ] = ∈ᵉ-⊑ (∉ᴱ-∈ᵉ ℓ∉E) ε⊑ε′
```

```
_[_∣_]ʰ : ∅ ⊢ σ ⇒ʰ τ/ε → ∅ ⊢ σ ⇒ᵖ τ₂ → ∅ ⊢ τ₂ ⇒ τ/ε → ∅ ⊢ τ/ε
(Λʰ_ H τ/ε≡) [ _,ᵀ_⦅_⦆ τ V σ[τ]≡ ∣ K ]ʰ = H[τ] [ V ∣ K ]ʰ
  where H[τ] = subst (∅ ⊢_) (cong₂ _⇒ʰ_ σ[τ]≡ ([]★∘weaken★∙ τ/ε≡)) (H [ τ ]ᵀ)
(ƛʰ M) [ V , W ∣ K ]ʰ = M [ V ] [ W ∣ K ]ʰ
Handler M [ ⦅⦆ ∣ K ]ʰ = M [ K ]
```

```
resume : {ℓ : -∈ᴸ L} → ℓ ∉ ε → □: τ₁ / ε′ ⊢ᴱ τ₂ / (dynamic ℓ ⦂ σ ∷ ε) → ∅ ⊢ σ ⇒ʰ τ₂ / ε → ∅ ⊢ τ₁ ⇒ τ₂ / ε
resume ℓ∉ E H = ƛ (handle′ ℓ∉ (E [ return (Var Z) /□]₁) (weaken H))
```

```
infix 2 _↦₀_ _↦₁_ _↦_

data _↦₀_ {L} : ∅ {L = L} ⊢ τ/ε → ∅ {L = L} ⊢ τ/ε → Set where
  β :
      (N : ∅ ▷ τ₁ ⊢ τ₂ / ε)
    → (V : ∅ ⊢ τ₁)
      ----------------------
    → appᵛ (ƛ N) V ↦₀ N [ V ]

  βᵀ :
      (N : ∅ ▷★ κ ⊢ τ₂)
    → (τ₁ : L ⨟ ∅ ⊢ᵀ κ)
      ---------------------
    → appᵀ (Λ N) τ₁ refl ↦₀ return (N [ τ₁ ]ᵀ)

  β->>= :
      (V : ∅ ⊢ τ₁)
    → (N : ∅ ▷ τ₁ ⊢ τ₂ / ε)
    → (return V >>= N) ↦₀ (N [ V ])
```

```
  hop : ∀ {ℓ : -∈ᴸ L} {E[perform]}
    → {ℓ∉ : ℓ ∉ ε′}
    → {E : □: τ / (dynamic ℓ ⦂ σ ∷ ε) ⊢ᴱ τ′ / (dynamic ℓ ⦂ σ ∷ ε′)}
    → ℓ ∉ᴱ E
    → (V : ∅ ⊢ _)
    → (H : _)
    → E [ perform (name ℓ) V /□] ≡ E[perform]
    → handle′ ℓ∉ E[perform] H ↦₀ H [ V ∣ resume ℓ∉ E H ]ʰ
```

```
  hret :
      {ℓ : -∈ᴸ L}
    → {ℓ∉ : ℓ ∉ ε}
    → (V : ∅ ⊢ τ₁)
    → (H : ∅ ⊢ σ ⇒ʰ τ₁ / ε)
    → handle′ ℓ∉ (return V) H ↦₀ return V

  subret :
      (ε⊑ε′ : ε ⊑ ε′)
    → (V : ∅ ⊢ τ)
    → subeffect ε⊑ε′ (return V) ↦₀ return V
```

```
↠ᴰ-new : ∅ ▷ SCO ↠ᴰ L ▷- ⨟ ∅
↠ᴰ-new = (λ{ Z → dynamic Z ; (S x) → Var x })

↠★-new : (L , ∅ ▷ SCO) ↠★ ((L ▷-) , ∅)
↠★-new = →ᴸ-S , ↠ᴰ-new

open ⦅_⦆_↠_

↠-new : ∅ {L = L} ▷★ SCO ▷ Name (Var Z) ↠ ∅
var★ ↠-new = ↠★-new
tra∈ ↠-new Z = name Z
tra∈ ↠-new (S (S★ ()) wkn≡)
```

```
new-weaken★ : weaken★ $ τ ≡ τ′ → tra★ ↠★-new τ′ ≡ weaken★L τ
new-weaken★ {τ = τ} `refl =
  tra★ ↠★-new (weaken★ τ) ≡⟨ tra★-∘ τ ⟩
  tra★ (↠★-new ∘[↠★] →★-S★ {K = K∈★}) τ ≡⟨ tra★-≡ (_,_ (λ())) τ ⟩
  weaken★L τ ∎
  where open ≡-Reasoning
```

```
new : ∅ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′
    → weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′
    → ∅ ⊢ weaken★L τ / dynamic Z ⦂ weaken★L σ ∷ weaken★L ε
new M (wτ≡ , wσ≡ , wε≡) = subst (∅ ⊢_) eq (tra ↠-new M)
  where
    eq = cong₂ (λ τ Z⦂σ∷ε → τ / Z⦂σ∷ε) (new-weaken★ wτ≡) (cong₂ (_ ⦂_∷_) (new-weaken★ wσ≡) (new-weaken★ wε≡))
```

```
fresh : Z ∉ weaken★L {Δ = ∅} ε
fresh {ε = ι} = ι
fresh {ε = dynamic ℓ ⦂ σ ∷ ε} = (λ()) ∷ fresh {ε = ε}
```

```
data _↦₁_ {L} : ∅ {L = L} ⊢ τ/ε → ∅ {L = (L ▷-)} ⊢ weaken★L τ/ε → Set where
  generate :
      (M : ∅ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / Var Z ⦂ σ′ ∷ ε′)
    → (H : ∅ ⊢ σ ⇒ʰ τ / ε)
    → {wkn≡³ : weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′}
    → handle M H wkn≡³ ↦₁
      handle′ (fresh {ε = ε}) (new M wkn≡³) (weakenᴸ H)
```

```
data _↦_ {L} {τ/ε} (M : ∅ {L = L} ⊢ τ/ε) : ∀ {L′ τ/ε′}  → ∅ {L = L′} ⊢ τ/ε′ → Set where
  step₀ : ∀ {M′} → M ↦₀ M′ → M ↦ M′
  step₁ : ∀ {M′} → M ↦₁ M′ → M ↦ M′
```

```
↦-relabel : ∀ {L L′ τ/ε τ/ε′} {M : ∅ {L = L} ⊢ τ/ε} {M′ : ∅ {L = L′} ⊢ τ/ε′}
  → M ↦ M′
  → L →ᴸ L′
↦-relabel (step₀ M↦M′) = Z
↦-relabel (step₁ M↦M′) = S Z
```

```
infix 4 _≈ᴱ_
record _≈ᴱ_ {L L′} {τ/ε₁ τ/ε₂ : L ⨟ ∅ ⊢ᵀ COM} {τ/ε₁′ τ/ε₂′ : L′ ⨟ ∅ ⊢ᵀ COM} (E : □: τ/ε₁ ⊢ᴱ τ/ε₂) (E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′) : Set where
  constructor _,_
  field
    ≈ᴱ-→ᴸ : L →ᴸ L′
    ≈ᴱ-≡ᴱ : renᴱᴸ ≈ᴱ-→ᴸ E ≡ᴱ E′

open _≈ᴱ_
```

```
≈ᴱ-refl : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} → E ≈ᴱ E
≈ᴱ-refl = record {
  ≈ᴱ-→ᴸ = →ᴸ-id ;
  ≈ᴱ-≡ᴱ = renᴱᴸ-id }
```

```
≈ᴱ-S : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} → E ≈ᴱ renᴱᴸ →ᴸ-S E
≈ᴱ-S = record {
  ≈ᴱ-→ᴸ = →ᴸ-S ;
  ≈ᴱ-≡ᴱ = refl }
```

```
infix 2 _⟶_

data _⟶_ {L L′} {τ/ε : L ⨟ ∅ ⊢ᵀ COM} {τ/ε′} : ∅ {L = L} ⊢ τ/ε → ∅ {L = L′} ⊢ τ/ε′ → Set where
  ξ : ∀ {M : ∅ ⊢ τ/ε₁} {M′}
    → (M↦M′ : M ↦ M′)
    → (E : □: τ/ε₁ ⊢ᴱ τ/ε)
    → (E′ : □: τ/ε₁′ ⊢ᴱ τ/ε′)
    → ∀ {E[M] E′[M′]}
    → E ≈ᴱ E′
    → E [ M /□] ≡ E[M]
    → E′ [ M′ /□] ≡ E′[M′]
    → E[M] ⟶ E′[M′]
```

```
data Progress (M : ∅ ⊢ τ / ε) : Set where
  step : ∀ {M′ : ∅ ⊢ τ/ε′}
    → M ⟶ M′
    → Progress M

  value :
      {V : _}
    → return V ≡ M
    → Progress M

  op : ∀ {σ₁ ε₁}
      {ℓ : _}
    → {E : □: τ₂ / (dynamic ℓ ⦂ σ₁ ∷ ε₁) ⊢ᴱ _}
    → ℓ ∉ᴱ E
    → {V : ∅ ⊢ _}
    → E [ perform (name ℓ) V /□] ≡ M
    → Progress M
```

```
^₀ : {M : ∅ {L = L} ⊢ τ/ε} {M′ : ∅ ⊢ τ/ε} → M ↦₀ M′ → M ⟶ M′
^₀ M↦₀M′ = ξ (step₀ M↦₀M′) □ □ ≈ᴱ-refl refl refl
```

```
^₁ : {M : ∅ {L = L} ⊢ τ/ε} {M′ : ∅ ⊢ weaken★L τ/ε} → M ↦₁ M′ → M ⟶ M′
^₁ M↦₁M′ = ξ (step₁ M↦₁M′) □ □ ≈ᴱ-S refl refl
```

```
renᴱᴸ-≡ : ∀ {rᴸ rᴸ′ : L →ᴸ L′} → renᴱᴸ rᴸ E ≡ᴱ renᴱᴸ rᴸ′ E
renᴱᴸ-≡ {rᴸ = rᴸ} {rᴸ′ = rᴸ′} with unique-→ᴸ rᴸ rᴸ′
... | refl = refl
```

```
renᴱ-[/□]ᴱ : ∀ {rᴸ : L →ᴸ L′}
    (E₁ : □: τ₂ ⊢ᴱ τ₁)
    {E₂ : □: τ₃ ⊢ᴱ τ₂}
  → renᴱᴸ rᴸ (E₁ [ E₂ /□]ᴱ) ≡ renᴱᴸ rᴸ E₁ [ renᴱᴸ rᴸ E₂ /□]ᴱ
renᴱ-[/□]ᴱ □ = refl
renᴱ-[/□]ᴱ ([ E ]>>= N) = cong ([_]>>= renᴸ _ N) (renᴱ-[/□]ᴱ E)
renᴱ-[/□]ᴱ (handle′ ℓ [ E ] H) = cong (λ M → handle′ _ [ M ] (renᴸ _ H)) (renᴱ-[/□]ᴱ E)
renᴱ-[/□]ᴱ (subeffect′ ε⊑ε′ [ E ]) = cong (subeffect′ (ren⊑ᴸ _ ε⊑ε′) [_]) (renᴱ-[/□]ᴱ E)
```

```
⦅_[_/□]⦆ : ∀ {rᴸ : L →ᴸ L′}
    {E₁ : □: τ₂ ⊢ᴱ τ₁} {E₂ : □: τ₃ ⊢ᴱ τ₂}
    {E₁′ : □: τ₂′ ⊢ᴱ τ₁′} {E₂′ : □: τ₃′ ⊢ᴱ τ₂′}
  → renᴱᴸ rᴸ E₁ ≡ᴱ E₁′
  → renᴱᴸ rᴸ E₂ ≡ᴱ E₂′
  → renᴱᴸ rᴸ (E₁ [ E₂ /□]ᴱ) ≡ᴱ E₁′ [ E₂′ /□]ᴱ
⦅_[_/□]⦆ {E₁ = E₁} refl refl = ≡ᴱ-refl (renᴱ-[/□]ᴱ E₁)
```

```
≈ᴱ-[/□] : ∀ {E₁ : □: τ₂ ⊢ᴱ τ₁} {E₂ : □: τ₃ ⊢ᴱ τ₂}
    {E₁′ : □: τ₂′ ⊢ᴱ τ₁′} {E₂′ : □: τ₃′ ⊢ᴱ τ₂′}
  → E₁ ≈ᴱ E₁′
  → E₂ ≈ᴱ E₂′
  → (E₁ [ E₂ /□]ᴱ) ≈ᴱ E₁′ [ E₂′ /□]ᴱ
≈ᴱ-[/□] E₁≈ E₂≈ = record {
  ≈ᴱ-→ᴸ = ≈ᴱ-→ᴸ E₁≈ ;
  ≈ᴱ-≡ᴱ = ⦅ ≈ᴱ-≡ᴱ E₁≈ [ ≡ᴱ-trans renᴱᴸ-≡ (≈ᴱ-≡ᴱ E₂≈) /□]⦆ }
```

```
≈ᴱ-trans : {E : □: τ₁ ⊢ᴱ τ₂} {E′ : □: τ₁′ ⊢ᴱ τ₂′} {E″ : □: τ₁″ ⊢ᴱ τ₂″}
  → E ≈ᴱ E′ → E′ ≈ᴱ E″ → E ≈ᴱ E″
≈ᴱ-trans (rᴸ , refl) (rᴸ′ , refl) = (rᴸ′ ∘ᴸ rᴸ , ≡ᴱ-sym renᴱᴸ-∘ᴸ)
```

```
assoc-E[E[M]] : ∀ {τ₁ : L ⨟ ∅ ⊢ᵀ COM} {τ₂ τ₃} {M}
    (E₁ : □: τ₂ ⊢ᴱ τ₁)
    {E₂ : □: τ₃ ⊢ᴱ τ₂}
  → E₁ [ E₂ /□]ᴱ [ M /□] ≡ E₁ [ E₂ [ M /□] /□]
assoc-E[E[M]] □ = refl
assoc-E[E[M]] ([ E ]>>= N) = cong (_>>= N) (assoc-E[E[M]] E)
assoc-E[E[M]] (handle′ ℓ [ E ] H) = cong (λ M → handle′ ℓ M H) (assoc-E[E[M]] E)
assoc-E[E[M]] (subeffect′ ε⊑ε′ [ E ]) = cong (subeffect ε⊑ε′) (assoc-E[E[M]] E)

assoc-E[E[M]]∙ : ∀ {τ₁ : L ⨟ ∅ ⊢ᵀ COM} {τ₂ τ₃} {M} {E₂[M]}
    {E₁ : □: τ₂ ⊢ᴱ τ₁}
    {E₂ : □: τ₃ ⊢ᴱ τ₂}
  → E₂ [ M /□] ≡ E₂[M]
  → E₁ [ E₂ /□]ᴱ [ M /□] ≡ E₁ [ E₂[M] /□]
assoc-E[E[M]]∙ {E₁ = E₁} refl = assoc-E[E[M]] E₁
```

```
⟶-cong : ∀ {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′} {M : _} {M′ : _}
  → E ≈ᴱ E′
  → M ⟶ M′
  → E [ M /□] ⟶ E′ [ M′ /□]
⟶-cong {E = E} {E′ = E′} E≈E′ (ξ N↦N′ E₁ E₁′ E₁≈E₁′ ≡M ≡M′) = ξ N↦N′ (E [ E₁ /□]ᴱ) (E′ [ E₁′ /□]ᴱ) (≈ᴱ-[/□] E≈E′ E₁≈E₁′) (assoc-E[E[M]]∙ ≡M) (assoc-E[E[M]]∙ ≡M′)
```

```
≡ᴱ-⊢≡ : {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′}
  → E ≡ᴱ E′ → τ/ε₂ ≡ τ/ε₂′
≡ᴱ-⊢≡ refl = refl
```

```
⟶-→ᴸ : {M : ∅ {L = L} ⊢ τ/ε} {M′ : ∅ {L = L′} ⊢ τ/ε′}
  → M ⟶ M′ → ∃[ rᴸ ] (ren★L rᴸ τ/ε ≡ τ/ε′)
⟶-→ᴸ (ξ _ _ _ E≈ _ _) = ≈ᴱ-→ᴸ E≈ , ≡ᴱ-⊢≡ (≈ᴱ-≡ᴱ E≈)
```

```
⟶-cong′ : ∀ {M : ∅ ⊢ τ/ε} {M′} {E : □: τ/ε ⊢ᴱ τ/ε₁}
  → M ⟶ M′
  → ∃[ τ/ε₁′ ] Σ[ E′ ∈ □: τ/ε′ ⊢ᴱ τ/ε₁′ ] (E ≈ᴱ E′) × (E [ M /□] ⟶ E′ [ M′ /□])
⟶-cong′ M⟶M′ with ⟶-→ᴸ M⟶M′
... | rᴸ , refl = _ , _ , E≈ , ⟶-cong E≈ M⟶M′
  where E≈ = rᴸ , refl
```

```
progress : (M : ∅ ⊢ τ / ε) → Progress M
progress (return x) = value refl
progress (appᵛ (ƛ N) V) = step (^₀ (β N V))
progress (appᵀ (Λ N) τ refl) = step (^₀ (βᵀ N τ))
progress (perform (name ℓ) V) = op □ refl
progress (handle M H wkn≡³) = step (^₁ (generate M H))
progress (M >>= N) with progress M
... | value {V = V} refl = step (^₀ (β->>= V N))
... | op ℓ∉E ≡M = op ([ ℓ∉E ]>>= N) (cong (_>>= N) ≡M)
... | step M⟶M′ = step (proj₂ (proj₂ (proj₂ (⟶-cong′ M⟶M′))))
```

```
progress (handle′ ℓ∉ε M H) with progress M
... | value {V = V} refl = step (^₀ (hret V H))
... | step M⟶M′ = step (proj₂ (proj₂ (proj₂ (⟶-cong′ M⟶M′))))
... | op ℓ∉E ≡M with split-∈ᵉ (∉ᴱ-∈ᵉ ℓ∉E) | ≡M
...   | inj₁ (refl , refl) | refl = step (^₀ (hop ℓ∉E _ _ refl))
...   | inj₂ ℓ₁∈ε | ≡M = op (handle′ (∈ᵉ-∉-≢ ℓ₁∈ε ℓ∉ε) [ ℓ∉E ] H) (cong (λ M → handle′ ℓ∉ε M H) ≡M)
```

```
progress (subeffect ε⊑ε′ M) with progress M
... | value {V = V} refl = step (^₀ (subret ε⊑ε′ V))
... | op ℓ∉E ≡M = op (subeffect′ ε⊑ε′ [ ℓ∉E ]) (cong (subeffect ε⊑ε′) ≡M)
... | step M⟶M′ = step (proj₂ (proj₂ (proj₂ (⟶-cong′ M⟶M′))))
```

```
module ⟶★-Reasoning where
  infix 2 _⟶★_
  infix 3 _∎
  infixr 2 _⟶⟨_⟩_

  data _⟶★_ {L} {τ/ε : L ⨟ ∅ ⊢ᵀ COM} : ∀ {L′} {τ/ε′ : L′ ⨟ ∅ ⊢ᵀ COM} → ∅ {L = L} ⊢ τ/ε → ∅ {L = L′} ⊢ τ/ε′ → Set where
    _∎ : ∀ M → M ⟶★ M
    _⟶⟨_⟩_ : ∀ (M : ∅ ⊢ τ/ε) {M′ : ∅ ⊢ τ/ε′} {M″ : ∅ ⊢ τ/ε″}
      → M ⟶ M′
      → M′ ⟶★ M″
      → M ⟶★ M″
```

```
  infixr 2 _≡⟨_⟩_
  _≡⟨_⟩_ : ∀ (M : ∅ ⊢ τ/ε) {M′ : ∅ ⊢ τ/ε′} {M″ : ∅ ⊢ τ/ε″} → M ≡ᴹ M′ → M′ ⟶★ M″ → M ⟶★ M″
  M ≡⟨ refl ⟩ M⟶M″ = M⟶M″
```

```
open ⟶★-Reasoning using (_⟶★_) public
open ⟶★-Reasoning
```

```
⟶-⟶★ : ∀ {M : ∅ ⊢ τ/ε} {M′ : ∅ ⊢ τ/ε′} → M ⟶ M′ → M ⟶★ M′
⟶-⟶★ {M = M} {M′ = M′} M⟶M′ = M ⟶⟨ M⟶M′ ⟩ M′ ∎
```

```
⟶★-cong-lemma : ∀ {τ/ε₁ : L ⨟ ∅ ⊢ᵀ COM} {τ/ε₁′ : L′ ⨟ ∅ ⊢ᵀ COM} {τ/ε₁″ : L″ ⨟ ∅ ⊢ᵀ COM}
    {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′} {E″ : □: τ/ε₁″ ⊢ᴱ τ/ε₂″}
  → E ≈ᴱ E″
  → E ≈ᴱ E′
  → L′ →ᴸ L″
  → E′ ≈ᴱ E″
⟶★-cong-lemma {E = E} (rᴸ′ , refl) (rᴸ , refl) rᴸ″ = rᴸ″ , renᴱᴸ-∘ᴸ {rᴸ = rᴸ} {E = E}
```

```
⟶★-cong-aux : ∀ {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {M : ∅ ⊢ τ/ε₁} {M′ : ∅ {L = L′} ⊢ τ/ε₁′}
  → M ⟶★ M′
  → ∃[ τ/ε₂′ ] Σ[ E′ ∈ □: τ/ε₁′ ⊢ᴱ τ/ε₂′ ] (E ≈ᴱ E′) × (E [ M /□] ⟶★ E′ [ M′ /□])
⟶★-cong-aux {E = E} (M ∎) = _ , E , ≈ᴱ-refl , (E [ M /□] ∎)
⟶★-cong-aux {E = E} (M ⟶⟨ M⟶M′ ⟩ ⟶★M″) with ⟶-cong′ M⟶M′
... | _ , E′ , E≈ , E[M]⟶E′[M′] with ⟶★-cong-aux ⟶★M″
... | _ , E″ , E′≈ , E′[M′]⟶★E″[M″] = _ , E″ , ≈ᴱ-trans E≈ E′≈ , (E [ M /□] ⟶⟨ E[M]⟶E′[M′] ⟩ E′[M′]⟶★E″[M″])
```

```
≈ᴱ-inj : ∀ {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′} {E″ : □: τ/ε₁′ ⊢ᴱ τ/ε₂″}
  → E ≈ᴱ E′ → E ≈ᴱ E″ → _≡_ {A = ∃[ τ/ε₂′ ] □: τ/ε₁′ ⊢ᴱ τ/ε₂′} (τ/ε₂′ , E′) (τ/ε₂″ , E″)
≈ᴱ-inj (rᴸ , e′) (rᴸ′ , e) with unique-→ᴸ rᴸ rᴸ′
≈ᴱ-inj (rᴸ , refl) (.rᴸ , refl) | refl = refl
```

```
⟶★-cong : ∀ {E : □: τ/ε₁ ⊢ᴱ τ/ε₂} {E′ : □: τ/ε₁′ ⊢ᴱ τ/ε₂′} {M : ∅ ⊢ τ/ε₁} {M′ : ∅ {L = L′} ⊢ τ/ε₁′}
  → E ≈ᴱ E′
  → M ⟶★ M′
  → E [ M /□] ⟶★ E′ [ M′ /□]
⟶★-cong E≈E′ M⟶★ with ⟶★-cong-aux M⟶★
... | _ , E″ , E≈E″ , E[M]⟶★ with ≈ᴱ-inj E≈E′ E≈E″
... | refl = E[M]⟶★
```

```
⦅_⟶★_⦆ : ∀ {M : ∅ ⊢ τ/ε₁} {M′ : ∅ ⊢ τ/ε₁′} {N : ∅ ⊢ τ/ε₂} {N′ : ∅ ⊢ τ/ε₂′} → M′ ≡ᴹ M → N′ ≡ᴹ N → M ⟶★ N → M′ ⟶★ N′
⦅ refl ⟶★ refl ⦆ M⟶★ = M⟶★
```

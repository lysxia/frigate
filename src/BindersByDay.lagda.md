Variant of the syntax from "Binders by day, labels by night" by Biernacki et al.
https://ii.uni.wroc.pl/~mpirog/papers/binders-labels.pdf

Differences from the paper (to enable `FCN2BBD`)
- Mappings from names to signatures appear in types, not contexts
- Added name abstraction in effect signatures
- Made `return` effect-polymorphic (also makes the operational semantics simpler).

```
module BindersByDay where

open import Common using (_$_≡_)
open import Labels

open import Function.Base using (_∘_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; cong)
```

## Types

- Kinds `κ : Kind`
- Type contexts `Δ : Contextᴷ`
- Type variables `α : κ ∈★ Δ`
- (Untyped) handler names `n : -∈ᴺ Δ`
- Types `τ : Δ ⊢ᵀ κ`, `σ : Δ ⊢ᵀ SIG`, `ε : Δ ⊢ᵀ EFF`, `τ/ε : Δ ⊢ᵀ COM`

```
infix 3 _∈★_ -∈ᴺ_ -∈ᴺ_⨟_

infixl 5 _▷_ _▷★_ _▷ᴺ-
```

Kinds: types, effects, signatures, computations.

```
data Kind : Set where
  TYP EFF SIG COM : Kind
```

Type contexts `Δ : Contextᴷ` map type variables to their kinds,
and introduce handler names.

```
data Contextᴷ : Set where
  ∅ : Contextᴷ
  _▷_ : Contextᴷ -> Kind -> Contextᴷ
  _▷ᴺ- : Contextᴷ → Contextᴷ
```

Type variables `α : κ ∈★ Δ`.

```
data _∈★_ (κ : Kind) : Contextᴷ → Set where
  Z : ∀ {Δ} → κ ∈★ Δ ▷ κ
  S_ : ∀ {Δ κ′} → κ ∈★ Δ → κ ∈★ Δ ▷ κ′
  Sᴺ_ : ∀ {Δ}   → κ ∈★ Δ → κ ∈★ Δ ▷ᴺ-
```

Handler names `n : -∈ᴺ Δ`.

```
data -∈ᴺ_ : Contextᴷ → Set where
  Z : ∀ {Δ} → -∈ᴺ Δ ▷ᴺ-
  S_ : ∀ {Δ κ} → -∈ᴺ Δ → -∈ᴺ Δ ▷ κ
  Sᴺ_ : ∀ {Δ} → -∈ᴺ Δ → -∈ᴺ Δ ▷ᴺ-
```

```
data -∈ᴺ_⨟_ (L : Labels) (Δ : Contextᴷ) : Set where
  dynamic : -∈ᴸ L → -∈ᴺ L ⨟ Δ
  static : -∈ᴺ Δ → -∈ᴺ L ⨟ Δ
```

Types `τ : Δ ⊢ᵀ κ`. (See Binders By Day paper.)

```
infixr 5 _⇒_ _⇒ʰ_ _⇒ᵖ_
infixr 9 _⦂_∷_
infix 8 _/_
infix 3 _⨟_⊢ᵀ_

-- Change: effect rows instead of arbitrary monoid expressions
-- Paper: e = var | [] | e + e | name
-- Here:  e = var | [] | name :: e
data _⨟_⊢ᵀ_ (L : Labels) (Δ : Contextᴷ) : Kind → Set where

  -- System F
  Var : ∀ {κ} → κ ∈★ Δ → L ⨟ Δ ⊢ᵀ κ
  _⇒_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ COM → L ⨟ Δ ⊢ᵀ TYP
  Forallᵀ : ∀ κ → L ⨟ Δ ▷ κ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ TYP
  Unit : L ⨟ Δ ⊢ᵀ TYP

  -- Computations
  _/_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ EFF → L ⨟ Δ ⊢ᵀ COM

  -- Effect rows
  ι : L ⨟ Δ ⊢ᵀ EFF
  _⦂_∷_ : -∈ᴺ L ⨟ Δ → L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ EFF → L ⨟ Δ ⊢ᵀ EFF

  -- Signatures
  -- Take one more argument of this type
  _⇒ˢ_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ SIG
  -- Take one more type argument of this kind
  Forallˢ : ∀ κ → L ⨟ Δ ▷ κ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ SIG
  Forallˢᴺ : L ⨟ Δ ▷ᴺ- ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ SIG
  -- Return a result of this type
  ⟨_⟩ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ SIG

  -- Name quantification
  Forallᴺ : L ⨟ Δ ▷ᴺ- ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ TYP

  -- Handler type
  _⇒ʰ_ : L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ COM → L ⨟ Δ ⊢ᵀ TYP

  -- Operation call type
  _⇒ᵖ_ : L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ TYP
```

```
private
  variable
    κ κ′ : Kind
    L L′ : Labels
    ℓ : -∈ᴸ L
    Δ Δ′ : Contextᴷ
    a a′ b : -∈ᴺ Δ
    τ τ′ τ₁ τ₂ : L ⨟ Δ ⊢ᵀ κ
    ε ε′ : L ⨟ Δ ⊢ᵀ EFF
    σ : L ⨟ Δ ⊢ᵀ SIG
    τ/ε : L ⨟ Δ ⊢ᵀ COM
```

### Renaming and substitution of types

```
infix 3 _→ᴷ_ _→ᴺ_ _→ᴰ_

_→ᴷ_ : Contextᴷ → Contextᴷ → Set
Δ →ᴷ Δ′ = ∀ {κ} → κ ∈★ Δ → κ ∈★ Δ′

_→ᴺ_ : Contextᴷ → Contextᴷ → Set
Δ →ᴺ Δ′ = -∈ᴺ Δ → -∈ᴺ Δ′

_→ᴰ_ : Contextᴷ → Contextᴷ → Set
Δ →ᴰ Δ′ = (Δ →ᴷ Δ′) × (Δ →ᴺ Δ′)

→ᴰ-id : Δ →ᴰ Δ
→ᴰ-id = (λ α → α) , (λ n → n)

→ᴰ-S : Δ →ᴰ Δ ▷ κ
→ᴰ-S = S_ , S_

→ᴰ-Sᴺ : Δ →ᴰ Δ ▷ᴺ-
→ᴰ-Sᴺ = Sᴺ_ , Sᴺ_
```

```
infix 3 _↠ᴷ_⨟_ _↠ᴺ_⨟_ _↠ᴰ_⨟_
_↠ᴷ_⨟_ : Contextᴷ → Labels → Contextᴷ → Set
Δ ↠ᴷ L′ ⨟ Δ′ = ∀ {κ} → κ ∈★ Δ → L′ ⨟ Δ′ ⊢ᵀ κ

_↠ᴺ_⨟_ : Contextᴷ → Labels → Contextᴷ → Set
Δ ↠ᴺ L′ ⨟ Δ′ = -∈ᴺ Δ → -∈ᴺ L′ ⨟ Δ′

_↠ᴰ_⨟_ : Contextᴷ → Labels → Contextᴷ → Set
Δ ↠ᴰ L′ ⨟ Δ′ = (Δ ↠ᴷ L′ ⨟ Δ′) × (Δ ↠ᴺ L′ ⨟ Δ′)

↠ᴰ-id : Δ ↠ᴰ L ⨟ Δ
↠ᴰ-id = Var , static
```

```
record Kit★ (_#_ : Labels × Contextᴷ → Kind → Set) : Set where
  no-eta-equality
  field
    from-∈★ : ∀ {κ} → κ ∈★ Δ → (L , Δ) # κ
    to-⊢★ : ∀ {κ} → (L , Δ) # κ → L ⨟ Δ ⊢ᵀ κ
    wkn★ : ∀ {κ κ′} → (L , Δ) # κ → (L , (Δ ▷ κ′)) # κ
    wkn★ᴺ : ∀ {κ} → (L , Δ) # κ → (L , (Δ ▷ᴺ-)) # κ

open Kit★
```

```
record ⦅_⦆_→★_ {_#_ : Labels × Contextᴷ → Kind → Set} (K : Kit★ _#_) (LΔ LΔ′ : Labels × Contextᴷ) : Set where
  constructor _;_;_
  field
    relabel : proj₁ LΔ →ᴸ proj₁ LΔ′
    tra∈★ : ∀ {κ} → κ ∈★ proj₂ LΔ → LΔ′ # κ
    tra∈ᴺ : -∈ᴺ proj₂ LΔ → -∈ᴺ proj₁ LΔ′ ⨟ proj₂ LΔ′

open ⦅_⦆_→★_
```

```
_∋★_ : Labels × Contextᴷ → Kind → Set
(L , Δ) ∋★ κ = κ ∈★ Δ

K∈★ : Kit★ _∋★_
K∈★ = record {
  from-∈★ = λ α → α ;
  to-⊢★ = Var ;
  wkn★ = S_ ;
  wkn★ᴺ = Sᴺ_ }
```

```
→★-id : ∀ {_#_} {K : Kit★ _#_} → ⦅ K ⦆ (L , Δ) →★ (L , Δ)
→★-id {K = K} = →ᴸ-id ; from-∈★ K ; static
```

```
→★-S★ : ∀ {_#_} {K : Kit★ _#_} → ∀ {κ} → ⦅ K ⦆ (L , Δ) →★ (L , (Δ ▷ κ))
→★-S★ {K = K} = →ᴸ-id ; (from-∈★ K ∘ S_) ; (λ n → static (S n))
```

```
→★-Sᴺ : ∀ {_#_} {K : Kit★ _#_} → ⦅ K ⦆ (L , Δ) →★ (L , (Δ ▷ᴺ-))
→★-Sᴺ {K = K} = →ᴸ-id ; (from-∈★ K ∘ Sᴺ_) ; (λ n → static (Sᴺ n))
```


```
tra∈ᶻ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → -∈ᴺ L ⨟ Δ → -∈ᴺ L′ ⨟ Δ′
tra∈ᶻ r (dynamic ℓ) = dynamic (ren∈ᴸ (relabel r) ℓ)
tra∈ᶻ r (static n) = tra∈ᴺ r n
```

```
weakenᶻ : -∈ᴺ L ⨟ Δ → -∈ᴺ L ⨟ Δ ▷ κ
weakenᶻ = tra∈ᶻ (→★-S★ {K = K∈★})
```

```
weakenᶻᴺ : -∈ᴺ L ⨟ Δ → -∈ᴺ L ⨟ Δ ▷ᴺ-
weakenᶻᴺ = tra∈ᶻ (→★-Sᴺ {K = K∈★})
```

```
tra∈★′ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → κ ∈★ Δ → L′ ⨟ Δ′ ⊢ᵀ κ
tra∈★′ {K = K} s α = to-⊢★ K (tra∈★ s α)
```

```
tra★▷ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → ⦅ K ⦆ (L , (Δ ▷ κ)) →★ (L′ , (Δ′ ▷ κ))
relabel (tra★▷ s) = relabel s
tra∈★ (tra★▷ {K = K} s) Z = from-∈★ K Z
tra∈★ (tra★▷ {K = K} s) (S α) = wkn★ K (tra∈★ s α)
tra∈ᴺ (tra★▷ s) (S n) = weakenᶻ (tra∈ᴺ s n)
```

```
tra★▷ᴺ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ⦅ K ⦆ (L , (Δ ▷ᴺ-)) →★ (L′ , (Δ′ ▷ᴺ-))
relabel (tra★▷ᴺ s) = relabel s
tra∈★ (tra★▷ᴺ {K = K} s) (Sᴺ α) = wkn★ᴺ K (tra∈★ s α)
tra∈ᴺ (tra★▷ᴺ s) Z = static Z
tra∈ᴺ (tra★▷ᴺ s) (Sᴺ n) = weakenᶻᴺ (tra∈ᴺ s n)
```

```
tra★ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
tra★ s (Var α) = tra∈★′ s α
tra★ s (τ₁ ⇒ τ₂) = tra★ s τ₁ ⇒ tra★ s τ₂
tra★ s (τ₁ ⇒ʰ τ₂) = tra★ s τ₁ ⇒ʰ tra★ s τ₂
tra★ s (τ₁ ⇒ᵖ τ₂) = tra★ s τ₁ ⇒ᵖ tra★ s τ₂
tra★ s (τ₁ ⇒ˢ τ₂) = tra★ s τ₁ ⇒ˢ tra★ s τ₂
tra★ s (τ / ε) = tra★ s τ / tra★ s ε
tra★ s Unit = Unit
tra★ s ι = ι
tra★ s (η ⦂ σ ∷ ε) = tra∈ᶻ s η ⦂ tra★ s σ ∷ tra★ s ε
tra★ s ⟨ τ ⟩ = ⟨ tra★ s τ ⟩
tra★ s (Forallᵀ κ τ) = Forallᵀ κ (tra★ (tra★▷ s) τ)
tra★ s (Forallˢ κ τ) = Forallˢ κ (tra★ (tra★▷ s) τ)
tra★ s (Forallᴺ τ) = Forallᴺ (tra★ (tra★▷ᴺ s) τ)
tra★ s (Forallˢᴺ τ) = Forallˢᴺ (tra★ (tra★▷ᴺ s) τ)
```

```
_→★_ : Labels × Contextᴷ → Labels × Contextᴷ → Set
_→★_ = ⦅ K∈★ ⦆_→★_
```

```
ren★ : (L , Δ) →★ (L′ , Δ′) → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
ren★ = tra★ {K = K∈★}
```

```
weaken★ : L ⨟ Δ ⊢ᵀ κ → L ⨟ Δ ▷ κ′ ⊢ᵀ κ
weaken★ = ren★ →★-S★

weaken★ᴺ : L ⨟ Δ ⊢ᵀ κ → L ⨟ Δ ▷ᴺ- ⊢ᵀ κ
weaken★ᴺ = ren★ →★-Sᴺ
```

```
_⊢★_ : Labels × Contextᴷ → Kind → Set
(L , Δ) ⊢★ κ = L ⨟ Δ ⊢ᵀ κ

K⊢★ : Kit★ _⊢★_
K⊢★ = record {
  from-∈★ = Var ;
  to-⊢★ = λ τ → τ ;
  wkn★ = weaken★ ;
  wkn★ᴺ = weaken★ᴺ }
```

```
_↠★_ : Labels × Contextᴷ → Labels × Contextᴷ → Set
_↠★_ = ⦅ K⊢★ ⦆_→★_
```

```
sub★ : (L , Δ) ↠★ (L′ , Δ′) → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
sub★ = tra★ {K = K⊢★}
```

```
-[_]★ᴷ : L ⨟ Δ ⊢ᵀ κ → Δ ▷ κ ↠ᴷ L ⨟ Δ
-[ τ ]★ᴷ Z = τ
-[ τ ]★ᴷ (S α) = Var α

-[_]★ᴰ : L ⨟ Δ ⊢ᵀ κ → Δ ▷ κ ↠ᴰ L ⨟ Δ
-[ τ ]★ᴰ = -[ τ ]★ᴷ , (λ{ (S n) → static n })

-[_]★ : L ⨟ Δ ⊢ᵀ κ → (L , (Δ ▷ κ)) ↠★ (L , Δ)
-[ τ ]★ = →ᴸ-id ; -[ τ ]★ᴷ ; (λ{ (S n) → static n })
```

```
-[_]★ᴺᴺ : -∈ᴺ L ⨟ Δ → Δ ▷ᴺ- ↠ᴺ L ⨟ Δ
-[ n ]★ᴺᴺ Z = n
-[ n ]★ᴺᴺ (Sᴺ α) = static α

-[_]★ᴺᴰ : -∈ᴺ L ⨟ Δ → Δ ▷ᴺ- ↠ᴰ L ⨟ Δ
-[ n ]★ᴺᴰ = (λ{ (Sᴺ α) → Var α }) , -[ n ]★ᴺᴺ

-[_]★ᴺ : -∈ᴺ L ⨟ Δ → (L , (Δ ▷ᴺ-)) ↠★ (L , Δ)
-[ n ]★ᴺ = →ᴸ-id ; (λ{ (Sᴺ α) → Var α }) ; -[ n ]★ᴺᴺ
```

```
-- Name substitution
infixr 9 _[_]★ᴺ
_[_]★ᴺ : (τ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ) (b : -∈ᴺ L ⨟ Δ) → L ⨟ Δ ⊢ᵀ κ
τ [ b ]★ᴺ = sub★ -[ b ]★ᴺ τ

-- Type substitution
infixr 9 _[_]★
_[_]★ : ∀ {Δ κ κ′} (τ : L ⨟ Δ ▷ κ′ ⊢ᵀ κ) (τ′ : L ⨟ Δ ⊢ᵀ κ′) → L ⨟ Δ ⊢ᵀ κ
τ [ σ ]★ = sub★ -[ σ ]★ τ

-- Generate a fresh label in place of a free name
newᵀ : L ⨟ Δ ▷ᴺ- ⊢ᵀ κ → L ▷- ⨟ Δ ⊢ᵀ κ
newᵀ = sub★ (→ᴸ-S ; (λ{ (Sᴺ α) → Var α }) ; -[ dynamic Z ]★ᴺᴺ)
```

### Subtyping

```
infix 3 _⦂_∈ᵉ_

data _⦂_∈ᵉ_ (η : _) (σ : L ⨟ Δ ⊢ᵀ SIG) : L ⨟ Δ ⊢ᵀ EFF → Set where
  Z : η ⦂ σ ∈ᵉ η ⦂ σ ∷ ε

  S_ : {η′ : _} {σ′ : _}
    → η ⦂ σ ∈ᵉ ε
      --------------------
    → η ⦂ σ ∈ᵉ η′ ⦂ σ′ ∷ ε
```

```
infix 3 _⊑_

data _⊑_ {Δ} : (_ _ : L ⨟ Δ ⊢ᵀ EFF) → Set where
  ι : ι ⊑ ε′

  _∷_ : {η : _}
    → η ⦂ σ ∈ᵉ ε′
    → ε ⊑ ε′
      ---------------
    → η ⦂ σ ∷ ε ⊑ ε′
```

```
∈ᵉ-⊑ : {η : _} → η ⦂ σ ∈ᵉ ε → ε ⊑ ε′ → η ⦂ σ ∈ᵉ ε′
∈ᵉ-⊑ Z (η∈ε′ ∷ ε⊑ε′) = η∈ε′
∈ᵉ-⊑ (S η∈ε) (η′∈ε′ ∷ ε⊑ε′) = ∈ᵉ-⊑ η∈ε ε⊑ε′
```

`ℓ ∉ ε`: the label `ℓ` does not belong to the effect row `ε`.
If `ε = η₁ ⦂ σ₁ ∷ ... ηₙ ⦂ σₙ ∷ ι`, then `ℓ ∉ ε` means that
none of the scopes `ηᵢ` is equal to `dynamic ℓ`.

```
data _∉_ {L : Labels} {Δ : Contextᴷ} (ℓ : -∈ᴸ L) : L ⨟ Δ ⊢ᵀ EFF → Set where
  ι : ℓ ∉ ι

  _∷_ : {ℓ′ : -∈ᴸ L} {σ : L ⨟ Δ ⊢ᵀ SIG} {ε : L ⨟ Δ ⊢ᵀ EFF}
    → .(ℓ′ ≢ ℓ)
    → ℓ ∉ ε
      ----------------
    → ℓ ∉ (dynamic ℓ′ ⦂ σ ∷ ε)
```

```
∉-unique : (p q : ℓ ∉ ε) → p ≡ q
∉-unique ι ι = refl
∉-unique (ℓ≢ ∷ p) (ℓ≢′ ∷ q) = cong (ℓ≢ ∷_) (∉-unique p q)
```

## Terms

- Contexts `Γ : Context Δ`
- Variables `x : τ ∈ Γ`
- Terms:
    + Values `V : Γ ⊢ τ`
    + Computations `M : Γ ⊢ τ / ε`
    + Handlers `H : Γ ⊢ σ ⇒ʰ τ / ε`

```
data Context L : (Δ : Contextᴷ) → Set where
  _▷_ : ∀ {Δ} → Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Context L Δ
  _▷ᴺ- : ∀ {Δ} → Context L Δ → Context L (Δ ▷ᴺ-)
  _▷★_ : ∀ {Δ} → Context L Δ → (κ : Kind) → Context L (Δ ▷ κ)
  ∅ : Context L ∅
```

```
private variable
  Γ Γ′ : Context L Δ
```

Variables may be:
- term variables, with types `τ` of kind `TYP` (bound by `ƛ_`)
- handler names, with signatures `σ` of kind `SIG` (bound by handlers and `Λᴺ_`)

```
infix 3 _∈_

data _∈_ : ∀ {Δ} (τ : L ⨟ Δ ⊢ᵀ TYP) (Γ : Context L Δ) → Set where
  Z : ∀ {Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} {Γ} → τ ∈ Γ ▷ τ
  S_ : ∀ {Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} {τ′ : L ⨟ Δ ⊢ᵀ TYP} {Γ} → τ ∈ Γ → τ ∈ Γ ▷ τ′
  Sᴺ_ : ∀ {Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} {τ₀} {Γ} → τ ∈ Γ → weaken★ᴺ $ τ ≡ τ₀ → τ₀ ∈ Γ ▷ᴺ-
  S★_ : ∀ {Δ} {τ : L ⨟ Δ ⊢ᵀ TYP} {τ₀} {Γ} → τ ∈ Γ → weaken★ $ τ ≡ τ₀ → τ₀ ∈ Γ ▷★ κ
```

```
infix 3 _⊢_
```

Signature instantiation: a signature `σ` is of the shape `∀ x₁ ... xₙ , τ₁ ⇒ˢ τ₂`
(an arrow under a bunch of quantifiers).
We say that `σ` instantiates to `τ₁′ ⇒ˢ τ₂′`
if `τ₁′` and `τ₂′` are substitution instances of `τ₁` and `τ₂`.

Terms are values or computations. This is a fine-grained CBV calculus.
Values and computations are distinguished by the kind of the type to the right of the turnstile
(values have type `Γ ⊢ τ`, where `τ : L ⨟ Δ ⊢ᵀ TYP`;
computations have type `Γ ⊢ τ / ε`, where `τ / ε : L ⨟ Δ ⊢ᵀ COM`).

```
data _⊢_ {L : Labels} {Δ} (Γ : Context L Δ) : ∀ {κ} (τ : L ⨟ Δ ⊢ᵀ κ) → Set where
```

Values `V : Γ ⊢ᵛ τ`.

```
  -- Variables
  Var : ∀ {τ : _ ⨟ _ ⊢ᵀ TYP}
    → τ ∈ Γ
      ------
    → Γ ⊢ τ

  -- Functions
  ƛ_ : ∀ {τ₁ τ₂/ε}
    → Γ ▷ τ₁ ⊢ τ₂/ε
      ------------
    → Γ ⊢ τ₁ ⇒ τ₂/ε

  -- Name abstractions
  ƛᴺ_ : ∀ {τ}
    → Γ ▷ᴺ- ⊢ τ
      --------------
    → Γ ⊢ Forallᴺ τ

  -- Type abstractions
  Λ_ : ∀ {κ τ}
    → Γ ▷★ κ ⊢ τ
      ----------------
    → Γ ⊢ Forallᵀ κ τ

  -- Unit
  tt : Γ ⊢ Unit

  -- Operation arguments
  -- End of arguments
  ⦅⦆ : Γ ⊢ ⟨ τ ⟩ ⇒ᵖ τ

  -- Value argument
  _,_ : Γ ⊢ τ₁ → Γ ⊢ τ₂ ⇒ᵖ τ′ → Γ ⊢ (τ₁ ⇒ˢ τ₂) ⇒ᵖ τ′

  -- Type arguments
  _,ᵀ_⦅_⦆ : ∀ {κ σ σ′ σ[τ]} → (τ : L ⨟ Δ ⊢ᵀ κ) → Γ ⊢ σ[τ] ⇒ᵖ σ′ → σ [ τ ]★ ≡ σ[τ] → Γ ⊢ Forallˢ κ σ ⇒ᵖ σ′

  -- Name arguments
  _,ᴺ_⦅_⦆ : ∀ {σ σ′ σ[a]} → (a : -∈ᴺ L ⨟ Δ) → Γ ⊢ σ[a] ⇒ᵖ σ′ → σ [ a ]★ᴺ ≡ σ[a] → Γ ⊢ Forallˢᴺ σ ⇒ᵖ σ′
```

Handlers `H : Γ ⊢ σ ⇒ʰ τ / ε` are values. For simplicity, they consist of just one handler clause
and no return clause (redundant for lexically scoped handlers (or if you have masking like Koka)).

```
  Λʰ_ : ∀ {κ τ τ/ε weaken-τ/ε}
    → Γ ▷★ κ ⊢ τ ⇒ʰ weaken-τ/ε
    → weaken★ $ τ/ε ≡ weaken-τ/ε
      -------------------------
    → Γ ⊢ Forallˢ κ τ ⇒ʰ τ/ε

  ƛʰᴺ_ : ∀ {τ τ/ε weaken-τ/ε}
    → Γ ▷ᴺ- ⊢ τ ⇒ʰ weaken-τ/ε
    → weaken★ᴺ $ τ/ε ≡ weaken-τ/ε
      ------------------------
    → Γ ⊢ Forallˢᴺ τ ⇒ʰ τ/ε

  ƛʰ_ :
      Γ ▷ τ₁ ⊢ τ₂ ⇒ʰ τ/ε
      -----------------------
    → Γ ⊢ (τ₁ ⇒ˢ τ₂) ⇒ʰ τ/ε

  Handler :
      Γ ▷ (τ₁ ⇒ τ/ε) ⊢ τ/ε
      ------------------
    → Γ ⊢ ⟨ τ₁ ⟩ ⇒ʰ τ/ε
```

Computations `M : Γ ⊢ τ / ε`.

```
  return : ∀ {τ ε}
    → Γ ⊢ τ      -- V
      -----------
    → Γ ⊢ τ / ε  -- return V

  _>>=_ : ∀ {τ₁ τ₂ ε}
    → Γ ⊢ τ₁ / ε       -- M
    → Γ ▷ τ₁ ⊢ τ₂ / ε  -- N
      -------------
    → Γ ⊢ τ₂ / ε       -- let X = M in N

  appᵛ : ∀ {τ₁ τ/ε}
    → Γ ⊢ τ₁ ⇒ τ/ε  -- M
    → Γ ⊢ τ₁       -- N
      -------------- value application
    → Γ ⊢ τ/ε       -- M N

  appᴺ : ∀ {σ σ[b]}
    → Γ ⊢ Forallᴺ σ             -- M
    → (b : -∈ᴺ L ⨟ Δ)                -- b
    → σ [ b ]★ᴺ ≡ σ[b]
      -------------------------- name application
    → Γ ⊢ σ[b] / ι          -- M b

  appᵀ : ∀ {κ σ σ[τ]}
    → Γ ⊢ Forallᵀ κ σ    -- M
    → (τ : L ⨟ _ ⊢ᵀ κ)   -- τ
    → σ [ τ ]★ ≡ σ[τ]
      ------------------- type application
    → Γ ⊢ σ[τ] / ι  -- M σ

  perform : ∀ {σ τ}
    → (a : -∈ᴺ L ⨟ Δ)                      -- a : operation name with signature σ
    → Γ ⊢ σ ⇒ᵖ τ
      --------------------------------
    → Γ ⊢ τ / (a ⦂ σ ∷ ε)        -- perform a M

  handle : ∀ {τ σ ε τ′ σ′ ε′}
    → Γ ▷ᴺ- ⊢ τ′ / static Z ⦂ σ′ ∷ ε′  -- M : handled computation
    → Γ ⊢ σ ⇒ʰ τ / ε                    -- H : perform clause
    → weaken★ᴺ $ τ ≡ τ′ × weaken★ᴺ $ σ ≡ σ′ × weaken★ᴺ $ ε ≡ ε′
      -------------------------------
    → Γ ⊢ τ / ε                       -- handle M with { H }

  handle′ : ∀ {σ τ ε}
    → {ℓ : -∈ᴸ L}
    → ℓ ∉ ε
    → Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε          -- M : handled computation
    → Γ ⊢ σ ⇒ʰ τ / ε                 -- H : perform clause
      -------------------------------
    → Γ ⊢ τ / ε                       -- handle M with { H }

  subeffect : ∀ {τ ε ε′}
    → ε ⊑ ε′
    → Γ ⊢ τ / ε
      -----------
    → Γ ⊢ τ / ε′
```

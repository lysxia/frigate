Syntax from "First-class names for effect handlers" by Xie eet al.
https://www.microsoft.com/en-us/research/uploads/prod/2021/05/namedh-tr.pdf

```
module FirstClassNames where

open import Common using (_$_≡_)
open import Labels

open import Function.Base using (_∘_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; cong)
```

## Types

- Kinds `κ : Kind`
- Type contexts `Δ : Contextᴷ`
- Type variables `α : κ ∈★ Δ`
- Types `τ : Type Τ κ`

Kinds: types, computations, signatures, effect rows, scopes (≈ handler names).

```
data Kind : Set where
  TYP COM SIG EFF SCO : Kind
```

Type contexts `Δ : Contextᴷ`

```
infixl 5 _▷_ _▷★_

data Contextᴷ : Set where
  ∅ : Contextᴷ
  _▷_ : Contextᴷ -> Kind -> Contextᴷ
```

Type variables `α : κ ∈★ Δ`

```
infix 3 _∈★_

data _∈★_ (κ : Kind) : Contextᴷ → Set where
  Z : ∀ {Δ} → κ ∈★ (Δ ▷ κ)
  S_ : ∀ {Δ κ′} → κ ∈★ Δ → κ ∈★ (Δ ▷ κ′)
```

Types `τ : Type Τ κ`

In the paper, there is a notion of labels `ℓ`,
with a global map `Σ` from labels `ℓ` to signatures `σ`.
We simplify by replacing labels with signatures, and by removing `Σ`.
- In rows, we replace labels with signatures.
  In the paper, appending a label to a row is denoted: `< ℓ^η | ε >`
  here we write `η ⦂ σ ∷ ε` instead.
- The type `ev ℓ^η` in the paper is written `Name η` here (type of scoped handler names).
  The `ℓ` was redundant with the one in the rows.

```
infixr 8 _/_
infixr 5 _⇒_ _⇒ʰ_ _⇒ᵖ_
infixr 9 _⦂_∷_
infix 3 _⨟_⊢ᵀ_

data _⨟_⊢ᵀ_ (L : Labels) (Δ : Contextᴷ) : Kind → Set where
  -- System F
  Var : ∀ {κ} → κ ∈★ Δ → L ⨟ Δ ⊢ᵀ κ
  _⇒_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ COM → L ⨟ Δ ⊢ᵀ TYP
  Forallᵀ : ∀ κ → L ⨟ (Δ ▷ κ) ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ TYP
  Unit : L ⨟ Δ ⊢ᵀ TYP

  -- Computations
  _/_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ EFF → L ⨟ Δ ⊢ᵀ COM

  -- Effect rows (nil and cons)
  ι : L ⨟ Δ ⊢ᵀ EFF
  _⦂_∷_ : L ⨟ Δ ⊢ᵀ SCO → L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ EFF → L ⨟ Δ ⊢ᵀ EFF

  -- Signatures (types of operations)
  -- Take one more argument of this type
  _⇒ˢ_ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ SIG
  -- Take one more type argument of this kind
  Forallˢ : ∀ κ → L ⨟ (Δ ▷ κ) ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ SIG
  -- Return a result of this type
  ⟨_⟩ : L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ SIG

  -- Handler type
  _⇒ʰ_ : L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ COM → L ⨟ Δ ⊢ᵀ TYP

  -- Operation call type
  _⇒ᵖ_ : L ⨟ Δ ⊢ᵀ SIG → L ⨟ Δ ⊢ᵀ TYP → L ⨟ Δ ⊢ᵀ TYP

  -- Type of scoped names.
  -- The body of a handler will basically be a function of type
  -- `∀ (η : SCO) → Name η → ...`
  Name : L ⨟ Δ ⊢ᵀ SCO → L ⨟ Δ ⊢ᵀ TYP

  -- Run-time scope
  dynamic : -∈ᴸ L → L ⨟ Δ ⊢ᵀ SCO
```

NB. A type of kind `SCO` is either a `Var` (bound handler name) or `dynamic` (run-time scope label)

The following `variable` declaration assigns types to Agda variables to spare
us from quantifying over them explicitly, as those quantifiers can get unwieldly.
When a variable appears free in a type signature, Agda will automatically
insert a universal quantifier for it at the front, as well as any variables
occurring in its type.
By default, `variable` declarations are publicly exported by the enclosing
module. We make this declaration `private` instead to avoid collisions with
`BindersByDay`, which uses the same variables.

```
private variable
  L L′ : Labels
  ℓ : -∈ᴸ L
  Δ Δ′ : Contextᴷ
  κ κ′ : Kind
  τ τ₁ τ₂ τ′ τ₁′ τ₂′ σ σ′ : L ⨟ Δ ⊢ᵀ κ
  τ/ε τ/ε′ τ₂/ε : L ⨟ Δ ⊢ᵀ COM
  ε ε′ ε″ : L ⨟ Δ ⊢ᵀ EFF
  η η′ : SCO ∈★ Δ
```

### Renaming and substitution

```
infix 3 _→ᴰ_

_→ᴰ_ : Contextᴷ → Contextᴷ → Set
Δ →ᴰ Δ′ = ∀ {κ} → κ ∈★ Δ → κ ∈★ Δ′

→ᴰ-id : Δ →ᴰ Δ
→ᴰ-id = λ α → α

→ᴰ-S : Δ →ᴰ Δ ▷ κ
→ᴰ-S = S_
```

```
infix 3 _↠ᴰ_⨟_
_↠ᴰ_⨟_ : Contextᴷ → Labels → Contextᴷ → Set
Δ ↠ᴰ L′ ⨟ Δ′ = ∀ {κ} → κ ∈★ Δ → L′ ⨟ Δ′ ⊢ᵀ κ

↠ᴰ-id : Δ ↠ᴰ L ⨟ Δ
↠ᴰ-id = Var
```

```
record Kit★ (_#_ : Labels × Contextᴷ → Kind → Set) : Set where
  no-eta-equality
  field
    from-∈★ : ∀ {κ} → κ ∈★ Δ → (L , Δ) # κ
    to-⊢★ : ∀ {κ} → (L , Δ) # κ → L ⨟ Δ ⊢ᵀ κ
    wkn★ : ∀ {κ κ′} → (L , Δ) # κ → (L , (Δ ▷ κ′)) # κ

open Kit★
```

    tra★ᴸ : (L , Δ) →★ (L′ , Δ′) → L →ᴸ L′
    tra★-var : (L , Δ) →★ (L′ , Δ′) → Δ ↠ᴰ L′ ⨟ Δ′
    tra★▷ : (L , Δ) →★ (L′ , Δ′) → ∀ {κ} → (L , (Δ ▷ κ)) →★ (L′ , (Δ′ ▷ κ))

```
record ⦅_⦆_→★_ {_#_ : Labels × Contextᴷ → Kind → Set} (K : Kit★ _#_) (LΔ LΔ′ : Labels × Contextᴷ) : Set where
  constructor _,_
  field
    relabel : proj₁ LΔ →ᴸ proj₁ LΔ′
    tra∈★ : ∀ {κ} → κ ∈★ proj₂ LΔ → LΔ′ # κ

open ⦅_⦆_→★_
```

```
tra∈★′ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → κ ∈★ Δ → L′ ⨟ Δ′ ⊢ᵀ κ
tra∈★′ {K = K} s α = to-⊢★ K (tra∈★ s α)
```

```
tra★▷ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → ⦅ K ⦆ (L , (Δ ▷ κ)) →★ (L′ , (Δ′ ▷ κ))
relabel (tra★▷ s) = relabel s
tra∈★ (tra★▷ {K = K} s) Z = from-∈★ K Z
tra∈★ (tra★▷ {K = K} s) (S α) = wkn★ K (tra∈★ s α)

tra★ : {_#_ : _} {K : Kit★ _#_}
  → ⦅ K ⦆ (L , Δ) →★ (L′ , Δ′)
  → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
tra★ s (Var α) = tra∈★′ s α
tra★ s (τ₁ ⇒ τ₂) = tra★ s τ₁ ⇒ tra★ s τ₂
tra★ s (τ₁ ⇒ʰ τ₂) = tra★ s τ₁ ⇒ʰ tra★ s τ₂
tra★ s (τ₁ ⇒ᵖ τ₂) = tra★ s τ₁ ⇒ᵖ tra★ s τ₂
tra★ s (τ₁ ⇒ˢ τ₂) = tra★ s τ₁ ⇒ˢ tra★ s τ₂
tra★ s (τ / ε) = tra★ s τ / tra★ s ε
tra★ s Unit = Unit
tra★ s ι = ι
tra★ s (η ⦂ σ ∷ ε) = tra★ s η ⦂ tra★ s σ ∷ tra★ s ε
tra★ s ⟨ τ ⟩ = ⟨ tra★ s τ ⟩
tra★ s (Forallᵀ κ τ) = Forallᵀ κ (tra★ (tra★▷ s) τ)
tra★ s (Forallˢ κ τ) = Forallˢ κ (tra★ (tra★▷ s) τ)
tra★ s (Name η) = Name (tra★ s η)
tra★ s (dynamic ℓ) = dynamic (ren∈ᴸ (relabel s) ℓ)
```

```
_∋★_ : Labels × Contextᴷ → Kind → Set
(L , Δ) ∋★ κ = κ ∈★ Δ

K∈★ : Kit★ _∋★_
K∈★ = record {
  from-∈★ = λ α → α ;
  to-⊢★ = Var ;
  wkn★ = S_ }
```

```
_→★_ : Labels × Contextᴷ → Labels × Contextᴷ → Set
_→★_ = ⦅ K∈★ ⦆_→★_
```

```
ren★ : (L , Δ) →★ (L′ , Δ′) → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
ren★ = tra★ {K = K∈★}
```

```
→★-id : ∀ {_#_} {K : Kit★ _#_} → ⦅ K ⦆ (L , Δ) →★ (L , Δ)
→★-id {K = K} = →ᴸ-id , from-∈★ K
```

```
→★-S★ : ∀ {_#_} {K : Kit★ _#_} → ∀ {κ} → ⦅ K ⦆ (L , Δ) →★ (L , Δ ▷ κ)
→★-S★ {K = K} = →ᴸ-id , (from-∈★ K ∘ S_)
```

One-variable weakening.

```
weaken★ : L ⨟ Δ ⊢ᵀ κ → L ⨟ (Δ ▷ κ′) ⊢ᵀ κ
weaken★ = ren★ →★-S★
```

Substitution

```
_⊢★_ : Labels × Contextᴷ → Kind → Set
(L , Δ) ⊢★ κ = L ⨟ Δ ⊢ᵀ κ

K⊢★ : Kit★ _⊢★_
K⊢★ = record {
  from-∈★ = Var ;
  to-⊢★ = λ τ → τ ;
  wkn★ = weaken★ }
```

```
_↠★_ : Labels × Contextᴷ → Labels × Contextᴷ → Set
_↠★_ = ⦅ K⊢★ ⦆_→★_
```

```
sub★ : (L , Δ) ↠★ (L′ , Δ′) → ∀ {κ} → L ⨟ Δ ⊢ᵀ κ → L′ ⨟ Δ′ ⊢ᵀ κ
sub★ = tra★ {K = K⊢★}
```

One-variable substitution.

```
-[_]★ᴰ : L ⨟ Δ ⊢ᵀ κ → Δ ▷ κ ↠ᴰ L ⨟ Δ
-[ σ ]★ᴰ Z = σ
-[ σ ]★ᴰ (S x) = Var x
```

```
-[_]★ : L ⨟ Δ ⊢ᵀ κ → (L , Δ ▷ κ) ↠★ (L , Δ)
-[ σ ]★ = →ᴸ-id , -[ σ ]★ᴰ
```

```
_[_]★ : L ⨟ (Δ ▷ κ′) ⊢ᵀ κ → L ⨟ Δ ⊢ᵀ κ′ → L ⨟ Δ ⊢ᵀ κ
τ [ σ ]★ = sub★ -[ σ ]★ τ
```

### Subtyping

```
infix 3 _⦂_∈ᵉ_

data _⦂_∈ᵉ_ (η : _) (σ : L ⨟ Δ ⊢ᵀ SIG) : L ⨟ Δ ⊢ᵀ EFF → Set where
  Z : η ⦂ σ ∈ᵉ η ⦂ σ ∷ ε

  S_ : {η′ : _}
    → η ⦂ σ ∈ᵉ ε
      --------------------
    → η ⦂ σ ∈ᵉ η′ ⦂ σ′ ∷ ε
```

```
infix 3 _⊑_

data _⊑_ {Δ} : (_ _ : L ⨟ Δ ⊢ᵀ EFF) → Set where
  ι : ι ⊑ ε′

  _∷_ : {η : _}
    → η ⦂ σ ∈ᵉ ε′
    → ε ⊑ ε′
      ---------------
    → η ⦂ σ ∷ ε ⊑ ε′
```

```
∈ᵉ-⊑ : {η : _} → η ⦂ σ ∈ᵉ ε → ε ⊑ ε′ → η ⦂ σ ∈ᵉ ε′
∈ᵉ-⊑ Z (η∈ε′ ∷ ε⊑ε′) = η∈ε′
∈ᵉ-⊑ (S η∈ε) (η′∈ε′ ∷ ε⊑ε′) = ∈ᵉ-⊑ η∈ε ε⊑ε′
```

`ℓ ∉ ε`: the label `ℓ` does not belong to the effect row `ε`.
If `ε = η₁ ⦂ σ₁ ∷ ... ηₙ ⦂ σₙ ∷ ι`, then `ℓ ∉ ε` means that
none of the scopes `ηᵢ` is equal to `dynamic ℓ`.

```
data _∉_ {L : Labels} {Δ : Contextᴷ} (ℓ : -∈ᴸ L) : L ⨟ Δ ⊢ᵀ EFF → Set where
  ι : ℓ ∉ ι

  _∷_ : {ℓ′ : -∈ᴸ L} {σ : L ⨟ Δ ⊢ᵀ SIG} {ε : L ⨟ Δ ⊢ᵀ EFF}
    → .(ℓ′ ≢ ℓ)
    → ℓ ∉ ε
      ----------------
    → ℓ ∉ (dynamic ℓ′ ⦂ σ ∷ ε)
```

```
∉-unique : (p q : ℓ ∉ ε) → p ≡ q
∉-unique ι ι = refl
∉-unique (η≢ ∷ p) (η≢′ ∷ q) = cong (η≢ ∷_) (∉-unique p q)
```

## Terms

- Contexts `Γ : Context Δ`
- Variables `x : τ ∈ Γ`
- Terms
    + Values `V : Γ ⊢ τ`
    + Computations `M : Γ ⊢ τ / ε`
    + Handlers `H : Γ ⊢ σ ⟹ τ / ε`

```
data Context (L : Labels) : Contextᴷ → Set where
  ∅ : Context L ∅
  _▷_ : Context L Δ → L ⨟ Δ ⊢ᵀ TYP → Context L Δ
  _▷★_ : Context L Δ → (κ : Kind) → Context L (Δ ▷ κ)
```

```
private variable
  Γ : Context L Δ
```

Variables `x : τ ∈ Γ`

```
infix 3 _∈_

data _∈_ : L ⨟ Δ ⊢ᵀ TYP → Context L Δ → Set where
  Z : τ ∈ Γ ▷ τ
  S_ : τ ∈ Γ → τ ∈ Γ ▷ τ₂
  S★_ : ∀ {weaken★[τ]} → τ ∈ Γ → weaken★ $ τ ≡ weaken★[τ] → weaken★[τ] ∈ Γ ▷★ κ
```

### Terms

```
infix 3 _⊢_
data _⊢_ {L : Labels} {Δ : Contextᴷ} (Γ : Context L Δ) : L ⨟ Δ ⊢ᵀ κ → Set
```

Values

```
data _⊢_ {L} {Δ} Γ where
  -- Variables
  Var :
      τ ∈ Γ
      ------
    → Γ ⊢ τ

  -- Functions
  ƛ_ :
      Γ ▷ τ₁ ⊢ τ₂/ε
      ---------------
    → Γ ⊢ τ₁ ⇒ τ₂/ε

  -- Type abstractions
  Λ_ :
      Γ ▷★ κ ⊢ τ
      ----------------
    → Γ ⊢ Forallᵀ κ τ

  -- Unit
  tt : Γ ⊢ Unit

  name :
      (ℓ : -∈ᴸ L)
    → Γ ⊢ Name (dynamic ℓ)
```

Signature instantiation: a signature `σ` is of the shape `∀ x₁ ... xₙ , τ₁ ⇒ˢ τ₂`
(an arrow under a bunch of quantifiers).
We say that `σ` instantiates to `τ₁′ ⇒ˢ τ₂′`
if `τ₁′` and `τ₂′` are substitution instances of `τ₁` and `τ₂`.

```
  ⦅⦆ : Γ ⊢ ⟨ τ ⟩ ⇒ᵖ τ
  _,_ : Γ ⊢ τ₁ → Γ ⊢ τ₂ ⇒ᵖ τ′ → Γ ⊢ (τ₁ ⇒ˢ τ₂) ⇒ᵖ τ′
  _,ᵀ_⦅_⦆ : ∀ {κ σ σ′ σ[τ]} τ → Γ ⊢ σ[τ] ⇒ᵖ σ′ → σ [ τ ]★ ≡ σ[τ] → Γ ⊢ Forallˢ κ σ ⇒ᵖ σ′
```

Computations

```
  return :
      Γ ⊢ τ
      ---------
    → Γ ⊢ τ / ε

  _>>=_ :
      Γ ⊢ τ / ε
    → Γ ▷ τ ⊢ τ₂ / ε
      --------------
    → Γ ⊢ τ₂ / ε

  appᵛ :
    Γ ⊢ τ ⇒ τ₂ →
    Γ ⊢ τ →
    ----------
    Γ ⊢ τ₂

  appᵀ : ∀ {σ[τ]}
    → Γ ⊢ Forallᵀ κ σ
    → (τ : L ⨟ Δ ⊢ᵀ κ)
    → σ [ τ ]★ ≡ σ[τ]
      ------------------
    → Γ ⊢ σ[τ] / ι

  perform : {η : _}
    → Γ ⊢ Name η
    → Γ ⊢ σ ⇒ᵖ τ
      --------------------
    → Γ ⊢ τ / (η ⦂ σ ∷ ε)

  -- The handled computation is really a polymorphic function:
  --
  -- (Λ η → ƛ n → M) : ∀ (η : SCO) → Name η → τ / (η ⦂ σ ∷ ε)
  -- H : σ ⟹ τ / ε
  -- --------------------------------------------------------
  -- handle(η,n) M H : τ / ε
  handle : ∀ {τ τ′ σ σ′ ε ε′}
    → Γ ▷★ SCO ▷ Name (Var Z) ⊢ τ′ / (Var Z ⦂ σ′ ∷ ε′)
    → Γ ⊢ σ ⇒ʰ τ / ε
    → weaken★ $ τ ≡ τ′ × weaken★ $ σ ≡ σ′ × weaken★ $ ε ≡ ε′
      ----------
    → Γ ⊢ τ / ε

  handle′ :
      {ℓ : -∈ᴸ L}
    → ℓ ∉ ε
    → Γ ⊢ τ / dynamic ℓ ⦂ σ ∷ ε
    → Γ ⊢ σ ⇒ʰ τ / ε
      --------------
    → Γ ⊢ τ / ε

  subeffect :
      ε ⊑ ε′
    → Γ ⊢ τ / ε
      ----------
    → Γ ⊢ τ / ε′
```

Handlers (BindersByDay style: no return clause, single polymorphic operation clause)

```
  Λʰ_ : ∀ {τ/ε′}
    → Γ ▷★ κ ⊢ σ ⇒ʰ τ/ε′
    → weaken★ $ τ/ε ≡ τ/ε′
      ------------------------
    → Γ ⊢ Forallˢ κ σ ⇒ʰ τ/ε

  ƛʰ_ :
      Γ ▷ τ₁ ⊢ τ₂ ⇒ʰ τ/ε
      -----------------------
    → Γ ⊢ (τ₁ ⇒ˢ τ₂) ⇒ʰ τ/ε

  Handler :
      Γ ▷ (τ₁ ⇒ τ/ε) ⊢ τ/ε
      ------------------
    → Γ ⊢ ⟨ τ₁ ⟩ ⇒ʰ τ/ε
```

## Example

```
-- Signature for State:
--
-- traditional GADT style:
--   data State s r where
--     Get : unit -> State s s
--     Put : s -> State s unit
--
-- Church encoding (expressible in System F):
--   forall r. (s -> r) + (s * (unit -> r)) -> r
```

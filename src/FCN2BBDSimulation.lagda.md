```
module FCN2BBDSimulation where

open import Function.Base using (_∘_)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Empty using (⊥)
open import Data.Product using (∃-syntax; Σ-syntax; _×_; _,_; proj₁; proj₂)
open import Relation.Binary.PropositionalEquality.Core as ≡ using (_≡_; _≢_; refl; sym; cong; cong₂; subst)
open import Relation.Nullary using (Dec; yes; no)
```

```
open import Common using (_$_≡_; `_; `refl)
open import Labels
open import BindersByDay as B
open import BBDSubstitution as B
open import BBDSemantics as B
open import FirstClassNames as F
open import FCNSubstitution as F
open import FCNSemantics as F
open import FCN2BBD
open import FCN2BBDSubstitution
open import FCN2BBDSemantics
```

```
private variable
  L L′ : Labels
  ℓ ℓ′ : -∈ᴸ L
  κ κ′ : F.Kind
  Δ : F.Contextᴷ
  τ τ′ τ₁ τ₂ σ τε : L F.⨟ Δ ⊢ᵀ κ
  σ′ : L F.⨟ Δ ⊢ᵀ SIG
  ε ε′ : L F.⨟ Δ ⊢ᵀ EFF
  ⌜κ⌝ : ⟪ κ ⟫K
  ⌜κ′⌝ : ⟪ κ′ ⟫K
  ⌜Δ⌝ ⌜Δ′⌝ : ⟪ Δ ⟫ᴷ
  Γ Γ′ : F.Context L Δ
  ⌜Γ⌝ ⌜Γ′⌝ : ⟪ ⌜Δ⌝ ⊢ Γ ⟫ᴳ
  η η′ : L F.⨟ Δ ⊢ᵀ SCO
  ⌜σ⌝ ⌜σ′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ σ′ ⟫ᵀ
  ⌜ε⌝ ⌜ε′⌝ : ⦅ ⌜Δ⌝ , (λ()) ⦆⟪ ε ⟫ᵀ
  x : τ F.∈ Γ
```

```
-- ad hoc reasoning for single-step
module ⟶-Reasoning where
  infixr 3 _∎
  infixr 2 _⟶⟨_⟩_
  infixr 2 _≡⟨_⟩⦅_⦆_

  _≡⟨_⟩⦅_⦆_ : ∀ {τ τ′ τ″ : L B.⨟ ∅ ⊢ᵀ COM}
    → (M : ∅ B.⊢ τ)
    → {M′ : ∅ B.⊢ τ′} {M″ : ∅ B.⊢ τ″}
    → M B.≡ᴹ M′
    → (_%_ : ∀ {τ τ′ : L B.⨟ ∅ ⊢ᵀ COM} (N : ∅ B.⊢ τ) (N′ : ∅ B.⊢ τ′) → Set)
    → M′ % M″
    → M % M″
  M ≡⟨ refl ⟩⦅ _%_ ⦆ M%M″ = M%M″

  _∎ : ∀ {τ : L B.⨟ ∅ ⊢ᵀ COM} (M : ∅ B.⊢ τ) → M B.≡ᴹ M
  _∎ M = refl

  _⟶⟨_⟩_ : ∀ {τ τ′ τ″ : L B.⨟ ∅ ⊢ᵀ COM}
      (M : ∅ B.⊢ τ)
      {M′ : ∅ B.⊢ τ′}
      {M″ : ∅ B.⊢ τ″}
    → M B.⟶ M′
    → M′ B.≡ᴹ M″
    → M B.⟶ M″
  M ⟶⟨ M⟶M′ ⟩ refl = M⟶M′
```

```
sim-generate-aux : ∀ {τ σ ε} {τ′ σ′ ε′}
    (M : ∅ {L = L} ▷ᴺ- ▷ Unit B.⊢ τ′ / static Z ⦂ σ′ ∷ ε′)
  → (wkn≡³ : weaken★ᴺ $ τ ≡ τ′ × weaken★ᴺ $ σ ≡ σ′ × weaken★ᴺ $ ε ≡ ε′)
  → B.new (appᵛ (ƛ M) tt) wkn≡³ B.⟶ B.new (M B.[ tt ]) wkn≡³
sim-generate-aux {τ′ = τ′} {σ′} {ε′} M wkn≡³ =
  B.new (appᵛ (ƛ M) tt) wkn≡³                 ≡⟨ B.≡ᴹ-subst _ _ ⟩⦅ B._⟶_ ⦆
  appᵛ (ƛ (B.tra (B.tra▷ B.↠-new) M)) tt      ⟶⟨ B.^₀ (β _ _) ⟩
  (B.tra (B.tra▷ B.↠-new) M) B.[ tt ]         ≡⟨ B.tra-[] B.↠-new M tt ⟩⦅ (λ N N′ → N B.≡ᴹ N′) ⦆
  B.tra B.↠-new (M B.[ tt ])                  ≡⟨ B.≡ᴹ-sym (B.≡ᴹ-subst _ _) ⟩⦅ (λ N N′ → N B.≡ᴹ N′) ⦆
  B.new (M B.[ tt ]) wkn≡³    ∎
  where
    open ⟶-Reasoning
```

```
open B.⟶★-Reasoning
```

```
sim↦₀ : ∀ {M : ∅ {L = L} F.⊢ τ} {M′ : ∅ {L = L} F.⊢ τ}
  → {⌜τ⌝ : ⦅ ∅ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ}
  → M F.↦₀ M′
  → (⌜M⌝ : ⟪ ∅ ⊢ ⌜τ⌝ ∣ M ⟫)
  → (⌞ ⌜M⌝ ⌟ B.⟶★ ⌞ ⦅ ⌜COM⌝ ⦆⌜ M′ ⌝ ⌟)
sim↦₀ (β N V) (appᵛ {⌜τ⌝ = _ / _} (ƛ ⌜N⌝) ⌜V⌝) =
  appᵛ (ƛ ⌞ ⌜N⌝ ⌟) ⌞ ⌜V⌝ ⌟    ⟶⟨ B.^₀ (β ⌞ ⌜N⌝ ⌟ ⌞ ⌜V⌝ ⌟) ⟩
  ⌞ ⌜N⌝ ⌟ B.[ ⌞ ⌜V⌝ ⌟ ]       ≡⟨ B.≡ᴹ-[] (≡ᴹ-⌞⌟ ⌜N⌝ ⌜ N ⌝) (≡ᴹ-⌞⌟ ⌜V⌝ ⌜ V ⌝) ⟩
  ⌞ ⌜ N ⌝ ⌟ B.[ ⌞ ⌜ V ⌝ ⌟ ]   ≡⟨ B.≡ᴹ-sym ⟦ N [ V ]⟧ ⟩
  ⌞ ⌜ N F.[ V ] ⌝ ⌟           ∎
sim↦₀ (βᵀ N τ) (appᵀ (Λ ⌜N⌝) ⌜τ⌝ x) =
  appᵀ (Λ ⌞ ⌜N⌝ ⌟) ⌞ ⌜τ⌝ ⌟ᵀ _        ⟶⟨ ^ (B.βᵀ′ ⌞ ⌜N⌝ ⌟ ⌞ ⌜τ⌝ ⌟ᵀ _) ⟩
  return (⌞ ⌜N⌝ ⌟ B.[ ⌞ ⌜τ⌝ ⌟ᵀ ]ᵀ)   ≡⟨ B.≡ᴹ-return refl (⟦ ⌜N⌝ [ ⌜τ⌝ ]ᵀ⟧ _ ⌜ N F.[ τ ]ᵀ ⌝) ⟩
  return ⌞ ⌜ N F.[ τ ]ᵀ ⌝ ⌟          ∎
sim↦₀ (βᵀ N η) (appᴺ (ƛᴺ ⌜N⌝) .η x) =
  appᴺ (ƛᴺ ⌞ ⌜N⌝ ⌟) ⌞ η ⌟ᶻ _         ⟶⟨ ^ (B.βᴺ′ ⌞ ⌜N⌝ ⌟ ⌞ η ⌟ᶻ _) ⟩
  return (⌞ ⌜N⌝ ⌟ B.[ ⌞ η ⌟ᶻ ]ᴺ)     ≡⟨ B.≡ᴹ-return refl (⟦ ⌜N⌝ [ η ]ᴺ⟧ _ ⌜ N F.[ η ]ᵀ ⌝) ⟩
  return ⌞ ⌜ N F.[ η ]ᵀ ⌝ ⌟          ∎
sim↦₀ (β->>= V N) (return ⌜V⌝ >>= ⌜N⌝) =
  return ⌞ ⌜V⌝ ⌟ >>= ⌞ ⌜N⌝ ⌟  ⟶⟨ B.^₀ (β->>= ⌞ ⌜V⌝ ⌟ ⌞ ⌜N⌝ ⌟) ⟩
  ⌞ ⌜N⌝ ⌟ B.[ ⌞ ⌜V⌝ ⌟ ]       ≡⟨ B.≡ᴹ-[] (≡ᴹ-⌞⌟ ⌜N⌝ ⌜ N ⌝) (≡ᴹ-⌞⌟ ⌜V⌝ ⌜ V ⌝) ⟩
  ⌞ ⌜ N ⌝ ⌟ B.[ ⌞ ⌜ V ⌝ ⌟ ]   ≡⟨ B.≡ᴹ-sym ⟦ N [ V ]⟧ ⟩
  ⌞ ⌜ N F.[ V ] ⌝ ⌟           ∎
sim↦₀ (hop {τ = τ} {ε = ε} {ℓ∉ = ℓ∉} {E = E} ℓ∉E V H E[perform]≡) (handle′ {⌜σ⌝ = ⌜σ⌝} ⌜ℓ∉⌝ ⌜M⌝ ⌜H⌝) =
  handle′ _ ⌞ ⌜M⌝ ⌟ ⌞ ⌜H⌝ ⌟            ⟶⟨ B.^₀ (hop (⌞ ⦅ ⌜E⌝ ⦆⌜ ℓ∉E ⌝∉ᴱ ⌟∉ᴱ)
                                                    ⌞ ⌜V⌝ ⌟
                                                    ⌞ ⌜H⌝ ⌟
                                                    (⟦_[_/□]⟧∙ ⦅□: _ ⊢ᴱ _ ⦆⌜ E ⌝ (perform (name _) ⦅ _ ⦆⌜ V ⌝′) _ E[perform]≡)) ⟩
  ⌞ ⌜H⌝ ⌟ B.[ ⌞ ⌜V⌝ ⌟ ∣ B.resume ⌞ ⌜ℓ∉⌝ ⌟∉ ⌞ ⌜E⌝ ⌟ᴱ ⌞ ⌜H⌝ ⌟ ]ʰ ≡⟨ B.≡ᴹ-refl (cong (⌞ ⌜H⌝ ⌟ B.[ ⌞ ⌜V⌝ ⌟ ∣_]ʰ) (⟦resume⟧ ⌜ℓ∉⌝ ⌜E⌝ ⌜H⌝ ⦅ ⌜ τ ⌝ᵀ ⇒ (_ / _) ⦆⌜ F.resume ℓ∉ E H ⌝′)) ⟩
  ⌞ ⌜H⌝ ⌟ B.[ ⌞ ⌜V⌝ ⌟ ∣ ⌞ ⦅ _ ⦆⌜ F.resume ℓ∉ E H ⌝′ ⌟ ]ʰ       ≡⟨ ⟦ ⌜H⌝ [ ⌜V⌝ ∣ ⦅ _ ⦆⌜ F.resume ℓ∉ E H ⌝′ ]ʰ⟧ ⦅ _ ⦆⌜ H F.[ V ∣ F.resume ℓ∉ E H ]ʰ ⌝′ ⟩
  ⌞ ⦅ _ ⦆⌜ H F.[ V ∣ F.resume ℓ∉ E H ]ʰ ⌝′ ⌟                   ≡⟨ ≡ᴹ-⌞⌟ _ _ ⟩
  ⌞ ⌜ H F.[ V ∣ F.resume ℓ∉ E H ]ʰ ⌝ ⌟ ∎
  where
    ⌜V⌝ = ⦅ ⌜σ⌝ ⇒ᵖ ⌜ τ ⌝ᵀ ⦆⌜ V ⌝′
    ⌜E⌝ = ⦅□: ⌜ τ ⌝ᵀ / (dynamic _ ⦂ _ ∷ ⌜ ε ⌝ᵀ) ⊢ᴱ _ ⦆⌜ E ⌝
sim↦₀ (hret V H) (handle′ ⌜ℓ∉⌝ (return ⌜V⌝) ⌜H⌝) =
  handle′ _ (return ⌞ ⌜V⌝ ⌟) _    ⟶⟨ B.^₀ (hret ⌞ ⌜V⌝ ⌟ _) ⟩
  return ⌞ ⌜V⌝ ⌟                  ≡⟨ B.≡ᴹ-return (≡-⌞⌟ᵀ _ _) (≡ᴹ-⌞⌟ ⌜V⌝ ⌜ V ⌝) ⟩
  return ⌞ ⌜ V ⌝ ⌟                ∎
sim↦₀ (subret ε⊑ε′ V) (subeffect x (return ⌜V⌝)) =
  subeffect _ (return ⌞ ⌜V⌝ ⌟)    ⟶⟨ B.^₀ (subret _ ⌞ ⌜V⌝ ⌟) ⟩
  return ⌞ ⌜V⌝ ⌟                  ≡⟨ B.≡ᴹ-return (≡-⌞⌟ᵀ _ _) (≡ᴹ-⌞⌟ ⌜V⌝ ⌜ V ⌝) ⟩
  return ⌞ ⌜ V ⌝ ⌟                ∎
```

```
sim↦₁ : ∀ {M : ∅ {L = L} F.⊢ τ} {M′ : ∅ F.⊢ F.weaken★L τ}
  → {⌜τ⌝ : ⦅ ∅ , ⌜κ⌝ ⦆⟪ τ ⟫ᵀ}
  → M F.↦₁ M′
  → (⌜M⌝ : ⟪ ∅ ⊢ ⌜τ⌝ ∣ M ⟫)
  → (⌞ ⌜M⌝ ⌟ B.⟶★ ⌞ ⦅ ⌜COM⌝ ⦆⌜ M′ ⌝ ⌟)
sim↦₁ (generate M H {wkn≡³ = wkn≡³}) (handle {⌜ε⌝ = ⌜ε⌝} ⌜M⌝ ⌜H⌝ ⌜wkn≡³⌝) =
  handle (appᵛ (ƛ ⌞ ⌜M⌝ ⌟) tt) ⌞ ⌜H⌝ ⌟ w      ⟶⟨ B.^₁ (generate (appᵛ (ƛ ⌞ ⌜M⌝ ⌟) tt) ⌞ ⌜H⌝ ⌟) ⟩
  handle′ `fresh (B.new (appᵛ (ƛ ⌞ ⌜M⌝ ⌟) tt) w) (B.weakenᴸ ⌞ ⌜H⌝ ⌟)  ⟶⟨ B.⟶-cong (≡ᴱ⇒≈ᴱ B.⦅handle′ refl [ B.□≡□ refl ] refl ⦆) (sim-generate-aux ⌞ ⌜M⌝ ⌟ w) ⟩
  handle′ `fresh (B.new (⌞ ⌜M⌝ ⌟ B.[ tt ]) w) (B.weakenᴸ ⌞ ⌜H⌝ ⌟)  ≡⟨ B.≡ᴹ-handle′ ⟦fresh⟧ (⟦new⟧ ⌜M⌝ wkn≡³ w) (⟦weakenᴸ⟧ ⌜H⌝) ⟩
  handle′ ⌞ ⌜ F.fresh ⌝∉ ⌟∉ ⌞ ⌜ F.new M wkn≡³ ⌝ ⌟ ⌞ ⌜ F.weakenᴸ H ⌝ ⌟  ∎
  where
    w = _
    `fresh = B.fresh {ε = ⌞ ⌜ε⌝ ⌟ᵀ}
```

```
sim↦ : ∀ {M : ∅ {L = L} F.⊢ τ} {M′ : ∅ {L = L′} F.⊢ τ′}
  → {⌜τ⌝ : ⦅ ∅ , ⌜COM⌝ ⦆⟪ τ ⟫ᵀ}
  → (⌜M⌝ : ⟪ ∅ ⊢ ⌜τ⌝ ∣ M ⟫)
  → M F.↦ M′
  → ⌞ ⌜M⌝ ⌟ B.⟶★ ⌞ ⦅ ⌜COM⌝ ⦆⌜ M′ ⌝ ⌟
sim↦ ⌜M⌝ (step₀ M↦M′) = sim↦₀ M↦M′ ⌜M⌝
sim↦ ⌜M⌝ (step₁ M↦M′) = sim↦₁ M↦M′ ⌜M⌝
```

```
⌞⌟-sim⟶ : ∀ {M : ∅ {L = L} F.⊢ τ} {M′ : ∅ {L = L′} F.⊢ τ′}
  → {⌜τ⌝ : ⦅ ∅ , ⌜COM⌝ ⦆⟪ τ ⟫ᵀ}
  → (⌜M⌝ : ⟪ ∅ ⊢ ⌜τ⌝ ∣ M ⟫)
  → M F.⟶ M′
  → ⌞ ⌜M⌝ ⌟ B.⟶★ ⌞ ⦅ ⌜COM⌝ ⦆⌜ M′ ⌝ ⌟
⌞⌟-sim⟶ ⌜M⌝ (ξ N↦N′ E E′ E≈E′ E[N]≡ E′[N′]≡) with split-⌜⌝-[/□] {E = E} E[N]≡ ⌜M⌝
... | ⌜E⌝ , ⌜N⌝ , ≡⟦E[N]⟧ = B.⟶★-cong′ (≈ᴱ-⌞⌟ E≈E′) (sim↦ ⌜N⌝ N↦N′) ≡⟦E[N]⟧ (⟦ ⌜ E′ ⌝ᴱ [ ⌜ _ ⌝ /□]⟧∙ ⌜ _ ⌝ E′[N′]≡)
```

```
sim⟶ : ∀ {M : ∅ {L = L} F.⊢ τ} {M′ : ∅ {L = L′} F.⊢ τ′}
  → M F.⟶ M′
  → ⌞ ⦅ ⌜COM⌝ ⦆⌜ M ⌝ ⌟ B.⟶★ ⌞ ⦅ ⌜COM⌝ ⦆⌜ M′ ⌝ ⌟
sim⟶ {M = M} {M′} = ⌞⌟-sim⟶ ⦅ ⌜COM⌝ ⦆⌜ M ⌝
```

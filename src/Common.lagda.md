```
module Common where

open import Data.Empty using (⊥) public
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
```

```
⊥-elim : ∀ {A : Set} → .⊥ → A
⊥-elim ()
```

```
cong₃ : {A B C D : Set} (f : A → B → C → D) {a a′ : A} {b b′ : B} {c c′ : C}
  → a ≡ a′ → b ≡ b′ → c ≡ c′ → f a b c ≡ f a′ b′ c′
cong₃ f refl refl refl = refl
```

```
infix 4 _$_≡_
record _$_≡_ {A B : Set} (f : A → B) (x : A) (y : B) : Set where
  constructor `_
  field
    unwrap : f x ≡ y
```

```
pattern `refl = ` refl
```

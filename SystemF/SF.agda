-- Intrinsically-typed System F
module SF where

open import Relation.Binary.PropositionalEquality as E using (_≡_; refl; trans; sym; cong; subst; cong₂)
open import Function.Base using (_∘_)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.List using (List; []; _∷_)
open import Data.Nat using (ℕ; zero; suc)

infix 7 `_
infixl 6 _⇒_
infixl 5 _▷★_ _▷_
infixl 4 _∙_ _·_
infix 3 _∋★_ _⊢★_ _∋_ _⊢_ _↦_

data Kind : Set where
  T : Kind

data Ctx★ : Set where
  ∅ : Ctx★
  _▷★_ : Ctx★ → Kind → Ctx★

data _∋★_ : Ctx★ → Kind → Set where
  Z : {Δ : Ctx★} {k : Kind} → Δ ▷★ k ∋★ k
  S : {Δ : Ctx★} {k l : Kind} → Δ ∋★ k → Δ ▷★ l ∋★ k

data _⊢★_ (Δ : Ctx★) : Kind → Set where
  `_ : {k : Kind} → Δ ∋★ k → Δ ⊢★ k
  [∀] : {k l : Kind} → Δ ▷★ k ⊢★ l → Δ ⊢★ l
  _⇒_ : Δ ⊢★ T → Δ ⊢★ T → Δ ⊢★ T

_⇒★ʳ_ : Ctx★ → Ctx★ → Set
Δ ⇒★ʳ Δ' = ∀ {k} → Δ ∋★ k → Δ' ∋★ k

_⇒★_ : Ctx★ → Ctx★ → Set
Δ ⇒★ Δ' = ∀ {k} → Δ ⊢★ k → Δ' ⊢★ k

_⇒★ˢ_ : Ctx★ → Ctx★ → Set
Δ ⇒★ˢ Δ' = ∀ {k} → Δ ∋★ k → Δ' ⊢★ k

ren★▷ : ∀ {Δ Δ' k} → Δ ⇒★ʳ Δ' → (Δ ▷★ k) ⇒★ʳ (Δ' ▷★ k)
ren★▷ ρ Z = Z
ren★▷ ρ (S α) = S (ρ α)

ren★ : ∀ {Δ Δ'} → Δ  ⇒★ʳ Δ' → Δ  ⇒★ Δ'
ren★ ρ (` α) = ` ρ α
ren★ ρ ([∀] A) = [∀] (ren★ (ren★▷ ρ) A)
ren★ ρ (A ⇒ B) = ren★ ρ A ⇒ ren★ ρ B

weaken★ : ∀ {Δ k} → Δ ⇒★ (Δ ▷★ k)
weaken★ = ren★ S

sub★▷ : ∀ {Δ Δ' k} → Δ ⇒★ˢ Δ' → (Δ ▷★ k) ⇒★ˢ (Δ' ▷★ k)
sub★▷ σ Z = ` Z
sub★▷ σ (S α) = weaken★ (σ α)

sub★ : ∀ {Δ Δ'} → Δ ⇒★ˢ Δ' → Δ ⇒★ Δ'
sub★ σ (` α) = σ α
sub★ σ ([∀] A) = [∀] (sub★ (sub★▷ σ) A)
sub★ σ (A ⇒ B) = sub★ σ A ⇒ sub★ σ B

_▷ˢ_ : ∀ {Δ Δ' k} → Δ ⇒★ˢ Δ' → Δ' ⊢★ k → (Δ ▷★ k) ⇒★ˢ Δ'
σ ▷ˢ A = λ{ Z → A ; (S α) → σ α }

_[_]★ : ∀ {Δ k l} → Δ ▷★ k ⊢★ l → Δ ⊢★ k → Δ ⊢★ l
A [ B ]★ = sub★ (`_ ▷ˢ B) A

data Ctx : Ctx★ → Set where
  ∅ : Ctx ∅
  _▷★_ : ∀ {Δ} → Ctx Δ → (k : Kind) → Ctx (Δ ▷★ k)
  _▷_ : ∀ {Δ} → Ctx Δ → Δ ⊢★ T → Ctx Δ

data _∋_ : {Δ : Ctx★} (Γ : Ctx Δ) → Δ ⊢★ T → Set where
  Z : ∀ {Δ} {Γ : Ctx Δ} {A : Δ ⊢★ T} → Γ ▷ A ∋ A
  S : ∀ {Δ} {Γ : Ctx Δ} {A B : Δ ⊢★ T} → Γ ∋ B → Γ ▷ A ∋ B
  S★ : ∀ {Δ} {Γ : Ctx Δ} {k} {B} → Γ ∋ B → Γ ▷★ k ∋ weaken★ B

data _⊢_ {Δ : Ctx★} (Γ : Ctx Δ) : Δ ⊢★ T → Set where
  `_ : ∀ {A} → Γ ∋ A → Γ ⊢ A
  Λ_ : ∀ {k} {A} → Γ ▷★ k ⊢ A → Γ ⊢ [∀] A
  _∙_ : ∀ {k A} → Γ ⊢ [∀] A → (B : Δ ⊢★ k) → Γ ⊢ A [ B ]★
  ƛ_ : ∀ {A B} → Γ ▷ A ⊢ B → Γ ⊢ A ⇒ B
  _·_ : ∀ {A B} → Γ ⊢ A ⇒ B → Γ ⊢ A → Γ ⊢ B

Ren : ∀ {Δ Δ'} → (ρ : Δ ⇒★ʳ Δ') → Ctx Δ → Ctx Δ' → Set
Ren ρ Γ Γ' = ∀ {A} → Γ ∋ A → Γ' ∋ ren★ ρ A

ren★-ren★ : ∀ {Δ Δ' Δ''} (ρ : Δ ⇒★ʳ Δ') (ρ' : Δ' ⇒★ʳ Δ'') (ρ'' : Δ ⇒★ʳ Δ'') {k} → (∀ α → ρ' (ρ α) ≡ ρ'' α) → (A : Δ ⊢★ k) → ren★ ρ' (ren★ ρ A) ≡ ren★ ρ'' A
ren★-ren★ _ _ _ eq (` α) = cong `_ (eq α)
ren★-ren★ ρ ρ' ρ'' eq ([∀] A) rewrite ren★-ren★ (ren★▷ ρ) (ren★▷ ρ') (ren★▷ ρ'') (λ{ Z → refl ; (S α) → cong S (eq α) }) A = refl
ren★-ren★ ρ ρ' ρ'' eq (A ⇒ B) rewrite ren★-ren★ ρ ρ' ρ'' eq A | ren★-ren★ ρ ρ' ρ'' eq B = refl

renS▷ : ∀ {Δ Δ' Δ''} (σ : Δ ⇒★ˢ Δ') (ρ' : Δ' ⇒★ʳ Δ'') (σ'' : Δ ⇒★ˢ Δ'') {k l} → (α : _ ∋★ k) →
  ren★ ρ' (σ α) ≡ σ'' α →
  ren★ (ren★▷ ρ') (ren★ S (σ α)) ≡ ren★ (S {l = l}) (σ'' α)
renS▷ σ ρ' σ'' α eq = begin
  ren★ (ren★▷ ρ') (ren★ S (σ α)) ≡⟨ ren★-ren★ S (ren★▷ ρ') (λ β → S (ρ' β)) (λ _ → refl) (σ α) ⟩
  ren★ (λ β → S (ρ' β)) (σ α) ≡⟨ sym (ren★-ren★ ρ' S _ (λ _ → refl) (σ α)) ⟩
  ren★ S (ren★ ρ' (σ α)) ≡⟨ cong (ren★ S) eq ⟩
  ren★ S (σ'' α) ∎
  where open E.≡-Reasoning

ren★∘sub★ : ∀ {Δ Δ' Δ''} (σ : Δ ⇒★ˢ Δ') (ρ' : Δ' ⇒★ʳ Δ'') (σ'' : Δ ⇒★ˢ Δ'') {k} → (∀ {l} (α : _ ∋★ l) → ren★ ρ' (σ α) ≡ σ'' α) → (A : Δ ⊢★ k) → ren★ ρ' (sub★ σ A) ≡ sub★ σ'' A
ren★∘sub★ σ ρ' σ'' eq (` α) = eq α
ren★∘sub★ σ ρ' σ'' eq ([∀] A)
  rewrite ren★∘sub★ (sub★▷ σ) (ren★▷ ρ') (sub★▷ σ'') (λ{ Z → refl ; (S α) → renS▷ σ ρ' σ'' α (eq α) }) A
 = refl
ren★∘sub★ σ ρ' σ'' eq (A ⇒ B)
  rewrite ren★∘sub★ σ ρ' σ'' eq A | ren★∘sub★ σ ρ' σ'' eq B = refl

sub★∘ren★ : ∀ {Δ Δ' Δ''} (ρ : Δ ⇒★ʳ Δ') (σ' : Δ' ⇒★ˢ Δ'') (σ'' : Δ ⇒★ˢ Δ'') {k} → (∀ {l} (α : _ ∋★ l) → σ' (ρ α) ≡ σ'' α) → (A : Δ ⊢★ k) → sub★ σ' (ren★ ρ A) ≡ sub★ σ'' A
sub★∘ren★ ρ σ' σ'' eq (` α) = eq α
sub★∘ren★ ρ σ' σ'' eq ([∀] A)
  rewrite sub★∘ren★ (ren★▷ ρ) (sub★▷ σ') (sub★▷ σ'') (λ{ Z → refl ; (S α) → cong (ren★ S) (eq α) }) A = refl
sub★∘ren★ ρ σ' σ'' eq (A ⇒ B)
  rewrite sub★∘ren★ ρ σ' σ'' eq A | sub★∘ren★ ρ σ' σ'' eq B = refl

weaken★-ren★ : ∀ {Δ Δ'} (ρ : Δ ⇒★ʳ Δ') {k l} {A : Δ ⊢★ k} → weaken★ {k = l} (ren★ ρ A) ≡ ren★ (ren★▷ ρ) (weaken★ A)
weaken★-ren★ ρ = trans (ren★-ren★ _ _ (λ α → S (ρ α)) (λ _ → refl) _) (sym (ren★-ren★ _ _ _ (λ _ → refl) _))

weaken★-sub★ : ∀ {Δ Δ'} (σ : Δ ⇒★ˢ Δ') {k l} {A : Δ ⊢★ k} → weaken★ {k = l} (sub★ σ A) ≡ sub★ (sub★▷ σ) (weaken★ A)
weaken★-sub★ σ {A = A} = begin
  ren★ S (sub★ σ A) ≡⟨ ren★∘sub★ _ _ _ (λ α → refl) A ⟩
  sub★ (ren★ S ∘ σ) A ≡⟨ sym (sub★∘ren★ _ _ _ (λ α → refl) A) ⟩
  sub★ (sub★▷ σ) (ren★ S A) ∎
  where open E.≡-Reasoning

ren▷★ : ∀ {Δ Δ'} {ρ★ : Δ ⇒★ʳ Δ'} {k} {Γ Γ'} → Ren ρ★ Γ Γ' → Ren (ren★▷ ρ★) (Γ ▷★ k) (Γ' ▷★ k)
ren▷★ ρ (S★ x) = subst (_ ∋_) (weaken★-ren★ _) (S★ (ρ x))

ren▷ : ∀ {Δ Δ'} {ρ★ : Δ ⇒★ʳ Δ'} {A} {Γ Γ'} → Ren ρ★ Γ Γ' → Ren ρ★ (Γ ▷ A) (Γ' ▷ ren★ ρ★ A)
ren▷ ρ Z = Z
ren▷ ρ (S x) = S (ρ x)

ren★-[]★ : ∀ {Δ Δ'} (ρ★ : Δ ⇒★ʳ Δ') {k l} {A : Δ ▷★ k ⊢★ l} {B} → ren★ (ren★▷ ρ★) A [ ren★ ρ★ B ]★ ≡ ren★ ρ★ (A [ B ]★)
ren★-[]★ ρ★ {A = A} {B} = begin
  sub★ (`_ ▷ˢ ren★ ρ★ B) (ren★ (ren★▷ ρ★) A)
    ≡⟨ sub★∘ren★ _ _ _ (λ{ Z → refl ; (S α) → refl }) A ⟩
  sub★ (λ{ Z → ren★ ρ★ B ; (S α) → ` ρ★ α }) A
    ≡⟨ sym (ren★∘sub★ (`_ ▷ˢ B) ρ★ _ (λ{ Z → refl ; (S α) → refl }) A) ⟩
  ren★ ρ★ (sub★ (`_ ▷ˢ B) A) ∎
  where open E.≡-Reasoning

sub★∘sub★ : ∀ {Δ Δ' Δ''} {σ : Δ ⇒★ˢ Δ'} {σ' : Δ' ⇒★ˢ Δ''} {σ'' : Δ ⇒★ˢ Δ''} →
  (∀ {k} (α : Δ ∋★ k) → sub★ σ' (σ α) ≡ σ'' α) →
  ∀ {k} (A : Δ ⊢★ k) → sub★ σ' (sub★ σ A) ≡ sub★ σ'' A
sub★∘sub★ eq (` α) = eq α
sub★∘sub★ {σ = σ} {σ' = σ'} eq ([∀] A) = cong [∀] (sub★∘sub★ (λ{ Z → refl ; (S α) → trans (sym (weaken★-sub★ σ' {A = σ α})) (cong weaken★ (eq α)) }) A)
sub★∘sub★ eq (A ⇒ B) = cong₂ _⇒_ (sub★∘sub★ eq A) (sub★∘sub★ eq B)

sub★Id' : ∀ {Δ} {k} {σ : Δ ⇒★ˢ Δ} →
  (∀ {k} (α : Δ ∋★ k) → σ α ≡ ` α) →
  ∀ (A : Δ ⊢★ k) → sub★ σ A ≡ A
sub★Id' eq (` α) = eq α
sub★Id' eq ([∀] A) = cong [∀] (sub★Id' (λ{ Z → refl ; (S α) → cong weaken★ (eq α) }) A)
sub★Id' eq (A ⇒ B) = cong₂ _⇒_ (sub★Id' eq A) (sub★Id' eq B)

sub★Id : ∀ {Δ} {k} {A : Δ ⊢★ k} → sub★ `_ A ≡ A
sub★Id {A = A} = sub★Id' (λ α → refl) A

[]★∘weaken★ : ∀ {Δ k l} {B : Δ ⊢★ l} (A : Δ ⊢★ k) → weaken★ A [ B ]★ ≡ A
[]★∘weaken★ A = trans (sub★∘ren★ _ _ _ (λ α → refl) A) sub★Id

sub★-[]★ : ∀ {Δ Δ'} (σ★ : Δ ⇒★ˢ Δ') {k l} {A : Δ ▷★ k ⊢★ l} {B} → sub★ (sub★▷ σ★) A [ sub★ σ★ B ]★ ≡ sub★ σ★ (A [ B ]★)
sub★-[]★ σ★ {A = A} {B = B} = begin
  sub★ (`_ ▷ˢ sub★ σ★ B) (sub★ (sub★▷ σ★) A)
    ≡⟨ sub★∘sub★ (λ{ Z → refl ; (S α) → []★∘weaken★ (σ★ α) }) A ⟩
  sub★ (λ{ Z → sub★ σ★ B ; (S α) → σ★ α }) A
    ≡⟨ sym (sub★∘sub★ (λ{ Z → refl ; (S α) → refl }) A) ⟩
  sub★ σ★ (sub★ (`_ ▷ˢ B) A) ∎
  where open E.≡-Reasoning

ren : ∀ {Δ Δ'} {ρ★ : Δ ⇒★ʳ Δ'} {A} {Γ Γ'} → Ren ρ★ Γ Γ' → Γ ⊢ A → Γ' ⊢ ren★ ρ★ A
ren ρ (` x) = ` ρ x
ren ρ (Λ M) = Λ ren (ren▷★ ρ) M
ren {ρ★ = ρ★} ρ (_∙_ {A = A} M B)
  = subst (_ ⊢_) (ren★-[]★ ρ★ {A = A} {B = B}) (ren ρ M ∙ ren★ ρ★ B)
ren ρ (ƛ M) = ƛ (ren (ren▷ ρ) M)
ren ρ (M · N) = ren ρ M · ren ρ N

ρ★Id : ∀ {Δ} → Δ ⇒★ʳ Δ
ρ★Id α = α

is-ρ★Id : ∀ {Δ} (ρ : Δ ⇒★ʳ Δ) → Set
is-ρ★Id ρ = ∀ {k} (α : _ ∋★ k) → ρ α ≡ α

ren★Id' : ∀ {Δ} {ρ : Δ ⇒★ʳ Δ} → is-ρ★Id ρ → ∀ {k} (A : Δ ⊢★ k) → ren★ ρ A ≡ A
ren★Id' eq (` α) = cong `_ (eq α)
ren★Id' eq ([∀] A) = cong [∀] (ren★Id' (λ{ Z → refl ; (S α) → cong S (eq α) }) A)
ren★Id' eq (A ⇒ B) = cong₂ _⇒_ (ren★Id' eq A) (ren★Id' eq B)

ren★Id : ∀ {Δ} {k} {A : Δ ⊢★ k} → ren★ ρ★Id A ≡ A
ren★Id = ren★Id' (λ α → refl) _

weaken-ρId : ∀ {Δ} {Γ : Ctx Δ} {A} → Ren ρ★Id Γ (Γ ▷ A)
weaken-ρId = subst (_ ∋_) (sym ren★Id) ∘ S

weaken : ∀ {Δ} {Γ : Ctx Δ} {A B} → Γ ⊢ A → Γ ▷ B ⊢ A
weaken = subst (_ ⊢_) ren★Id ∘ ren weaken-ρId

weakenT : ∀ {Δ} {Γ : Ctx Δ} {A k} → Γ ⊢ A → Γ ▷★ k ⊢ weaken★ A
weakenT = ren S★

Sub : ∀ {Δ Δ'} (σ★ : Δ ⇒★ˢ Δ') → Ctx Δ → Ctx Δ' → Set
Sub σ★ Γ Γ' = ∀ {A} → Γ ∋ A → Γ' ⊢ sub★ σ★ A

sub▷★ : ∀ {Δ Δ'} {σ★ : Δ ⇒★ˢ Δ'} {Γ Γ'} → Sub σ★ Γ Γ' → ∀ {k} → Sub (sub★▷ σ★) (Γ ▷★ k) (Γ' ▷★ k)
sub▷★ {σ★ = σ★} σ (S★ {B = B} x) = subst (_ ⊢_) (weaken★-sub★ σ★ {A = B}) (weakenT (σ x))

sub▷ : ∀ {Δ Δ'} {σ★ : Δ ⇒★ˢ Δ'} {Γ Γ'} → Sub σ★ Γ Γ' → ∀ {A} → Sub σ★ (Γ ▷ A) (Γ' ▷ sub★ σ★ A)
sub▷ σ Z = ` Z
sub▷ σ (S x) = weaken (σ x)

sub : ∀ {Δ Δ'} {σ★ : Δ ⇒★ˢ Δ'} {Γ Γ'} → Sub σ★ Γ Γ' → ∀ {A} → Γ ⊢ A → Γ' ⊢ sub★ σ★ A
sub σ (` x) = σ x
sub σ (Λ M) = Λ sub (sub▷★ σ) M
sub {σ★ = σ★} σ (_∙_ {A = A} M B) = subst (_ ⊢_) (sub★-[]★ _ {A = A}) (sub σ M ∙ sub★ σ★ B)
sub σ (ƛ M) = ƛ sub (sub▷ σ) M
sub σ (N · M) = sub σ N · sub σ M

record is-Sub★ {Δ Δ'} (d : Δ ⇒★ Δ') : Set where
  field
    σ★ : Δ ⇒★ˢ Δ'
    d-is-sub★ : ∀ {k} (A : Δ ⊢★ k) → sub★ σ★ A ≡ d A

record Sub' {Δ Δ'} (d : Δ ⇒★ Δ') (Γ : Ctx Δ) (Γ' : Ctx Δ') : Set where
  field
    s★ : is-Sub★ d
    g : ∀ {A} → Γ ∋ A → Γ' ⊢ d A

sub' : ∀ {Δ Δ'} {d : Δ ⇒★ Δ'} {Γ Γ'} → Sub' d Γ Γ'
  → ∀ {A} → Γ ⊢ A → Γ' ⊢ d A
sub' s = subst (_ ⊢_) (d-is-sub★ _) ∘ sub (subst (_ ⊢_) (sym (d-is-sub★ _)) ∘ g)
  where
    open Sub' s
    open is-Sub★ s★

σId : ∀ {Δ} {Γ : Ctx Δ} → Sub' (λ A → A) Γ Γ
σId = record
  { s★ = record { σ★ = `_ ; d-is-sub★ = λ A → sub★Id }
  ; g = `_ }

_▷ᶻ_ : ∀ {Δ Δ'} {Γ : Ctx Δ} {Γ' : Ctx Δ'} {d : Δ ⇒★ Δ'} {A} → Sub' d Γ Γ' → Γ ⊢ A → Sub' d (Γ ▷ A) Γ'
σ ▷ᶻ M = record
  { s★ = σ .s★
  ; g = λ{ Z → sub' σ M ; (S x) → σ .g x } }
  where open Sub'

_[_] : ∀ {Δ} {Γ : Ctx Δ} {A B : Δ ⊢★ T} → Γ ▷ A ⊢ B → Γ ⊢ A → Γ ⊢ B
N [ M ] = sub' (σId ▷ᶻ M) N

σT : ∀ {Δ} {Γ : Ctx Δ} {k} {B} → Sub (`_ ▷ˢ B) (Γ ▷★ k) Γ
σT = λ{ (S★ x) → ` subst (_ ∋_) (sym ([]★∘weaken★ _)) x }

_[_]T : ∀ {Δ} {Γ : Ctx Δ} {k} {A} → Γ ▷★ k ⊢ A → (B : Δ ⊢★ k) → Γ ⊢ A [ B ]★
N [ B ]T = sub σT N

data Value {Δ} {Γ : Ctx Δ} : {A : Δ ⊢★ T} → Γ ⊢ A → Set where
  ƛ_ : ∀ {A B} (N : Γ ▷ A ⊢ B) → Value (ƛ N)
  Λ_ : ∀ {k A} {N : Γ ▷★ k ⊢ A} → Value N → Value (Λ N)

data _↦_ {Δ} {Γ : Ctx Δ} : {A : Δ ⊢★ T} → Γ ⊢ A → Γ ⊢ A → Set where
  β-ƛ : ∀ {A B} (N : Γ ▷ A ⊢ B) {V : Γ ⊢ A}
    → Value V
    → (ƛ N) · V ↦ N [ V ]
  β-Λ : ∀ {k A} {V : Γ ▷★ k ⊢ A}
    → Value V
    → (B : Δ ⊢★ k)
    → (Λ V) ∙ B ↦ V [ B ]T
  _·[_] : ∀ {A B} (N : Γ ⊢ A ⇒ B) {M M' : Γ ⊢ A}
    → M ↦ M'
    → N · M ↦ N · M'
  [_]·_ : ∀ {A B} {N N' : Γ ⊢ A ⇒ B} {V : Γ ⊢ A}
    → N ↦ N'
    → Value V
    → N · V ↦ N' · V
  [_]∙_ : ∀ {k A} {N N' : Γ ⊢ [∀] A}
    → N ↦ N'
    → (B : Δ ⊢★ k)
    → N ∙ B ↦ N' ∙ B
  Λ[_] : ∀ {k A} {N N' : Γ ▷★ k ⊢ A}
    → N ↦ N'
    → Λ N ↦ Λ N'

NoVar : (Δ : Ctx★) → Ctx Δ
NoVar ∅ = ∅
NoVar (Δ ▷★ k) = NoVar Δ ▷★ k

novar : {Δ : Ctx★} {A : Δ ⊢★ T} → NoVar Δ ∋ A → ⊥
novar {Δ ▷★ k} (S★ x) = novar x

data Progress {Δ} {Γ : Ctx Δ} {A} (M : Γ ⊢ A) : Set where
  Step : ∀ {M'} → M ↦ M' → Progress M
  Done : Value M → Progress M

progress : ∀ {Δ} {A : Δ ⊢★ T} → (M : NoVar Δ ⊢ A) → Progress M
progress (ƛ N) = Done (ƛ N)
progress (Λ N) with progress N
... | Done v = Done (Λ v)
... | Step N↦N' = Step Λ[ N↦N' ]
progress (` x) = ⊥-elim (novar x)
progress (N ∙ B) with progress N
... | Done (Λ v) = Step (β-Λ v B)
... | Step N↦N' = Step ([ N↦N' ]∙ B)
progress (L · M) with progress M
... | Step M↦M' = Step (L ·[ M↦M' ])
... | Done v with progress L
...   | Done (ƛ N) = Step (β-ƛ N v)
...   | Step L↦L' = Step ([ L↦L' ]· v)

trace : ∀ {Δ} {A : Δ ⊢★ T} → (M : NoVar Δ ⊢ A) → ℕ → List (NoVar Δ ⊢ A)
trace M zero = M ∷ []
trace M (suc n) with progress M
... | Done _ = M ∷ []
... | Step {M' = M'} _ = M ∷ trace M' n

pattern ∀α,α⇒α = [∀] (` Z ⇒ ` Z)
pattern λx,x = ƛ (` Z)

ID : ∅ ⊢ ∀α,α⇒α
ID = Λ (ƛ (` Z))

ID·ID : ∅ ⊢ ∀α,α⇒α
ID·ID = ID ∙ ∀α,α⇒α · ID

_ : trace ID·ID 3 ≡
      (ID ∙ ∀α,α⇒α · ID) ∷
      (λx,x · ID) ∷
      ID ∷ []
_ = refl

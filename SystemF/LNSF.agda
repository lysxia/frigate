-- Locally nameless extrinsically typed System F
module LNSF where

open import Function.Base using (_∘_)
open import Data.Empty using (⊥; ⊥-elim)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; cong; cong₂; subst)
open import Relation.Binary.PropositionalEquality.Core

infix 3 _⊢★_⦂_ _⨟_⊢_⦂_ _↦_ _⊢★ᴿ_⦂_ _⊢ᴿ_⨟_⦂_ _⊢★ˢ_⦂_
infixl 4 _▷_
infixr 7 _⇒_

data Kind : Set where
  T : Kind

data inc (𝕍 : Set) : Set where
  Z : inc 𝕍
  S : 𝕍 → inc 𝕍

data Type (𝔸 : Set) : Set where
  `_ : 𝔸 → Type 𝔸
  [∀] : Kind → Type (inc 𝔸) → Type 𝔸
  _⇒_ : Type 𝔸 → Type 𝔸 → Type 𝔸

data Term (𝔸 𝕏 : Set) : Set where
  `_ : 𝕏 → Term 𝔸 𝕏
  ƛ_ : Term 𝔸 (inc 𝕏) → Term 𝔸 𝕏
  Λ_ : Term (inc 𝔸) 𝕏 → Term 𝔸 𝕏
  _∙_ : Term 𝔸 𝕏 → Type 𝔸 → Term 𝔸 𝕏
  _·_ : Term 𝔸 𝕏 → Term 𝔸 𝕏 → Term 𝔸 𝕏

map-inc : ∀ {𝔸 𝔹} → (𝔸 → 𝔹) → inc 𝔸 → inc 𝔹
map-inc ρ Z = Z
map-inc ρ (S α) = S (ρ α)

ren★ : ∀ {𝔸 𝔹} → (𝔸 → 𝔹) → Type 𝔸 → Type 𝔹
ren★ ρ (` α) = ` ρ α
ren★ ρ ([∀] k A) = [∀] k (ren★ (map-inc ρ) A)
ren★ ρ (A ⇒ B) = ren★ ρ A ⇒ ren★ ρ B

sub★▷ : ∀ {𝔸 𝔹} → (𝔸 → Type 𝔹) → inc 𝔸 → Type (inc 𝔹)
sub★▷ σ★ Z = ` Z
sub★▷ σ★ (S α) = ren★ S (σ★ α)

sub★ : ∀ {𝔸 𝔹} → (𝔸 → Type 𝔹) → Type 𝔸 → Type 𝔹
sub★ σ (` α) = σ α
sub★ σ ([∀] k A) = [∀] k (sub★ (sub★▷ σ) A)
sub★ σ (A ⇒ B) = sub★ σ A ⇒ sub★ σ B

_▷_ : {𝔸 𝕋 : Set} → (𝔸 → 𝕋) → 𝕋 → inc 𝔸 → 𝕋
(Δ ▷ k) Z = k
(Δ ▷ k) (S α) = Δ α

_[_]★ : ∀ {𝔸} → Type (inc 𝔸) → Type 𝔸 → Type 𝔸
A [ B ]★ = sub★ (`_ ▷ B) A

data _⊢★_⦂_ {𝔸 : Set} (Δ : 𝔸 → Kind) : Type 𝔸 → Kind → Set where
  `_ : (α : 𝔸) → Δ ⊢★ ` α ⦂ Δ α
  [∀] : ∀ {k A} → Δ ▷ k ⊢★ A ⦂ T → Δ ⊢★ [∀] k A ⦂ T
  _⇒_ : ∀ {A B} → Δ ⊢★ A ⦂ T → Δ ⊢★ B ⦂ T → Δ ⊢★ A ⇒ B ⦂ T

data _⨟_⊢_⦂_ {𝔸 𝕏 : Set} (Δ : 𝔸 → Kind) (Γ : 𝕏 → Type 𝔸) : Term 𝔸 𝕏 → Type 𝔸 → Set where
  `_ : (x : 𝕏)
      -----------------
    → Δ ⨟ Γ ⊢ ` x ⦂ Γ x
  ƛ_ : {A B : Type 𝔸} {M : Term 𝔸 (inc 𝕏)}
    → Δ ⨟ Γ ▷ A ⊢ M ⦂ B
      -------------------
    → Δ ⨟ Γ ⊢ ƛ M ⦂ A ⇒ B
  Λ_ : {k : Kind} {A : Type (inc 𝔸)} {M : Term (inc 𝔸) 𝕏}
    → Δ ▷ k ⨟ ren★ S ∘ Γ ⊢ M ⦂ A
      -------------------------
    → Δ ⨟ Γ ⊢ Λ M ⦂ [∀] k A
  _∙_ : {k : Kind} {A : Type (inc 𝔸)} {M : Term 𝔸 𝕏} {B : Type 𝔸}
    → Δ ⨟ Γ ⊢ M ⦂ [∀] k A
    → Δ ⊢★ B ⦂ k
      ------------------------
    → Δ ⨟ Γ ⊢ M ∙ B ⦂ A [ B ]★
  _·_ : {A B : Type 𝔸} {N M : Term 𝔸 𝕏}
    → Δ ⨟ Γ ⊢ N ⦂ A ⇒ B
    → Δ ⨟ Γ ⊢ M ⦂ A
      -----------------
    → Δ ⨟ Γ ⊢ N · M ⦂ B

ren : ∀ {𝔸 𝔹 𝕏 𝕐} → (𝔸 → 𝔹) → (𝕏 → 𝕐) → Term 𝔸 𝕏 → Term 𝔹 𝕐
ren ρ★ ρ (` x) = ` ρ x
ren ρ★ ρ (ƛ M) = ƛ ren ρ★ (map-inc ρ) M
ren ρ★ ρ (Λ M) = Λ ren (map-inc ρ★) ρ M
ren ρ★ ρ (M ∙ B) = ren ρ★ ρ M ∙ ren★ ρ★ B
ren ρ★ ρ (N · M) = ren ρ★ ρ N · ren ρ★ ρ M

id : ∀ {A : Set} → A → A
id x = x

sub▷★ : ∀ {𝔸 𝕏 𝕐 : Set} → (𝕏 → Term 𝔸 𝕐) → 𝕏 → Term (inc 𝔸) 𝕐
sub▷★ σ = ren S id ∘ σ

sub▷ : ∀ {𝔸 𝕏 𝕐 : Set} → (𝕏 → Term 𝔸 𝕐) → inc 𝕏 → Term 𝔸 (inc 𝕐)
sub▷ σ Z = ` Z
sub▷ σ (S x) = ren id S (σ x)

sub : ∀ {𝔸 𝔹 𝕏 𝕐} → (𝔸 → Type 𝔹) → (𝕏 → Term 𝔹 𝕐) → Term 𝔸 𝕏 → Term 𝔹 𝕐
sub σ★ σ (` x) = σ x
sub σ★ σ (ƛ M) = ƛ sub σ★ (sub▷ σ) M
sub σ★ σ (Λ M) = Λ sub (sub★▷ σ★) (sub▷★ σ) M
sub σ★ σ (M ∙ B) = sub σ★ σ M ∙ sub★ σ★ B
sub σ★ σ (N · M) = sub σ★ σ N · sub σ★ σ M

_[_] : ∀ {𝔸 𝕏} → Term 𝔸 (inc 𝕏) → Term 𝔸 𝕏 → Term 𝔸 𝕏
N [ M ] = sub `_ (`_ ▷ M) N

_[_]T : ∀ {𝔸 𝕏} → Term (inc 𝔸) 𝕏 → Type 𝔸 → Term 𝔸 𝕏
N [ B ]T = sub (`_ ▷ B) `_ N

data Value {𝔸 𝕏 : Set} : Term 𝔸 𝕏 → Set where
  ƛ_ : (N : Term 𝔸 (inc 𝕏)) → Value (ƛ N)
  Λ_ : {N : Term (inc 𝔸) 𝕏} → Value N → Value (Λ N)

data _↦_ {𝔸 𝕏 : Set} : Term 𝔸 𝕏 → Term 𝔸 𝕏 → Set where
  β-ƛ : ∀ {N V} → Value V → (ƛ N) · V ↦ N [ V ]
  β-Λ : ∀ {N A} → (Λ N) ∙ A ↦ N [ A ]T
  Λ[_] : ∀ {N N'} → N ↦ N' → Λ N ↦ Λ N'
  [_]∙_ : ∀ {N N'} → N ↦ N' → (B : Type 𝔸) → N ∙ B ↦ N' ∙ B
  _·[_] : ∀ L {M M'} → M ↦ M' → L · M ↦ L · M'
  [_]·_ : ∀ {L L'} → L ↦ L' → ∀ {V} → Value V → L · V ↦ L' · V

-- * Renaming is a functor

map-inc-id' : ∀ {𝔸 : Set} {ρ : 𝔸 → 𝔸} →
  (∀ α → ρ α ≡ α) → (A : inc 𝔸) →
  map-inc ρ A ≡ A
map-inc-id' ρ-id Z = refl
map-inc-id' ρ-id (S α) = cong S (ρ-id α)

map-inc∘map-inc : ∀ {𝔸 𝔹 ℂ : Set} {ρ : 𝔸 → 𝔹} {ρ' : 𝔹 → ℂ} {ρ'' : 𝔸 → ℂ}
  → (∀ α → ρ' (ρ α) ≡ ρ'' α)
  → (A : inc 𝔸)
  → map-inc ρ' (map-inc ρ A) ≡ map-inc ρ'' A
map-inc∘map-inc ρ≡ Z = refl
map-inc∘map-inc ρ≡ (S α) = cong S (ρ≡ α)

ren★id' : ∀ {𝔸 : Set} {ρ : 𝔸 → 𝔸}
  → (∀ α → ρ α ≡ α) → {A : Type 𝔸}
  → ren★ ρ A ≡ A
ren★id' ρ-id {A = ` α} = cong `_ (ρ-id α)
ren★id' ρ-id {A = [∀] k A} = cong ([∀] k) (ren★id' (map-inc-id' ρ-id))
ren★id' ρ-id {A = A ⇒ B} = cong₂ _⇒_ (ren★id' ρ-id) (ren★id' ρ-id)

ren★id : ∀ {𝔸 : Set} {A : Type 𝔸} → ren★ (λ α → α) A ≡ A
ren★id = ren★id' (λ α → refl)

ren★∘ren★ : ∀ {𝔸 𝔹 ℂ : Set} {ρ : 𝔸 → 𝔹} {ρ' : 𝔹 → ℂ} {ρ'' : 𝔸 → ℂ}
  → (∀ α → ρ' (ρ α) ≡ ρ'' α)
  → (A : Type 𝔸)
  → ren★ ρ' (ren★ ρ A) ≡ ren★ ρ'' A
ren★∘ren★ ρ≡ (` α) = cong `_ (ρ≡ α)
ren★∘ren★ ρ≡ ([∀] k A) = cong ([∀] k) (ren★∘ren★ (map-inc∘map-inc ρ≡) A)
ren★∘ren★ ρ≡ (A ⇒ B) = cong₂ _⇒_ (ren★∘ren★ ρ≡ A) (ren★∘ren★ ρ≡ B)

-- * Renaming and substitution are a monad

sub★▷id : ∀ {𝔸 : Set} {σ : 𝔸 → Type 𝔸}
  → (∀ α → σ α ≡ ` α)
  → (∀ α → sub★▷ σ α ≡ ` α)
sub★▷id σ≡ Z = refl
sub★▷id σ≡ (S α) = cong (ren★ S) (σ≡ α)

sub★id' : ∀ {𝔸 : Set} {σ : 𝔸 → Type 𝔸}
  → (∀ α → σ α ≡ ` α)
  → ∀ A → sub★ σ A ≡ A
sub★id' σ≡ (` α) = σ≡ α
sub★id' σ≡ ([∀] k A) = cong ([∀] k) (sub★id' (sub★▷id σ≡) A)
sub★id' σ≡ (A ⇒ B) = cong₂ _⇒_ (sub★id' σ≡ A) (sub★id' σ≡ B)

sub★id : ∀ {𝔸 : Set} (A : Type 𝔸) → sub★ `_ A ≡ A
sub★id = sub★id' (λ _ → refl)

ren★▷-is-sub★▷ : ∀ {𝔸 𝔹 : Set} {ρ : 𝔸 → 𝔹} {σ : 𝔸 → Type 𝔹}
  → ((α : 𝔸) → ` ρ α ≡ σ α)
  → ((α : inc 𝔸) → ` map-inc ρ α ≡ sub★▷ σ α)
ren★▷-is-sub★▷ ρ≡σ Z = refl
ren★▷-is-sub★▷ ρ≡σ (S α) = cong (ren★ S) (ρ≡σ α)

ren★-is-sub★ : ∀ {𝔸 𝔹 : Set} {ρ : 𝔸 → 𝔹} {σ : 𝔸 → Type 𝔹}
  → ((α : 𝔸) → ` ρ α ≡ σ α)
  → (A : Type 𝔸) → ren★ ρ A ≡ sub★ σ A
ren★-is-sub★ ρ≡σ (` α) = ρ≡σ α
ren★-is-sub★ ρ≡σ ([∀] k A) = cong ([∀] k) (ren★-is-sub★ (ren★▷-is-sub★▷ ρ≡σ) A)
ren★-is-sub★ ρ≡σ (A ⇒ B) = cong₂ _⇒_ (ren★-is-sub★ ρ≡σ A) (ren★-is-sub★ ρ≡σ B)

sub★▷∘map-inc : ∀ {𝔸 𝔹 ℂ : Set} {ρ : 𝔸 → 𝔹} {σ' : 𝔹 → Type ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → σ' (ρ α) ≡ σ'' α)
  → (α : inc 𝔸) → sub★▷ σ' (map-inc ρ α) ≡ sub★▷ σ'' α
sub★▷∘map-inc σ≡ Z = refl
sub★▷∘map-inc σ≡ (S α) = cong (ren★ S) (σ≡ α)

sub★∘ren★ : ∀ {𝔸 𝔹 ℂ : Set} {ρ : 𝔸 → 𝔹} {σ' : 𝔹 → Type ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → σ' (ρ α) ≡ σ'' α)
  → (A : Type 𝔸) → sub★ σ' (ren★ ρ A) ≡ sub★ σ'' A
sub★∘ren★ σ≡ (` α) = σ≡ α
sub★∘ren★ σ≡ ([∀] k A) = cong ([∀] k) (sub★∘ren★ (sub★▷∘map-inc σ≡) A)
sub★∘ren★ σ≡ (A ⇒ B) = cong₂ _⇒_ (sub★∘ren★ σ≡ A) (sub★∘ren★ σ≡ B)

map-inc∘sub★▷ : ∀ {𝔸 𝔹 ℂ : Set} {σ : 𝔸 → Type 𝔹} {ρ' : 𝔹 → ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → ren★ ρ' (σ α) ≡ σ'' α)
  → ((α : inc 𝔸) → ren★ (map-inc ρ') (sub★▷ σ α) ≡ sub★▷ σ'' α)
map-inc∘sub★▷ σ≡ Z = refl
map-inc∘sub★▷ {σ = σ} {ρ'} {σ''} σ≡ (S α) = begin
    ren★ (map-inc ρ') (ren★ S (σ α)) ≡⟨ ren★∘ren★ (λ α → refl) (σ α) ⟩
    ren★ (S ∘ ρ') (σ α) ≡⟨ sym (ren★∘ren★ (λ α → refl) (σ α)) ⟩
    ren★ S (ren★ ρ' (σ α)) ≡⟨ cong (ren★ S) (σ≡ α) ⟩
    ren★ S (σ'' α) ∎
  where open ≡-Reasoning

ren★∘sub★ : ∀ {𝔸 𝔹 ℂ : Set} {σ : 𝔸 → Type 𝔹} {ρ' : 𝔹 → ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → ren★ ρ' (σ α) ≡ σ'' α)
  → (A : Type 𝔸) → ren★ ρ' (sub★ σ A) ≡ sub★ σ'' A
ren★∘sub★ σ≡ (` α) = σ≡ α
ren★∘sub★ σ≡ ([∀] k A) = cong ([∀] k) (ren★∘sub★ (map-inc∘sub★▷ σ≡) A)
ren★∘sub★ σ≡ (A ⇒ B) = cong₂ _⇒_ (ren★∘sub★ σ≡ A) (ren★∘sub★ σ≡ B)

sub★▷-weaken : ∀ {𝔸 𝔹 : Set} {σ : 𝔸 → Type 𝔹}
  → (A : Type 𝔸) → sub★ (sub★▷ σ) (ren★ S A) ≡ ren★ S (sub★ σ A)
sub★▷-weaken {σ = σ} A = begin
    sub★ (sub★▷ σ) (ren★ S A) ≡⟨ sub★∘ren★ (λ β → refl) A ⟩
    sub★ (ren★ S ∘ σ) A ≡⟨ sym (ren★∘sub★ (λ β → refl) A) ⟩
    ren★ S (sub★ σ A) ∎
  where open ≡-Reasoning

sub★▷-∘ : ∀ {𝔸 𝔹 ℂ : Set} {σ : 𝔸 → Type 𝔹} {σ' : 𝔹 → Type ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → sub★ σ' (σ α) ≡ σ'' α)
  → (α : inc 𝔸) → sub★ (sub★▷ σ') (sub★▷ σ α) ≡ sub★▷ σ'' α
sub★▷-∘ σ≡ Z = refl
sub★▷-∘ {σ = σ} {σ'} {σ''} σ≡ (S α) = begin
    sub★ (sub★▷ σ') (ren★ S (σ α)) ≡⟨ sub★▷-weaken (σ α) ⟩
    ren★ S (sub★ σ' (σ α)) ≡⟨ cong (ren★ S) (σ≡ α) ⟩
    ren★ S (σ'' α) ∎
  where open ≡-Reasoning

sub★∘sub★ : ∀ {𝔸 𝔹 ℂ : Set} {σ : 𝔸 → Type 𝔹} {σ' : 𝔹 → Type ℂ} {σ'' : 𝔸 → Type ℂ}
  → ((α : 𝔸) → sub★ σ' (σ α) ≡ σ'' α)
  → (A : Type 𝔸) → sub★ σ' (sub★ σ A) ≡ sub★ σ'' A
sub★∘sub★ σ≡ (` α) = σ≡ α
sub★∘sub★ σ≡ ([∀] k A) = cong ([∀] k) (sub★∘sub★ (sub★▷-∘ σ≡) A)
sub★∘sub★ σ≡ (A ⇒ B) = cong₂ _⇒_ (sub★∘sub★ σ≡ A) (sub★∘sub★ σ≡ B)

[]★∘weaken : ∀ {𝔸} {B : Type 𝔸} A
  → ren★ S A [ B ]★ ≡ A
[]★∘weaken {B = B} A = begin
    sub★ (`_ ▷ B) (ren★ S A) ≡⟨ sub★∘ren★ (λ β → refl) A ⟩
    sub★ `_ A ≡⟨ sub★id A ⟩
    A ∎
  where open ≡-Reasoning

-- * Renaming preserves typing

_⊢★ᴿ_⦂_ : ∀ {𝔸 𝔹 : Set} (Δ' : 𝔹 → Kind) (ρ : 𝔸 → 𝔹) (Δ : 𝔸 → Kind) → Set
Δ' ⊢★ᴿ ρ ⦂ Δ = ∀ α → Δ' (ρ α) ≡ Δ α

⊢★ren★▷ : ∀ {𝔸 𝔹} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {ρ★ : 𝔸 → 𝔹} {k : Kind}
  → Δ' ⊢★ᴿ ρ★ ⦂ Δ
  → Δ' ▷ k ⊢★ᴿ map-inc ρ★ ⦂ Δ ▷ k
⊢★ren★▷ ⊢ρ Z = refl
⊢★ren★▷ ⊢ρ (S α) = ⊢ρ α

_⦂_↝★_ : ∀ {𝔸 𝔹} → (Type 𝔸 → Type 𝔹) → (𝔸 → Kind) → (𝔹 → Kind) → Set
f ⦂ Δ ↝★ Δ' = ∀ {k} {A} → Δ ⊢★ A ⦂ k → Δ' ⊢★ f A ⦂ k

⊢★ren★ : ∀ {𝔸 𝔹} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {ρ★ : 𝔸 → 𝔹}
  → Δ' ⊢★ᴿ ρ★ ⦂ Δ
  → ren★ ρ★ ⦂ Δ ↝★ Δ'
⊢★ren★ ⊢ρ (` α) = subst (_ ⊢★ _ ⦂_) (⊢ρ α) (` _)
⊢★ren★ ⊢ρ ([∀] ⊢A) = [∀] (⊢★ren★ (⊢★ren★▷ ⊢ρ) ⊢A)
⊢★ren★ ⊢ρ (⊢A ⇒ ⊢B) = ⊢★ren★ ⊢ρ ⊢A ⇒ ⊢★ren★ ⊢ρ ⊢B

_⊢ᴿ_⨟_⦂_ : ∀ {𝔸 𝔹 𝕏 𝕐 : Set} (Γ' : 𝕐 → Type 𝔹) (ρ★ : 𝔸 → 𝔹) (ρ : 𝕏 → 𝕐) (Γ : 𝕏 → Type 𝔸) → Set
Γ' ⊢ᴿ ρ★ ⨟ ρ ⦂ Γ = ∀ x → Γ' (ρ x) ≡ ren★ ρ★ (Γ x)

_⨟_⦂_⨟_↝_⨟_ : ∀ {𝔸 𝔹 𝕏 𝕐 : Set} (f★ : Type 𝔸 → Type 𝔹) (f : Term 𝔸 𝕏 → Term 𝔹 𝕐) (Δ : 𝔸 → Kind) (Γ : 𝕏 → Type 𝔸) (Δ' : 𝔹 → Kind) (Γ' : 𝕐 → Type 𝔹) → Set
f★ ⨟ f ⦂ Δ ⨟ Γ ↝ Δ' ⨟ Γ' = ∀ {A} {M} → Δ ⨟ Γ ⊢ M ⦂ A → Δ' ⨟ Γ' ⊢ f M ⦂ f★ A

⊢ren▷ : ∀ {𝔸 𝔹 𝕏 𝕐} {Γ : 𝕏 → Type 𝔸} {Γ' : 𝕐 → Type 𝔹} {ρ★ : 𝔸 → 𝔹} {ρ : 𝕏 → 𝕐} {A}
  → Γ' ⊢ᴿ ρ★ ⨟ ρ ⦂ Γ
  → Γ' ▷ ren★ ρ★ A ⊢ᴿ ρ★ ⨟ map-inc ρ ⦂ Γ ▷ A
⊢ren▷ ⊢ρ Z = refl
⊢ren▷ ⊢ρ (S x) = ⊢ρ x

⊢ren★▷ : ∀ {𝔸 𝔹 𝕏 𝕐} {Γ Γ'} {ρ★ : 𝔸 → 𝔹} {ρ : 𝕏 → 𝕐}
  → Γ' ⊢ᴿ ρ★ ⨟ ρ ⦂ Γ
  → ren★ S ∘ Γ' ⊢ᴿ map-inc ρ★ ⨟ ρ ⦂ ren★ S ∘ Γ
⊢ren★▷ {Γ = Γ} {Γ'} {ρ★} {ρ} ⊢ρ x = begin
    ren★ S (Γ' (ρ x)) ≡⟨ cong (ren★ S) (⊢ρ x) ⟩
    ren★ S (ren★ ρ★ (Γ x)) ≡⟨ ren★∘ren★ (λ α → refl) (Γ x) ⟩
    ren★ (S ∘ ρ★) (Γ x) ≡⟨ sym (ren★∘ren★ (λ α → refl) (Γ x)) ⟩
    ren★ (map-inc ρ★) (ren★ S (Γ x)) ∎
  where open ≡-Reasoning

ren★∘▷ : ∀ {𝔸 𝔹} {ρ★ : 𝔸 → 𝔹} {B} α
  → ren★ ρ★ ((`_ ▷ B) α) ≡ ((`_ ▷ ren★ ρ★ B) (map-inc ρ★ α))
ren★∘▷ Z = refl
ren★∘▷ (S α) = refl

ren★∘[]★ : ∀ {𝔸 𝔹} {ρ★ : 𝔸 → 𝔹} A B → ren★ ρ★ (A [ B ]★) ≡ ren★ (map-inc ρ★) A [ ren★ ρ★ B ]★
ren★∘[]★ {ρ★ = ρ★} A B = begin
  ren★ ρ★ (sub★ (`_ ▷ B) A) ≡⟨ ren★∘sub★ (λ β → refl) A ⟩
  sub★ (ren★ ρ★ ∘ (`_ ▷ B)) A ≡⟨ sym (sub★∘ren★ (sym ∘ ren★∘▷) A) ⟩
  sub★ (`_ ▷ ren★ ρ★ B) (ren★ (map-inc ρ★) A) ∎
  where open ≡-Reasoning

⊢ren : ∀ {𝔸 𝔹 𝕏 𝕐} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {Γ : 𝕏 → Type 𝔸} {Γ' : 𝕐 → Type 𝔹} {ρ★ : 𝔸 → 𝔹} {ρ : 𝕏 → 𝕐}
  → Δ' ⊢★ᴿ ρ★ ⦂ Δ
  → Γ' ⊢ᴿ ρ★ ⨟ ρ ⦂ Γ
  → ren★ ρ★ ⨟ ren ρ★ ρ ⦂ Δ ⨟ Γ ↝ Δ' ⨟ Γ'
⊢ren ⊢ρ★ ⊢ρ (` x) = subst (_ ⨟ _ ⊢ _ ⦂_) (⊢ρ x) (` _)
⊢ren ⊢ρ★ ⊢ρ (ƛ ⊢M) = ƛ ⊢ren ⊢ρ★ (⊢ren▷ ⊢ρ) ⊢M
⊢ren {Γ' = Γ'} ⊢ρ★ ⊢ρ (Λ ⊢M) = Λ ⊢ren (⊢★ren★▷ ⊢ρ★) (⊢ren★▷ {Γ' = Γ'} ⊢ρ) ⊢M
⊢ren ⊢ρ★ ⊢ρ (_∙_ {A = A} {B = B} ⊢L ⊢B) = subst (_ ⨟ _ ⊢ _ ⦂_) (sym (ren★∘[]★ A B)) (⊢ren ⊢ρ★ ⊢ρ ⊢L ∙ ⊢★ren★ ⊢ρ★ ⊢B)
⊢ren ⊢ρ★ ⊢ρ (⊢L · ⊢M) = ⊢ren ⊢ρ★ ⊢ρ ⊢L · ⊢ren ⊢ρ★ ⊢ρ ⊢M

-- * Substitution preserves typing

_⊢★ˢ_⦂_ : ∀ {𝔸 𝔹 : Set} (Δ' : 𝔹 → Kind) (σ : 𝔸 → Type 𝔹) (Δ : 𝔸 → Kind) → Set
Δ' ⊢★ˢ σ ⦂ Δ = ∀ α → Δ' ⊢★ σ α ⦂ Δ α

⊢★sub★▷ : ∀ {𝔸 𝔹} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {σ★ : 𝔸 → Type 𝔹} {k : Kind}
  → ((α : 𝔸) → Δ' ⊢★ σ★ α ⦂ Δ α)
  → ((α : inc 𝔸) → Δ' ▷ k ⊢★ sub★▷ σ★ α ⦂ (Δ ▷ k) α)
⊢★sub★▷ ⊢★σ★ Z = ` Z
⊢★sub★▷ ⊢★σ★ (S α) = ⊢★ren★ (λ _ → refl) (⊢★σ★ α)

⊢★sub★ : ∀ {𝔸 𝔹} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {σ★ : 𝔸 → Type 𝔹} {A : Type 𝔸} {k : Kind}
  → (∀ α → Δ' ⊢★ σ★ α ⦂ Δ α)
  → Δ ⊢★ A ⦂ k
  → Δ' ⊢★ sub★ σ★ A ⦂ k
⊢★sub★ ⊢★σ★ (` α) = ⊢★σ★ α
⊢★sub★ ⊢★σ★ ([∀] ⊢A) = [∀] (⊢★sub★ (⊢★sub★▷ ⊢★σ★) ⊢A)
⊢★sub★ ⊢★σ★ (⊢A ⇒ ⊢B) = ⊢★sub★ ⊢★σ★ ⊢A ⇒ ⊢★sub★ ⊢★σ★ ⊢B

sub★∘▷ : ∀ {𝔸 𝔹} {σ★ : 𝔸 → Type 𝔹} {B} α
  → sub★ σ★ ((`_ ▷ B) α) ≡ sub★ (`_ ▷ sub★ σ★ B) (sub★▷ σ★ α)
sub★∘▷ Z = refl
sub★∘▷ {σ★ = σ★} {B} (S α) = begin
  σ★ α ≡⟨ sym (sub★id (σ★ α)) ⟩
  sub★ `_ (σ★ α) ≡⟨ sym (sub★∘ren★ (λ β → refl) (σ★ α)) ⟩
  sub★ (`_ ▷ sub★ σ★ B) (ren★ S (σ★ α)) ∎
  where open ≡-Reasoning

sub★∘[]★ : ∀ {𝔸 𝔹} {σ★ : 𝔸 → Type 𝔹} A B → sub★ σ★ (A [ B ]★) ≡ sub★ (sub★▷ σ★) A [ sub★ σ★ B ]★
sub★∘[]★ {σ★ = σ★} A B = begin
  sub★ σ★ (sub★ (`_ ▷ B) A) ≡⟨ sub★∘sub★ (λ β → refl) A ⟩
  sub★ (sub★ σ★ ∘ (`_ ▷ B)) A ≡⟨ sym (sub★∘sub★ (sym ∘ sub★∘▷) A) ⟩
  sub★ (`_ ▷ sub★ σ★ B) (sub★ (sub★▷ σ★) A) ∎
  where open ≡-Reasoning

⊢sub▷ : ∀ {𝔸 𝔹 𝕏 𝕐} {Δ' : 𝔹 → Kind} {Γ : 𝕏 → Type 𝔸} {Γ' : 𝕐 → Type 𝔹} {σ★ : 𝔸 → Type 𝔹} {σ : 𝕏 → Term 𝔹 𝕐} {A : Type 𝔸}
  → (∀ x → Δ' ⨟ Γ' ⊢ σ x ⦂ sub★ σ★ (Γ x))
  → (x : inc 𝕏) → Δ' ⨟ Γ' ▷ sub★ σ★ A ⊢ sub▷ σ x ⦂ sub★ σ★ ((Γ ▷ A) x)
⊢sub▷ ⊢σ Z = ` Z
⊢sub▷ ⊢σ (S α) = subst (_ ⨟ _ ⊢ _ ⦂_) ren★id (⊢ren (λ α → refl) (λ x → sym ren★id) (⊢σ α))

⊢sub▷★ : ∀ {𝔸 𝔹 𝕏 𝕐} {Δ' : 𝔹 → Kind} {Γ : 𝕏 → Type 𝔸} {Γ' : 𝕐 → Type 𝔹} {σ★ : 𝔸 → Type 𝔹} {σ : 𝕏 → Term 𝔹 𝕐} {k}
  → (∀ x → Δ' ⨟ Γ' ⊢ σ x ⦂ sub★ σ★ (Γ x))
  → (x : 𝕏) → Δ' ▷ k ⨟ ren★ S ∘ Γ' ⊢ sub▷★ σ x ⦂ sub★ (sub★▷ σ★) (ren★ S (Γ x))
⊢sub▷★ {Γ = Γ} ⊢σ x = subst (_ ⨟ _ ⊢ _ ⦂_) (sym (sub★▷-weaken (Γ x))) (⊢ren (λ α → refl) (λ x → refl) (⊢σ x))

⊢sub : ∀ {𝔸 𝔹 𝕏 𝕐} {Δ : 𝔸 → Kind} {Δ' : 𝔹 → Kind} {Γ : 𝕏 → Type 𝔸} {Γ' : 𝕐 → Type 𝔹} {σ★ : 𝔸 → Type 𝔹} {σ : 𝕏 → Term 𝔹 𝕐} {A : Type 𝔸} {M : Term 𝔸 𝕏}
  → (∀ α → Δ' ⊢★ σ★ α ⦂ Δ α)
  → (∀ x → Δ' ⨟ Γ' ⊢ σ x ⦂ sub★ σ★ (Γ x))
  → Δ ⨟ Γ ⊢ M ⦂ A
  → Δ' ⨟ Γ' ⊢ sub σ★ σ M ⦂ sub★ σ★ A
⊢sub ⊢σ★ ⊢σ (` x) = ⊢σ x
⊢sub ⊢σ★ ⊢σ (ƛ ⊢N) = ƛ ⊢sub ⊢σ★ (⊢sub▷ ⊢σ) ⊢N
⊢sub {Γ = Γ} ⊢σ★ ⊢σ (Λ ⊢N) = Λ ⊢sub (⊢★sub★▷ ⊢σ★) (⊢sub▷★ {Γ = Γ} ⊢σ) ⊢N
⊢sub ⊢σ★ ⊢σ (_∙_ {A = A} ⊢L ⊢B) = subst (_ ⨟ _ ⊢ _ ⦂_) (sym (sub★∘[]★ A _)) (⊢sub ⊢σ★ ⊢σ ⊢L ∙ ⊢★sub★ ⊢σ★ ⊢B)
⊢sub ⊢σ★ ⊢σ (⊢L · ⊢M) = ⊢sub ⊢σ★ ⊢σ ⊢L · ⊢sub ⊢σ★ ⊢σ ⊢M

⊢▷ : ∀ {𝔸 𝕏} {Δ : 𝔸 → Kind} {Γ : 𝕏 → Type 𝔸} {M} {A}
  → Δ ⨟ Γ ⊢ M ⦂ A
  → ∀ x → Δ ⨟ Γ ⊢ (`_ ▷ M) x ⦂ (Γ ▷ A) x
⊢▷ ⊢M Z = ⊢M
⊢▷ ⊢M (S α) = ` α

⊢★▷ : ∀ {𝔸} {Δ : 𝔸 → Kind} {B} {k}
  → Δ ⊢★ B ⦂ k
  → ∀ α → Δ ⊢★ (`_ ▷ B) α ⦂ (Δ ▷ k) α
⊢★▷ ⊢B Z = ⊢B
⊢★▷ ⊢B (S α) = ` α

⊢[] : ∀ {𝔸 𝕏} {Δ : 𝔸 → Kind} {Γ : 𝕏 → Type 𝔸} {N : Term 𝔸 (inc 𝕏)} {M : Term 𝔸 𝕏} {A} {B} → Δ ⨟ Γ ▷ A ⊢ N ⦂ B → Δ ⨟ Γ ⊢ M ⦂ A → Δ ⨟ Γ ⊢ N [ M ] ⦂ B
⊢[] ⊢N ⊢M = subst (_ ⨟ _ ⊢ _ ⦂_) (sub★id _) (⊢sub `_ (subst (_ ⨟ _ ⊢ _ ⦂_) (sym (sub★id _)) ∘ ⊢▷ ⊢M) ⊢N)

⊢[]T : ∀ {𝔸 𝕏} {Δ : 𝔸 → Kind} {Γ : 𝕏 → Type 𝔸} {N : Term (inc 𝔸) 𝕏} {B : Type 𝔸} {A} {k} → Δ ▷ k ⨟ ren★ S ∘ Γ ⊢ N ⦂ A → Δ ⊢★ B ⦂ k → Δ ⨟ Γ ⊢ N [ B ]T ⦂ A [ B ]★
⊢[]T ⊢N ⊢B = ⊢sub (⊢★▷ ⊢B) (λ x → subst (_ ⨟ _ ⊢ _ ⦂_) (sym ([]★∘weaken _)) (` x)) ⊢N

data Progress {𝔸 : Set} (Δ : 𝔸 → Kind) (Γ : ⊥ → Type 𝔸) (A : Type 𝔸) (M : Term 𝔸 ⊥) : Set where
  Done : Value M → Progress Δ Γ A M
  Step : ∀ {M'} → M ↦ M' → Δ ⨟ Γ ⊢ M' ⦂ A → Progress Δ Γ A M

progress : {𝔸 : Set} {Δ : 𝔸 → Kind} {Γ : ⊥ → Type 𝔸} {A : Type 𝔸} {M : Term 𝔸 ⊥} → Δ ⨟ Γ ⊢ M ⦂ A → Progress Δ Γ A M
progress {M = ƛ N} (ƛ ⊢N) = Done (ƛ N) 
progress (Λ ⊢N) with progress ⊢N
... | Done v = Done (Λ v)
... | Step M↦M' ⊢M' = Step (Λ[ M↦M' ]) (Λ ⊢M')
progress {M = L ∙ B} (⊢L ∙ ⊢B) with progress ⊢L | ⊢L
... | Done (Λ N) | Λ ⊢N = Step β-Λ (⊢sub (⊢★▷ ⊢B) (λ()) ⊢N)
... | Step L↦L' ⊢L' | _ = Step ([ L↦L' ]∙ B) (⊢L' ∙ ⊢B)
progress {M = L · M} (⊢L · ⊢M) with progress ⊢M
... | Step M↦M' ⊢M' = Step (L ·[ M↦M' ]) (⊢L · ⊢M')
... | Done v with progress ⊢L | ⊢L
...   | Step L↦L' ⊢L' | _ = Step ([ L↦L' ]· v) (⊢L' · ⊢M)
...   | Done (ƛ N) | ƛ ⊢N = Step (β-ƛ v) (⊢[] ⊢N ⊢M)

Simple Blame Calculus with proof relevant casts.
Uses polarity to unify upcasts and downcasts.
Uses nested evaluation contexts.

Siek, Thiemann, and Wadler

```
module Core where

open import Data.Nat using (ℕ; zero; suc; _+_)
open import Data.Bool using (true; false) renaming (Bool to 𝔹)
open import Data.Unit using (⊤; tt)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂) renaming ([_,_] to case-⊎)
open import Relation.Binary.PropositionalEquality
     using (_≡_; _≢_; refl; trans; sym; cong; cong₂; cong-app; subst; inspect)
     renaming ([_] to [[_]])
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Decidable using (⌊_⌋; True; toWitness; fromWitness)
```

## Base types

```
data Base : Set where
  ′ℕ : Base
  ′𝔹 : Base
```

Interpretation of base types into Agda types.
```
rep : Base → Set 
rep ′ℕ  =  ℕ
rep ′𝔹  =  𝔹
```

Decision procedure for equality of base types.
```
_≡$?_ : (ι : Base) → (ι′ : Base) → Dec (ι ≡ ι′)
′ℕ  ≡$? ′ℕ  =  yes refl
′ℕ  ≡$? ′𝔹  =  no (λ ())
′𝔹  ≡$? ′ℕ  =  no (λ ())
′𝔹  ≡$? ′𝔹  =  yes refl
```


## Types

```
infixr 7 _⇒_
infix  8 $_

data Type : Set where
  ★ : Type
  $_ : (ι : Base) → Type
  _⇒_ : (A : Type) → (B : Type) → Type
```

Decision procedure for equality of types.
```
infix 4 _≡?_

_≡?_ : (A : Type) → (B : Type) → Dec (A ≡ B)
★       ≡? ★                                   =  yes refl
★       ≡? ($ _)                               =  no (λ ())
★       ≡? (_ ⇒ _)                             =  no (λ ())
($ _)   ≡? ★                                   =  no (λ ())
($ ι)   ≡? ($ κ)     with ι ≡$? κ
...                     | yes refl             =  yes refl
...                     | no  ι≢κ              =  no  (λ{refl → ι≢κ refl})
($ _)   ≡? (_ ⇒ _)                             =  no  (λ ())
(_ ⇒ _) ≡? ★                                   =  no  (λ ())
(_ ⇒ _) ≡? ($ _)                               =  no  (λ ())
(A ⇒ B) ≡? (A′ ⇒ B′) with A ≡? A′  | B ≡? B′ 
...                     | yes refl | yes refl  =  yes refl
...                     | no  A≢A′ | _         =  no  (λ{refl → A≢A′ refl})
...                     | _        | no  B≢B′  =  no  (λ{refl → B≢B′ refl})
```

## Ground types

```
data Ground : Type → Set where
  $_  :
       (ι : Base) 
       ------------
     → Ground ($ ι)

  ★⇒★ :
       --------------
       Ground (★ ⇒ ★)
```

Extract type from evidence that it is ground
```
ground : ∀ {G} → (g : Ground G) → Type
ground {G = G} g  =  G
```

Evidence for a ground type is unique.
```
uniqueG : ∀ {G} → (g : Ground G) → (h : Ground G) → g ≡ h
uniqueG ($ ι) ($ .ι) = refl
uniqueG ★⇒★   ★⇒★    = refl
```

Star is not ground
```
G≢★ : ∀ {G} → (g : Ground G) → G ≢ ★
G≢★ () refl
```

Decision procedure for whether a type is ground.
```
Ground? : ∀(A : Type) → Dec (Ground A)
Ground? ★                                 =  no λ ()
Ground? ($ ι)                             =  yes ($ ι)
Ground? (A ⇒ B) with A ≡? ★   | B ≡? ★  
...                | yes refl | yes refl  =  yes ★⇒★
...                | no  A≢★  | _         =  no  λ{★⇒★ → A≢★ refl}
...                | _        | no  B≢★   =  no  λ{★⇒★ → B≢★ refl}
```

## Precision

```
infix 4 _≤_
infixl 5 _⇑_

data _≤_ : Type → Type → Set where

  id : ∀ {A}
      -----
    → A ≤ A

  _⇑_ : ∀ {A G}
    → A ≤ G
    → Ground G
      -----
    → A ≤ ★

  _⇒_ : ∀ {A B A′ B′}
    → A ≤ A′
    → B ≤ B′
      ---------------
    → A ⇒ B ≤ A′ ⇒ B′
```

Domain and codomain of function precision.

```
dom : ∀ {A B A′ B′} → A ⇒ B ≤ A′ ⇒ B′ → A ≤ A′
dom id       =  id
dom (p ⇒ q)  =  p

cod : ∀ {A B A′ B′} → A ⇒ B ≤ A′ ⇒ B′ → B ≤ B′
cod id       =  id
cod (p ⇒ q)  =  q
```

The use of these two functions is reminiscent of some gradually-typed
source languages, where one defines

    dom ★        =  ★
    dom (A ⇒ B)  =  A

    cod ★        =  ★
    cod (A ⇒ B)  =  B

and has a typing rules resembling

    Γ ⊢ L : A
    Γ ⊢ M : dom A
    ------------------
    Γ ⊢ L · M : cod A

Our dom and cod will play a similar role when we define the
precedence rules for abstraction and application.

Lemma. Every ground type is more precise than ★.
```
G≤★ : ∀ {G} → Ground G → G ≤ ★
G≤★ ($ ι)  =  id ⇑ $ ι
G≤★ ★⇒★    =  (id ⇒ id) ⇑ ★⇒★
```

Lemma. ★ is not more precise than any ground type.
```
¬★≤G : ∀ {G} → Ground G → ¬ (★ ≤ G)
¬★≤G ($ ι) ()
¬★≤G ★⇒★   ()
```

Lemma. ★ is least precise.
```
★≤ : ∀ {A} → ★ ≤ A → A ≡ ★
★≤ {★} p  =  refl
★≤ {$ ι} ()
★≤ {A ⇒ B} ()
```

Lemma. Every type is more precise that ★. (Not true in general.)
```
A≤★ : ∀ {A} → A ≤ ★
A≤★ {★}      =  id
A≤★ {$ ι}    =  id ⇑ $ ι
A≤★ {A ⇒ B}  =  (A≤★ ⇒ A≤★) ⇑ ★⇒★
```

Lemma. Every type is either ★ or more precise than a ground type. (Not true in general.)
```
★⊎G : ∀ A → (A ≡ ★) ⊎ ∃[ G ](Ground G × A ≤ G)
★⊎G ★        =  inj₁ refl
★⊎G ($ ι)    =  inj₂ ($ ι , $ ι , id)
★⊎G (A ⇒ B)  =  inj₂ (★ ⇒ ★ , ★⇒★ , A≤★ ⇒ A≤★)
```

Lemma. If a type is more precise than a ground type, it is not ★.
```
≢★ : ∀ {A G} → Ground G → A ≤ G → A ≢ ★
≢★ g A≤G A≡★ rewrite A≡★ = ¬★≤G g A≤G
```

Lemma. ≤ is transitive
```
_⨟_ : ∀ {A B C} → A ≤ B → B ≤ C → A ≤ C
p ⨟ id                     =  p
p ⨟ (q ⇑ g)                =  (p ⨟ q) ⇑ g
_⨟_ {A = _ ⇒ _} p (q ⇒ r)  =  (dom p ⨟ q) ⇒ (cod p ⨟ r)
```

Lemmas. Left and right identity.
```
left-id : ∀ {A B} → (p : A ≤ B) → id ⨟ p ≡ p
left-id id                                     =  refl
left-id (p ⇑ g) rewrite left-id p              =  refl
left-id (p ⇒ q) rewrite left-id p | left-id q  =  refl
```

```
right-id : ∀ {A B} → (p : A ≤ B) → p ⨟ id {B} ≡ p
right-id p  =  refl
```

Lemma. Associativity.
```
assoc : ∀ {A B C D} (p : A ≤ B) (q : B ≤ C) (r : C ≤ D)
  → (p ⨟ q) ⨟ r ≡ p ⨟ (q ⨟ r)
assoc p q id                                                            =  refl
assoc p q (r ⇑ g) rewrite assoc p q r                                   =  refl
assoc p id (r ⇒ r′) rewrite left-id (r ⇒ r′)                            =  refl
assoc id (q ⇒ q′) (r ⇒ r′)
  rewrite left-id q | left-id q′ | left-id (q ⨟ r) | left-id (q′ ⨟ r′)   =  refl
assoc (p ⇒ p′) (q ⇒ q′) (r ⇒ r′) rewrite assoc p q r | assoc p′ q′ r′   =  refl
```

## Lemma. dom and cod are functors

```
dom-⨟ : ∀ {A B A′ B′ A″ B″} (p : A ⇒ B ≤ A′ ⇒ B′) (q : A′ ⇒ B′ ≤  A″ ⇒ B″)
    → dom p ⨟ dom q ≡ dom (p ⨟ q)
dom-⨟ id id = refl
dom-⨟ id (_ ⇒ _) = refl
dom-⨟ (_ ⇒ _) id = refl
dom-⨟ (_ ⇒ _) (_ ⇒ _) = refl

cod-⨟ : ∀ {A B A′ B′ A″ B″} (p : A ⇒ B ≤ A′ ⇒ B′) (q : A′ ⇒ B′ ≤  A″ ⇒ B″)
    → cod p ⨟ cod q ≡ cod (p ⨟ q)
cod-⨟ id id = refl
cod-⨟ id (_ ⇒ _) = refl
cod-⨟ (_ ⇒ _) id = refl
cod-⨟ (_ ⇒ _) (_ ⇒ _) = refl
```

Lemma. If `p : ★ ≤ ★` then `p ≡ id`.
```
★≤★→≡id : ∀ (p : ★ ≤ ★) → p ≡ id
★≤★→≡id id       =  refl
★≤★→≡id (p ⇑ g)  =  ⊥-elim (¬★≤G g p)
```

Decision procedure for precision.
```
_≤?_ : (A : Type) → (B : Type) → Dec (A ≤ B)
★ ≤? ★                                           =  yes id
★ ≤? ($ ι)                                       =  no (λ ())
★ ≤? (A ⇒ B)                                     =  no (λ ())
($ ι) ≤? ★                                       =  yes (id ⇑ $ ι)
($ ι) ≤? ($ ι′)       with ι ≡$? ι′
...                     | yes refl               =  yes id
...                     | no  ι≢ι′               =  no  λ{id → ι≢ι′ refl}
($ ι) ≤? (A ⇒ B)                                 =  no (λ ())
(A ⇒ B) ≤? ★         with A ≤? ★    | B ≤? ★
...                     | yes A≤★   | yes B≤★    =  yes ((A≤★ ⇒ B≤★) ⇑ ★⇒★)
...                     | no  ¬A≤★  | _          =  no  λ{((A≤★ ⇒ B≤★) ⇑ ★⇒★) → ¬A≤★ A≤★;
                                                          (id ⇑ ★⇒★)          → ¬A≤★ id}
...                     | _         | no  ¬B≤★   =  no  λ{((A≤★ ⇒ B≤★) ⇑ ★⇒★) → ¬B≤★ B≤★;
                                                          (id ⇑ ★⇒★)          → ¬B≤★ id}
(A ⇒ B) ≤? ($ ι)                                 =  no  (λ ())
(A ⇒ B) ≤? (A′ ⇒ B′) with A ≤? A′   | B ≤? B′
...                     | yes A≤A′  | yes B≤B′   =  yes (A≤A′ ⇒ B≤B′)
...                     | no  ¬A≤A′ | _          =  no  λ{(A≤A′ ⇒ B≤B′) → ¬A≤A′ A≤A′;
                                                          id            → ¬A≤A′ id}
...                     | _         | no  ¬B≤B′  =  no  λ{(A≤A′ ⇒ B≤B′) → ¬B≤B′ B≤B′;
                                                          id            → ¬B≤B′ id}
```

* Contexts and Variables

```
infixl 6 _▷_

data Context : Set where
  ∅   : Context
  _▷_ : Context → Type → Context

infix  4 _∋_
infix  9 S_

data _∋_ : Context → Type → Set where

  Z : ∀ {Γ A}
      ----------
    → Γ ▷ A ∋ A

  S_ : ∀ {Γ A B}
    → Γ ∋ A
      ---------
    → Γ ▷ B ∋ A
```

## Casts

```
infix  6 _=>_
infix  6 _==>_
infix  4 +_
infix  4 -_
```


Cast
```
data _=>_ : Type → Type → Set where

  +_ : ∀ {A B}
    → A ≤ B
      ------
    → A => B

  -_ : ∀ {A B}
    → B ≤ A
      ------
    → A => B
```

Decomposing a cast
```
data _==>_ : Type → Type → Set where

  id : ∀ {A}
      -------
    → A ==> A

  _⇒_ : ∀ {A A′ B B′}
    → A′ => A
    → B => B′
      -----------------
    → A ⇒ B ==> A′ ⇒ B′

  other : ∀ {A B}
      -------
    → A ==> B

split : ∀ {A B} → A => B → A ==> B
split (+ id)     =  id
split (- id)     =  id
split (+ s ⇒ t)  =  (- s) ⇒ (+ t)
split (- s ⇒ t)  =  (+ s) ⇒ (- t)
split (+ p ⇑ g)  =  other
split (- p ⇑ g)  =  other
```

## Terms

```
infix  4 _⊢_
infixl 5 _⟨_⟩
infix  6 _·_
infix  6 _⦅_⦆_
infix  8 `_

data _⊢_ : Context → Type → Set where

  `_ : ∀ {Γ} {A}
    → Γ ∋ A
      -----
    → Γ ⊢ A

  ƛ_ :  ∀ {Γ B A}
    → Γ ▷ A ⊢ B
      ---------
    → Γ ⊢ A ⇒ B

  _·_ : ∀ {Γ} {A B}
    → Γ ⊢ A ⇒ B
    → Γ ⊢ A
      ---------
    → Γ ⊢ B

  $_ : ∀ {Γ ι}
    → rep ι
      -------
    → Γ ⊢ $ ι

  _⦅_⦆_ : ∀ {Γ ι ι′ ι″}
    → Γ ⊢ $ ι
    → (rep ι → rep ι′ → rep ι″)
    → Γ ⊢ $ ι′
      ----------------------
    → Γ ⊢ $ ι″

  _⇑_ : ∀ {Γ G}
    → Γ ⊢ G
    → Ground G
      -----
    → Γ ⊢ ★

  _⟨_⟩ : ∀ {Γ A B}
    → Γ ⊢ A
    → A => B
      ------
    → Γ ⊢ B

  blame : ∀ {Γ A}
      -----
    → Γ ⊢ A
```

## Renaming maps, substitution maps, term maps

```
_→ᴿ_ : Context → Context → Set
Γ →ᴿ Δ = ∀ {A} → Γ ∋ A → Δ ∋ A

_→ˢ_ : Context → Context → Set
Γ →ˢ Δ = ∀ {A} → Γ ∋ A → Δ ⊢ A

_→ᵀ_ : Context → Context → Set
Γ →ᵀ Δ = ∀ {A} → Γ ⊢ A → Δ ⊢ A
```


## Renaming

Extension of renaming maps
```
ren▷ : ∀ {Γ Δ A}
  → (Γ →ᴿ Δ)
    ----------------------------
  → ((Γ ▷ A) →ᴿ (Δ ▷ A))
ren▷ ρ Z      =  Z
ren▷ ρ (S x)  =  S (ρ x)

ren : ∀ {Γ Δ}
  → (Γ →ᴿ Δ)
    --------
  → (Γ →ᵀ Δ)
ren ρ (` x)          = ` (ρ x)
ren ρ (ƛ N)          =  ƛ (ren (ren▷ ρ) N)
ren ρ (L · M)        =  (ren ρ L) · (ren ρ M)
ren ρ ($ k)          =  $ k
ren ρ (L ⦅ _⊕_ ⦆ M)  =  (ren ρ L) ⦅ _⊕_ ⦆ (ren ρ M)
ren ρ (M ⇑ g)        =  (ren ρ M) ⇑ g
ren ρ (M ⟨ ±p ⟩)     =  (ren ρ M) ⟨ ±p ⟩
ren ρ blame          =  blame

lift : ∀ {Γ : Context} {A : Type} → Γ →ᵀ (Γ ▷ A)
lift = ren S_
```

## Substitution

```
sub▷ : ∀ {Γ Δ A}
  → (Γ →ˢ Δ)
    --------------------------
  → ((Γ ▷ A) →ˢ (Δ ▷ A))
sub▷ σ Z      =  ` Z
sub▷ σ (S x)  =  lift (σ x)

sub : ∀ {Γ Δ : Context}
  → (Γ →ˢ Δ)
    --------
  → (Γ →ᵀ Δ)
sub σ (` x)          =  σ x
sub σ (ƛ  N)         =  ƛ (sub (sub▷ σ) N)
sub σ (L · M)        =  (sub σ L) · (sub σ M)
sub σ ($ k)          =  $ k
sub σ (L ⦅ _⊕_ ⦆ M)  =  (sub σ L) ⦅ _⊕_ ⦆ (sub σ M)
sub σ (M ⇑ g)        =  (sub σ M) ⇑ g
sub σ (M ⟨ ±p ⟩)     =  (sub σ M) ⟨ ±p ⟩
sub σ blame          =  blame
```

Special case of substitution, used in beta rule
```
σ₀ : ∀ {Γ A} → (M : Γ ⊢ A) → (Γ ▷ A) →ˢ Γ
σ₀ M Z      =  M
σ₀ M (S x)  =  ` x

_[_] : ∀ {Γ A B}
        → Γ ▷ A ⊢ B
        → Γ ⊢ A 
          ---------
        → Γ ⊢ B
_[_] {Γ} {A} N M =  sub {Γ ▷ A} {Γ} (σ₀ M) N
```

## Composition and identity

Rename composed with rename

```
ren∘ren▷ : ∀ {Γ Γ′ Γ″} {ρ : Γ →ᴿ Γ′} {ρ′ : Γ′ →ᴿ Γ″} {ρ″ : Γ →ᴿ Γ″}
  → (∀ {A} (x : Γ ∋ A) → ρ′ (ρ x) ≡ ρ″ x)
    --------------------------------------------------------------
  → (∀ {B A} (x : Γ ▷ B ∋ A) → ren▷ ρ′ (ren▷ ρ x) ≡ ren▷ ρ″ x)
ren∘ren▷ ρ∘ Z      =  refl
ren∘ren▷ ρ∘ (S x)  =  cong S_ (ρ∘ x)

ren∘ren : ∀ {Γ Γ′ Γ″} {ρ : Γ →ᴿ Γ′} {ρ′ : Γ′ →ᴿ Γ″} {ρ″ : Γ →ᴿ Γ″}
  → (∀ {A} (x : Γ ∋ A) → ρ′ (ρ x) ≡ ρ″ x)
    -------------------------------------------------
  → (∀ {A} (M : Γ ⊢ A) → ren ρ′ (ren ρ M) ≡ ren ρ″ M)
ren∘ren ρ≡ (` x)          =  cong `_ (ρ≡ x)
ren∘ren ρ≡ (ƛ N)          =  cong ƛ_ (ren∘ren (ren∘ren▷ ρ≡) N)
ren∘ren ρ≡ (L · M)        =  cong₂ _·_ (ren∘ren ρ≡ L) (ren∘ren ρ≡ M)
ren∘ren ρ≡ ($ k)          =  refl
ren∘ren ρ≡ (L ⦅ _⊕_ ⦆ M)  =  cong₂ _⦅ _⊕_ ⦆_ (ren∘ren ρ≡ L) (ren∘ren ρ≡ M)
ren∘ren ρ≡ (M ⇑ g)        =  cong (_⇑ g) (ren∘ren ρ≡ M)
ren∘ren ρ≡ (M ⟨ ±p ⟩)     =  cong (_⟨ ±p ⟩) (ren∘ren ρ≡ M)
ren∘ren ρ≡ blame          =  refl

lift∘ren : ∀ {Γ Δ A B} (ρ : Γ →ᴿ Δ) (M : Γ ⊢ B)
  → lift {A = A} (ren ρ M) ≡ ren (ren▷ ρ) (lift {A = A} M)
lift∘ren {Γ} ρ M  =  trans (ren∘ren ρ≡₁ M) (sym (ren∘ren ρ≡₂ M))
  where
  ρ≡₁ : ∀ {A} (x : Γ ∋ A) → S (ρ x) ≡ S (ρ x)  
  ρ≡₁ x = refl
  ρ≡₂ : ∀ {A} (x : Γ ∋ A) → ren▷ ρ (S x) ≡ S (ρ x)
  ρ≡₂ Z     = refl
  ρ≡₂ (S x) = refl

sub∘ren▷ : ∀ {Γ Γ′ Γ″} {ρ : Γ →ᴿ Γ′} {σ′ : Γ′ →ˢ Γ″} {σ″ : Γ →ˢ Γ″}
  → (∀ {A} (x : Γ ∋ A) → σ′ (ρ x) ≡ σ″ x)
    ----------------------------------------------------------
  → (∀ {B A} (x : Γ ▷ B ∋ A) → sub▷ σ′ (ren▷ ρ x) ≡ sub▷ σ″ x)
sub∘ren▷ σ≡ Z      =  refl
sub∘ren▷ σ≡ (S x)  =  cong (ren S_) (σ≡ x)

sub∘ren : ∀ {Γ Γ′ Γ″} {ρ : Γ →ᴿ Γ′} {σ′ : Γ′ →ˢ Γ″} {σ″ : Γ →ˢ Γ″}
  → (∀ {A} (x : Γ ∋ A) → σ′ (ρ x) ≡ σ″ x)
    ----------------------------------------------------------
  → (∀ {A} (M : Γ ⊢ A) → sub σ′ (ren ρ M) ≡ sub σ″ M)
sub∘ren σ≡ (` x)          =  σ≡ x
sub∘ren σ≡ (ƛ N)          =  cong ƛ_ (sub∘ren (sub∘ren▷ σ≡) N)
sub∘ren σ≡ (L · M)        =  cong₂ _·_ (sub∘ren σ≡ L) (sub∘ren σ≡ M)
sub∘ren σ≡ ($ k)          =  refl
sub∘ren σ≡ (L ⦅ _⊕_ ⦆ M)  =  cong₂ _⦅ _⊕_ ⦆_ (sub∘ren σ≡ L) (sub∘ren σ≡ M)
sub∘ren σ≡ (M ⇑ g)        =  cong (_⇑ g) (sub∘ren σ≡ M)
sub∘ren σ≡ (M ⟨ ±p ⟩)     =  cong (_⟨ ±p ⟩) (sub∘ren σ≡ M)
sub∘ren σ≡ blame          =  refl

ren∘sub▷ : ∀ {Γ Γ′ Γ″} {σ : Γ →ˢ Γ′} {ρ′ : Γ′ →ᴿ Γ″} {σ″ : Γ →ˢ Γ″}
  → (∀ {A} (x : Γ ∋ A) → ren ρ′ (σ x) ≡ σ″ x)
    -------------------------------------------------------------------
  → (∀ {B A} (x : Γ ▷ B ∋ A) → ren (ren▷ ρ′) (sub▷ σ x) ≡ sub▷ σ″ x)
ren∘sub▷ σ≡ Z      =  refl
ren∘sub▷ {Γ′ = Γ′} {σ = σ} {ρ′ = ρ′} σ≡ {B = B} (S x)  =
    trans (trans (ren∘ren ρ∘₁ (σ x)) (sym (ren∘ren ρ∘₂ (σ x)))) (cong (ren S_) (σ≡ x))
  where
  ρ∘₁ : ∀ {A} (x : Γ′ ∋ A) → ren▷ {A = B} ρ′ (S_ x) ≡ S (ρ′ x)
  ρ∘₁ x = refl

  ρ∘₂ : ∀ {A} (x : Γ′ ∋ A) → S_ {B = B} (ρ′ x) ≡ S (ρ′ x)
  ρ∘₂ x = refl

ren∘sub : ∀ {Γ Γ′ Γ″} {σ : Γ →ˢ Γ′} {ρ′ : Γ′ →ᴿ Γ″} {σ″ : Γ →ˢ Γ″}
  → (∀ {A} (x : Γ ∋ A) → ren ρ′ (σ x) ≡ σ″ x)
    --------------------------------------------------------
  → (∀ {A} (M : Γ ⊢ A) → ren ρ′ (sub σ M) ≡ sub σ″ M)
ren∘sub σ≡ (` x)          =  σ≡ x
ren∘sub σ≡ (ƛ N)          =  cong ƛ_ (ren∘sub (ren∘sub▷ σ≡) N)
ren∘sub σ≡ (L · M)        =  cong₂ _·_ (ren∘sub σ≡ L) (ren∘sub σ≡ M)
ren∘sub σ≡ ($ k)          =  refl
ren∘sub σ≡ (L ⦅ _⊕_ ⦆ M)  =  cong₂ _⦅ _⊕_ ⦆_ (ren∘sub σ≡ L) (ren∘sub σ≡ M)
ren∘sub σ≡ (M ⇑ g)        =  cong (_⇑ g) (ren∘sub σ≡ M)
ren∘sub σ≡ (M ⟨ ±p ⟩)     =  cong (_⟨ ±p ⟩) (ren∘sub σ≡ M)
ren∘sub σ≡ blame          =  refl

lift∘sub : ∀ {Γ Δ A B} (σ : Γ →ˢ Δ) (M : Γ ⊢ B)
  → lift {A = A} (sub σ M) ≡ sub (sub▷ σ) (lift {A = A} M)
lift∘sub {Γ} σ M  =  trans (ren∘sub σ≡₁ M) (sym (sub∘ren σ≡₂ M))
  where
  σ≡₁ : ∀ {A} (x : Γ ∋ A) → lift (σ x) ≡ lift (σ x)  
  σ≡₁ x = refl
  σ≡₂ : ∀ {A} (x : Γ ∋ A) → sub▷ σ (S x) ≡ lift (σ x)
  σ≡₂ Z     = refl
  σ≡₂ (S x) = refl
```

## Renaming and substitution by identity is the identity

```
Idᴿ : ∀ {Γ} → (ρ : Γ →ᴿ Γ) → Set
Idᴿ {Γ} ρ  =  ∀ {A} (x : Γ ∋ A) → ρ x ≡ x

Idˢ : ∀ {Γ} → (σ : Γ →ˢ Γ) → Set
Idˢ {Γ} σ  =  ∀ {A} (x : Γ ∋ A) → σ x ≡ ` x

Idᵀ : ∀ {Γ} → (θ : Γ →ᵀ Γ) → Set
Idᵀ {Γ} θ  =  ∀ {A} (M : Γ ⊢ A) → θ M ≡ M

renId▷ : ∀ {Γ} {ρ : Γ →ᴿ Γ}
  → Idᴿ {Γ} ρ
    ----------------------------
  → ∀ {A} → Idᴿ {Γ ▷ A} (ren▷ ρ)
renId▷ ρId Z                    =  refl
renId▷ ρId (S x) rewrite ρId x  =  refl

renId : ∀ {Γ} {ρ : Γ →ᴿ Γ}
  → Idᴿ ρ
    -------------
  → Idᵀ (ren ρ)
renId ρId (` x) rewrite ρId x                               =  refl
renId ρId (ƛ M) rewrite renId (renId▷ ρId) M                =  refl
renId ρId (L · M) rewrite renId ρId L | renId ρId M         =  refl
renId ρId ($ k)                                             =  refl
renId ρId (L ⦅ _⊕_ ⦆ M) rewrite renId ρId L | renId ρId M   =  refl
renId ρId (M ⇑ g) rewrite renId ρId M                       =  refl
renId ρId (M ⟨ ±p ⟩) rewrite renId ρId M                    =  refl
renId ρId blame                                             =  refl

subId▷ : ∀ {Γ} {σ : Γ →ˢ Γ}
  → Idˢ {Γ} σ
    ----------------------------
  → ∀ {A} → Idˢ {Γ ▷ A} (sub▷ σ)
subId▷ σId Z                    =  refl
subId▷ σId (S x) rewrite σId x  =  refl

subId : ∀ {Γ} {σ : Γ →ˢ Γ}
  → Idˢ σ
    -------------
  → Idᵀ (sub σ)
subId σId (` x) rewrite σId x                               =  refl
subId σId (ƛ M) rewrite subId (subId▷ σId) M                =  refl
subId σId (L · M) rewrite subId σId L | subId σId M         =  refl
subId σId ($ k)                                             =  refl
subId σId (L ⦅ _⊕_ ⦆ M) rewrite subId σId L | subId σId M    =  refl
subId σId (M ⇑ g) rewrite subId σId M                       =  refl
subId σId (M ⟨ ±p ⟩) rewrite subId σId M                    =  refl
subId σId blame                                             =  refl
```

## Values

```
data Value : ∀ {Γ A} → Γ ⊢ A → Set where

  ƛ_ : ∀{Γ A B}
    → (N : Γ ▷ A ⊢ B)
      ---------------
    → Value (ƛ N)

  $_ : ∀{Γ ι}
    → (k : rep ι)
      -------------------
    → Value {Γ = Γ} ($ k)

  _⇑_ : ∀{Γ G}{V : Γ ⊢ G}
    → (v : Value V)
    → (g : Ground G)
      ------------------
    → Value (V ⇑ g)
```


Extract term from evidence that it is a value.
```
value : ∀ {Γ A} {V : Γ ⊢ A}
  → (v : Value V)
    -------------
  → Γ ⊢ A
value {V = V} v  =  V  
```


Renaming preserves values
```
ren-val : ∀ {Γ Δ A} {V : Γ ⊢ A}
  → (ρ : Γ →ᴿ Δ)
  → Value V
    ------------------
  → Value (ren ρ V)
ren-val ρ (ƛ N)    =  ƛ (ren (ren▷ ρ) N)
ren-val ρ ($ k)    =  $ k
ren-val ρ (V ⇑ g)  =  (ren-val ρ V) ⇑ g
```

Substitution preserves values
```
sub-val : ∀ {Γ Δ A} {V : Γ ⊢ A}
  → (σ : Γ →ˢ Δ)
  → Value V
    ------------------
  → Value (sub σ V)
sub-val σ (ƛ N)    =  ƛ (sub (sub▷ σ) N)
sub-val σ ($ k)    =  $ k
sub-val σ (V ⇑ g)  =  (sub-val σ V) ⇑ g
```

## Frames

```
infix  5 [_]⇑_
infix  5 [_]⟨_⟩
infix  6 [_]·_
infix  6 _·[_]
infix  6 [_]⦅_⦆_
infix  6 _⦅_⦆[_]
infix  7 _⟦_⟧

data Frame (Γ : Context) (C : Type) : Type → Set where

  □ : Frame Γ C C

  [_]·_ : ∀ {A B}
    → (E : Frame Γ C (A ⇒ B))
    → (M : Γ ⊢ A)
      ---------------
    → Frame Γ C B

  _·[_] : ∀ {A B}{V : Γ ⊢ A ⇒ B}
    → (v : Value V)
    → (E : Frame Γ C A)
      ----------------
    → Frame Γ C B

  [_]⦅_⦆_ : ∀ {ι ι′ ι″}
    → (E : Frame Γ C ($ ι))
    → (_⊕_ : rep ι → rep ι′ → rep ι″)
    → (N : Γ ⊢ $ ι′)
      ------------------
    → Frame Γ C ($ ι″)

  _⦅_⦆[_] : ∀ {ι ι′ ι″}{V : Γ ⊢ $ ι}
    → (v : Value V)
    → (_⊕_ : rep ι → rep ι′ → rep ι″)
    → (E : Frame Γ C ($ ι′))
      -------------------
    → Frame Γ C ($ ι″)

  [_]⇑_ : ∀ {G}
    → (E : Frame Γ C G)
    → (g : Ground G)
      --------------
    → Frame Γ C ★

  [_]⟨_⟩ : ∀ {A B}
    → (E : Frame Γ C A)
    → (±p : A => B)
      -------------
    → Frame Γ C B
```

The plug function inserts an expression into the hole of a frame.
```
_⟦_⟧ : ∀{Γ A B} → Frame Γ A B → Γ ⊢ A → Γ ⊢ B
□ ⟦ M ⟧                 =  M
([ E ]· M) ⟦ L ⟧        =  E ⟦ L ⟧ · M
(v ·[ E ]) ⟦ M ⟧        =  value v · E ⟦ M ⟧
([ E ]⦅ _⊕_ ⦆ N) ⟦ M ⟧  =  E ⟦ M ⟧ ⦅ _⊕_ ⦆ N
(v ⦅ _⊕_ ⦆[ E ]) ⟦ N ⟧  =  value v ⦅ _⊕_ ⦆ E ⟦ N ⟧
([ E ]⇑ g) ⟦ M ⟧        =  E ⟦ M ⟧ ⇑ g
([ E ]⟨ ±p ⟩) ⟦ M ⟧     =  E ⟦ M ⟧ ⟨ ±p ⟩
```

Composition of two frames
```
_∘∘_ : ∀{Γ A B C} → Frame Γ B C → Frame Γ A B → Frame Γ A C
□ ∘∘ F                 =  F
([ E ]· M) ∘∘ F        =  [ E ∘∘ F ]· M
(v ·[ E ]) ∘∘ F        =  v ·[ E ∘∘ F ]
([ E ]⦅ _⊕_ ⦆ N) ∘∘ F  =  [ E ∘∘ F ]⦅ _⊕_ ⦆ N
(v ⦅ _⊕_ ⦆[ E ]) ∘∘ F  =  v ⦅ _⊕_ ⦆[ E ∘∘ F ]
([ E ]⇑ g) ∘∘ F        =  [ E ∘∘ F ]⇑ g
([ E ]⟨ ±p ⟩) ∘∘ F     =  [ E ∘∘ F ]⟨ ±p ⟩
```

Composition and plugging
```
∘∘-lemma : ∀ {Γ A B C}
  → (E : Frame Γ B C)
  → (F : Frame Γ A B)
  → (M : Γ ⊢ A)
    -----------------------------
  → E ⟦ F ⟦ M ⟧ ⟧ ≡ (E ∘∘ F) ⟦ M ⟧
∘∘-lemma □ F M                                         =  refl
∘∘-lemma ([ E ]· M₁) F M       rewrite ∘∘-lemma E F M  =  refl
∘∘-lemma (v ·[ E ]) F M        rewrite ∘∘-lemma E F M  =  refl
∘∘-lemma ([ E ]⦅ _⊕_ ⦆ N) F M  rewrite ∘∘-lemma E F M  =  refl
∘∘-lemma (v ⦅ _⊕_ ⦆[ E ]) F M  rewrite ∘∘-lemma E F M  =  refl
∘∘-lemma ([ E ]⇑ g) F M        rewrite ∘∘-lemma E F M  =  refl
∘∘-lemma [ E ]⟨ ±p ⟩ F M       rewrite ∘∘-lemma E F M  =  refl
```

## Reduction

```
infix 2 _↦_ _—→_

data _↦_ : ∀ {Γ A} → (Γ ⊢ A) → (Γ ⊢ A) → Set where

  β : ∀ {Γ A B} {N : Γ ▷ A ⊢ B} {W : Γ ⊢ A}
    → Value W
      --------------------
    → (ƛ N) · W ↦ N [ W ]

  δ : ∀ {Γ ι ι′ ι″} {_⊕_ : rep ι → rep ι′ → rep ι″} {k : rep ι} {k′ : rep ι′}
      --------------------------------------------
    → _⦅_⦆_ {Γ = Γ} ($ k) _⊕_ ($ k′) ↦ $ (k ⊕ k′)

  ident : ∀ {Γ A} {V : Γ ⊢ A} {±p : A => A}
    → split ±p ≡ id
    → Value V
      --------------
    → V ⟨ ±p ⟩ ↦ V

  wrap : ∀{Γ A A′ B B′} {N : Γ ▷ A ⊢ B}
      {∓s : A′ => A} {±t : B => B′} {±p : A ⇒ B => A′ ⇒ B′}
    → split ±p ≡ ∓s ⇒ ±t
      ----------------------------------------------------
    → (ƛ N) ⟨ ±p ⟩ ↦ ƛ (lift (ƛ N) · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩)

  expand : ∀{Γ A G} {V : Γ ⊢ A} {p : A ≤ G}
    → Value V
    → (g : Ground G)
      -------------------------------
    → V ⟨ + p ⇑ g ⟩ ↦ V ⟨ + p ⟩ ⇑ g

  collapse : ∀{Γ G A} {V : Γ ⊢ G} {p : A ≤ G}
    → Value V
    → (g : Ground G)
      --------------------------------
    → (V ⇑ g) ⟨ - p ⇑ g ⟩ ↦ V ⟨ - p ⟩

  collide  : ∀{Γ G H A} {V : Γ ⊢ G} {p : A ≤ H}
    → Value V
    → (g : Ground G)
    → (h : Ground H)
    → G ≢ H
      -----------------------------
    → ((V ⇑ g) ⟨ - p ⇑ h ⟩) ↦ blame


data _—→_ : ∀ {Γ A} → (Γ ⊢ A) → (Γ ⊢ A) → Set where

  ξξ : ∀ {Γ A B} {M N : Γ ⊢ A} {M′ N′ : Γ ⊢ B}
    → ( E : Frame Γ A B)
    → M′ ≡ E ⟦ M ⟧
    → N′ ≡ E ⟦ N ⟧
    → M ↦ N
      --------
    → M′ —→ N′
```

Notation
```
pattern ξ E M—→N = ξξ E refl refl M—→N
```

## Reflexive and transitive closure of reduction

```
infixr 1 _++_
infix  1 begin_
infix  2 _—↠_
infixr 2 _—→⟨_⟩_
infixr 2 _—↠⟨_⟩_
infix  3 _∎

data _—↠_ : ∀{Γ A} → Γ ⊢ A → Γ ⊢ A → Set where

  _∎ : ∀ {Γ A} (M : Γ ⊢ A)
      ---------
    → M —↠ M

  _—→⟨_⟩_ : ∀ {Γ A} (L : Γ ⊢ A) {M N : Γ ⊢ A}
    → L —→ M
    → M —↠ N
      ---------
    → L —↠ N

begin_ : ∀ {Γ A} {M N : Γ ⊢ A} → (M —↠ N) → (M —↠ N)
begin M—↠N = M—↠N
```

Convenience function to build a sequence of length one.
```
unit : ∀ {Γ A} {M N : Γ ⊢ A} → (M ↦ N) → (M —↠ N)
unit {M = M} {N = N} M↦N  =  M —→⟨ ξ □ M↦N ⟩ N ∎
```

Apply ξ to each element of a sequence
```
ξ* : ∀{Γ A B} {M N : Γ ⊢ A} → (E : Frame Γ A B) → M —↠ N → E ⟦ M ⟧ —↠ E ⟦ N ⟧
ξ* E (M ∎) = E ⟦ M ⟧ ∎
ξ* E (L —→⟨ ξξ {M = L′} {N = M′} F refl refl L′↦M′ ⟩ M—↠N)
  =  (E ⟦ L ⟧ —→⟨ ξξ (E ∘∘ F) (∘∘-lemma E F L′)
       (∘∘-lemma E F M′) L′↦M′ ⟩ (ξ* E M—↠N))
```

Concatenate two sequences.
```
_++_ : ∀ {Γ A} {L M N : Γ ⊢ A} → L —↠ M → M —↠ N → L —↠ N
(M ∎) ++ M—↠N                =  M—↠N
(L —→⟨ L—→M ⟩ M—↠N) ++ N—↠P  =  L —→⟨ L—→M ⟩ (M—↠N ++ N—↠P)
```

Alternative notation for sequence concatenation.
```
_—↠⟨_⟩_ : ∀ {Γ A} (L : Γ ⊢ A) {M N : Γ ⊢ A}
  → L —↠ M
  → M —↠ N
    ---------
  → L —↠ N
L —↠⟨ L—↠M ⟩ M—↠N  =  L—↠M ++ M—↠N
```

## Irreducible terms

Values are irreducible.  The auxiliary definition rearranges the
order of the arguments because it works better for Agda.  
```
value-irreducible : ∀ {Γ A} {V M : Γ ⊢ A} → Value V → ¬ (V —→ M)
value-irreducible v V—→M = nope V—→M v
   where
   nope : ∀ {Γ A} {V M : Γ ⊢ A} → V —→ M → Value V → ⊥
   nope (ξ □ (β x)) ()
   nope (ξ □ δ) ()
   nope (ξ □ (ident e v)) ()
   nope (ξ □ (wrap e)) ()
   nope (ξ □ (expand v g)) ()
   nope (ξ □ (collapse v g)) ()
   nope (ξ □ (collide v g h G≢H)) ()
   nope (ξ ([ E ]⇑ g) V—→M) (v ⇑ g)  =  nope (ξ E V—→M) v
```

Variables are irreducible.
```
variable-irreducible : ∀ {Γ A} {x : Γ ∋ A} {N : Γ ⊢ A}
    ------------
  → ¬ (` x —→ N)
variable-irreducible (ξ □ ())
```

Boxes are irreducible (at the top level)
```
box-irreducible : ∀ {Γ G} {M : Γ ⊢ G} {N : Γ ⊢ ★}
  → (g : Ground G)
    --------------
  → ¬ (M ⇑ g ↦ N)
box-irreducible g ()
```

Blame is irreducible.
```
blame-irreducible : ∀ {Γ A} {M′ : Γ ⊢ A}  → ¬ (blame —→ M′)
blame-irreducible (ξ □ ())
```

## Progress

Every term that is well typed and closed is either
blame or a value or takes a reduction step.

```
data Progress {A} : (∅ ⊢ A) → Set where

  blame : ∀ {B}
   → (E : Frame ∅ B A)
     ---------------------
   → Progress (E ⟦ blame ⟧)

  step : ∀ {M N : ∅ ⊢ A}
    → M —→ N
      ----------
    → Progress M

  done : ∀ {M : ∅ ⊢ A}
    → Value M
      ----------
    → Progress M

progress± : ∀ {A B} {V : ∅ ⊢ A}
  → (v : Value V)
  → (±p : A => B)
    --------------------
  → ∃[ M ](V ⟨ ±p ⟩ ↦ M)
progress± v ±p with split ±p in e
progress± v     _ | id                   =  _ , ident e v
progress± (ƛ _) _ | _ ⇒ _                =  _ , wrap e
progress± v       (+ _ ⇑ g) | other      =  _ , expand v g
progress± (v ⇑ g) (- _ ⇑ h) | other
    with ground g ≡? ground h
... | yes refl rewrite uniqueG g h       =  _ , collapse v h
... | no  G≢H                            =  _ , collide v g h G≢H

progress : ∀ {A}
  → (M : ∅ ⊢ A)
    -----------
  → Progress M

progress (ƛ N)                           =  done (ƛ N)
progress (L · M) with progress L
... | blame E                            =  blame ([ E ]· M)
... | step (ξ E L↦L′)                    =  step (ξ ([ E ]· M) L↦L′)
... | done (ƛ N) with progress M
...     | blame E                        =  blame ((ƛ N) ·[ E ])
...     | step (ξ E M↦M′)                =  step (ξ ((ƛ N) ·[ E ]) M↦M′)
...     | done w                         =  step (ξ □ (β w))
progress ($ k)                           =  done ($ k)
progress (L ⦅ _⊕_ ⦆ M) with progress L
... | blame E                            =  blame ([ E ]⦅ _⊕_ ⦆ M)
... | step (ξ E L↦L′)                    =  step (ξ ([ E ]⦅ _⊕_ ⦆ M) L↦L′)
... | done ($ k) with progress M
...     | blame E                        =  blame (($ k) ⦅ _⊕_ ⦆[ E ])
...     | step (ξ E M↦M′)                =  step (ξ (($ k) ⦅ _⊕_ ⦆[ E ]) M↦M′)
...     | done ($ k′)                    =  step (ξ □ δ)
progress (M ⇑ g) with progress M
... | blame E                            =  blame ([ E ]⇑ g)
... | step (ξ E M↦M′)                    =  step (ξ ([ E ]⇑ g) M↦M′)
... | done v                             =  done (v ⇑ g)
progress (M ⟨ ±p ⟩) with progress M
... | blame E                            =  blame ([ E ]⟨ ±p ⟩)
... | step (ξ E M↦M′)                    =  step (ξ ([ E ]⟨ ±p ⟩) M↦M′)
... | done v with progress± v ±p
...     | _ , V⟨±p⟩↦N                    =  step (ξ □ V⟨±p⟩↦N)
progress blame                           =  blame □
```


## Evaluation

Gas is specified by a natural number:
```
record Gas : Set where
  constructor gas
  field
    amount : ℕ
```
When our evaluator returns a term `N`, it will either give evidence that
`N` is a value, or indicate that blame occurred or it ran out of gas.
```
data Finished {A} : (∅ ⊢ A) → Set where

   done : ∀ {N : ∅ ⊢ A}
     → Value N
       ----------
     → Finished N

   blame : ∀ {B}
     → (E : Frame ∅ B A)
       ---------------------
     → Finished (E ⟦ blame ⟧)

   out-of-gas : {N : ∅ ⊢ A}
       ----------
     → Finished N
```
Given a term `L` of type `A`, the evaluator will, for some `N`, return
a reduction sequence from `L` to `N` and an indication of whether
reduction finished:
```
data Steps {A} : ∅ ⊢ A → Set where

  steps : {L N : ∅ ⊢ A}
    → L —↠ N
    → Finished N
      ----------
    → Steps L
```
The evaluator takes gas and a term and returns the corresponding steps:
```
eval : ∀ {A}
  → Gas
  → (L : ∅ ⊢ A)
    -----------
  → Steps L
eval (gas zero) L          =  steps (L ∎) out-of-gas
eval (gas (suc m)) L
    with progress L
... | done v               =  steps (L ∎) (done v)
... | blame E              =  steps (L ∎) (blame E)
... | step {L} {M} L—→M
    with eval (gas m) M
... | steps M—↠N fin       =  steps (L —→⟨ L—→M ⟩ M—↠N) fin
```

## Type erasure

```
infix 6 _≤★

pattern  _≤★ ι   =  id ⇑ ($ ι)
pattern  ★⇒★≤★   =  id ⇑ ★⇒★

infix  6 _·★_
infix  6 _⦅_⦆★_
infix  8 $★_

pattern  ƛ★_ N          =  (ƛ N) ⟨ + ★⇒★≤★ ⟩
pattern  _·★_ L M       =  (L ⟨ - ★⇒★≤★ ⟩) · M
pattern  $★_ {ι = ι} k  =  $ k ⇑ $ ι
pattern  _⦅_⦆★_ {ι = ι} {ι′} {ι″} M _⊕_ N
  =  ((M ⟨ - ι ≤★ ⟩) ⦅ _⊕_ ⦆ (N ⟨ - ι′ ≤★ ⟩)) ⟨ + ι″ ≤★ ⟩

data Static : ∀ {Γ A} → (Γ ⊢ A) → Set where

  `_ : ∀ {Γ A}
    → (x : Γ ∋ A)
      ------------
    → Static (` x)

  ƛ_ : ∀ {Γ A B} {N : Γ ▷ A ⊢ B}
    → Static N
      ------------
    → Static (ƛ N)

  _·_ : ∀ {Γ A B} {L : Γ ⊢ A ⇒ B} {M : Γ ⊢ A}
    → Static L
    → Static M
      --------------
    → Static (L · M)

  $_ : ∀ {Γ ι}
    → (k : rep ι)
      -------------------
    → Static {Γ = Γ} ($ k)

  _⦅_⦆_ : ∀ {Γ ι ι′ ι″} {M : Γ ⊢ $ ι} {N : Γ ⊢ $ ι′}
    → Static M
    → (_⊕_ : rep ι → rep ι′ → rep ι″)
    → Static N
      --------------------
    → Static (M ⦅ _⊕_ ⦆ N)

static : ∀ {Γ A} {M : Γ ⊢ A}
  → (m : Static M)
    -------------
  → Γ ⊢ A
static {M = M} m  =  M

⌈_⌉ᴳ : Context → Context
⌈ ∅ ⌉ᴳ = ∅
⌈ Γ ▷ A ⌉ᴳ = ⌈ Γ ⌉ᴳ ▷ ★

⌈_⌉ˣ : ∀ {Γ A} → (Γ ∋ A) → (⌈ Γ ⌉ᴳ ∋ ★)
⌈ Z ⌉ˣ          = Z
⌈ S x ⌉ˣ        = S ⌈ x ⌉ˣ

⌈_⌉ : ∀ {Γ A} {M : Γ ⊢ A} → Static M → (⌈ Γ ⌉ᴳ ⊢ ★)
⌈ ` x ⌉          =  ` ⌈ x ⌉ˣ
⌈ ƛ N ⌉          =  ƛ★ ⌈ N ⌉
⌈ L · M ⌉        =  ⌈ L ⌉ ·★ ⌈ M ⌉
⌈ $ k ⌉          =  $★ k
⌈ M ⦅ _⊕_ ⦆ N ⌉  =  ⌈ M ⌉ ⦅ _⊕_ ⦆★ ⌈ N ⌉
```

## Examples

The following abbreviations cause Agda to produce more readable output
when using `eval`.  In particular, the specialised `$ℕ★_`, `$𝔹★_`, and
`_⦅_⦆ℕ★_` lead to more readable results than the generic `$★_` and
`_⦅_⦆★_`.  After the output is produced, rewriting `ℕ★` and `𝔹★`
yields the more generic operators, which are fine for input.

```
pattern  $ℕ      =  $ ′ℕ
pattern  $𝔹      =  $ ′𝔹
pattern  ℕ≤★     =  id ⇑ $ℕ
pattern  𝔹≤★     =  id ⇑ $𝔹
pattern  ℕ⇒ℕ≤★   =  ℕ≤★ ⇒ ℕ≤★ ⇑ ★⇒★

infix  6 _⦅_⦆ℕ★_
infix  8 $ℕ★_
infix  8 $𝔹★_

pattern  $ℕ★_ k          =  $ k ⇑ $ℕ
pattern  $𝔹★_ k          =  $ k ⇑ $𝔹
pattern  _⦅_⦆ℕ★_ M _⊕_ N
  =  ((M ⟨ - ℕ≤★ ⟩) ⦅ _⊕_ ⦆ (N ⟨ - ℕ≤★ ⟩)) ⟨ + ℕ≤★ ⟩

inc     :  ∅ ⊢ $ℕ ⇒ $ℕ
inc     =  ƛ (` Z ⦅ _+_ ⦆ $ 1)

Inc     :  Static inc
Inc     =  ƛ (` Z ⦅ _+_ ⦆ $ 1)

inc★    :  ∅ ⊢ ★
inc★    =  ⌈ Inc ⌉

inc★′   :  ∅ ⊢ ★
inc★′   =  inc ⟨ + ℕ⇒ℕ≤★ ⟩

inc2—↠3  : inc · ($ 2) —↠ $ 3
inc2—↠3  =
  begin
    (ƛ (` Z ⦅ _+_ ⦆ $ 1)) · $ 2
  —→⟨ ξ □ (β ($ 2)) ⟩
    $ 2 ⦅ _+_ ⦆ $ 1
  —→⟨ ξ □ δ ⟩ $ 3
  ∎

inc★2★—↠3★  : inc★ ·★ ($★ 2) —↠ $★ 3
inc★2★—↠3★  =
  begin
    (ƛ★ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ·★ $ℕ★ 2
  —→⟨ ξ ([ [ □ ]⟨ - ★⇒★≤★ ⟩ ]· $ℕ★ 2) (expand (ƛ _) ★⇒★) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⟨ + id ⟩ ⇑ ★⇒★) ·★ $ℕ★ 2
  —→⟨ ξ ([ [ [ □ ]⇑ ★⇒★ ]⟨ - ★⇒★≤★ ⟩ ]· $ℕ★ 2) (ident refl (ƛ _)) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⇑ ★⇒★) ·★ $ℕ★ 2
  —→⟨ ξ ([ □ ]· $ℕ★ 2) (collapse (ƛ _) ★⇒★) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⟨ - id ⟩) · $ℕ★ 2
  —→⟨ ξ ([ □ ]· $ℕ★ 2) (ident refl (ƛ _)) ⟩
    (ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) · $ℕ★ 2
  —→⟨ ξ □ (β ($ℕ★ 2)) ⟩
    $ℕ★ 2 ⦅ _+_ ⦆ℕ★ $ℕ★ 1
  —→⟨ ξ [ [ □ ]⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ]⟨ + ℕ≤★ ⟩ (collapse ($ 2) $ℕ) ⟩
    ($ 2 ⟨ - id ⟩) ⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ [ □ ]⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ]⟨ + ℕ≤★ ⟩ (ident refl ($ 2)) ⟩
    $ 2 ⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ $ 2 ⦅ _+_ ⦆[ □ ] ]⟨ + ℕ≤★ ⟩ (collapse ($ 1) $ℕ) ⟩
    $ 2 ⦅ _+_ ⦆ ($ 1 ⟨ - id ⟩) ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ $ 2 ⦅ _+_ ⦆[ □ ] ]⟨ + ℕ≤★ ⟩ (ident refl ($ 1)) ⟩
    $ 2 ⦅ _+_ ⦆ $ 1 ⟨ + ℕ≤★ ⟩ —→⟨ ξ [ □ ]⟨ + ℕ≤★ ⟩ δ ⟩
    $ 3 ⟨ + ℕ≤★ ⟩
  —→⟨ ξ □ (expand ($ 3) $ℕ) ⟩
    $ 3 ⟨ + id ⟩ ⇑ $ℕ
  —→⟨ ξ ([ □ ]⇑ $ℕ) (ident refl ($ 3)) ⟩
    $ℕ★ 3
  ∎

inc★′2★—↠3★  : inc★′ ·★ ($★ 2) —↠ $★ 3
inc★′2★—↠3★  =
  begin
    ((ƛ (` Z ⦅ _+_ ⦆ $ 1)) ⟨ + ℕ⇒ℕ≤★ ⟩) ·★ $ℕ★ 2
  —→⟨ ξ ([ [ □ ]⟨ - ★⇒★≤★ ⟩ ]· $ℕ★ 2) (expand (ƛ _) ★⇒★) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ $ 1)) ⟨ + ℕ≤★ ⇒ ℕ≤★ ⟩ ⇑ ★⇒★) ·★ $ℕ★ 2
  —→⟨ ξ ([ [ [ □ ]⇑ ★⇒★ ]⟨ - ★⇒★≤★ ⟩ ]· $ℕ★ 2) (wrap refl) ⟩
    ((ƛ ((ƛ (` Z ⦅ _+_ ⦆ $ 1)) · (` Z ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩)) ⇑ ★⇒★) ·★ $ℕ★ 2
  —→⟨ ξ ([ □ ]· $ℕ★ 2) (collapse (ƛ _) ★⇒★) ⟩
    ((ƛ ((ƛ (` Z ⦅ _+_ ⦆ $ 1)) · (` Z ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩)) ⟨ - id ⟩) · $ℕ★ 2
  —→⟨ ξ ([ □ ]· $ℕ★ 2) (ident refl (ƛ _)) ⟩
    (ƛ ((ƛ (` Z ⦅ _+_ ⦆ $ 1)) · (` Z ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩)) · $ℕ★ 2
  —→⟨ ξ □ (β ($ℕ★ 2)) ⟩
    (ƛ (` Z ⦅ _+_ ⦆ $ 1)) · ($ℕ★ 2 ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ (ƛ (` Z ⦅ _+_ ⦆ $ 1)) ·[ □ ] ]⟨ + ℕ≤★ ⟩ (collapse ($ 2) $ℕ) ⟩
    (ƛ (` Z ⦅ _+_ ⦆ $ 1)) · ($ 2 ⟨ - id ⟩) ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ (ƛ (` Z ⦅ _+_ ⦆ $ 1)) ·[ □ ] ]⟨ + ℕ≤★ ⟩ (ident refl ($ 2)) ⟩
    (ƛ (` Z ⦅ _+_ ⦆ $ 1)) · $ 2 ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ □ ]⟨ + ℕ≤★ ⟩ (β ($ 2)) ⟩
    $ 2 ⦅ _+_ ⦆ $ 1 ⟨ + ℕ≤★ ⟩
  —→⟨ ξ [ □ ]⟨ + ℕ≤★ ⟩ δ ⟩
    $ 3 ⟨ + ℕ≤★ ⟩
  —→⟨ ξ □ (expand ($ 3) $ℕ) ⟩
    $ 3 ⟨ + id ⟩ ⇑ $ℕ
  —→⟨ ξ ([ □ ]⇑ $ℕ) (ident refl ($ 3)) ⟩
    $ℕ★ 3
  ∎

inc★true★—↠blame : inc★ ·★ ($★ true) —↠
  ([ [ □ ]⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ]⟨ + ℕ≤★ ⟩) ⟦ blame ⟧
inc★true★—↠blame =
  begin
    (ƛ★ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ·★ $𝔹★ true
  —→⟨ ξ ([ [ □ ]⟨ - ★⇒★≤★ ⟩ ]· $𝔹★ true) (expand (ƛ _) ★⇒★) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⟨ + id ⟩ ⇑ ★⇒★) ·★ $𝔹★ true
  —→⟨ ξ ([ [ [ □ ]⇑ ★⇒★ ]⟨ - ★⇒★≤★ ⟩ ]· $𝔹★ true) (ident refl (ƛ _)) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⇑ ★⇒★) ·★ $𝔹★ true
  —→⟨ ξ ([ □ ]· $𝔹★ true) (collapse (ƛ _) ★⇒★) ⟩
    ((ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) ⟨ - id ⟩) · $𝔹★ true
  —→⟨ ξ ([ □ ]· $𝔹★ true) (ident refl (ƛ _)) ⟩
    (ƛ (` Z ⦅ _+_ ⦆ℕ★ $ℕ★ 1)) · $𝔹★ true
  —→⟨ ξ □ (β ($𝔹★ true)) ⟩
    $𝔹★ true ⦅ _+_ ⦆ℕ★ $ℕ★ 1
  —→⟨ ξ [ [ □ ]⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ]⟨ + ℕ≤★ ⟩ (collide ($ true) $𝔹 $ℕ (λ())) ⟩
    blame ⦅ _+_ ⦆ ($ℕ★ 1 ⟨ - ℕ≤★ ⟩) ⟨ + ℕ≤★ ⟩
  ∎
```

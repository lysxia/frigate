Simple Blame Calculus with proof relevant casts.
Uses polarity to unify upcasts and downcasts.
Uses nested evaluation contexts.

Siek, Thiemann, and Wadler

```
module Prec where

open import Data.Nat using (ℕ; zero; suc; _+_)
open import Data.Bool using (true; false) renaming (Bool to 𝔹)
open import Data.Unit using (⊤; tt)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂) renaming ([_,_] to case-⊎)
open import Relation.Binary.PropositionalEquality
     using (_≡_; _≢_; refl; trans; sym; cong; cong₂; cong-app; subst)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Decidable using (⌊_⌋; True; toWitness; fromWitness)
open import Core
```


## Precision on contexts

```
infix 5 _≤ᴳ_

data _≤ᴳ_ : Context → Context → Set where

  ∅ :
      ------
      ∅ ≤ᴳ ∅

  _▷_ : ∀{Γ Γ′ A A′}
    → Γ ≤ᴳ Γ′
    → A ≤ A′
      ------------------
    → Γ ▷ A ≤ᴳ Γ′ ▷ A′
```

## Reflexivity and transitivity of context precision

```
idᴳ : ∀ {Γ} → Γ ≤ᴳ Γ
idᴳ {∅}      =  ∅
idᴳ {Γ ▷ A}  =  idᴳ ▷ id

_⨟ᴳ_ : ∀ {Γ Γ′ Γ″} → Γ ≤ᴳ Γ′ → Γ′ ≤ᴳ Γ″ → Γ ≤ᴳ Γ″
∅ ⨟ᴳ ∅                    =  ∅
(Γ≤ ▷ A≤) ⨟ᴳ (Γ′≤ ▷ A′≤)  =  (Γ≤ ⨟ᴳ Γ′≤) ▷ (A≤ ⨟ A′≤)

left-idᴳ : ∀ {Γ Δ} → (Γ≤Δ : Γ ≤ᴳ Δ) → idᴳ ⨟ᴳ Γ≤Δ ≡ Γ≤Δ
left-idᴳ ∅ = refl
left-idᴳ (Γ≤Δ ▷ p) rewrite left-idᴳ Γ≤Δ | left-id p = refl

right-idᴳ : ∀ {Γ Δ} → (Γ≤Δ : Γ ≤ᴳ Δ) → Γ≤Δ ⨟ᴳ idᴳ ≡ Γ≤Δ
right-idᴳ ∅ = refl
right-idᴳ (Γ≤Δ ▷ p) rewrite right-idᴳ Γ≤Δ | right-id p = refl
```

## Precision on variables

```
infix 4 _⊢_≤ˣ_⦂_

data _⊢_≤ˣ_⦂_ : ∀ {Γ Γ′ A A′} → Γ ≤ᴳ Γ′ → Γ ∋ A → Γ′ ∋ A′ → A ≤ A′ → Set where

  Z≤Z : ∀ {Γ Γ′ A A′} {Γ≤ : Γ ≤ᴳ Γ′} {A≤ : A ≤ A′}
      ----------------------
    → Γ≤ ▷ A≤ ⊢ Z ≤ˣ Z ⦂ A≤ 

  S≤S : ∀ {Γ Γ′ A A′ B B′ x x′} {Γ≤ : Γ ≤ᴳ Γ′} {A≤ : A ≤ A′} {B≤ : B ≤ B′}
    → Γ≤ ⊢ x ≤ˣ x′ ⦂ A≤ 
      ---------------------------
    → Γ≤ ▷ B≤ ⊢ S x ≤ˣ S x′ ⦂ A≤

```

## Commuting diagram

```
commute≤ : ∀ {A B C} (±p : A => B) (q : B ≤ C) (r : A ≤ C) → Set
commute≤ (+ p) q r  =  p ⨟ q ≡ r
commute≤ (- p) q r  =  p ⨟ r ≡ q

≤commute : ∀ {A B C} (p : A ≤ B) (±q : B => C) (r : A ≤ C) → Set
≤commute p (+ q) r  =  p ⨟ q ≡ r
≤commute p (- q) r  =  r ⨟ q ≡ p
```

## Lemmas about commuting diagrams

```
drop⇑ : ∀ {A B G} {±p : A => B} {q : B ≤ G} {r : A ≤ G} {g : Ground G}
  → commute≤ ±p (q ⇑ g) (r ⇑ g)
    ---------------------------
  → commute≤ ±p q r
drop⇑ {±p = + _} refl = refl
drop⇑ {±p = - _} refl = refl

ident≤ : ∀ {A B} {q r : A ≤ B}
  → (±p : A => A)
  → split ±p ≡ id
  → commute≤ ±p q r
    -----
  → q ≡ r
ident≤ {q = q} (+ id) refl refl rewrite left-id q = refl
ident≤ {r = r} (- id) refl refl rewrite left-id r = refl

≤ident : ∀ {A B} {p r : A ≤ B}
  → (±q : B => B)
  → split ±q ≡ id
  → ≤commute p ±q r
    -----
  → p ≡ r
≤ident (+ id) refl refl = refl
≤ident (- id) refl refl = refl

dom≤ :  ∀ {A A′ A″ B B′ B″}
    {±p : A ⇒ B => A′ ⇒ B′} {q : A′ ⇒ B′ ≤ A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
    {∓s : A′ => A} {±t : B => B′}
  → split ±p ≡ ∓s ⇒ ±t
  → commute≤ ±p q r
    ---------------------------
  → commute≤ ∓s (dom r) (dom q)
dom≤ {±p = + s ⇒ t} {q = q} refl refl = dom-⨟ (s ⇒ t) q
dom≤ {±p = - s ⇒ t} {r = r} refl refl = dom-⨟ (s ⇒ t) r

cod≤ :  ∀ {A A′ A″ B B′ B″}
    {±p : A ⇒ B => A′ ⇒ B′} {q : A′ ⇒ B′ ≤ A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
    {∓s : A′ => A} {±t : B => B′}
  → split ±p ≡ ∓s ⇒ ±t
  → commute≤ ±p q r
    ---------------------------
  → commute≤ ±t (cod q) (cod r)
cod≤ {±p = + s ⇒ t} {q = q} refl refl = cod-⨟ (s ⇒ t) q
cod≤ {±p = - s ⇒ t} {r = r} refl refl = cod-⨟ (s ⇒ t) r

≤dom :  ∀ {A A′ A″ B B′ B″}
    {p : A ⇒ B ≤ A′ ⇒ B′} {±q : A′ ⇒ B′ => A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
    {∓s : A″ => A′} {±t : B′ => B″}
  → split ±q ≡ ∓s ⇒ ±t
  → ≤commute p ±q r
    ---------------------------
  → ≤commute (dom r) ∓s (dom p)
≤dom {p = p} {±q = + s ⇒ t} {r = r} refl refl = dom-⨟ p (s ⇒ t)
≤dom {p = p} {±q = - s ⇒ t} {r = r} refl refl = dom-⨟ r (s ⇒ t)

≤cod :  ∀ {A A′ A″ B B′ B″}
    {p : A ⇒ B ≤ A′ ⇒ B′} {±q : A′ ⇒ B′ => A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
    {∓s : A″ => A′} {±t : B′ => B″}
  → split ±q ≡ ∓s ⇒ ±t
  → ≤commute p ±q r
    ---------------------------
  → ≤commute (cod p) ±t (cod r)
≤cod {p = p} {±q = + s ⇒ t} refl refl = cod-⨟ p (s ⇒ t)
≤cod {±q = - s ⇒ t} {r = r} refl refl = cod-⨟ r (s ⇒ t)
```

## Precision on terms

```
infix 4 _⊢_≤ᴹ_⦂_

data _⊢_≤ᴹ_⦂_ : ∀ {Γ Γ′ A A′} → Γ ≤ᴳ Γ′ → Γ ⊢ A → Γ′ ⊢ A′ → A ≤ A′ → Set where

  `≤` : ∀ {Γ Γ′ A A′ x x′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′}
    → Γ≤ ⊢ x ≤ˣ x′ ⦂ p
      --------------------
    → Γ≤ ⊢ ` x ≤ᴹ ` x′ ⦂ p

  ƛ≤ƛ : ∀ {Γ Γ′ A A′ B B′ N N′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ⇒ B ≤ A′ ⇒ B′}
    → Γ≤ ▷ dom p ⊢ N ≤ᴹ N′ ⦂ cod p
      ----------------------------
    → Γ≤ ⊢ ƛ N ≤ᴹ ƛ N′ ⦂ p

  ·≤· : ∀ {Γ Γ′ A A′ B B′ L L′ M M′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ⇒ B ≤ A′ ⇒ B′}
    → Γ≤ ⊢ L ≤ᴹ L′ ⦂ p
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ dom p
      -----------------------------
    → Γ≤ ⊢ L · M ≤ᴹ L′ · M′ ⦂ cod p

  $≤$ : ∀ {Γ Γ′ ι} {Γ≤ : Γ ≤ᴳ Γ′}
    → (k : rep ι)
      ------------------------
    → Γ≤ ⊢ $ k ≤ᴹ $ k ⦂ id

  ⦅⦆≤⦅⦆ : ∀ {Γ Γ′ ι ι′ ι″ M M′ N N′} {Γ≤ : Γ ≤ᴳ Γ′}
    → (_⊕_ : rep ι → rep ι′ → rep ι″)
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ id
    → Γ≤ ⊢ N ≤ᴹ N′ ⦂ id
      -------------------------------------
    → Γ≤ ⊢ M ⦅ _⊕_ ⦆ N ≤ᴹ M′ ⦅ _⊕_ ⦆ N′ ⦂ id
    
  ⇑≤⇑ : ∀ {Γ Γ′ G M M′} {Γ≤ : Γ ≤ᴳ Γ′}
    → (g : Ground G)
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ id
      -----------------------------
    → Γ≤ ⊢ (M ⇑ g) ≤ᴹ (M′ ⇑ g) ⦂ id

  ≤⇑ : ∀ {Γ Γ′ A G M M′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ G}
    → (g : Ground G)
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ p
      --------------------------
    → Γ≤ ⊢ M ≤ᴹ (M′ ⇑ g) ⦂ p ⇑ g

  ⟨⟩≤ : ∀ {Γ Γ′ A B C} {M : Γ ⊢ A} {M′ : Γ′ ⊢ C}
         {Γ≤ : Γ ≤ᴳ Γ′} {±p : A => B} {q : B ≤ C} {r : A ≤ C}
    → commute≤ ±p q r
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ r
      -------------------------
    → Γ≤ ⊢ (M ⟨ ±p ⟩) ≤ᴹ M′ ⦂ q

  ≤⟨⟩ : ∀ {Γ Γ′ A B C} {M : Γ ⊢ A} {M′ : Γ′ ⊢ B}
         {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ B} {±q : B => C} {r : A ≤ C}
    → ≤commute p ±q r
    → Γ≤ ⊢ M ≤ᴹ M′ ⦂ p
      -------------------------
    → Γ≤ ⊢ M ≤ᴹ (M′ ⟨ ±q ⟩) ⦂ r

  blame≤ : ∀ {Γ Γ′ A A′ M′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′}
      ---------------------
    → Γ≤ ⊢ blame ≤ᴹ M′ ⦂ p

  wrap≤ : ∀ {Γ Γ′ A A′ A″ B B′ B″}
             {Γ≤ : Γ ≤ᴳ Γ′} {N : Γ ▷ A ⊢ B} {N′ : Γ′ ▷ A″ ⊢ B″}
             {±p : A ⇒ B => A′ ⇒ B′} {q : A′ ⇒ B′ ≤ A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
             {∓s : A′ => A} {±t : B => B′}
    → split ±p ≡ ∓s ⇒ ±t
    → commute≤ ±p q r
    → Γ≤ ⊢ ƛ N ≤ᴹ ƛ N′ ⦂ r
      -----------------------------------------------------
    → Γ≤ ⊢ ƛ (lift (ƛ N) · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩) ≤ᴹ ƛ N′ ⦂ q

  ≤wrap : ∀ {Γ Γ′ A A′ A″ B B′ B″}
             {Γ≤ : Γ ≤ᴳ Γ′} {N : Γ ▷ A ⊢ B} {N′ : Γ′ ▷ A′ ⊢ B′}
             {p : A ⇒ B ≤ A′ ⇒ B′} {±q : A′ ⇒ B′ => A″ ⇒ B″} {r : A ⇒ B ≤ A″ ⇒ B″}
             {∓s : A″ => A′} {±t : B′ => B″}
    → split ±q ≡ ∓s ⇒ ±t
    → ≤commute p ±q r
    → Γ≤ ⊢ ƛ N ≤ᴹ ƛ N′ ⦂ p
      -----------------------------------------------------
    → Γ≤ ⊢ ƛ N ≤ᴹ ƛ (lift (ƛ N′) · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩) ⦂ r
```

## Upcast congruence

```
+≤+ : ∀ {Γ Γ′ A B A′ B′} {M : Γ ⊢ A} {M′ : Γ′ ⊢ A′} {Γ≤ : Γ ≤ᴳ Γ′}
    {p : A ≤ B} {q : A′ ≤ B′} {s : A ≤ A′} {t : B ≤ B′}
  → p ⨟ t ≡ s ⨟ q
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ s
    ------------------------------------
  → Γ≤ ⊢ (M ⟨ + p ⟩) ≤ᴹ (M′ ⟨ + q ⟩) ⦂ t
+≤+ e M≤M′ = ⟨⟩≤ e (≤⟨⟩ refl M≤M′)
```

Here is the derivation:

    Γ≤ ⊢ M ≤ᴹ M′ ⦂ s
    s ⨟ q ≡ r
    ---------------------- ≤+
    Γ≤ ⊢ M ≤ᴹ (M′ + q) ⦂ r
    p ⨟ t ≡ r
    ---------------------------- +≤
    Γ≤ ⊢ (M + p) ≤ᴹ (M′ + q) ⦂ t

Here it is illustrated:

                   s
         M : A    ---→    M′ : A′
           |       \         |
         p |      r \        | q
           -         ↘       -
      (M + p) : B ---→ (M′ + q) : B′
                   t


## Downcast congruence

```
-≤- : ∀ {Γ Γ′ A B A′ B′} {M : Γ ⊢ B} {M′ : Γ′ ⊢ B′} {Γ≤ : Γ ≤ᴳ Γ′}
    {p : A ≤ B} {q : A′ ≤ B′} {s : A ≤ A′} {t : B ≤ B′}
  → s ⨟ q ≡ p ⨟ t
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ t
    ------------------------------------
  → Γ≤ ⊢ (M ⟨ - p ⟩) ≤ᴹ (M′ ⟨ - q ⟩) ⦂ s
-≤- e M≤M′ = ≤⟨⟩ e (⟨⟩≤ refl M≤M′)
```

Here is the derivation:

    Γ≤ ⊢ M ≤ᴹ M′ ⦂ t
    p ⨟ t ≡ r
    ---------------------- ≤+
    Γ≤ ⊢ (M - p) ≤ᴹ M′ ⦂ r
    s ⨟ q ≡ r
    ---------------------------- +≤
    Γ≤ ⊢ (M - p) ≤ᴹ (M′ - q) ⦂ t

Here it is illustrated:

                   s
         M : A    ---→    M′ : A′
           |       \         |
         p |      r \        | q
           -         ↘       -
      (M + p) : B ---→ (M′ + q) : B′
                   t


## Reflexivity of term precision

```
reflˣ : ∀ {Γ A}
    → (x : Γ ∋ A)
      -------------------
    → idᴳ ⊢ x ≤ˣ x ⦂ id
reflˣ Z      =  Z≤Z
reflˣ (S x)  =  S≤S (reflˣ x)

reflᴹ : ∀ {Γ A}
    → (M : Γ ⊢ A)
      -------------------
    → idᴳ ⊢ M ≤ᴹ M ⦂ id
reflᴹ (` x)          =  `≤` (reflˣ x)
reflᴹ (ƛ M)          =  ƛ≤ƛ (reflᴹ M)
reflᴹ (L · M)        =  ·≤· (reflᴹ L) (reflᴹ M)
reflᴹ ($ k)          =  $≤$ k
reflᴹ (M ⦅ _⊕_ ⦆ N)  =  ⦅⦆≤⦅⦆ _⊕_ (reflᴹ M) (reflᴹ N)
reflᴹ (M ⇑ g)        =  ⇑≤⇑ g (reflᴹ M)
reflᴹ (M ⟨ + p ⟩)    =  +≤+ (sym (left-id p)) (reflᴹ M)
reflᴹ (M ⟨ - p ⟩)    =  -≤- (left-id p) (reflᴹ M)
reflᴹ blame          =  blame≤
```

## Renaming

Extension
```
ren▷≤ : ∀ {Γ Γ′ Δ Δ′} {Γ≤ : Γ ≤ᴳ Γ′} {Δ≤ : Δ ≤ᴳ Δ′} {ρ : Γ →ᴿ Δ} {ρ′ : Γ′ →ᴿ Δ′} 
  → (∀ {A A′} {A≤ : A ≤ A′} {x x′}
      → Γ≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ⊢ ρ x ≤ˣ ρ′ x′ ⦂ A≤)
    ------------------------------------------------------
  → (∀ {A A′ B B′} {A≤ : A ≤ A′} {B≤ : B ≤ B′} {x x′}
      → Γ≤ ▷ B≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ▷ B≤ ⊢ ren▷ ρ x ≤ˣ ren▷ ρ′ x′ ⦂ A≤)
ren▷≤ ρ≤ Z≤Z       =  Z≤Z
ren▷≤ ρ≤ (S≤S x≤)  =  S≤S (ρ≤ x≤)
```

Preservation of precision under renaming
```
ren≤ : ∀ {Γ Γ′ Δ Δ′} {Γ≤ : Γ ≤ᴳ Γ′} {Δ≤ : Δ ≤ᴳ Δ′} {ρ : Γ →ᴿ Δ} {ρ′ : Γ′ →ᴿ Δ′} 
  → (∀ {A A′} {A≤ : A ≤ A′} {x x′}
      → Γ≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ⊢ ρ x ≤ˣ ρ′ x′ ⦂ A≤)
    -------------------------------------------
  → (∀ {A A′} {A≤ : A ≤ A′} {M M′}
      → Γ≤ ⊢ M ≤ᴹ M′ ⦂ A≤
      → Δ≤ ⊢ ren ρ M ≤ᴹ ren ρ′ M′ ⦂ A≤)
ren≤ ρ≤ (`≤` x)              =  `≤` (ρ≤ x)
ren≤ ρ≤ (ƛ≤ƛ N≤)             =  ƛ≤ƛ (ren≤ (ren▷≤ ρ≤) N≤)
ren≤ ρ≤ (·≤· L≤ M≤)          =  ·≤· (ren≤ ρ≤ L≤) (ren≤ ρ≤ M≤)
ren≤ ρ≤ ($≤$ k)              =  $≤$ k
ren≤ ρ≤ (⦅⦆≤⦅⦆ _⊕_ M≤ N≤)     =  ⦅⦆≤⦅⦆ _⊕_ (ren≤ ρ≤ M≤) (ren≤ ρ≤ N≤) 
ren≤ ρ≤ (⇑≤⇑ g M≤)           =  ⇑≤⇑ g (ren≤ ρ≤ M≤)
ren≤ ρ≤ (≤⇑ g M≤)            =  ≤⇑ g (ren≤ ρ≤ M≤)
ren≤ ρ≤ (⟨⟩≤ e M≤)           =  ⟨⟩≤ e (ren≤ ρ≤ M≤)
ren≤ ρ≤ (≤⟨⟩ e M≤)           =  ≤⟨⟩ e (ren≤ ρ≤ M≤)
ren≤ ρ≤ blame≤               =  blame≤
ren≤ {ρ = ρ} ρ≤ (wrap≤ {A′ = A′}{N = N} i e ƛN≤ƛN′)
  rewrite sym (lift∘ren {A = A′} ρ (ƛ N))
  =  wrap≤ i e (ren≤ ρ≤ ƛN≤ƛN′)
ren≤ {ρ′ = ρ′} ρ≤ (≤wrap {A″ = A″}{N′ = N′} i e ƛN≤ƛN′)
  rewrite sym (lift∘ren {A = A″} ρ′ (ƛ N′))
  =  ≤wrap i e (ren≤ ρ≤ ƛN≤ƛN′)
```

## Substitution

Extension
```
sub▷≤ : ∀ {Γ Γ′ Δ Δ′} {Γ≤ : Γ ≤ᴳ Γ′} {Δ≤ : Δ ≤ᴳ Δ′} {σ : Γ →ˢ Δ} {σ′ : Γ′ →ˢ Δ′} 
  → (∀ {A A′} {A≤ : A ≤ A′} {x x′}
      → Γ≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ⊢ σ x ≤ᴹ σ′ x′ ⦂ A≤)
    ------------------------------------------------------
  → (∀ {A A′ B B′} {A≤ : A ≤ A′} {B≤ : B ≤ B′} {x x′}
      → Γ≤ ▷ B≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ▷ B≤ ⊢ sub▷ σ x ≤ᴹ sub▷ σ′ x′ ⦂ A≤)
sub▷≤ σ≤ Z≤Z       =  `≤` Z≤Z
sub▷≤ σ≤ (S≤S x≤)  =  ren≤ S≤S (σ≤ x≤)
```

Preservation of precision under substitution
```
sub≤ : ∀ {Γ Γ′ Δ Δ′} {Γ≤ : Γ ≤ᴳ Γ′} {Δ≤ : Δ ≤ᴳ Δ′} {σ : Γ →ˢ Δ} {σ′ : Γ′ →ˢ Δ′} 
  → (∀ {A A′} {A≤ : A ≤ A′} {x x′}
      → Γ≤ ⊢ x ≤ˣ x′ ⦂ A≤
      → Δ≤ ⊢ σ x ≤ᴹ σ′ x′ ⦂ A≤)
    -------------------------------------------
  → (∀ {A A′} {A≤ : A ≤ A′} {M M′}
      → Γ≤ ⊢ M ≤ᴹ M′ ⦂ A≤
      → Δ≤ ⊢ sub σ M ≤ᴹ sub σ′ M′ ⦂ A≤)
sub≤ σ≤ (`≤` x)              =  σ≤ x
sub≤ σ≤ (ƛ≤ƛ N≤)             =  ƛ≤ƛ (sub≤ (sub▷≤ σ≤) N≤)
sub≤ σ≤ (·≤· L≤ M≤)          =  ·≤· (sub≤ σ≤ L≤) (sub≤ σ≤ M≤)
sub≤ σ≤ ($≤$ k)              =  $≤$ k
sub≤ σ≤ (⦅⦆≤⦅⦆ _⊕_ M≤ N≤)     =  ⦅⦆≤⦅⦆ _⊕_ (sub≤ σ≤ M≤) (sub≤ σ≤ N≤) 
sub≤ σ≤ (⇑≤⇑ g M≤)           =  ⇑≤⇑ g (sub≤ σ≤ M≤)
sub≤ σ≤ (≤⇑ g M≤)            =  ≤⇑ g (sub≤ σ≤ M≤)
sub≤ σ≤ (⟨⟩≤ e M≤)           =  ⟨⟩≤ e (sub≤ σ≤ M≤)
sub≤ σ≤ (≤⟨⟩ e M≤)           =  ≤⟨⟩ e (sub≤ σ≤ M≤)
sub≤ σ≤ blame≤               =  blame≤
sub≤ {σ = σ} σ≤ (wrap≤ {A′ = A′}{N = N} i e ƛN≤ƛN′)
  rewrite sym (lift∘sub {A = A′} σ (ƛ N))
  =  wrap≤ i e (sub≤ σ≤ ƛN≤ƛN′)
sub≤ {σ′ = σ′} σ≤ (≤wrap {A″ = A″}{N′ = N′} i e ƛN≤ƛN′)
  rewrite sym (lift∘sub {A = A″} σ′ (ƛ N′))
  =  ≤wrap i e (sub≤ σ≤ ƛN≤ƛN′)
```

Preservation of precision under substitution, special case for beta

```
σ₀≤σ₀ : ∀ {Γ Γ′ A A′ M M′} {Γ≤ : Γ ≤ᴳ Γ′} {s : A ≤ A′}
  → (Γ≤ ⊢ M ≤ᴹ M′ ⦂ s)
  → ∀ {B B′ x x′} {t : B ≤ B′}
  → (Γ≤ ▷ s ⊢ x ≤ˣ x′ ⦂ t)
    -------------------------------------------------
  → (Γ≤ ⊢ σ₀ M x ≤ᴹ σ₀ M′ x′ ⦂ t)
σ₀≤σ₀ M≤M′ Z≤Z         =  M≤M′
σ₀≤σ₀ M≤M′ (S≤S x≤x′)  =  `≤` x≤x′

[]≤[] : ∀ {Γ Γ′ A A′ B B′ N N′ M M′} {Γ≤ : Γ ≤ᴳ Γ′} {s : A ≤ A′} {t : B ≤ B′}
        → Γ≤ ▷ s ⊢ N ≤ᴹ N′ ⦂ t
        → Γ≤ ⊢ M ≤ᴹ M′ ⦂ s
          -----------------------------
        → Γ≤ ⊢ N [ M ] ≤ᴹ N′ [ M′ ] ⦂ t
[]≤[] N≤N′ M≤M′ =  sub≤ (σ₀≤σ₀ M≤M′) N≤N′
```


## Relating a term to its type erasure

```
ƛ≤ƛ★ : ∀ {Γ Γ′ A B N N′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ⇒ B ≤ ★ ⇒ ★}
  → Γ≤ ▷ dom p ⊢ N ≤ᴹ N′ ⦂ cod p
    ----------------------------
  → Γ≤ ⊢ ƛ N ≤ᴹ ƛ★ N′ ⦂ p ⇑ ★⇒★
ƛ≤ƛ★ N≤N′ = ≤⟨⟩ refl (ƛ≤ƛ N≤N′)  

·≤·★ : ∀ {Γ Γ′ A B L L′ M M′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ⇒ B ≤ ★ ⇒ ★}
  → Γ≤ ⊢ L ≤ᴹ L′ ⦂ p ⇑ ★⇒★
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ dom p
    ------------------------------
  → Γ≤ ⊢ L · M ≤ᴹ L′ ·★ M′ ⦂ cod p
·≤·★ L≤L′ M≤M′ = ·≤· (≤⟨⟩ refl L≤L′) M≤M′

$≤$★ : ∀ {Γ Γ′ ι} {Γ≤ : Γ ≤ᴳ Γ′} (k : rep ι) → Γ≤ ⊢ $ k ≤ᴹ $★ k ⦂ ι ≤★
$≤$★ {ι = ι} k  =  ≤⇑ ($ ι) ($≤$ k)

⦅⦆≤⦅⦆★ : ∀ {Γ Γ′ ι ι′ ι″ M M′ N N′} {Γ≤ : Γ ≤ᴳ Γ′}
  → (_⊕_ : rep ι → rep ι′ → rep ι″)
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ ι ≤★
  → Γ≤ ⊢ N ≤ᴹ N′ ⦂ ι′ ≤★
    ------------------------------------------
  → Γ≤ ⊢ M ⦅ _⊕_ ⦆ N ≤ᴹ M′ ⦅ _⊕_ ⦆★ N′ ⦂ ι″ ≤★
⦅⦆≤⦅⦆★ _⊕_ M≤M′ N≤N′ = ≤⟨⟩ refl (⦅⦆≤⦅⦆ _⊕_ (≤⟨⟩ refl M≤M′) (≤⟨⟩ refl N≤N′))    

⌈_⌉≤ᴳ : ∀ (Γ : Context) → Γ ≤ᴳ ⌈ Γ ⌉ᴳ
⌈ ∅ ⌉≤ᴳ          =  ∅
⌈ Γ ▷ A ⌉≤ᴳ      =  ⌈ Γ ⌉≤ᴳ ▷ A≤★

⌈_⌉≤ˣ : ∀ {Γ A} → (x : Γ ∋ A) → ⌈ Γ ⌉≤ᴳ ⊢ x ≤ˣ ⌈ x ⌉ˣ ⦂ A≤★ 
⌈ Z ⌉≤ˣ          =  Z≤Z
⌈ S x ⌉≤ˣ        =  S≤S ⌈ x ⌉≤ˣ

⌈_⌉≤ : ∀ {Γ A} {M : Γ ⊢ A} → (m : Static M) → ⌈ Γ ⌉≤ᴳ ⊢ M ≤ᴹ (⌈ m ⌉) ⦂ A≤★ 
⌈ ` x ⌉≤         =  `≤` ⌈ x ⌉≤ˣ
⌈ ƛ N ⌉≤         =  ƛ≤ƛ★ ⌈ N ⌉≤
⌈ L · M ⌉≤       =  ·≤·★ ⌈ L ⌉≤ ⌈ M ⌉≤
⌈ $ k ⌉≤         =  $≤$★ k
⌈ M ⦅ _⊕_ ⦆ N ⌉≤  =  ⦅⦆≤⦅⦆★ _⊕_ ⌈ M ⌉≤ ⌈ N ⌉≤
```

## Example

```
inc≤inc★ : ∅ ⊢ inc ≤ᴹ inc★ ⦂ ℕ⇒ℕ≤★
inc≤inc★ = ⌈ Inc ⌉≤

inc≤inc★′ : ∅ ⊢ inc ≤ᴹ inc★′ ⦂ ℕ⇒ℕ≤★
inc≤inc★′ = ≤⟨⟩ refl (reflᴹ inc)

inc2≤inc★2★ : ∅ ⊢ inc · ($ 2) ≤ᴹ inc★ ·★ ($★ 2) ⦂ ℕ≤★
inc2≤inc★2★ = ⌈ Inc · ($ 2) ⌉≤

inc2≤inc★′2★ : ∅ ⊢ inc · ($ 2) ≤ᴹ inc★′ ·★ ($★ 2) ⦂ ℕ≤★
inc2≤inc★′2★ = ·≤·★ inc≤inc★′ ($≤$★ 2)
```

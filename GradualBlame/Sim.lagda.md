Simple Blame Calculus with proof relevant casts.
Uses polarity to unify upcasts and downcasts.
Uses nested evaluation contexts.

Siek, Thiemann, and Wadler

```
module Sim where

open import Data.Nat using (ℕ; zero; suc; _+_)
open import Data.Bool using (true; false) renaming (Bool to 𝔹)
open import Data.Unit using (⊤; tt)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂) renaming ([_,_] to case-⊎)
open import Relation.Binary.PropositionalEquality
     using (_≡_; _≢_; refl; trans; sym; cong; cong₂; cong-app; subst; inspect)
     renaming ([_] to [[_]])
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Decidable using (⌊_⌋; True; toWitness; fromWitness)
open import Core
open import Prec
```

## Cast lemma
```
cast : ∀ {Γ Γ′ A B C} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ B} {r : A ≤ C}
           {V : Γ ⊢ A} {V′ : Γ′ ⊢ B}
  → Value V
  → Value V′
  → (±q : B => C)
  → ≤commute p ±q r
  → Γ≤ ⊢ V ≤ᴹ V′ ⦂ p
    -------------------------------------------------------
  → ∃[ W ](Value W × (V′ ⟨ ±q ⟩ —↠ W ) × (Γ≤ ⊢ V ≤ᴹ W ⦂ r))
cast v v′ ±q e V≤V′ with split ±q in e′
cast v v′ ±q e V≤V′ | id
    rewrite ≤ident ±q e′ e
    =  _ , v′ , unit (ident e′ v′) , V≤V′
cast (ƛ _) (ƛ _) ±q e ƛN≤ƛN′ | ∓s ⇒ ±t
    =  ƛ _ , ƛ _ , unit (wrap e′) , ≤wrap e′ e ƛN≤ƛN′
cast v v′ (+ q ⇑ g) refl V≤V′ | other
    with cast v v′ (+ q) refl V≤V′
... |  W′ , w , V′+—↠W′ , V≤W′
    =  (W′ ⇑ g) , (w ⇑ g) , (unit (expand v′ g) ++ ξ* ([ □ ]⇑ g) V′+—↠W′) , ≤⇑ g V≤W′
cast v (v′ ⇑ g) (- q ⇑ .g) refl (≤⇑ .g  V≤V′) | other
    with cast v v′ (- q) refl V≤V′
... |  W′ , w′ , V′-—↠W′ , V≤W′
    =  W′ , w′ , (unit (collapse v′ g) ++ V′-—↠W′) , V≤W′
```

## Catch up lemma

Catch up Lemma.  If `V ≤ᴹ M′` then `M′ —↠ V′` and `V ≤ᴹ V′` for some `V′`.
```
catchup : ∀ {Γ Γ′ A A′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′} {V : Γ ⊢ A} {M′ : Γ′ ⊢ A′}
  → Value V
  → Γ≤ ⊢ V ≤ᴹ M′ ⦂ p
    ----------------------------------------------------
  → ∃[ V′ ](Value V′ × (M′ —↠ V′) × (Γ≤ ⊢ V ≤ᴹ V′ ⦂ p))
catchup (ƛ _) (ƛ≤ƛ {N′ = N′} ƛN≤ƛN′)
    =  ƛ N′ , ƛ N′ , (ƛ N′ ∎) , ƛ≤ƛ ƛN≤ƛN′
catchup ($ _) ($≤$ k)
    =  $ k , $ k , ($ k ∎) , ($≤$ k)
catchup (v ⇑ g) (⇑≤⇑ {M = V} {M′ = M′} .g V≤M′)
    with catchup v V≤M′
... |  V′ , v′ , M′—↠V′ , V≤V′
    =  V′ ⇑ g , v′ ⇑ g , ξ* ([ □ ]⇑ g) M′—↠V′ , ⇑≤⇑ g V≤V′
catchup v (≤⇑ h V≤M′)
    with catchup v V≤M′
... |  V′ , v′ , M′—↠V′ , V≤V′
    =  V′ ⇑ h , v′ ⇑ h , ξ* ([ □ ]⇑ h) M′—↠V′ , ≤⇑ h V≤V′
catchup v (≤⟨⟩ {M′ = M′} {±q = ±q} e V≤M′)
    with catchup v V≤M′
... |  V′ , v′ , M′—↠V′ , V≤V′
    with cast v v′ ±q e V≤V′
... |  W , w , V⟨±q⟩—↠W , V≤W
    =  W , w , (ξ* ([ □ ]⟨ ±q ⟩) M′—↠V′ ++ V⟨±q⟩—↠W) , V≤W
catchup (ƛ _) (wrap≤ e′ e ƛN≤ƛN′)
    =  _ , ƛ _ , (_ ∎) , wrap≤ e′ e ƛN≤ƛN′
catchup (ƛ _) (≤wrap e′ e ƛN≤ƛN′)
    =  _ , ƛ _ , (_ ∎) , ≤wrap e′ e ƛN≤ƛN′
```

## Substitution lemma

```
lift[] : ∀ {Γ A B}
  → (V : Γ ⊢ B)
  → (W : Γ ⊢ A)
    --------------------
  → (lift V) [ W ] ≡ V
lift[] {Γ = Γ} V W = trans (sub∘ren σ∘ V) (subId σId V)
  where
  σ∘ : ∀ {A} (x : Γ ∋ A) → σ₀ W (S x) ≡ ` x
  σ∘ x = refl
  σId : ∀ {A} (x : Γ ∋ A) → ` x ≡ ` x
  σId x = refl
```

The following lemma describes β-reduction of a wrap applied to a
value, using the lemma above to simplify the right-hand side of the
reduction as required.

```
wrapβ : ∀ {Γ A B A′ B′ ∓s ±t} {V : Γ ⊢ A ⇒ B} {W : Γ ⊢ A′} {±p : A ⇒ B => A′ ⇒ B′}
  → split ±p ≡ ∓s ⇒ ±t
  → Value W
    ---------------------------------------------------------------
  → (ƛ (lift V · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩)) · W —→ V · (W ⟨ ∓s ⟩) ⟨ ±t ⟩
wrapβ {∓s = ∓s}{±t = ±t}{V = V}{W} e w  =
  subst
    (λ X → (ƛ (lift V · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩)) · W —→ X · (W ⟨ ∓s ⟩) ⟨ ±t ⟩)
    (lift[] V W)
    (ξ □ (β w))
```

## β-reduction is a simulation

```
simβ : ∀ {Γ Γ′ A A′ B B′} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ⇒ B ≤ A′ ⇒ B′}
    {N : Γ ▷ A ⊢ B} {N′ : Γ′ ▷ A′ ⊢ B′} {W : Γ ⊢ A} {W′ : Γ′ ⊢ A′}
  → Value W
  → Value W′
  → Γ≤ ⊢ ƛ N ≤ᴹ ƛ N′ ⦂ p
  → Γ≤ ⊢ W ≤ᴹ W′ ⦂ dom p
    -----------------------------------------------------------
  → ∃[ M′ ](((ƛ N′) · W′ —↠ M′) × (Γ≤ ⊢ N [ W ] ≤ᴹ M′ ⦂ cod p))

simβ w w′ (ƛ≤ƛ N≤N′) W≤W′
    =  _ , (unit (β w′)) , ([]≤[] N≤N′ W≤W′)

simβ {W = W}{W′} w w′ (wrap≤ {N = N}{N′} e′ e ƛN≤ƛN′) W≤W′
    rewrite lift[] (ƛ N) W
    =  (ƛ N′) · W′ , (_ ∎) , (⟨⟩≤ (cod≤ e′ e) (·≤· ƛN≤ƛN′ (⟨⟩≤ (dom≤ e′ e) W≤W′)))

simβ {W = W}{W′} w w′ (≤wrap {N = N}{N′}{∓s = ∓s}{±t = ±t} e′ e ƛN≤ƛN′) W≤W′
    with cast w w′ ∓s (≤dom e′ e) W≤W′
... |  W″ , w″ , W′-—↠W″ , W≤W″
    with simβ w w″ ƛN≤ƛN′ W≤W″
... |  M′ , [ƛN′]·W″—↠M′ , N[W]≤M′
    =  (M′ ⟨ ±t ⟩) ,
       (begin
          (ƛ (lift (ƛ N′) · (` Z ⟨ ∓s ⟩) ⟨ ±t ⟩)) · W′
        —→⟨ wrapβ e′ w′ ⟩
          ((ƛ N′) · (W′ ⟨ ∓s ⟩)) ⟨ ±t ⟩
        —↠⟨ ξ* ([ (ƛ _) ·[ □ ] ]⟨ _ ⟩) W′-—↠W″ ⟩
          ((ƛ N′) · W″) ⟨ ±t ⟩
        —↠⟨ ξ* ([ □ ]⟨ _ ⟩) [ƛN′]·W″—↠M′ ⟩
          M′ ⟨ ±t ⟩
        ∎) ,
       (≤⟨⟩ (≤cod e′ e) N[W]≤M′)
```   

## Term precision is a simulation (Gradual Guarantee)
```
sim : ∀ {Γ Γ′ A A′ M M′ N} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′}
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ p
  → M —→ N
    -----------------------------------------
  → ∃[ N′ ]((M′ —↠ N′) × (Γ≤ ⊢ N ≤ᴹ N′ ⦂ p))
sim (`≤` x≤x′) M—→N
    =  ⊥-elim (variable-irreducible M—→N)
sim (ƛ≤ƛ ƛN≤ƛN′) M—→N
    =  ⊥-elim (value-irreducible (ƛ _) M—→N)
sim (·≤· L≤L′ M≤M′) (ξ ([ E ]· _) L↦N)
    with sim L≤L′ (ξ E L↦N)
... |  N′ , L′—↠N′ , N≤N′
    =  N′ · _ , ξ* ([ □ ]· _) L′—↠N′ , ·≤· N≤N′ M≤M′
sim (·≤· V≤L′ M≤M′) (ξ (v ·[ E ]) M↦N)
    with catchup v V≤L′
... |  V′ , v′ , L′—↠V′ , V≤V′
    with sim M≤M′ (ξ E M↦N)
... |  N′ , M′—↠N′ , N≤N′
    =  V′ · N′ , (ξ* ([ □ ]· _) L′—↠V′ ++ ξ* (v′ ·[ □ ]) M′—↠N′) , ·≤· V≤V′ N≤N′
sim (·≤· ƛN≤L′ W≤M′) (ξ □ (β w))
    with catchup (ƛ _) ƛN≤L′
... |  ƛ N′ , v′ , L′—↠ƛN′ , ƛN≤ƛN′
    with catchup w W≤M′
... |  W′ , w′ , M′—↠W′ , W≤W′
    with simβ w w′ ƛN≤ƛN′ W≤W′
... |  M′ , ƛN′·W′—↠M′ , N[V]≤M′
    =  M′ , (ξ* ([ □ ]· _) L′—↠ƛN′ ++ ξ* (v′ ·[ □ ]) M′—↠W′ ++ ƛN′·W′—↠M′) , N[V]≤M′
sim ($≤$ k) M—→N
    =  ⊥-elim (value-irreducible ($ _) M—→N)
sim (⦅⦆≤⦅⦆ _⊕_ L≤L′ M≤M′) (ξ ([ E ]⦅ ._⊕_ ⦆ _) L↦N)
    with sim L≤L′ (ξ E L↦N)
... |  N′ , L′—↠N′ , N≤N′
    =  N′ ⦅ _⊕_ ⦆ _ , ξ* ([ □ ]⦅ _⊕_ ⦆ _) L′—↠N′ , ⦅⦆≤⦅⦆ _⊕_ N≤N′ M≤M′
sim (⦅⦆≤⦅⦆ _⊕_ V≤L′ M≤M′) (ξ (v ⦅ ._⊕_ ⦆[ E ]) M↦N)
    with catchup v V≤L′
... |  V′ , v′ , L′—↠V′ , V≤V′
    with sim M≤M′ (ξ E M↦N)
... |  N′ , M′—↠N′ , N≤N′
    =  V′ ⦅ _⊕_ ⦆ N′ , (ξ* ([ □ ]⦅ _⊕_ ⦆ _) L′—↠V′ ++ ξ* (v′ ⦅ _⊕_ ⦆[ □ ]) M′—↠N′) , ⦅⦆≤⦅⦆ _⊕_ V≤V′ N≤N′
sim (⦅⦆≤⦅⦆ _⊕_ V≤L′ W≤M′) (ξ □ δ)
    with catchup ($ _) V≤L′
... |  $ k , $ .k , L′—↠V′ , ($≤$ .k)
    with catchup ($ _) W≤M′
... |  $ k′ , $ .k′ , M′—↠W′ , ($≤$ .k′)
    =  $ (k ⊕ k′) , (ξ* ([ □ ]⦅ _⊕_ ⦆ _) L′—↠V′ ++ ξ* ($ k ⦅ _⊕_ ⦆[ □ ]) M′—↠W′ ++ unit δ) , $≤$ (k ⊕ k′)
sim (⇑≤⇑ g M≤M′) (ξ □ M↦N)
    =  ⊥-elim (box-irreducible g M↦N)
sim (⇑≤⇑ g M≤M′) (ξ ([ E ]⇑ .g) M↦N)
    with sim M≤M′ (ξ E M↦N)
... |  N′ , M′—↠N′ , N≤N′
    =  N′ ⇑ g , ξ* ([ □ ]⇑ g) M′—↠N′ , ⇑≤⇑ g N≤N′
sim (≤⇑ g M≤M′) M—→N
    with sim M≤M′ M—→N
... |  N′ , M′—↠N′ , N≤N′
    =  N′ ⇑ g , ξ* ([ □ ]⇑ g) M′—↠N′ , ≤⇑ g N≤N′
sim (⟨⟩≤ e M≤M′) (ξ ([ E ]⟨ ∓s ⟩) M↦N)
    with sim M≤M′ (ξ E M↦N)
... |  N′ , M′—↠N′ , N≤N′
    =  N′ , M′—↠N′ , ⟨⟩≤ e N≤N′
sim (⟨⟩≤ {±p = ±p}{q = q}{r = r} e V≤M′) (ξ □ (ident e′ v))
    rewrite ident≤ ±p e′ e
    =  _ , (_ ∎) , V≤M′
sim (⟨⟩≤ {q = id} e V≤M′) (ξ □ (wrap e′))
    with catchup (ƛ _) V≤M′
... |  V′ , ƛ _ , M′—↠V′ , ƛN≤ƛN′
    =  V′ , M′—↠V′ , wrap≤ e′ e ƛN≤ƛN′
sim (⟨⟩≤ {q = _ ⇒ _} e V≤M′) (ξ □ (wrap e′))
    with catchup (ƛ _) V≤M′
... |  V′ , ƛ _ , M′—↠V′ , ƛN≤ƛN′
    =  V′ , M′—↠V′ , wrap≤ e′ e ƛN≤ƛN′
sim (⟨⟩≤ {q = q ⇑ ★⇒★} e V≤M′) (ξ □ (wrap e′))
    with catchup (ƛ _) V≤M′
... |  V′ ⇑ ★⇒★ , (ƛ _) ⇑ ★⇒★ , M′—↠V′⇑ , ≤⇑ ★⇒★ ƛN≤ƛN′
    =  V′ ⇑ ★⇒★ , M′—↠V′⇑ , ≤⇑ ★⇒★ (wrap≤ e′ (drop⇑ e) ƛN≤ƛN′)
sim (⟨⟩≤ {M = V} {±p = + p ⇑ .g} {q = id} {r = r} refl V≤M′) (ξ □ (expand v g))
    with catchup v V≤M′
... |  V′ ⇑ .g , v′ ⇑ .g , M′—↠V′⇑ , ≤⇑ _ V≤V′
    =  V′ ⇑ g , M′—↠V′⇑ , ⇑≤⇑ g (⟨⟩≤ refl V≤V′)
sim (⟨⟩≤ {M = V} {±p = + p ⇑ .g} {q = q ⇑ h} refl V≤M′) (ξ □ (expand v g))
    =  ⊥-elim (¬★≤G h q)
sim (⟨⟩≤ {M = V ⇑ .g} {±p = - p ⇑ .g} {r = id} refl V⇑≤M′) (ξ □ (collapse v g))
   with catchup (v ⇑ g) V⇑≤M′
... |  V′ ⇑ .g , v′ ⇑ .g , M′—↠V′⇑ , ⇑≤⇑ .g V≤V′
    =  V′ ⇑ g , M′—↠V′⇑ , ≤⇑ g (⟨⟩≤ refl V≤V′)
sim (⟨⟩≤ {M = V ⇑ .g} {±p = - p ⇑ .h} {r = id} refl V⇑≤M′) (ξ □ (collide v g h G≢H))
    =  _ , (_ ∎) , blame≤
sim (⟨⟩≤ {M = V ⇑ .g} {±p = - p ⇑ .g} {r = r ⇑ h} refl V⇑≤M′) (ξ □ (collapse v g))
    =  ⊥-elim (¬★≤G h r)
sim (⟨⟩≤ {M = V ⇑ .g} {±p = - p ⇑ .h} {r = r ⇑ h′} refl V⇑≤M′) (ξ □ (collide v g h G≢H))
    =  ⊥-elim (¬★≤G h′ r)
sim (≤⟨⟩ {±q = ±q} e M≤M′) M—→N
    with sim M≤M′ M—→N
... |  N′ , M′—↠N′ , N≤N′
    =  N′ ⟨ ±q ⟩ , ξ* ([ □ ]⟨ ±q ⟩) M′—↠N′ , ≤⟨⟩ e N≤N′
sim blame≤ M—→N
    =  ⊥-elim (blame-irreducible M—→N)
sim (wrap≤ i e V≤V′) M—→N
    =  ⊥-elim (value-irreducible (ƛ _) M—→N)
sim (≤wrap i e V≤V′) M—→N
    =  ⊥-elim (value-irreducible (ƛ _) M—→N)
```

## Simulation extended to sequences

```
sim* : ∀ {Γ Γ′ A A′ M M′ N} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′}
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ p
  → M —↠ N
    -----------------------------------------
  → ∃[ N′ ]((M′ —↠ N′) × (Γ≤ ⊢ N ≤ᴹ N′ ⦂ p))
sim* M≤M′ (_ ∎)
    =  _ , (_ ∎) , M≤M′
sim* L≤L′ (L —→⟨ L—→M ⟩ M—↠N)
    with sim L≤L′ L—→M
... |  M′ , L′—↠M′ , M≤M′
    with sim* M≤M′ M—↠N
... |  N′ , M′—↠N′ , N≤N′
    =  N′ , (L′—↠M′ ++ M′—↠N′) , N≤N′
```

The gradual guarantee for reduction to a value.
```
gg : ∀ {Γ Γ′ A A′ M M′ V} {Γ≤ : Γ ≤ᴳ Γ′} {p : A ≤ A′}
  → Γ≤ ⊢ M ≤ᴹ M′ ⦂ p
  → M —↠ V
  → Value V
    ---------------------------------------------------
  → ∃[ V′ ](Value V′ × (M′ —↠ V′) × (Γ≤ ⊢ V ≤ᴹ V′ ⦂ p))
gg M≤M′ M—↠V v
    with sim* M≤M′ M—↠V
... |  N′ , M′—↠N′ , V≤N′
    with catchup v V≤N′
... |  V′ , v′ , N′—↠V′ , V≤V′
    =  V′ , v′ , (M′—↠N′ ++ N′—↠V′) , V≤V′
```


## Example

In our running example, we showed how to increment two and its
imprecise equivalent, and computed the reduction sequences for each,
and also showed that the two original terms are related.  Applying
the gradual guarantee to the more precise reduction sequence yields
the more precise reduction sequence.

Recall the example from the end of Core, where we define
the following:

  * `inc`, the increment function
  * `inc★`, the type erasure of the increment function
  * `inc★′`, the increment function upcast to type `★`
  * `inc2—↠3`, the reduction sequence `inc · 2 —↠ $ 3`
  * `inc★2★—↠3★`, the reduction sequence `inc★ ·★ ($★ 2) —↠ $★ 3`
  * `inc★′2★—↠3★`, the reduction sequence `inc★′ ·★ ($★ 2) —↠ $★ 3`

And at the example at the end of Prec we provide

  * `inc2≤inc★2★`, evidence that `∅ ⊢ inc2 ≤ᴹ inc★2★ ⦂ ℕ≤★`.
  * `inc2≤inc★′2★`, evidence that `∅ ⊢ inc2 ≤ᴹ inc★′2★ ⦂ ℕ≤★`.

Applying `gg` to `inc2≤inc★2★`, `inc2—↠3`, and evidence that `3`
yields the reduction sequence `inc★2★—↠3★`, and similarly for
`inc★′2★`.
```
_ : gg inc2≤inc★2★ inc2—↠3 ($ 3) ≡
      ($★ 3 , $ 3 ⇑ $ℕ , inc★2★—↠3★ , $≤$★ 3)
_ = refl

_ : gg inc2≤inc★′2★ inc2—↠3 ($ 3) ≡
      ($★ 3 , $ 3 ⇑ $ℕ , inc★′2★—↠3★ , $≤$★ 3)
_ = refl
```

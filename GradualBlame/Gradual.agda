-- Contents:
-- - Siek&Taha's gradual calculus
-- - A translation to the blame calculus (defined in Core)
module Gradual where

open import Data.Nat using (ℕ; zero; suc; _+_)
open import Data.Bool using (true; false) renaming (Bool to 𝔹)
open import Data.Unit using (⊤; tt)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂) renaming ([_,_] to case-⊎)
open import Relation.Binary.PropositionalEquality
     using (_≡_; _≢_; refl; trans; sym; cong; cong₂; cong-app; subst; inspect)
     renaming ([_] to [[_]])
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Decidable using (⌊_⌋; True; toWitness; fromWitness)

-- Reuse the syntax of types from the blame calculus.
open import Core using (Base; rep; Type; ★; $_; _⇒_; _≤_; Context; _▷_; _∋_)

-- Consistency relation.
-- It is equivalent to the composition of _≤_ and flip _≤_ (cf lemma ≈-≤≥)
data _≈_ : Type -> Type -> Set where
  ★≈_ : (A : Type) -> ★ ≈ A
  _=★ : (A : Type) -> A ≈ ★
  ⇒=⇒ : {A B A' B' : Type} -> A ≈ A' -> B ≈ B' -> (A ⇒ B) ≈ (A' ⇒ B')
  ≊-refl : {A : Type} -> A ≈ A

-- Try to compute the domain and codomain of a type.
-- Fail if the type is not consistent with a function type.
as-arr : Type -> Maybe (Type × Type)
as-arr ★ = just (★ , ★)
as-arr (A ⇒ B) = just (A , B)
as-arr ($ _) = nothing

infix  4 _⊢_
infix  6 _·_
infix  6 _⦅_⦆_
infix  8 `_

-- Siek&Taha's gradual calculus.
--
-- The application rule is where the action happens.
-- The other four rules are standard STLC (copied from Core).
data _⊢_ : Context → Type → Set where
  _·_ : ∀ {Γ} {A⇒B A' A B}
    → Γ ⊢ A⇒B
    → Γ ⊢ A
    → as-arr A⇒B ≡ just (A' , B)
    → A ≈ A'
      ---------
    → Γ ⊢ B

  `_ : ∀ {Γ} {A}
    → Γ ∋ A
      -----
    → Γ ⊢ A

  ƛ_ :  ∀ {Γ B A}
    → Γ ▷ A ⊢ B
      ---------
    → Γ ⊢ A ⇒ B

  $_ : ∀ {Γ ι}
    → rep ι
      -------
    → Γ ⊢ $ ι

  -- This should also be gradualized
  _⦅_⦆_ : ∀ {Γ ι ι′ ι″}
    → Γ ⊢ $ ι
    → (rep ι → rep ι′ → rep ι″)
    → Γ ⊢ $ ι′
      ----------------------
    → Γ ⊢ $ ι″

  -- Explicit cast
  -- NB: This character is not '@' "commercial at" (which is reserved in Agda).
  -- This is "Full width commercial at". Completely different thing.
  _＠_ : ∀ {Γ A B}
    → Γ ⊢ A
    → A ≈ B
      -----
    → Γ ⊢ B

-- TODO: translate extrinsinc to intrinsic

-- Threesome
data _≤≥_ (A B : Type) : Set where
  up-down : {C : Type} -> A ≤ C -> B ≤ C -> A ≤≥ B

≈-≤≥ : {A B : Type} -> A ≈ B -> A ≤≥ B
≈-≤≥ (★≈ _) = up-down Core.id Core.A≤★
≈-≤≥ (_ =★) = up-down Core.A≤★ Core.id
≈-≤≥ (⇒=⇒ H1 H2) with ≈-≤≥ H1 | ≈-≤≥ H2
... | up-down HA1 HB1 | up-down HA2 HB2 = up-down (HA1 ⇒ HA2) (HB1 ⇒ HB2)
≈-≤≥ ≊-refl = up-down Core.id Core.id

-- In Siek&Taha's calculus, casts relate any two consistent types.
-- We encode them to an upcast followed by a downcast.
consistent-cast : ∀ {Γ A A'} -> A ≈ A' -> Γ Core.⊢ A -> Γ Core.⊢ A'
consistent-cast E N with ≈-≤≥ E
... | up-down U D = N Core.⟨ Core.+ U ⟩ Core.⟨ Core.- D ⟩

-- Translate Siek&Taha's gradual calculus to the blame calculus
translate : ∀ {Γ A} -> Γ ⊢ A -> Γ Core.⊢ A
translate (` X) = Core.` X
translate (ƛ M) = Core.ƛ (translate M)
translate ($ V) = Core.$ V
translate (M ⦅ f ⦆ N) = translate M Core.⦅ f ⦆ translate N -- TODO
-- The interesting case, application: M · N
-- - M : A' ⇒ B, N : A
--     + A = A' -- just apply M to N, no cast
--     + A ≢ A' -- Cast N from A to A'
-- - M : ★      -- Cast M from ★ to (A -> ★)
translate (_·_ {_} {A' ⇒ B} {_} {A} M N refl A≈A') with A' Core.≡? A
... | yes refl = translate M Core.· translate N
... | no _ = translate M Core.· consistent-cast A≈A' (translate N)
translate (_·_ {_} {★} M N refl _)
  = (translate M Core.⟨ Core.- Core.A≤★ ⟩) Core.· translate N
translate (M ＠ A≈B) = consistent-cast A≈B (translate M)

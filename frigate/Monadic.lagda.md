```
module Monadic where

-- A universe problem:
-- 1. The "expand" operation relies on an instance of the continuation monad "Cont"
--    in a higher universe, because computations will take a handler, which is a
--    polymorphic function, as an argument.
-- 2. The "Freest" monad will be applied to that continuation monad, bumping
--    the universe of that argument in the definition of "Freest".
-- 3. In "reify'", implemented using "handle", we set the carrier "z" of
--    "handle" to "m a", so "z" must also be in a higher universe.

open import Agda.Primitive using (Level; lsuc; lzero; _⊔_)

++_ : Level -> Level
++ l = lsuc lzero ⊔ l

record Monad {l : Level} (m : Set -> Set l) : Set (++ l) where
  field
    return : {a : Set} -> a -> m a
    _>>=_ : {a b : Set} -> m a -> (a -> m b) -> m b
-- Technically those "monads" aren't even endofunctors.
-- Luckily that's not necessary for the (return;_>>=_) presentation.
-- But that means there is no join when l is not lzero.

-- This monad is so free it's not lawful (wrt. propositional equality).
data Freest {l : Level} (m : Set -> Set l) (a : Set) : Set (++ l) where
  Ret : a -> Freest m a
  Bind : {b : Set} -> Freest m b -> (b -> Freest m a) -> Freest m a
  Eff : m a -> Freest m a

cata : {l₁ l₂ : Level} {f : Set -> Set l₁} {m : Set -> Set l₂} -> {{Monad m}} ->
  (h : {a : Set} -> f a -> m a) ->
  {a : Set} -> Freest f a -> m a
cata {_} {_} {f} {m} {{mm}} h = go
  where
    open Monad mm
    go : {a : Set} -> Freest f a -> m a
    go (Ret x) = return x
    go (Bind u k) = go u >>= \x -> go (k x)
    go (Eff e) = h e

-- Monadic reflection à la Filinski: reflect and reify

reflect : {l : Level} {m : Set -> Set l} {a : Set} -> m a -> Freest m a
reflect = Eff

reify : {l : Level} {m : Set -> Set l} {a : Set} -> {{Monad m}} -> Freest m a -> m a
reify {l} {m} {{mm}} = go
  where
    open Monad mm
    go : {a : Set} -> Freest m a -> m a
    go (Ret x) = return x
    go (Bind u k) = go u >>= \y -> go (k y)
    go (Eff u) = u

-- Algebraic effects and handlers: perform and handle

perform : {l : Level} {m : Set -> Set l} {a : Set} -> m a -> Freest m a
perform = Eff

handle : {l₁ l₂ : Level} {f : Set -> Set l₁} {a : Set} {z : Set l₂}
  (r : a -> z)
  (h : {b : Set} -> f b -> (b -> z) -> z) ->
  Freest f a -> z
handle {_} {_} {f} {_} {z} r h u = go u r
  where
    go : {a : Set} -> Freest f a -> (a -> z) -> z
    go (Ret x) r = r x
    go (Bind u k) r = go u \y -> go (k y) r
    go (Eff e) r = h e r

-- reify using handle (we already have reflect = perform)

reify' : {l : Level} {m : Set -> Set l} {a : Set} -> {{Monad m}} -> Freest m a -> m a
reify' {{mm}} = handle return _>>=_
  where
    open Monad mm

-- handle using reify, via the continuation monad

record Cont {l : Level} (r : Set l) (a : Set) : Set l where
  constructor MkCont
  field
    runCont : (a -> r) -> r

open Cont

instance
  Monad-Cont : {l : Level} {r : Set l} -> Monad (Cont r)
  Monad-Cont = record
    { return = \x -> MkCont \k -> k x
    ; _>>=_ = \u h -> MkCont \k -> runCont u \x -> runCont (h x) k
    }

-- "Macro expansion" of "perform" in "f" to "reify" in the continuation monad.

expand : {l₁ l₂ : Level} {f : Set -> Set l₁} {a : Set} {z : Set l₂} ->
  (let h = {b : Set} -> f b -> (b -> z) -> z) ->
  Freest f a -> Freest (Cont (h -> z)) a
expand (Ret x) = Ret x
expand (Bind u c) = Bind (expand u) \o -> expand (c o)
expand (Eff e) = Eff (MkCont \k h -> h e \o -> k o h)

handle' : {l₁ l₂ : Level} {f : Set -> Set l₁} {a : Set} {z : Set l₂}
  (r : a -> z)
  (h : {b : Set} -> f b -> (b -> z) -> z) ->
  Freest f a -> z
handle' r h u = runCont (reify (expand u)) (\x _ -> r x) h
```

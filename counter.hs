{- counter.hs: count comments and lines of code in .lagda.md files.

Usage:

  cat src/*.lagda.md|runghc counter.hs

Counting method:
Triple backticks either start or end a code block.
Triple backtick lines are not counted.
Code blocks with an explicit language (next to the opening triple backtick, with no space)
are counted as comments.
Code lines starting with `--` are counted as comments
Ignore empty (spaces only) lines.
 -}

{-# LANGUAGE BangPatterns #-}

import Control.Exception (throw, try)
import Data.Char (isSpace, isAlpha)
import System.IO.Error (isEOFError)

main :: IO ()
main = do
  putStrLn "Ignoring spaces"
  (comments, code) <- counter
  putStrLn ("Comments: " ++ show comments)
  putStrLn ("Code: " ++ show code)

data Mode = Comments | Code | FakeCode

flipMode :: String -> Mode -> Mode
flipMode ('`' : '`' : '`' : c : _) Comments | isAlpha c = FakeCode
flipMode line@('`' : '`' : '`' : c : _) _ | isAlpha c = error ("Bad quote: " ++ show line)
flipMode _ Comments = Code
flipMode _ Code = Comments
flipMode _ FakeCode = Comments

counter :: IO (Int, Int)
counter = count Comments 0 0
  where
    count !mode !comments !code = do
      i <- try getLine
      case i of
        Left e | isEOFError e ->
          case mode of
            Comments -> pure (comments, code)
            Code -> putStrLn "WARNING: Malformed input, unterminated code block" >> pure (comments, code)
        Left e -> throw e
        Right line
          | all isSpace line      -> count mode comments code
          | isThreeBackticks line -> count (flipMode line mode) comments code
          | isComment line        -> count mode (comments + 1) code
          | Comments <- mode      -> count mode (comments + 1) code
          | FakeCode <- mode      -> count mode (comments + 1) code
          | otherwise             -> count mode comments (code + 1)

isComment :: String -> Bool
isComment (' ' : line) = isComment line
isComment ('-' : '-' : _) = True
isComment _ = False

isThreeBackticks :: String -> Bool
isThreeBackticks ('`' : '`' : '`' : _) = True
isThreeBackticks _ = False
